<?php

namespace Modules\Product\Controllers;

use Core\Route;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Core\Arr;
use Modules\Content\Models\Control;
use Modules\Product\Models\Product AS Model;

class Product extends Base {
    
    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->_limit = (int) Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        
        $alias = Route::param('alias');
        $product = Model::selectProduct($alias);
        if ($_POST) {
            $name = $_POST['name'];
            $parent_id = $product->id;
            $email = $_POST['email'];
            $text = $_POST['message'];
            $stars = $_POST['review'];
            Model::insertReview($name, $parent_id, $email, $text, $stars);
        }
        
        // Хлебные крошки
        $category = Model::selectCategory($product->parent_id);
        $categoryTop = Model::selectCategory(Model::selectCategory($product->parent_id)->parent_id);
        //Похожие товары
        $word = substr($product->name,0,strpos(trim($product->name),' '));
        $likeProducts = Model::selectLikeProduct($word);
        // Отзывы
        $reviews = Model::selectProductReviews($product->id);
        $reviewsCount = Model::selectProductReviewsCounts($product->id);
        
        $rating = 0;
        $summStars = null;
        foreach ($reviews as $review) {
            $summStars+=$review->stars;
        }
        if ($reviewsCount != 0) {
            $rating = round($summStars / $reviewsCount);
        }
        
        

        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        // Render template
        $this->_content = View::tpl(['pager' => $this->_pager->create(), 
                                     'title' => $title, 
                                     'likeProducts' => $likeProducts, 
                                     'product' => $product,
                                     'reviews' => $reviews,
                                     'reviewsCount' => $reviewsCount,
                                     'rating' => $rating,
                                     'category' => $category,
                                     'categoryTop' => $categoryTop], 'Product/Page');
    }

}
