<?php

namespace Modules\Product\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Product extends Common {

    public static function selectProduct($alias) {
        $id = DB::select()
                ->from('catalog')
                ->where('alias', '=', $alias)
                ->find();
        return $id;
    }

    public static function selectProductReviews($id) {
        $rev = DB::select()
                ->from('reviews')
                ->where('parent_id', '=', $id)
                ->find_all();
        return $rev;
    }

    public static function selectProductReviewsCounts($id) {
        $rev = DB::select()
                ->from('reviews')
                ->where('parent_id', '=', $id)
                ->find_all()
                ->count();
        return $rev;
    }

    public static function selectLikeProduct($name) {
        $id = DB::select()
                ->from('catalog')
                ->where('name', 'like', "%$name%")
                ->find_all();
        return $id;
    }
    
    public static function selectCategory($parent_id) {
        $cat = DB::select()
                ->from('catalog_tree')
                ->where('id', '=', $parent_id)
                ->find();
        return $cat;
    }

    public static function insertReview($name, $parent_id, $email, $text, $stars) {
        $rev = Common::factory('reviews')->insert([
            'name' => $name,
            'parent_id' => $parent_id,
            'email' => $email,
            'text' => $text,
            'stars' => $stars,
        ]);
        return $rev;
    }

}
