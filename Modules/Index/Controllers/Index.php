<?php

namespace Modules\Index\Controllers;

use Core\Route;
use Core\Config;
use Modules\Base;
use Modules\Content\Models\Control;
use Core\Common;

class Index extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        $name = $_POST['name'];
        $phone = $_POST['tel'];
        if ($_POST) {
            Common::factory('callback')
                    ->insert([
                        'name' => $name,
                        'phone' => $phone
            ]);
        }
        $this->_template = 'Main';
        // Check for existance
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $this->_content = $this->current->text;
    }

}
