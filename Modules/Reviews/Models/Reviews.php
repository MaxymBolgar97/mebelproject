<?php

namespace Modules\Reviews\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Reviews extends Common {

    public static function selectReviews() {
        $id = DB::select()
                ->from('reviewsMain')
                ->find_all();
        return $id;
    }

    public static function insertReview($name, $email, $message) {
        return Common::factory('reviewsMain')
        ->insert([
        'name' => $name,
        'email' => $email,
        'text' => $message
        ]);
    }

        public static function countsRows() {
            return DB::select([DB::expr('COUNT(' . 'reviewsMain' . '.id)'), 'count'])->from('reviewsMain');
        }
    }
    