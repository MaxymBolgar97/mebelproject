<?php

namespace Modules\Reviews\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\Reviews\Models\Reviews as Model;

class Reviews extends Base {

    public $current;
    public $page;
    public $limit;
    public $offset;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->limit = Config::get('basic.limit_backend');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        $reviews = Model::selectReviews();
        // Seo
        if ($_POST) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];

            if ($name AND $email AND $message) {
                Model::insertReview($name, $email, $message);
            }

            HTTP::redirect('/reviews');
        }
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $page = (int) Route::param('page') ? (int) Route::param('page') : 1;
        $count = Model::countsRows();
        $pager = Pager::factory($page, $count, $this->limit)->create();
        //canonicals settings
        $this->_use_canonical = 1;
        $this->_canonical = 'news';
        // Render template
        $this->_content = View::tpl(['result' => $result, 'pager' => $pager, 'reviews' => $reviews], 'Reviews/Page');
    }

}
