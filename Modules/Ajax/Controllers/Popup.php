<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Message;
use Core\HTML;
use Core\View;
use Core\Common;
use Core\Config;
use Modules\Ajax;
use Modules\Content\Models\Control;
use Modules\Catalog\Models\Items;
use Modules\Catalog\Models\Modifications;
use Modules\Catalog\Models\Features;
use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Additionals;

class Popup extends Ajax
{

	public function authAction() {
		
		die (View::tpl([], 'Popup/Auth'));
		
	}
	
	public function callbackAction() {
		
		die (View::tpl([], 'Popup/Callback'));
		
	}
	
	public function callTechnologistAction() {
		
		die (View::tpl([], 'Popup/CallTechnologist'));
		
	}
	
	public function advantagesAction() {
		
		$id = Arr::get($this->post, 'id');
		$advantage = Common::factory('advantages')->getRow($id);
		die (View::tpl(['text' => $advantage->text], 'Popup/Advantages'));
		
	}
	
	public function buyOneClickAction() {
		
		$id = Arr::get($this->post, 'id');
		die (View::tpl(['id' => $id], 'Popup/Item/BuyOneClick'));
		
	}
	
	public function buyCreditAction() {
		
		$id = Arr::get($this->post, 'id');
		die (View::tpl(['text' => Config::get('static.text_credit')], 'Popup/Item/BuyCredit'));
		
	}
	
	public function deliveryAction() {
		
		$page = Control::getRow('delivery-and-payment', 'alias');
        $other = json_decode($page->other, 1);
		$delivery = Arr::get($other, 'delivery');
		$install = Arr::get($other, 'install');
		$time = Arr::get($other, 'times');
		die (View::tpl(['delivery' => $delivery, 'deliveryImage' => $page->image2, 'install' => $install, 'installImage' => $page->image, 'time' => $time], 'Popup/Delivery'));
		
	}
	
	public function paymentAction() {
		$page = Control::getRow('delivery-and-payment', 'alias');
        $other = json_decode($page->other, 1);
		$payment = Arr::get($other, 'payment');
		die (View::tpl(['payment' => $payment, 'image' => $page->image3], 'Popup/Payment'));
		
	}
	
	public function warrantyAction() {
		
		$page = Control::getRow('warranty', 'alias');
        $other = json_decode($page->other, 1);
		die (View::tpl(['other' => $other, 'page' => $page], 'Popup/Warranty'));
		
	}
	
	public function cartAction() {
		
		die (View::tpl([], 'Cart/Popup'));
		
	}

}