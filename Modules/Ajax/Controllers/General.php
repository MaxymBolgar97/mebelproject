<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Common;
use Core\Message;
use Modules\Cart\Models\Cart;
use Core\HTML;
use Modules\Ajax;
use Core\User;
use Core\Cookie;
use Modules\User\Models\Favorites;
use Modules\Catalog\Models\Items;

class General extends Ajax
{

    public function removeConnectionAction()
    {
        $id = Arr::get($_POST, 'id');
        $row = Common::factory('users_networks')->getRow($id);
        if ($row) {
            Common::factory('users_networks')->delete($id);
        }
        Message::GetMessage(1, 'Вы успешно удалили связь Вашего аккаунта и соц. сети!', 5000);
        $this->success();
    }
	
	public function getCartAction() {
		
		$cart = Cart::factory()->get_list_for_basket('update');
		die(json_encode($cart));
		
	}
    
     public function addToCartAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
		$params = json_decode(str_replace("&quot;", '"', Arr::get($this->post,'params', '{}')),1);
		$params = $params['options'];
        // Add one item to cart
        Cart::factory()->add($catalog_id, $params);
        $result = Cart::factory()->get_list_for_basket('add');
        
       die(json_encode($result));
    }


    // Edit count items in the cart
    public function editCartCountItemsAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
      	$params = json_decode(str_replace("&quot;", '"', Arr::get($this->post,'params', '{}')),1);
        $count = Arr::get($params, 'quantity.selected', 0);
        if (!$count) {
            $this->error('Can\'t change to zero!');
        }
        // Edit count items in cirrent position
        Cart::factory()->edit($catalog_id, $count);
		$result = Cart::factory()->get_list_for_basket('change');
        
		 die(json_encode($result));
    }


    // Delete item from the cart
    public function deleteItemFromCartAction()
    {
        // Get and check incoming data
        $catalog_id = Arr::get($this->post, 'id', 0);
        if (!$catalog_id) {
            $this->error('No such item!');
        }
        // Add one item to cart
        Cart::factory()->delete($catalog_id);
		$result = Cart::factory()->get_list_for_basket('change');
		 die(json_encode($result));
    }
    
    public function addFavoriteAction() {
		
		$id = Arr::get($_POST, 'id');
		if (!$id) {
			die(json_encode(array(
				'error' => 1,
				'count' => 1,
				'message' => 'Не указан товар!'
			)));
		}
		$user_id = User::info()->id;
		if (!$user_id) {
			die(json_encode(array(
				'error' => 1,
				'count' => 1,
				'message' => 'Зарегистрируйтесь для того, чтоб добавлять товары в избранное!'
			)));
		}
		$favorite = Favorites::findFavorite($user_id, $id);
		if ($favorite) {
			Favorites::delete($favorite->id);
			$operation = 'remove';
		} else {
			Favorites::insert(['user_id' => $user_id, 'catalog_id' => $id]);
			$operation = 'add';
		}
		$count = Favorites::getCountByUser($user_id);
		die(json_encode(array(
			'success' => 1,
			'count' => $count,
			'operation' => $operation,
			'message' => ''
		)));
	}
    
    public function compareAction() {
		
		$id = Arr::get($_POST, id);
		$compareData = Cookie::get('compare');
		$compareArr = [];
		if ($compareData) {
			$compareArr = json_decode($compareData,1);
		}
		if (in_array($id, $compareArr)) {
			unset($compareArr[$id]);
			$operation = 'remove';
		} else {
			$compareArr[$id] = $id;
			$operation = 'add';
		}
		Cookie::set('compare', json_encode($compareArr), 60 * 60 * 24 * 365);
		
		die(json_encode([
			'success' => 1,
			'count' => sizeof($compareArr),
			'message' > '',
			'operation' => $operation
		]));
		
	}
	
	public function clearCompareAction() {
		
		$categoryID = Arr::get($_POST, 'catalog_tree_id');
		$compareData = Cookie::get('compare');
		$compareArr = [];
		if ($compareData) {
			$compareArr = json_decode($compareData,1);
		}
	
		$items = Items::getItemsByIds($compareArr);
		foreach ($items as $item) {
			if ($item->parent_id == $categoryID) {
				unset($compareArr[$item->id]);
			}
		}
		Cookie::set('compare', json_encode($compareArr), 60 * 60 * 24 * 365);
		
		die(json_encode([
			'success' => true
		]));
		
	}
	
	public function review_markAction() {
		
		$id = Arr::get($this->post, 'id');
		$type = Arr::get($this->post, 'type');
		
		$review = Common::factory('catalog_comments')->getRow($id);
		if (!$review) {
			die(json_encode(array(
				'type' => 'error',
				'message' => 'Такого отзыва не существует',
				'count' => 0
			)));
		} 
		
		$ids = Cookie::get('catalog_comments');
		if ($ids) {
			$ids = json_decode($ids);
			if (in_array($id, $ids)) {
					die(json_encode(array(
					'type' => 'error',
					'message' => 'Оценка уже поставлена',
					'count' => ($type == 'no') ? (int) $review->dislikes : (int) $review->likes
				)));
			}
		}
			
		$ids[] = $id;
		Cookie::set('catalog_comments', json_encode($ids), 60 * 60 * 24 * 365);
		if ($type == 'no') {
			Common::factory('catalog_comments')->update(['dislikes' => $review->dislikes+1],$id);
			die(json_encode(array(
				'type' => 'success',
				'message' => '',
				'count' => $review->dislikes+1
			)));
		} else {
			Common::factory('catalog_comments')->update(['likes' => $review->likes+1],$id);
			die(json_encode(array(
				'type' => 'success',
				'message' => '',
				'count' => $review->likes+1
			)));
		}	
	}

}