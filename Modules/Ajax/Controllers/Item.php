<?php
namespace Modules\Ajax\Controllers;

use Core\Arr;
use Core\Message;
use Core\HTML;
use Core\View;
use Core\Common;
use Core\Config;
use Modules\Ajax;
use Modules\Content\Models\Control;
use Modules\Catalog\Models\Items;
use Modules\Catalog\Models\Modifications;
use Modules\Catalog\Models\Features;
use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Additionals;

class Item extends Ajax
{
	
	public function productImageAction() {
		
		$id = Arr::get($_POST, 'id');
		$current = Arr::get($_POST, 'current');
		
		if (!$id) die();
		
		$item = Items::getRow($id);
		$images = Items::getItemImages($id);
		
		die (View::tpl(['item' => $item, 'images' => $images, 'current' => $current], 'Catalog/Item/Image'));
		
	}
	
	public function itemModificationsAction() {
		
		$productID = Arr::get($_POST, 'productId');
		$modificationsParams = Arr::get($_POST, 'list.paramId3');
		$modificationID = Arr::get($modificationsParams, 'list.0.valueId');
		
		$item = Items::getRow($productID);
		$cost = Arr::get($_POST, 'price');
		$current =  Modifications::getRow($modificationID);	
		$modifications = Modifications::getModifications($productID, 'sort', 'ASC');
		
		$prevLink = '';
		$nextLink = '';
		$list = Arr::get($_POST, 'list', []);
		if (Arr::get($list, 'paramId4', null)) {
			$nextLink = HTML::link('ajax/item/itemFeatures');
		}
		if (Arr::get($list, 'paramId2', null)) {
			$prevLink = HTML::link('ajax/item/itemAdditionals');
		} elseif (Arr::get($list, 'paramId1', null)) {
			$prevLink = HTML::link('ajax/item/itemMaterials');
		}


		die (View::tpl([
						'modifications' => $modifications,	
						'current' => $current, 
						'currentCost' => $cost, 
						'prev' => $prevLink, 
						'next' => $nextLink
					], 'Catalog/Item/Modifications'));
		
	}
	
	public function itemMaterialsAction() {
		
		$productID = Arr::get($_POST, 'productId');
		$modificationsParams = Arr::get($_POST, 'list.paramId3');
		$modificationID = Arr::get($modificationsParams, 'list.0.valueId');
		$materialParams = Arr::get($_POST, 'list.paramId1');
		$materialID = Arr::get($materialParams, 'list.0.itemId');
		$colorID = Arr::get($materialParams, 'list.0.valueId');
		
		$item = Items::getRow($productID);
		$group = Groups::getRow($item->parent_id);
		$cost = Arr::get($_POST, 'price');
		$materials = Modifications::getMaterials($modificationID, 'materials.sort', 'ASC');
		$colors = [];
		$newMaterials = [];
		foreach ($materials as $material) {
			if ($item->cost_type == 3) {
				$colors[$material->material_id] = Modifications::getColorsWithCost($material->material_id, $modificationID);
			} else {
				$colors[$material->material_id] = Modifications::getColors($material->material_id);
			}
			$material->material_cost = round($material->cost + $material->cost*$group->markup/100);
			if ($materialID == $material->material_id) {
				$currentMaterial = $material;
			}
			$newMaterials[] = $material;
		}
		
		$prevLink = '';
		$nextLink = '';
		$list = Arr::get($_POST, 'list', []);
		if (Arr::get($list, 'paramId2', null)) {
			$nextLink = HTML::link('ajax/item/itemAdditionals');
		} elseif (Arr::get($list, 'paramId3', null) and Modifications::countModifications($productID)>1) {
			$nextLink = HTML::link('ajax/item/itemModifications');
		} elseif (Arr::get($list, 'paramId4', null)) {
			$nextLink = HTML::link('ajax/item/itemFeatures');
		}

		die (View::tpl([
						'materials' => $newMaterials,
						'currentCost' => $cost, 
						'colors' => $colors,
						'currentColor' => $colorID,
						'currentMaterial' => $currentMaterial,
						'costType' => $item->cost_type,
						'prev' => $prevLink, 
						'next' => $nextLink
					], 'Catalog/Item/Materials'));
		
	}
	
	public function itemAdditionalsAction() {
		$productID = Arr::get($_POST, 'productId');
		$cost = Arr::get($_POST, 'price');
		$additionalsParams = Arr::get($_POST, 'list.paramId2');
		$additionalID = Arr::get($additionalsParams, 'list.0.itemId');
		$valueID = Arr::get($additionalsParams, 'list.0.valueId');
		$additionals = Additionals::getAdditionals($productID);
		
		$values = [];
		$current = null;
		foreach ($additionals as $additional)  {
			$values[$additional->id] = Additionals::getAdditionalValues($additional->id);
			if ($additional->id == $additionalID) {
				$current = $additional;
			}
		}
		
		$list = Arr::get($_POST, 'list', []);
		$prevLink = '';
		$nextLink = '';
		if (Arr::get($list, 'paramId1', null)) {
			$prevLink = HTML::link('ajax/item/itemMaterials');
		}
		if (Arr::get($list, 'paramId3', null) and Modifications::countModifications($productID)>1) {
			$nextLink = HTML::link('ajax/item/itemModifications');
		} elseif (Arr::get($list, 'paramId4', null)) {
			$nextLink = HTML::link('ajax/item/itemFeatures');
		}

		die (View::tpl([
						'additionals' => $additionals, 
						'cost' => $cost, 
						'current' => $current, 
						'currentValue' => $valueID, 
						'values' => $values, 
						'prev' => $prevLink, 
						'next' => $nextLink
					], 'Catalog/Item/Additionals'));
		
	}
	
	public function itemFeaturesAction() {
		
		$productID = Arr::get($_POST, 'productId');
		$featuresParams = Arr::get($_POST, 'list.paramId4', []);
		$valuesIDS = [];
		foreach (Arr::get($featuresParams, 'list') as $item) {
			$valuesIDS[] = $item['valueId'];
		}
		$featuresCost = 0;
		$current = null;
		$cost = Arr::get($_POST, 'price');
		
		$features = Features::getFeatures($productID);
		$values = [];
		foreach ($features as $key => $feature)  {
			$values[$feature->id] = Features::getFeaturesValues($feature->id);
			$minCosts[$feature->id] = Features::getFeaturesMinCost($feature->id);
			foreach ($values[$feature->id] as $value) {
				if (in_array($value->id, $valuesIDS)) {
					$featuresCost = $featuresCost + $value->cost;
				}
			}
		}
		
		$prevLink = '';
		$nextLink = '';
		$list = Arr::get($_POST, 'list', []);
		if (Arr::get($list, 'paramId3', null) and Modifications::countModifications($productID)>1) {
			$prevLink = HTML::link('ajax/item/itemModifications');
		} elseif(Arr::get($list, 'paramId2', null)) {
			$prevLink = HTML::link('ajax/item/itemAdditionals');
		} elseif (Arr::get($list, 'paramId1', null)) {
			$prevLink = HTML::link('ajax/item/itemMtaerials');
		}
		
		$item = Items::getRow($productID);
		
		die (View::tpl([
						'features' => $features,
						'cost' => $cost, 
						'featuresCost' => $featuresCost,
						'currentValues' => $valuesIDS, 
						'values' => $values, 
						'minCosts' => $minCosts, 
						'prev' => $prevLink, 
						'next' => $nextLink
					], 'Catalog/Item/Features'));
		
	}
	
	public static function changeModificationAction() {
		
		$data = $_POST;
		
		$productID = Arr::get($_POST, 'productId');
		$modificationsParams = Arr::get($_POST, 'list.paramId3');
		$modificationID = Arr::get($modificationsParams, 'list.0.valueId');
		
		$data['list']['paramId1'] = [];
		
		$material = Modifications::getMinMaterial($modificationID);
		if ($material) {
			$colors = Modifications::getColors($material->material_id);
			if (sizeof($colors)) {
				$color = $colors[0];
			}
			$data['list']['paramId1'] = [
				'itemName' => $material->material_name,
				'valueName' => ($color) ? $color->name : '',
				'valueImage' => ($color and HTML::is_file('images/colors/'.$color->image)) ? HTML::media('images/colors/'.$color->image) : HTML::media('images/no-image.png'),
				'list' =>  [
					['itemId' => $material->material_id, 'valueId' => ($color) ? $color->id : 0]
				]
			];	
		}
		die( json_encode([
			'success' => true,
			'params' => $data
		]));
		
	}

}