<?php

return [
    'ajax/<action>' => 'ajax/general/<action>',
    'form/<action>' => 'ajax/form/<action>',
    'popup/<action>' => 'ajax/popup/<action>',
    'ajax/item/<action>' => 'ajax/item/<action>',
];
