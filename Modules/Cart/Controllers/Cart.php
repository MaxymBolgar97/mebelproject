<?php

// Тут костыли и магия

namespace Modules\Cart\Controllers;

use Core\Route;
use Modules\Content\Models\Control;
use Modules\Cart\Models\Cart AS C;
use Core\View;
use Core\Config;
use Modules\Base;
use Core\User;
use Core\HTTP;

class Cart extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    // Cart page with order form
    public function indexAction() {

        if (Config::get('error')) {
            return false;
        }

        if (!empty($_GET['id'])) {
            $_SESSION['prod'][] = $_GET['id'];
            HTTP::redirect($_SERVER['HTTP_REFERER']);
        }
        
        if (isset($_SESSION['prod'])) {
            $_SESSION['prod'] = array_unique($_SESSION['prod']);
        }
        
        $cart = [];
        $products = [];
        
        if (is_array($_SESSION['prod'])) {
            foreach ($_SESSION['prod'] as $item) {
                $cart[] = $item;
                $products[] = C::selectProd($item);
            }
        }



        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        $this->_content = View::tpl([
                    'products' => $products,
                    'user' => $user,
                        ], 'Cart/Index');
    }
    
    public function clearAction() {
        
        if (Config::get('error')) {
            return false;
        }
        unset($_SESSION['prod']);
        HTTP::redirect('/');
    }

}
