<?php

namespace Modules\Cart\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Cart extends Common {
    
    public static function selectProd($id){
        return DB::select()
                ->from('catalog')
                ->where('id', '=', $id)
                ->find();
    }
    
    public static function insertOrder($phone, $catalog_id, $counts, $user_id, $name, $adress, $text, $delivery, $email, $floor){
        return Common::factory('orders_simple')
                ->insert([
                    'phone' => $phone,
                    'catalog_id' => $catalog_id,
                    'counts' => $counts,
                    'user_id' => $user_id,
                    'name' => $name,
                    'adress' => $adress,
                    'text' => $text,
                    'delivery' => $delivery,
                    'email' => $email,
                    'floor' => $floor,
                ]);
    }
}