<?php

// Тут костыли и магия

namespace Modules\Order\Controllers;

use Core\Route;
use Modules\Content\Models\Control;
use Modules\Order\Models\Order AS Model;
use Core\View;
use Core\Config;
use Modules\Base;
use Core\User;
use Core\HTTP;
use Core\System;

class Order extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    // Cart page with order form
    public function indexAction() {

        if (Config::get('error')) {
            return false;
        }

        $products = [];
        $totalSum = 0;
        $totalCount = 0;

        if (isset($_POST['order'])) {
            $hash = $_POST['hash'];
            $user_ip = System::getRealIP();
            for ($i = 0; $i < count($_POST['productID']); $i++) {
                $products[$_POST['productID'][$i]]['cost'] = $_POST['productCOST'][$_POST['productID'][$i]];
                $products[$_POST['productID'][$i]]['count'] = $_POST['counts'][$_POST['productID'][$i]];
                $products[$_POST['productID'][$i]]['image'] = $_POST['productIMAGE'][$_POST['productID'][$i]];
                $products[$_POST['productID'][$i]]['name'] = $_POST['productNAME'][$_POST['productID'][$i]];
                $products[$_POST['productID'][$i]]['summ'] = $products[$_POST['productID'][$i]]['cost'] * $products[$_POST['productID'][$i]]['count'];
                $totalSum += $products[$_POST['productID'][$i]]['summ'];
                $totalCount += $products[$_POST['productID'][$i]]['count'];

                Model::insertCart($user_ip, $_POST['productID'][$i], $_POST['counts'][$_POST['productID'][$i]], $products[$_POST['productID'][$i]]['cost'] * $products[$_POST['productID'][$i]]['count'], $hash);
            }
            
            $userID = 0;
            $user = User::info();
            if ($user) {
                $userID = $user->id;
            }
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        $this->_content = View::tpl([
                    'products' => $products,
                    'totalSum' => $totalSum,
                    'totalCount' => $totalCount,
                    'userID' => $userID,
                    'hash' => $hash,
                    'user' => $user,
                        ], 'Order/Index');
    }
    
    public function confirmAction() {
        
        $phone = $_POST['tel'];
        $name = $_POST['name'];
        $adress = $_POST['adress'];
        $text = $_POST['message'];
        $delivery = $_POST['shipping'];
        $email = $_POST['email'];
        $user_id = $_POST['userID'];
        $floor = $_POST['floor'];
        $hash = $_POST['hash'];
        $totalSum = $_POST['totalSum'];
        
        $res = Model::insertOrder($phone, $name, $adress, $text, $delivery, $email, $user_id, $floor, $hash, $totalSum);
        
        if ($res) {
            unset($_SESSION['prod']);
            HTTP::redirect('/');
        }
        
        
    }

}
