<?php

namespace Modules\Order\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Order extends Common {

    public static function insertCart($user_ip, $catalog_id, $count, $summItems, $hash) {
        return Common::factory('carts_items')
                        ->insert([
                            'user_ip' => $user_ip,
                            'catalog_id' => $catalog_id,
                            'count' => $count,
                            'summItems' => $summItems,
                            'hash' => $hash,
        ]);
    }

    public static function insertOrder($phone, $name, $adress, $text, $delivery, $email, $user_id, $floor, $hash, $totalSum) {
        return Common::factory('orders_simple')
                        ->insert([
                            'phone' => $phone,
                            'name' => $name,
                            'adress' => $adress,
                            'text' => $text,
                            'delivery' => $delivery,
                            'email' => $email,
                            'user_id' => $user_id,
                            'floor' => $floor,
                            'hash' => $hash,
                            'totalSum' => $totalSum,
        ]);
    }
    
    public static function deleteCart($hash){
        return DB::delete('carts_items')
                ->where('hash', '=', $hash)
                ->find_all();

    }

}
