<?php

namespace Modules\News\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class News extends Common {

    public static function selectNews() {
        $id = DB::select()
                ->from('news')
                ->find_all();
        return $id;
    }

    public static function selectArticle($id) {
        $id = DB::select()
                ->from('news')
                ->where('id', '=', $id)
                ->find();
        return $id;
    }

}
