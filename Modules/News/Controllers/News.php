<?php

namespace Modules\News\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\News\Models\News AS Model;

class News extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $result = Model::selectNews();
        $this->_content = View::tpl(['news' => $result], 'News/Page');
    }

    public function articleAction() {
        if (Config::get('error')) {
            return false;
        }
        $id = Route::param('id');
        $article = Model::selectArticle($id);
        $result = Model::selectNews();
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Render template
        $result = Model::selectNews();
        $this->_content = View::tpl(['news' => $result, 'article' => $article], 'News/Article');
    }

}
