<?php

namespace Modules\Tops\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\Tops\Models\Tops as Model;

class Tops extends Base {

    public $current;
    public $page;
    public $limit;
    public $offset;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        
        $result = Model::selectTops();
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;

        $this->_content = View::tpl(['result' => $result], 'Tops/Page');
    }

}
