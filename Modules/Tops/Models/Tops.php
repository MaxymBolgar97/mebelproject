<?php

namespace Modules\Tops\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Tops extends Common {

    public static function selectTops() {
        $id = DB::select()
                ->from('catalog')
                ->where('top', '=', 1)
                ->find_all();
        return $id;
    }
    }
    