<?php

namespace Modules\Auth\Controllers;

use Core\Route;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Core\Config AS conf;
use Core\Arr;
use Core\System;
use Core\User;
use Modules\Content\Models\Control;
use Modules\Auth\Models\Auth AS Model;

class Auth extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->_limit = (int) Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function loginAction() {
       $email = $_POST['email']; 
       $password = trim($_POST['password']);
       $remember = $_POST['remember'];
       
        $user = User::factory()->get_user_by_email($email, $password);
        if ($user) {
            User::factory()->auth($user, $remember);
            HTTP::redirect('/user');
        }
        HTTP::redirect('/');
    }

    public function registrationAction() {
        $email = $_POST['email'];
        $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();

        if ($user) {
            echo '<script>alert("Пользователь с указанным E-Mail адресом уже зарегистрирован!");</script>';
        }

        $password = trim($_POST['password']);

        if (mb_strlen($password, 'UTF-8') < conf::get('main.password_min_length')) {
            $msg = 'Пароль не может содержать меньше ' . conf::get('main.password_min_length') . ' символов!';
            echo '<script>alert("' . $msg . '");</script>';
        }
        

        $data = [
            'email' => $email,
            'password' => $password,
            'ip' => System::getRealIP(),
            'status' => 1,
        ];

        User::factory()->registration($data);
        $user = DB::select()->from('users')->where('email', '=', $email)->as_object()->execute()->current();
        User::factory()->auth($user, 0);
        
        HTTP::redirect('/');
    }

}
