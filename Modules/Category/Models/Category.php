<?php

namespace Modules\Category\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Category extends Common {

    public static function selectCategories($alias) {
        $id = DB::select()
                ->from('catalog_tree')
                ->where('alias', '=', $alias)
                ->find();
        return $id;
    }
    
    public static function selectTopCategory($alias) {
        $id = DB::select()
                ->from('catalog_tree')
                ->where('id', '=', $alias)
                ->find();
        return $id;
    }
    
        public static function selectProducts($parent_id) {
        $cat = DB::select()
                ->from('catalog')
                ->where('parent_id', '=', $parent_id)
                ->find_all();
        return $cat;
    }
    
    public static function selectProductsCount($parent_id) {
        $cat = DB::select()
                ->from('catalog')
                ->where('parent_id', '=', $parent_id)
                ->find_all()
                ->count();
        return $cat;
    }
    
    
    public static function selectBetweenProducts($parent_id, $from, $to) {
        $cat = DB::select()
                ->from('catalog')
                ->where('parent_id', '=', $parent_id)
                ->where('cost', '>=', $from)
                ->where('cost', '<=', $to)
                ->find_all();
        return $cat;
    }

}