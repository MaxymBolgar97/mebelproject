<?php

namespace Modules\Category\Controllers;

use Core\Route;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Core\Arr;
use Modules\Content\Models\Control;
use Modules\Category\Models\Category AS Model;

class Category extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->_limit = (int) Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        //Сбрасываю фильтр
        if (Route::param('alias') == 'clear') {
            Arr::clearArray($_POST);
            Arr::clearArray($_GET);
            $back = $_SERVER['HTTP_REFERER'];
            header("Location:$back");
        }


        $alias = Route::param('alias');
        $id = Model::selectCategories($alias);
        $category = Model::selectTopCategory($id->parent_id);
        $parent_id = $id->id;
        $title = $id->name;
        $products = Model::selectProducts($parent_id);
        $productsCount = Model::selectProductsCount($parent_id);
        if ($_POST) {
            $from = $_POST['ot'];
            $to = $_POST['do'];
            $products = Model::selectBetweenProducts($parent_id, $from, $to);
        }

        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        // Render template
        $this->_content = View::tpl(['products' => $products, 'pager' => $this->_pager->create(), 'title' => $title, 'category' => $category, 'productsCount' => $productsCount], 'Category/Page');
    }

}
