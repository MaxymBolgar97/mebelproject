<?php

return [
    'user' => 'user/user/index',
    'logout' => 'user/user/logout',
    'user/editProfile' => 'user/user/edit',
    'user/editPassword' => 'user/user/editPassword',
];