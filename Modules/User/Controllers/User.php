<?php

namespace Modules\User\Controllers;

use Core\Route;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Core\Arr;
use Core\Message;
use Core\Common;
use Core\Config as conf;
use Core\User as UserInfo;
use Modules\Content\Models\Control;
use Modules\User\Models\User AS Model;

class User extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->_limit = (int) Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }
        $user = UserInfo::info();
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        // Render template
        $this->_content = View::tpl(['pager' => $this->_pager->create(),
                    'user' => $user], 'User/Page');
    }

    public function logoutAction() {
        UserInfo::factory()->logout();
        HTTP::redirect('/');
    }

    public function editAction() {
        if (Config::get('error')) {
            return false;
        }
        $user = UserInfo::info();

        if ($_POST) {
            $FIO = explode(' ', trim($_POST['name']));

            $last_name = $FIO[0];
            $name = $FIO[1];
            $middle_name = $FIO[2];
            $address = trim($_POST['address']);
            $floor = trim($_POST['number']);
            $email = trim($_POST['email']);
            $phone = trim($_POST['tel']);

            $check = DB::select([DB::expr('COUNT(users.id)'), 'count'])
                            ->from('users')
                            ->where('email', '=', $email)
                            ->where('id', '!=', UserInfo::info()->id)
                            ->as_object()->execute()->current();

            if (is_object($check) and $check->count) {
                Message::GetMessage(2, 'Пользователь с указанным E-Mail адресом уже зарегистрирован!');
                HTTP::redirect('/user');
            }
            if (!$phone/* or !preg_match('/^\+38 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/', $phone, $matches) */) {
                Message::GetMessage(2, 'Номер телефона введен неверно!');
                HTTP::redirect('/user');
            }
            // Save new users data
            Common::factory('users')->update(['name' => $name,
                'last_name' => $last_name,
                'middle_name' => $middle_name,
                'email' => $email,
                'phone' => $phone,
                'adress' => $address,
                'floor' => $floor
                    ], UserInfo::info()->id);
            HTTP::redirect('/user/editProfile');
        }
        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        // Render template
        $this->_content = View::tpl(['pager' => $this->_pager->create(),
                    'user' => $user], 'User/Edit');
    }

    public function editPasswordAction() {
        if (Config::get('error')) {
            return false;
        }
        $user = UserInfo::info();

        if ($_POST) {

            $old_pass = trim($_POST['old_pass']);
            $new_pass = trim($_POST['new_pass']);
            $new_pass2 = trim($_POST['new_pass2']);

            // Check incoming data
            if (!UserInfo::factory()->check_password($old_pass, UserInfo::info()->password)) {
                Message::GetMessage(2, 'Старый пароль введен неверно!');
            }
            if (mb_strlen($new_pass, 'UTF-8') < conf::get('main.password_min_length')) {
                $msg = 'Пароль не может быть короче ' . conf::get('main.password_min_length') . ' символов!';
                Message::GetMessage(2, "$msg");
            }
            if (UserInfo::factory()->check_password($new_pass, UserInfo::info()->password)) {
                Message::GetMessage(2, "Нельзя поменять пароль на точно такой же!");
            }
            if ($new_pass != $new_pass2) {
                Message::GetMessage(2, 'Поля "Новый пароль" и "Подтвердите новый пароль" должны совпадать!');
        }

        // Change password for new
        UserInfo::factory()->update_password(UserInfo::info()->id, $new_pass);
        HTTP::redirect('/user/editProfile');
    }
    // Seo
    $this->_seo['h1'] = $this->current->h1;
    $this->_seo['title'] = $this->current->title;
    $this->_seo['keywords'] = $this->current->keywords;
    $this->_seo['description'] = $this->current->description;
    // Generate pagination
    $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
    //canonicals settings
    $this->_use_canonical = 1;
    // Render template
    $this->_content = View::tpl(['pager' => $this->_pager->create(),
                'user' => $user], 'User/EditPassword');
}

}
