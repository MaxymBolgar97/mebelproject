<?php

namespace Modules\Sale\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Sale extends Common {

    public static function selectSales() {
        $sale = DB::select()
                ->from('slider')
                ->where('status', '=', 1)
                ->order_by('sort')
                ->find_all();
        return $sale;
    }

}
