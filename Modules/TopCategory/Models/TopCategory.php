<?php

namespace Modules\TopCategory\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class TopCategory extends Common {

    public static function selectPodCategories($alias) {
        $id = DB::select()
                ->from('catalog_tree')
                ->where('alias', '=', $alias)
                ->find();
        return $id;
    }
    
        public static function selectCategories($parent_id) {
        $cat = DB::select()
                ->from('catalog_tree')
                ->where('parent_id', '=', $parent_id)
                ->find_all();
        return $cat;
    }
    
        public static function selectCategoriesCount($parent_id) {
        $cat = DB::select()
                ->from('catalog_tree')
                ->where('parent_id', '=', $parent_id)
                ->find_all()
                ->count();
        return $cat;
    }

}