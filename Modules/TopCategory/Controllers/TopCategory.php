<?php

namespace Modules\TopCategory\Controllers;

use Core\Route;
use Core\View;
use Core\QB\DB;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\Content\Models\Control;
use Modules\TopCategory\Models\TopCategory AS Model;

class TopCategory extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';

        $this->_page = !(int) Route::param('page') ? 1 : (int) Route::param('page');
        $this->_limit = (int) Config::get('basic.limit_articles');
        $this->_offset = ($this->_page - 1) * $this->_limit;
    }

    public function indexAction() {
        if (Config::get('error')) {
            return false;
        }

        $alias = Route::param('alias');
        $id = Model::selectPodCategories($alias);
        $parent_id = $id->id;
        $title = $id->name;
        $categories = Model::selectCategories($parent_id);
        $categoriesCount = Model::selectCategoriesCount($parent_id);


        // Seo
        $this->_seo['h1'] = $this->current->h1;
        $this->_seo['title'] = $this->current->title;
        $this->_seo['keywords'] = $this->current->keywords;
        $this->_seo['description'] = $this->current->description;
        // Generate pagination
        $this->_pager = Pager::factory($this->_page, $count, $this->_limit);
        //canonicals settings
        $this->_use_canonical = 1;
        // Render template
        $this->_content = View::tpl(['categories' => $categories, 'pager' => $this->_pager->create(), 'title' => $title, 'categoriesCount' => $categoriesCount], 'TopCategory/Page');
    }

}
