<?php

namespace Core;

use Modules\Catalog\Models\Groups;
use Modules\Catalog\Models\Items;
use Modules\News\Models\News;
use Core\QB\DB;
use Modules\Cart\Models\Cart;
use Modules\Catalog\Models\Filter;
use Core\User;
use Core\Common;

/**
 *  Class that helps with widgets on the site
 */
class Widgets {

    static $_instance; // Constant that consists self class
    public $_data = []; // Array of called widgets
    public $_tree = []; // Only for catalog menus on footer and header. Minus one query

    // Instance method

    static function factory() {
        if (self::$_instance == NULL) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     *  Get widget
     * @param  string $name [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string        [Widget HTML]
     */
    public static function get($name, $array = [], $save = true, $cache = false) {
        $arr = explode('_', $name);
        $viewpath = implode('/', $arr);

        if (APPLICATION == 'backend' && !Config::get('error')) {
            $w = WidgetsBackend::factory();
        } else {
            $w = Widgets::factory();
        }

        $_cache = Cache::instance();
        if ($cache) {
            if (!$_cache->get($name)) {
                $data = NULL;
                if ($save && isset($w->_data[$name])) {
                    $data = $w->_data[$name];
                } else {
                    if ($save && isset($w->_data[$name])) {
                        $data = $w->_data[$name];
                    } else if (method_exists($w, $name)) {
                        $result = $w->$name($array);
                        if ($result !== null && $result !== false) {
                            $array = array_merge($array, $result);
                            $data = View::widget($array, $viewpath);
                        } else {
                            $data = null;
                        }
                    } else {
                        $data = $w->common($viewpath, $array);
                    }
                }
                $_cache->set($name, HTML::compress($data, true));
                return $w->_data[$name] = $data;
            } else {
                return $_cache->get($name);
            }
        }
        if ($_cache->get($name)) {
            $_cache->delete($name);
        }
        if ($save && isset($w->_data[$name])) {
            return $w->_data[$name];
        }
        if (method_exists($w, $name)) {
            $result = $w->$name($array);
            if ($result !== null && $result !== false) {
                if (is_array($result)) {
                    $array = array_merge($array, $result);
                }
                return $w->_data[$name] = View::widget($array, $viewpath);
            } else {
                return $w->_data[$name] = null;
            }
        }
        return $w->_data[$name] = $w->common($viewpath, $array);
    }

    /**
     *  Common widget method. Uses when we have no widgets called $name
     * @param  string $viewpath [Name of template file]
     * @param  array $array [Array with data -> go to template]
     * @return string            [Widget HTML or NULL if template doesn't exist]
     */
    public function common($viewpath, $array) {
        if (file_exists(HOST . '/Views/Widgets/' . $viewpath . '.php')) {
            return View::widget($array, $viewpath);
        }
        return null;
    }

    public function Index_Block2() {

        $productsSale = DB::select()
                ->from('catalog')
                ->where('sale', '=', '1')
                ->find_all();

        $productsHits = DB::select()
                ->from('catalog')
                ->where('top', '=', '1')
                ->find_all();


        $array['productsSale'] = $productsHits;
        $array['productsHits'] = $productsSale;


        return $array;
    }

    public function Index_Block4() {
        if (isset($_POST['send'])) {
            $name = $_POST['name'];
            $phone = $_POST['tel'];

            if ($_POST) {
                Common::factory('callback')
                        ->insert([
                            'name' => $name,
                            'phone' => $phone,
                ]);
            }
        }

        return true;
    }

    public function Index_Block5() {

        $reviews = DB::select()
                ->from('reviewsMain')
                ->find_all();

        $news = DB::select()
                ->from('news')
                ->find_all();

        $array['reviews'] = $reviews;
        $array['news'] = $news;

        return $array;
    }
    public function Index_MainSlider() {
        
        $slider = DB::select()
                ->from('slider')
                ->where('status', '=', 1)
                ->order_by('sort')
                ->find_all();
        
        $array['slider'] = $slider;
        
        return $array;
    }

    public function Header() {

        $categories = DB::select()
                ->from('catalog_tree')
                ->where('parent_id', '=', '0')
                ->order_by('sort')
                ->find_all();

        $level3 = DB::select()
                ->from('catalog_tree')
                ->where('parent_id', '<>', '0')
                ->order_by('sort')
                ->find_all();

        $topMenu = DB::select()
                ->from('topMenu')
                ->where('status', '=', '1')
//                ->order_by('sort')
                ->find_all();

        $phone = DB::select()
                ->from('config')
                ->where('key', '=', 'phone')
                ->find();

        $lvl1 = [];
        $lvl2 = [];
        $lvl3 = [];

        foreach ($topMenu as $value) {
            if ($value->parent_id == 0) {
                $lvl1[$value->parent_id][] = $value;
            }
            if ($value->id < 150) {
                $lvl2[$value->parent_id][] = $value;
            }
            if ($value->id > 150) {
                $lvl3[$value->parent_id][] = $value;
            }
        }

        $arr = [];

        foreach ($level3 as $value) {
            $arr[$value->parent_id][] = $value;
        }

        $topMenu = Common::factory('sitemenu')->getRows(1, 'sort', 'ASC');
        $user = User::info();
        $array['user'] = $user;
        $array['categories'] = $categories;
        $array['arr'] = $arr;
        $array['topMenu'] = $topMenu;

        $array['lvl1'] = $lvl1;
        $array['lvl2'] = $lvl2;
        $array['lvl3'] = $lvl3;
        $array['phone'] = $phone;
        $array['countCart'] = count($_SESSION['prod']) ?: 0;
        return $array;
    }

    public function Footer() {
        if (isset($_POST['send'])) {
            $email = $_POST['email'];
            Common::factory('subscribers')
                    ->insert([
                        'email' => $email,
            ]);
        }

        $cop = DB::select()
                ->from('config')
                ->where('key', '=', 'copyright')
                ->find();

        $topMenu = DB::select()
                ->from('sitemenu')
                ->find_all();
        
        $categories = DB::select()
                ->from('catalog_tree')
                ->where('parent_id', '=', '0')
                ->find_all();
        
        $topCat = DB::select()
                ->from('topMenu')
                ->where('parent_id', '=', '0')
                ->and_where('status', '=', 1)
                ->find_all();
        
        $socials = DB::select()
                ->from('config')
                ->where('group', '=', 'socials')
                ->find_all();


        $array['cop'] = $cop;
        $array['topMenu'] = $topMenu;
        $array['categories'] = $categories;
        $array['topCat'] = $topCat;
        $array['socials'] = $socials;
        return $array;
    }

}
