var validationTranslate = {
	required: "Это поле необходимо заполнить!",
	required_checker: "Этот параметр - обязателен!",
	required_select: "Этот параметр - обязателен!",

	password: "Укажите корректный пароль!",
	remote: "Пожалуйста, введите правильное значение!",
	email: "Пожалуйста, введите корректный адрес электронной почты!",
	url: "Пожалуйста, введите корректный URL!",
	date: "Пожалуйста, введите корректную дату!",
	dateISO: "Пожалуйста, введите корректную дату в формате ISO!",
	number: "Пожалуйста, введите число!",
	digits: "Пожалуйста, вводите только цифры!",
	creditcard: "Пожалуйста, введите правильный номер кредитной карты!",
	equalTo: "Пожалуйста, введите такое же значение ещё раз!",

	maxlength: "Пожалуйста, введите не более {0} символов!",
	maxlength_checker: "Пожалуйста, выберите не более {0} параметров!",
	maxlength_select: "Пожалуйста, выберите не более {0} пунктов!",

	minlength: "Пожалуйста, введите не менее {0} символов!",
	minlength_checker: "Пожалуйста, выберите не менее {0} параметров!",
	minlength_select: "Пожалуйста, выберите не менее {0} пунктов!",

	rangelength: "Пожалуйста, введите значение длиной от {0} до {1} символов!",
	rangelength_checker: "Пожалуйста, выберите от {0} до {1} параметров!",
	rangelength_select: "Пожалуйста, выберите от {0} до {1} пунктов!",

	range: "Пожалуйста, укажите значение от {0} до {1}!",
	max: "Пожалуйста, укажите значение, меньше или равное {0}!",
	min: "Пожалуйста, укажите значение, больше или равное {0}!",

	filetype: "Допустимые расширения файлов: {0}!",
	filesize: "Максимальный объем {0}KB!",
	filesizeEach: "Максимальный объем каждого файла {0}KB!",

	pattern: "Укажите значение соответствующее маске {0}!",
	word: "Введите корректное словесное значение!",
	noempty: "Поле не может быть пустым!",
	login: "Введите корректный логин!",
	phoneUA: "Некорректный формат украинского номера",
	phone: "Введите коректный номер телефона"
};

var mfpTranslate = {
	tClose: 'Закрыть (ESC)',
	tLoading: 'Загрузка контента ...',
	tNotFound: 'Контент не найден',
	tError: 'Невозможно загрузить <a href="%url%" target="_blank">Контент</a>.',
	tErrorImage: 'Невозможно загрузить <a href="%url%" target="_blank">Изображение #%curr%</a>.',
	tPrev: 'Предыдущая (клавиша Left)',
	tNext: 'Следующая (клавиша Right)',
	tCounter: '%curr% из %total%'
};

var select2Translate = {
	lang: 'ru',
	define: function () {
		function ending(count, one, couple, more) {
			if (count % 10 < 5 && count % 10 > 0 &&
				count % 100 < 5 || count % 100 > 20) {
				if (count % 10 > 1) {
					return couple;
				}
			} else {
				return more;
			}
			
			return one;
		}
		
		return {
			errorLoading: function () {
				return 'Невозможно загрузить результаты';
			},
			inputTooLong: function (args) {
				var overChars = args.input.length - args.maximum;
				
				var message = 'Пожалуйста, введите на ' + overChars + ' символ';
				
				message += ending(overChars, '', 'a', 'ов');
				
				message += ' меньше';
				
				return message;
			},
			inputTooShort: function (args) {
				var remainingChars = args.minimum - args.input.length;
				
				var message = 'Пожалуйста, введите еще хотя бы ' + remainingChars +
					' символ';
				
				message += ending(remainingChars, '', 'a', 'ов');
				
				return message;
			},
			loadingMore: function () {
				return 'Загрузка данных…';
			},
			maximumSelected: function (args) {
				var message = 'Вы можете выбрать не более ' + args.maximum + ' элемент';
				
				message += ending(args.maximum, '', 'a', 'ов');
				
				return message;
			},
			noResults: function () {
				return 'Совпадений не найдено';
			},
			searching: function () {
				return 'Поиск…';
			}
		};
	}
};

var woldTranslate = {
	ru: {
		'title': 'Студия Wezom',
		'close': 'Закрыть',
		'small': ' или младше',
		'end-1': 'Сайт может работать неправильно. Мы рекомендуем использовать <b>%b</b>.</p>',
		'end-2': 'Сайт может работать неправильно. Мы рекомендуем использовать <b>%b</b>.</p>',
		'end-3': 'Сайт может работать неправильно.</p>',
		'inform': '<p>Вы используете старый браузер - <b>%w</b>!',
		'device': '<p>Вы используете - <b>%w</b>!',
		'old-os': '<p>Вы используете устаревшую операционную систему - <b>%w</b>!',
		'ffx-esr': '<p>Вы используете <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>Вы используете неизвестный нам браузер!'
	}
}