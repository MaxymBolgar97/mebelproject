var validationTranslate = {
	required: "Це поле необхідно заповнити!",
	required_checker: "Цей параметр - обов'язковий!",
	required_select: "Цей параметр - обов'язковий!",

	password: "Вкажіть корректний пароль!",
	remote: "Будь ласка, введіть правильне значення!",
	email: "Будь ласка, введіть коректну адресу електронної пошти!",
	url: "Будь ласка, введіть коректний URL!",
	date: "Будь ласка, введіть коректну дату!",
	dateISO: "Будь ласка, введіть коректну дату у форматі ISO!",
	number: "Будь ласка, введіть число!",
	digits: "Будь ласка, вводите тільки цифри!",
	creditcard: "Будь ласка, введіть правильний номер кредитної картки!",
	equalTo: "Будь ласка, введіть таке ж значення ще раз!",

	maxlength: "Будь ласка, введіть не більш {0} символів!",
	maxlength_checker: "Будь ласка, виберіть не більш {0} параметрів!",
	maxlength_select: "Будь ласка, виберіть не більш {0} пунктів!",

	minlength: "Будь ласка, введіть не менш {0} символів!",
	minlength_checker: "Будь ласка, виберіть не менше {0} параметрів!",
	minlength_select: "Будь ласка, виберіть не менш {0} пунктів!",

	rangelength: "Будь ласка , введіть значення довжиною від {0} до {1} символів!",
	rangelength_checker: "Будь ласка, виберіть від {0} до {1} параметрів!",
	rangelength_select: "Будь ласка, виберіть від {0} до {1} пунктів!",

	range: "Будь ласка, вкажіть значення від {0} до {1}!",
	max: "Будь ласка, вкажіть значення, менше або рівне {0}!",
	min: "Будь ласка, вкажіть значення, більше або рівне {0}!.",

	filetype: "Допустимі розширення файлів: {0}!",
	filesize: "Максимальний обсяг {0}KB!",
	filesizeEach: "Максимальный размер каждого файла {0}KB!",

	pattern: "Вкажіть значення відповідне {0}!",
	word: "Введіть коректне ім'я!",
	noempty: "Поле не може бути порожім!",
	login: "Введіть коректний логін!",
	phoneUA: "Некоректний формат українського номера",
	phone: "Введіть коректний номер телефона"
};

var mfpTranslate = {
	tClose: 'Закрити (ESC)',
	tLoading: 'Завантаження контенту ...',
	tNotFound: 'Контент не знайдено',
	tError: 'Неможливо завантажити <a href="%url%" target="_blank">контент</a>.',
	tErrorImage: 'Неможливо завантажити <a href="%url%" target="_blank">Зображенння #%curr%</a>.',
	tPrev: 'Попередня (клавіша Left)',
	tNext: 'Наступна (клавіша Right)',
	tCounter: '%curr% з %total%'
};

var select2Translate = {
	lang: 'uk',
	define: function () {
		function ending(count, one, couple, more) {
			if (count % 100 > 10 && count % 100 < 15) {
				return more;
			}
			if (count % 10 === 1) {
				return one;
			}
			if (count % 10 > 1 && count % 10 < 5) {
				return couple;
			}
			return more;
		}
		
		return {
			errorLoading: function () {
				return 'Неможливо завантажити результати';
			},
			inputTooLong: function (args) {
				var overChars = args.input.length - args.maximum;
				return 'Будь ласка, видаліть ' + overChars + ' ' +
					ending(args.maximum, 'літеру', 'літери', 'літер');
			},
			inputTooShort: function (args) {
				var remainingChars = args.minimum - args.input.length;
				return 'Будь ласка, введіть ' + remainingChars + ' або більше літер';
			},
			loadingMore: function () {
				return 'Завантаження інших результатів…';
			},
			maximumSelected: function (args) {
				return 'Ви можете вибрати лише ' + args.maximum + ' ' +
					ending(args.maximum, 'пункт', 'пункти', 'пунктів');
			},
			noResults: function () {
				return 'Нічого не знайдено';
			},
			searching: function () {
				return 'Пошук…';
			}
		};
	}
};

var woldTranslate = {
	ua: {
		'title': 'Студія Wezom',
		'close': 'Закрити',
		'small': ' або молодше',
		'end-1': 'Сайт може працювати неправильно. Ми рекомендуємо використовувати <b>%b</b>.</p>',
		'end-2': 'Сайт може працювати неправильно. Ми рекомендуємо використовувати <b>%b</b>.</p>',
		'end-3': 'Сайт може працювати неправильно.</p>',
		'inform': '<p>Ви використовуєте старий браузер - <b>%w</b>!',
		'device': '<p>Ви використовуєте - <b>%w</b>!',
		'old-os': '<p>Ви використовуєте застарілу операційну систему - <b>%w</b>!',
		'ffx-esr': '<p>Ви використовуєте <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>Ви використовуєте невідомий нам браузер!'
	}
}