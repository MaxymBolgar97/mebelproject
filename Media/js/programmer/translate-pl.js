var validationTranslate = {
	required: "To pole należy wypełnić!",
	required_checker: "Parametr ten jest wymagany!",
	required_select: "Parametr ten jest wymagany!",

	password: "Określ paroll!",
	remote: "Proszę, wprowadźcie prawidłowe znaczenie!",
	email: "Proszę wpisać poprawny adres e-mail!",
	url: "Proszę podać poprawny adres URL!",
	date: "Wpisz poprawną datę!",
	dateISO: "Proszę podać poprawną datę w formacie ISO!",
	number: "Proszę wpisać numery!",
	digits: "Proszę wpisać tylko liczby!",
	creditcard: "Proszę podać poprawny numer karty kredytowej!",
	equalTo: "Proszę ponownie wprowadzić wartość!",

	maxlength: "Proszę wpisać nie więcej niż {0} znaków!",
	maxlength_checker: "Proszę wybrać nie więcej niż {0} parametrów!",
	maxlength_select: "Proszę wybrać nie więcej niż {0} pozycji!",

	minlength: "Proszę podać co najmniej {0} znaków!",
	minlength_checker: "Wybierz co najmniej {0} opcje!",
	minlength_select: "Wybierz co najmniej {0} przedmioty!",

	rangelength: "Prosimy podać wartość pomiędzy {0} {1} znaków!",
	rangelength_checker: "Proszę wybrać od {0} do {1} parametrów!",
	rangelength_select: "Proszę wybrać od {0} do {1} punktów!",

	range: "Prosimy podać wartość między {0} - {1}!",
	max: "Prosimy podać wartość mniejsza lub równa {0}!",
	min: "Prosimy podać wartość większą lub równą {0}!",

	filetype: "Dopuszczalne rozszerzenia plików: {0}!",
	filesize: "Maksymalny rozmiar {0}KB!",
	filesizeEach: "Maksymalny rozmiar każdego plika {0}KB!",

	pattern: "Określ wartość odpowiadającą maski {0}!",
	word: "Wprowadź poprawne znaczenie słów!",
	noempty: "Pole nie może być puste!",
	login: "Proszę podać poprawną nazwę użytkownika!",
	phoneUA: "Nieprawidłowy format numeru telefonu ukraiński",
	phone: "Wprowadź korektno numer telefonu"
};

var mfpTranslate = {
	tClose: 'Zamknąć (ESC)',
	tLoading: 'Zawartość do treści ...',
	tNotFound: 'Nie znaleziono treści',
	tError: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">treści</a>.',
	tErrorImage: 'Niemożliwe do ściągnięcia <a href="%url%" target="_blank">Obrazów #%curr%</a>.',
	tPrev: 'Poprzednia (klucz Left)',
	tNext: 'Następna (klucz Right)',
	tCounter: '%curr% z %total%'
};

var select2Translate = {
	lang: 'pl',
	define: function () {
		var charsWords = ['znak', 'znaki', 'znaków'];
		var itemsWords = ['element', 'elementy', 'elementów'];
		
		var pluralWord = function pluralWord(numberOfChars, words) {
			if (numberOfChars === 1) {
				return words[0];
			} else if (numberOfChars > 1 && numberOfChars <= 4) {
				return words[1];
			} else if (numberOfChars >= 5) {
				return words[2];
			}
		};
		
		return {
			errorLoading: function () {
				return 'Nie można załadować wyników.';
			},
			inputTooLong: function (args) {
				var overChars = args.input.length - args.maximum;
				
				return 'Usuń ' + overChars + ' ' + pluralWord(overChars, charsWords);
			},
			inputTooShort: function (args) {
				var remainingChars = args.minimum - args.input.length;
				
				return 'Podaj przynajmniej ' + remainingChars + ' ' +
					pluralWord(remainingChars, charsWords);
			},
			loadingMore: function () {
				return 'Trwa ładowanie…';
			},
			maximumSelected: function (args) {
				return 'Możesz zaznaczyć tylko ' + args.maximum + ' ' +
					pluralWord(args.maximum, itemsWords);
			},
			noResults: function () {
				return 'Brak wyników';
			},
			searching: function () {
				return 'Trwa wyszukiwanie…';
			}
		};
	}
};

var woldTranslate = {
	pl: {
		'title': 'Studio Wezom',
		'close': 'Zamknąć',
		'small': ' lub młodszy',
		'end-1': 'Strona może nie działać prawidłowo. Zalecamy używanie <b>%b</b>.</p>',
		'end-2': 'Strona może nie działać prawidłowo. Zalecamy używanie <b>%b</b>.</p>',
		'end-3': 'Strona może nie działać prawidłowo.</p>',
		'inform': '<p>Używasz starej przeglądarki - <b>%w</b>!',
		'device': '<p>Używasz - <b>%w</b>!',
		'old-os': '<p>Używasz przestarzałej system operacyjny - <b>%w</b>!',
		'ffx-esr': '<p>Używasz <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>Korzystania z nieznanych nam robi!'
	}
}