var validationTranslate = {
	required: "This field is required!",
	required_checker: "This parameter is required!",
	required_select: "This parameter is required!",

	remote: "Please fix this field.",
	email: "Please enter a valid email address.",
	url: "Please enter a valid URL.",
	date: "Please enter a valid date.",
	dateISO: "Please enter a valid date (ISO).",
	number: "Please enter a valid number.",
	digits: "Please enter only digits.",
	creditcard: "Please enter a valid credit card number.",
	equalTo: "Please enter the same value again.",

	maxlength: "Please enter no more than {0} characters.",
	maxlength_checker: "Please select no more than {0} parameters!",
	maxlength_select: "Please select no more than {0} items!",

	minlength: "Please enter at least {0} characters.",
	minlength_checker: "Please select at least {0} options!",
	minlength_select: "Please select at least {0} items!",

	rangelength: "Please enter a value between {0} and {1} characters long.",
	rangelength_checker: "Please select from {0} to {1} options!",
	rangelength_select: "Please select from {0} to {1} items!",

	range: "Please enter a value between {0} and {1}.",
	max: "Please enter a value less than or equal to {0}.",
	min: "Please enter a value greater than or equal to {0}.",

	filetype: "Allowed file extensions: {0}!",
	filesize: "Maximum size {0}KB!",
	filesizeEach: "Maximum amount of each file {0}KB!",

	pattern: "Specify a value corresponding to the mask {0}!",
	word: "Please enter the correct word meanings!",
	noempty: "The field can not be empty!",
	login: "Please enter a valid username!",
	phoneUA: "Invalid phone number format Ukrainian",
	phone: "Please enter valid phone number!"
};

var mfpTranslate = {
	tClose: 'Close (ESC)',
	tLoading: 'Loading ...',
	tNotFound: 'Content not found',
	tError: '<a href="%url%" target="_blank">The content</a> could not be loaded.',
	tErrorImage: '<a href="%url%" target="_blank">The image #%curr%</a> could not be loaded.',
	tPrev: 'Previous (Left arrow key)',
	tNext: 'Next (Right arrow key)',
	tCounter: '%curr% of %total%'
};

var select2Translate = {
	lang: 'en',
	define: function () {
		return {
			errorLoading: function () {
				return 'The results could not be loaded.';
			},
			inputTooLong: function (args) {
				var overChars = args.input.length - args.maximum;
				
				var message = 'Please delete ' + overChars + ' character';
				
				if (overChars != 1) {
					message += 's';
				}
				
				return message;
			},
			inputTooShort: function (args) {
				var remainingChars = args.minimum - args.input.length;
				
				var message = 'Please enter ' + remainingChars + ' or more characters';
				
				return message;
			},
			loadingMore: function () {
				return 'Loading more results…';
			},
			maximumSelected: function (args) {
				var message = 'You can only select ' + args.maximum + ' item';
				
				if (args.maximum != 1) {
					message += 's';
				}
				
				return message;
			},
			noResults: function () {
				return 'No results found';
			},
			searching: function () {
				return 'Searching…';
			}
		};
	}
};

var woldTranslate = {
	en: {
		'title': 'Studio Wezom',
		'close': 'Close',
		'small': ' or younger',
		'end-1': 'The site may not work properly. We recommend you to use <b>%b</b>.</p>',
		'end-2': 'The site may not work properly. We recommend you to use <b>%b</b>.</p>',
		'end-3': 'The site may not work properly.</p>',
		'inform': '<p>You are using an old browser - <b>%w</b>!',
		'device': '<p>You are using - <b>%w</b>!',
		'old-os': '<p>You are using an outdated operating system - <b>%w</b>!',
		'ffx-esr': '<p>You are using <b> <a href="https://www.mozilla.org/en-US/firefox/organizations/" title="Firefox - ESR" target="_blank">Firefox - ESR</a></b>!',
		'unknown': '<p>You are using an unknown browser!'
	}
}