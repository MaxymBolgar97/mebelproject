if (window.wHTML !== undefined) {
    //Добавление товара в список сравнения
    wHTML.compareAdd = function ($container) {
        var $container = $container || $('body');

        if ($container.find('.js-compare').length) {
            $container.find('.js-compare').on('click', function(){
                var $this = $(this);
                var id = $this.data('id');

                wHTML.preloaderShow();

                $.ajax({
                    url: wConfig.compareURL,
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json',
                    success: function(response) {
                        wHTML.preloaderHide();

                        if (response.success) {
                            if ($('.js-compare-count').length) {
                                $('.js-compare-count').html(response.count);
                            }

                            if ($this.hasClass('is-active')) {
								$this.removeClass('is-active');
                                if ($this.find('span').length) {
                                    $this.find('span').text('К сравнению');
                                }
							} else {
								$this.addClass('is-active');
                                if ($this.find('span').length) {
                                    $this.find('span').text('В сравнении');
                                }
							}

                            if (response.message) {
                                wHTML.message({
                                    message: response.message,
                                    type: 'success',
									effect: 'popup',
									autoHide: false
                                });
                            }
                        }
                        if (response.error && response.message) {
                            wHTML.message({
                                message: response.message,
                                type: 'error',
								effect: 'popup',
								autoHide: false
                            });
                        }
                    }
                });
            })
        }
    };

    //Удаление товара со списка сравнения
    wHTML.compareRemove = function () {
        if ($('.js-compare-remove').length) {
            $('.js-compare-remove').on('click', function () {
                var $this = $(this);
                var id = $this.data('id');

                wHTML.preloaderShow();
                if (id) {
                    $.ajax({
                        url: wConfig.compareRemoveURL,
                        data: 'id=' + id,
                        dataType: 'json',
                        method: 'post',
                        success: function (response) {
                            wHTML.preloaderHide();

                            if (response.type = 'success') {
                                var $slide = $this.closest('.slick-slide');
                                var $compareTop = $this.closest('.js-compare-top');
                                var $compareList = $this.closest('.js-compare-list');
                                var $compareBottom = $compareList.find('.js-compare-bottom');
                                var $slides = $compareTop.find('.slick-slide');

                                if ($slides.length > 1) {
                                    //Если товаров на текущей категории больше 1, то удаляем этот товар
                                    var indexSlide = $slides.index($slide);

                                    $slide.removeClass('is-notice');
                                    $compareTop.slick('slickRemove', indexSlide);
                                    $compareBottom.slick('slickRemove', indexSlide);
                                } else {
                                    //Если на текущей категории всего 1 товар, то удаляем всю категорию
                                    var $compareTabs = $this.closest('.js-compare-tabs');
                                    var $compareTabsList = $compareTabs.find('.js-compare-list');
                                    var indexTab = $compareTabsList.index($compareList);

                                    if ($compareTabsList.length > 1) {
                                        //Если категорий больше 1, то удаляем эту категорию
                                        $compareList.remove();
                                        $compareTabs.find('.js-compare-tab').eq(indexTab).remove();
                                        $compareTabs.find('.js-compare-tab').eq(0).addClass('is-active');
                                        $compareTabs.find('.js-compare-list').eq(0).addClass('is-active');
                                    } else {
                                        //Если категория всего 1, то очищаем все
                                        $compareTabs.find('.js-compare-clear').trigger('click');
                                        $('.js-compare-tabs').html('<div class="view-text pv-30"><h4>Список товаров пуст!</h4></div>');
                                    }
                                }
								
                                $('.js-compare-not-difference').trigger('click');
								wHTML.compareUpdate(true);
                            }
                            if (response.error) {
                                wHTML.message({
                                    message: response.error,
                                    type: 'error'
                                });
                            }
                        }
                    });
                }
            });
        }
        if($('.js-compare-clear').length){
            $('.js-compare-clear').on('click', function () {
                var parent_id = $(this).attr('data-id');

                wHTML.preloaderShow();
                $.ajax({
                    url: wConfig.clearCompareURL,
                    type: 'post',
                    data: 'catalog_tree_id=' + parent_id,
                    dataType: 'json',
                    success: function (response) {
                        wHTML.preloaderHide();

                        if (response.success) {
                            window.location.reload();
                        }
                        if (response.error) {
                            wHTML.message({
                                message: response.response,
                                type: 'error'
                            });
                        }
                    }
                });
            })
        }
    };

    //Добавление товара в избранное
    wHTML.favoriteAdd = function ($container) {
        var $container = $container || $('body');

        if ($container.find('.js-favorite').length) {
            $container.find('.js-favorite').on('click', function(){
                var $this = $(this);
                var id = $this.data('id');

                wHTML.preloaderShow();

                $.ajax({
                    url: wConfig.favoriteURL,
                    type: 'post',
                    data: 'id=' + id,
                    dataType: 'json',
                    success: function(response) {
                        wHTML.preloaderHide();

                        if (response.success) {
                            if ($('.js-favorite-count').length) {
                                $('.js-favorite-count').html(response.count);
                            }
	
							if ($this.hasClass('is-active')) {
								$this.removeClass('is-active');
                                if ($this.find('span').length) {
                                    $this.find('span').text('В избранное');
                                }
							} else {
								$this.addClass('is-active');
                                if ($this.find('span').length) {
                                    $this.find('span').text('В избранном');
                                }
							}

                            if ($this.data('remove')) {
                                $this.closest('.js-products-item').parent().remove();
                            }
                            if (!$('.js-products-item').length) {
                                $('.js-products').html('<div class="view-text pv-30"><h4>Список товаров пуст!</h4></div>');
                            }

                            if (response.message) {
                                wHTML.message({
                                    message: response.message,
                                    type: 'success',
									effect: 'popup',
									autoHide: false
                                });
                            }
                        }
                        if (response.error && response.message) {
                            wHTML.message({
                                message: response.message,
                                type: 'error',
								effect: 'popup',
								autoHide: false
                            });
                        }
                    }
                });
            })
        }
    };
	
	//Выбор цены в фильтре
	wHTML.submitPriceFilter = function () {
		if($('.js-filter-price').length) {
			$('.js-filter-price').on('click', function(){
				var $this = $(this);
				var url = $this.data('url');
				
                                var cur_mincost = $this.closest('.js-range-slider').find('.js-range-slider-min').val();
                                var prev_mincost = $this.closest('.js-range-slider').find('.js-range-slider-min').attr('data-cur');
                                var mincost = $this.closest('.js-range-slider').find('.js-range-slider-min').attr('data-limit');

                                var cur_maxcost = $this.closest('.js-range-slider').find('.js-range-slider-max').val();
                                var prev_maxcost = $this.closest('.js-range-slider').find('.js-range-slider-max').attr('data-cur');
                                var maxcost = $this.closest('.js-range-slider').find('.js-range-slider-max').attr('data-limit');

                                var cost = '';
                                if (mincost != cur_mincost || maxcost != cur_maxcost) {
                                    if (cur_mincost != mincost) cost = 'mincost-'+cur_mincost;
                                }
                                if (maxcost != cur_maxcost || mincost != cur_mincost) {
                                    if (cur_maxcost != maxcost) {
                                        if (cost) {
                                            cost = cost + '/';
                                        }
                                        cost = cost + 'maxcost-'+cur_maxcost;
                                    }

                                }

                                if (url) {
                                    window.location.href = url.replace('mincost-{mincost}', cost);
                                } 
			});
		}
	};
	
	//Выбор параметров на странице товара
	wHTML.productParams = function () {
		function productParamsUpdate() {
			if (window['productParams']) {
				for (var key in productParams.list) {
					var item = productParams.list[key]; //Данные с json
					
					var $prodParam = $('.js-' + key); //Параметр на странице товара
					if ($prodParam.length) {
						var $prodParamItem = $prodParam.find('.js-prod-param-item'); //Вызов всплывайки
						var $prodParamName = $prodParam.find('.js-prod-param-name'); //Название выбранного параметра
						var $prodParamValue = $prodParam.find('.js-prod-param-value'); //Название выбранного значения
						var $prodParamImage = $prodParam.find('.js-prod-param-image'); //Выбранное изображение
						
						var $prodPrice = $('.js-prod-price'); //Цена
						var $prodBtn = $('.js-prod-btn'); //кнопка Купить
						
						//Перенос выбранного значения из всплывайки на страницу
						$prodParamName.html(item.itemName);
						$prodParamValue.html(item.valueName);
						$prodParamImage.attr('src', item.valueImage);
						$prodPrice.html(productParams.price);
						
						//Перенос информации о выбранных параметрах в кнопку Купить
						var prodBtnParam = $prodBtn.data('param');
						prodBtnParam.WezomCart.params.options = productParams;
						$prodBtn.data('param', prodBtnParam);
						
						//Перенос информации о выбранных параметрах в data-атрибут вызовов всплываек.
						$prodParamItem.data('param', productParams);
					}
					
					var $prodParamPopup = $('.js-product-param'); //Параметр во всплывайке
					if ($prodParamPopup.length) {
						var $prodParamItemPopup = $prodParamPopup.find('.js-prod-param-item'); //Вызов всплывайки
						
						//Перенос информации о выбранных параметрах в data-атрибут вызовов всплываек.
						$prodParamItemPopup.data('param', productParams);
					}
				}
			}
		}
		
		$('.js-product-param').each(function () {
			var $param = $(this);
			var paramId = $param.data('param-id');//ID параметра
			var paramPrice = $param.data('price') || 0;
			var $values = $param.find('.js-product-param-value');//Массив значений
			
			var $btn = $param.find('.js-product-param-btn');
			var $select = $param.find('.js-product-param-select');//Выбранное значение
			var $total = $param.find('.js-product-param-total');//Результирующая цена
			
			window.productParamsLocal = productParams;
			
			$values.on('click', function () {
				var $value = $(this);
				var $item = $value.closest('.js-product-param-item');
				
				//Галочка на выбранном
				if (!$param.data('multiple')) {
					$values.removeClass('is-active'); //Не активные все параметры (если всего нужно выбрать только одно значение)
				} else {
					$item.find('.js-product-param-value').removeClass('is-active'); //Не активными делаем все значения параметра, внутри которого кликнули (если можно выбрать несколько значений)
				}
				$value.addClass('is-active');
				
				productParamsLocal.list['paramId' + paramId].list = [];
				var total = paramPrice;
				var text = '';
				$values.filter('.is-active').each(function (index, value) {
					var $value = $(this);
					var $valueName = $value.find('.js-product-value-name');
					var $valueImage = $value.find('.js-product-value-image');
					
					var $item = $value.closest('.js-product-param-item');
					var $itemName = $item.find('.js-product-param-name');
					
					var name = $itemName.html();
					var value = $valueName.html();
					var image = $valueImage.attr('src');
					
					var itemPrice = $item.data('price') || 0;
					var valuePrice = $value.data('price') || 0;
					var itemTotal = $item.data('total') || 0;
					var valueTotal = $value.data('total') || 0;
					
					if (valuePrice || itemPrice) {// Дополнительная стоимость (для особенности
						total += itemPrice + valuePrice;
					}
					if (valueTotal) {
						total = valueTotal;
					} else if (itemTotal) {
						total = itemTotal;
					}
					
					if (text == '') {
						text += name + ' ' + value;
					} else {
						text += ', ' + name + ' ' + value;
					}
					
					if (index == 0) {
						productParamsLocal.list['paramId' + paramId].itemName = name;
						productParamsLocal.list['paramId' + paramId].valueName = value;
						productParamsLocal.list['paramId' + paramId].valueImage = image;
					}
					
					productParamsLocal.list['paramId' + paramId].list[index] = {
						'itemId': $item.data('item-id'),
						'valueId': $value.data('value-id')
					}
				});
				productParamsLocal.price = total;
				
				//Перенос выбранных значений в футер
				$select.html(text);
				$total.html(total);
			});
			$btn.on('click', function () {
				if ($(this).data('url')) {
					wHTML.preloaderShow();
					$.ajax({
						url: $(this).data('url'),
						type: 'post',
						data: productParamsLocal,
						dataType: 'json',
						success: function (response) {
							wHTML.preloaderHide();
							
							if (response.success) {
								//Изменения в объекте productParams
								productParams = response.params;
							}
							//Обновление информации на странице товара
							productParamsUpdate();
							
							$.magnificPopup.close();
						}
					})
				} else {
					//Изменения в объекте productParams
					productParams = productParamsLocal;
					
					//Обновление информации на странице товара
					productParamsUpdate();
					
					$.magnificPopup.close();
				}
			});
		});
		
		productParamsUpdate();
	};

    //Загрузить еще
    wHTML.reload = function () {
        $('.js-reload').each(function () {
            var $this = $(this);
            var url = $this.data('url');
            var params = $this.data('params') || {};
            var $container = $this.closest('.js-reload-container');

            if (url && params && $container.length) {
                $this.on('click', function () {
                    $this.addClass('is-load');

                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: params,
                        dataType: 'json',
                        success: function (response) {
                            if (response.success) {
                                $container.before(response.html);

                                if (response.end) {
                                    $this.remove();
                                } else {
                                    $this.data('params').page++;
                                    $this.removeClass('is-load');
                                }
                            }
                        }
                    });
                });
            }
        });
    };
	
	wHTML.reviewsLike = function () {
		if ($('.js-reviews-like').length) {
			$('.js-reviews-like').on('click', function () {
				var $this = $(this);
				var $count = $this.find('div');
				var type = $this.data('type');
				var reviewId = $this.data('review-id');
				var url = $this.data('url');
				
				wHTML.preloaderShow();
				
				$.ajax({
					url: url,
					data: 'id=' + reviewId + '&type=' + type,
					method: 'post',
					dataType: 'json',
					success: function (response) {
						wHTML.preloaderHide();
						
						if (response.type == 'success') {
							$count.html('(' + response.count + ')');
						}
                                                if (response.message) {
                                                    wHTML.message({
                                                        message: response.message,
                                                        type: 'success',
                                                        effect: 'popup',
                                                        autoHide: false
                                                    });
                                                }
					}
				});
			});
		}
	}
	
	jQuery(document).ready(function($) {
        wHTML.compareAdd();
        wHTML.compareRemove();
        wHTML.favoriteAdd();
        wHTML.submitPriceFilter();
        wHTML.reload();
        wHTML.reviewsLike();
        wHTML.productParams();
	});
}