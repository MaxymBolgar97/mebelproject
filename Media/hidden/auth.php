<?php ob_start(); ?>
<div class="mfp-popup mfp-popup--big">
    <div class="auth">
        <div class="grid grid--astretch">
            <div class="cell cell--12 cell--sm24 cell--smostart">
                <div class="auth-block js-auth-block">
                    <div class="grid iv-15">
                        <div class="cell cell--24">
                            <div class="auth-block__title">
                                <span>Вход на сайт</span>
                            </div>
                        </div>
                        <div class="cell cell--24">

                            <div class="grid i-10 i-sm15">
                                <form action="/auth/login" method="POST">
                                    <div class="cell cell--24">
                                        <div class="auth-block__name">
                                            <span>Ваш email</span>
                                        </div>
                                        <div class="control-holder control-holder--text">
                                            <input type="email" name="email" data-name="email" required data-rule-email="true">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="auth-block__name">
                                            <span>Пароль</span>
                                        </div>
                                        <div class="control-holder control-holder--text">
                                            <input type="password" name="password" data-name="password" required data-rule-password="true" data-rule-minlength="6">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="control-holder control-holder--flag">
                                            <label>
                                                <input type="checkbox" name="remember" data-name="remember" value="1">
                                                <ins>
                                                    <svg><use xlink:href="svg/sprite.svg#icon-check" /></svg>
                                                </ins>
                                                <span>Запомнить меня</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="grid grid--acenter grid--jbetween i-10">
                                            <div class="cell cell--10">
                                                <div class="btn btn--full js-form-submit">
                                                    <button><span>Вход</span></button>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <a class="auth-block__link js-form-remember">Забыли пароль?</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="auth-block js-auth-block hide">
                    <div class="grid iv-15">
                        <div class="cell cell--24">
                            <div class="auth-block__title">
                                <span>Вспомнить пароль</span>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="form js-form js-rem-form" data-form="true" data-ajax="hidden/response.php">
                                <div class="grid i-10 i-sm15">
                                    <div class="cell cell--24">
                                        <div class="auth-block__name">
                                            <span>Ваш email</span>
                                        </div>
                                        <div class="control-holder control-holder--text">
                                            <input type="email" name="email" data-name="email" required data-rule-email="true">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="btn btn--full js-form-submit">
                                            <span>Напомнить пароль</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <a class="auth-block__link js-form-return">Войти в аккаунт</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell cell--12 cell--sm24 cell--smoend">
                <div class="auth-block">
                    <div class="auth-block__vline-left sm-hide"></div>
                    <div class="auth-block__gline sm-show"></div>
                    <div class="grid iv-15">
                        <div class="cell cell--24">
                            <div class="auth-block__title">
                                <span>Новый покупатель</span>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="grid i-10 i-sm15">
                                <form action="/auth/registration" method="POST">
                                    <div class="cell cell--24">
                                        <div class="auth-block__name">
                                            <span>Ваш email</span>
                                        </div>
                                        <div class="control-holder control-holder--text">
                                            <input type="email" name="email" data-name="email" required data-rule-email="true">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="auth-block__name">
                                            <span>Пароль</span>
                                        </div>
                                        <div class="control-holder control-holder--text">
                                            <input type="password" name="password" data-name="password" required data-rule-password="true" data-rule-minlength="6">
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="control-holder control-holder--flag">
                                            <label>
                                                <input type="checkbox" name="agree" data-name="agree" value="1" required>
                                                <ins>
                                                    <svg><use xlink:href="svg/sprite.svg#icon-check" /></svg>
                                                </ins>
                                                <span>Я согласен с <a href="/" class="auth-block__link" target="_blank">условими использованияи</a> на обработку моих персональных данных</span>
                                            </label>
                                            <div>
                                                <label for="agree" class="has-error" style="display: none;"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--24">
                                        <div class="btn btn--green btn--full">
                                            <button><span>Регистрация</span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell cell--24 cell--smostart">
                <div class="auth-block auth-block--bg">
                    <div class="auth-block__gline"></div>
                    <div class="grid grid--acenter grid--jcenter i-10">
                        <div class="cell">
                            <div class="auth-block__info">
                                <span>Или войти через соц. сети:</span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="grid i-5">
                                <div class="cell">
                                    <a href="#" class="auth-block__social auth-block__social--fb js-ulogin" data-link=".ulogin-button-facebook">
                                        <svg><use xlink:href="svg/sprite.svg#icon-facebook" /></svg>
                                    </a>
                                </div>
                                <div class="cell">
                                    <a href="#" class="auth-block__social auth-block__social--tw js-ulogin" data-link=".ulogin-button-twitter">
                                        <svg><use xlink:href="svg/sprite.svg#icon-twitter" /></svg>
                                    </a>
                                </div>
                                <div class="cell">
                                    <a href="#" class="auth-block__social auth-block__social--gl js-ulogin" data-link=".ulogin-button-google">
                                        <svg><use xlink:href="svg/sprite.svg#icon-google-plus" /></svg>
                                    </a>
                                </div>
                                <div class="cell cell--0">
                                    <div id="uLogin" data-ulogin="display=small;theme=classic;fields=first_name,last_name;providers=facebook,twitter,google;hidden=;redirect_uri=http%3A%2F%2Finkubator.ks.ua%2Fhtml%2Fmebel048%2Fhidden%2Fulogin.php;mobilebuttons=0;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>