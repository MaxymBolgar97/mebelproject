<?php
$MENU_HOR = "[
    {
        'name': 'Для офиса',
        'icon': 'icon-menu-top-1',
        'list': [
            {
                'name': 'Столы',
                'column': 1,
                'image': 'images/icon-menu-left-3.png',
                'list': [
                    {
                        'name': 'Столы руководителя'
                    },
                    {
                        'name': 'Офисные столы для персонала'
                    },
                    {
                        'name': 'Письменные столы'
                    },
                    {
                        'name': 'Приставные столы'
                    },
                    {
                        'name': 'Конференц столы'
                    },
                    {
                        'name': 'Компьютерные столы'
                    },
                    {
                        'name': 'Журнальные столы'
                    },
                    {
                        'name': 'Стол папка'
                    },
                    {
                        'name': 'Стол книжка'
                    }
                ]
            },
            {
                'name': 'Кресла и стулья',
                'column': 2,
                'image': 'images/icon-menu-left-1.png',
                'list': [
                    {
                        'name': 'Кресла для руководителя'
                    },
                    {
                        'name': 'Офисные кресла для персонала'
                    },
                    {
                        'name': 'Компьютерные кресла'
                    },
                    {
                        'name': 'Стулья офисные'
                    },
                    {
                        'name': 'Конференц кресла'
                    },
                    {
                        'name': 'Мягкая мебель для зон ожидания'
                    }
                ]
            },
            {
                'name': 'Тумбы и комоды',
                'column': 3,
                'image': 'images/icon-menu-left-8.png',
                'list': [
                    {
                        'name': 'Тумбы для офиса'
                    },
                    {
                        'name': 'Комоды'
                    }
                ]
            },
            {
                'name': 'Шкафы',
                'column': 3,
                'image': 'images/icon-menu-left-7.png',
                'list': [
                    {
                        'name': 'Шкафы офисные',
                        'list': [
                            {
                                'name': 'Шкафы для одежды'
                            },
                            {
                                'name': 'Шкафы для документов'
                            }
                        ]
                    },
                    {
                        'name': 'Пеналы офисные'
                    },
                    {
                        'name': 'Стелажи для документов'
                    }
                ]
            },
            {
                'name': 'Без категории',
                'column': 4,
                'list': [
                    {
                        'name': 'Стойки ресепшн'
                    },
                    {
                        'name': 'Офисные диваны'
                    }
                ]
            },
            {
                'name': 'Подборки',
                'column': 4,
                'list': [
                    {
                        'name': 'Эконом класс'
                    },
                    {
                        'name': 'Премиум класс'
                    },
                    {
                        'name': 'Комплекты офисной мебели',
                        'list': [
                            {
                                'name': 'Кабинеты руководителя'
                            },
                            {
                                'name': 'Мебель для персонала'
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        'name': 'Для гостиниц',
        'icon': 'icon-menu-top-2',
        'list': [
            {
                'name': 'Кровати для гостиниц',
                'column': 1,
                'list': [
                    {
                        'name': 'Односпальные'
                    },
                    {
                        'name': 'Двуспальные'
                    }
                ]
            },
            {
                'name': 'Комоды и тумбы',
                'column': 2,
                'image': 'images/icon-menu-left-8.png',
                'list': [
                    {
                        'name': 'Комоды'
                    },
                    {
                        'name': 'Тумбы для обуви'
                    },
                    {
                        'name': 'Туалетные столики'
                    },
                    {
                        'name': 'ТВ тумбы под телевизор'
                    },
                    {
                        'name': 'Прикроватные тумбы'
                    }
                ]
            },
            {
                'name': 'Каркасы кроватей',
                'column': 3
            },
            {
                'name': 'Матрасы',
                'column': 3
            },
            {
                'name': 'Журнальные столы',
                'column': 3
            },
            {
                'name': 'Прихожие',
                'column': 3
            },
            {
                'name': 'Шкафы для одежды',
                'column': 4
            },
            {
                'name': 'Вешалки',
                'column': 4
            },
            {
                'name': 'Угловые секции',
                'column': 4
            },
            {
                'name': 'Зеркала',
                'column': 4
            }
        ]
    },
    {
        'name': 'Для кабаре',
        'icon': 'icon-menu-top-3',
        'list': [
            {
                'name': 'Столы для КаБаРе',
                'column': 1
            },
            {
                'name': 'Стулья для КаБаРе',
                'column': 1
            },
            {
                'name': 'Базы, основания, опоры для столов',
                'column': 2
            },
            {
                'name': 'Столешницы столов',
                'column': 3
            },
            {
                'name': 'Мягкая мебель для КаБаРе',
                'column': 4
            }
        ]
    },
    {
        'name': 'Для школы',
        'icon': 'icon-menu-top-4',
        'list': [
            {
                'name': 'Парты школьные',
                'column': 1
            },
            {
                'name': 'Стулья школьные',
                'column': 2
            }
        ]
    },
    {
        'name': 'Для дома',
        'icon': 'icon-menu-top-5',
        'list': [
            {
                'name': 'Кровати',
                'column': 1,
                'list': [
                    {
                        'name': 'Двухспальные кровати'
                    },
                    {
                        'name': 'Односпальные кровати'
                    },
                    {
                        'name': 'Детские кровати'
                    },
                    {
                        'name': 'Двухярусные кровати'
                    },
                    {
                        'name': 'Деревянные кровати'
                    }
                ]
            },
            {
                'name': 'Журнальные столы',
                'column': 1,
                'list': [
                    {
                        'name': 'Стеклянные журнальные столы'
                    },
                    {
                        'name': 'Деревянные журнальные столы'
                    }
                ]
            },
            {
                'name': 'Матрасы',
                'column': 2,
                'list': [
                    {
                        'name': 'Ортопедические'
                    },
                    {
                        'name': 'Футоны'
                    },
                    {
                        'name': 'Детские Матрасы'
                    },
                    {
                        'name': 'Латексные'
                    },
                    {
                        'name': 'Пружинные'
                    },
                    {
                        'name': 'Беспружинные'
                    }
                ]
            },
            {
                'name': 'Комоды и тумбы',
                'column': 3,
                'list': [
                    {
                        'name': 'Комоды'
                    },
                    {
                        'name': 'Тумбы для обуви'
                    },
                    {
                        'name': 'Туалетные столики'
                    },
                    {
                        'name': 'ТВ тумбы под телевизор'
                    },
                    {
                        'name': 'Прикроватные тумбы'
                    }
                ]
            },
            {
                'name': 'Компьютерные столы',
                'column': 3,
                'list': [
                    {
                        'name': 'Прямые'
                    },
                    {
                        'name': 'Угловые'
                    },
                    {
                        'name': 'С надстройкой'
                    },
                    {
                        'name': 'С полкой для клавиатуры'
                    },
                    {
                        'name': 'Комп. столы для ноутбука'
                    }
                ]
            },
            {
                'name': 'Каркасы кроватей',
                'column': 4
            },
            {
                'name': 'Прихожие',
                'column': 4
            },
            {
                'name': 'Гостинные',
                'column': 4
            },
            {
                'name': 'Детские',
                'column': 4
            },
            {
                'name': 'Спальни',
                'column': 4
            },
            {
                'name': 'Модульная мебель',
                'column': 4
            }
        ]
    },
    {
        'name': 'Мебель на заказ',
        'icon': 'icon-menu-top-6',
        'list': [
            {
                'name': 'Кухни на заказ',
                'column': 1
            },
            {
                'name': 'Детская мебель на заказ',
                'column': 1
            },
            {
                'name': 'Офисная мебель на заказ',
                'column': 1
            },
            {
                'name': 'Шкафы-купе на заказ',
                'column': 2
            },
            {
                'name': 'Спальни на заказ',
                'column': 2
            },
            {
                'name': 'Стенки на заказ',
                'column': 2
            },
            {
                'name': 'Мебель для гостиниц на заказ',
                'column': 3
            },
            {
                'name': 'Барные стойки на заказ',
                'column': 3
            },
            {
                'name': 'Торговая мебель на заказ',
                'column': 4
            },
            {
                'name': 'Мебель для ванной на заказ',
                'column': 4
            }
        ]
    }
]";

$MENU_VER = "[
    {
		'name': 'Кресла',
        'image': 'images/icon-menu-left-1.png',
        'list': [
            {
				'name': 'Кресла для руководителя'
            },
            {
				'name': 'Офисные кресла для персонала'
            },
            {
				'name': 'Компьютерные кресла'
            },
            {
				'name': 'Конференц кресла'
            }
        ]
    },
    {
		'name': 'Стулья',
        'image': 'images/icon-menu-left-2.png',
        'list': [
            {
				'name': 'Стулья для КаБаРе'
            },
            {
				'name': 'Стулья школьные'
            },
            {
				'name': 'Стулья офисные'
            }
        ]
    },
    {
		'name': 'Столы',
        'image': 'images/icon-menu-left-3.png',
        'list': [
            {
				'name': 'Столы руководителя'
            },
            {
				'name': 'Офисные столы для персонала'
            },
            {
				'name': 'Письменные столы'
            },
            {
				'name': 'Приставные столы'
            },
            {
				'name': 'Конференц столы'
            },
            {
				'name': 'Компьютерные столы'
            },
            {
				'name': 'Стол папка'
            },
            {
				'name': 'Стол книжка'
            },
            {
				'name': 'Парты школьные'
            },
            {
				'name': 'Журнальные столы'
            },
            {
				'name': 'Столы для КаБаРе'
            },
            {
				'name': 'Базы, основания, опоры для столов'
            },
            {
				'name': 'Столешницы столов'
            },
            {
				'name': 'Стойки ресепшн'
            }
        ]
    },
    {
		'name': 'Кровати',
        'image': 'images/icon-menu-left-4.png',
        'list': [
            {
				'name': 'Кровати односпальные'
            },
            {
				'name': 'Кровати двуспальные'
            },
            {
				'name': 'Кровати детские'
            },
            {
				'name': 'Кровати для гостиниц'
            },
            {
				'name': 'Двухярусные кровати'
            },
            {
				'name': 'Деревянные кровати'
            },
            {
				'name': 'Каркасы кроватей'
            }
        ]
    },
    {
		'name': 'Матрасы',
        'image': 'images/icon-menu-left-5.png',
        'list': [
            {
				'name': 'Ортопедические'
            },
            {
				'name': 'Футоны'
            },
            {
				'name': 'Детские Матрасы'
            },
            {
				'name': 'Латексные'
            },
            {
				'name': 'Пружинные'
            },
            {
				'name': 'Беспружинные'
            }
        ]
    },
    {
		'name': 'Мягкая мебель',
        'image': 'images/icon-menu-left-6.png',
        'list': [
            {
				'name': 'Офисные диваны'
            },
            {
				'name': 'Мягкая мебель для КаБаРе'
            },
            {
				'name': 'Мягкая мебель для зон ожидания'
            }
        ]
    },
    {
		'name': 'Шкафы',
        'image': 'images/icon-menu-left-7.png',
        'list': [
            {
				'name': 'Шкафы офисные',
                'list': [
                    {
						'name': 'Шкафы для одежды'
                    },
                    {
						'name': 'Шкафы для документов'
                    }
                ]
            },
            {
				'name': 'Пеналы офисные'
            },
            {
				'name': 'Стелажи для документов'
            },
            {
				'name': 'Угловые секции'
            }
        ]
    },
    {
		'name': 'Комоды и тумбы',
        'image': 'images/icon-menu-left-8.png',
        'list': [
            {
				'name': 'Комоды'
            },
            {
				'name': 'Тумбы для офиса'
            },
            {
				'name': 'Тумбы для обуви'
            },
            {
				'name': 'Туалетные столики'
            },
            {
				'name': 'ТВ тумбы под телевизор'
            },
            {
				'name': 'Прикроватные тумбы'
            }
        ]
    },
    {
		'name': 'Вешалки'
    },
    {
		'name': 'Прихожие',
        'image': 'images/icon-menu-left-9.png',
        'list': [
            {
				'name': 'Зеркала'
            }
        ]
    },
    {
		'name': 'Гостинные',
        'image': 'images/icon-menu-left-10.png',
    },
    {
		'name': 'Детские',
        'image': 'images/icon-menu-left-12.png',
    },
    {
		'name': 'Спальни',
        'image': 'images/icon-menu-left-13.png',
    },
    {
		'name': 'Модульная мебель',
        'image': 'images/icon-menu-left-14.png',
    },
    {
		'name': 'Комплекты офисной мебели',
        'image': 'images/icon-menu-left-15.png',
        'list': [
            {
				'name': 'Кабинеты руководителя'
            },
            {
				'name': 'Мебель для персонала'
            }
        ]
    }
]";