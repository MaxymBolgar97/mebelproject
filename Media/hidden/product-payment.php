<?php ob_start(); ?>
<div class="mfp-popup mfp-popup--bigest">
	<div class="popup-block">
		<div class="popup-block__body">
			<div class="popup-block__title popup-block__title--left mb-30">
				<span>Способы оплаты</span>
			</div>
			<div class="block6">
				<div class="grid i-30 i-lg15">
					<div class="cell cell--16 cell--lg14 cell--ms24">
						<div class="grid i-10">
							<div class="cell cell--4">
								<div class="block6__icon">
									<img src="images/icon-delivery-9.png" alt="">
								</div>
							</div>
							<div class="cell cell--20">
								<div class="block6__desc view-text">
                                    <p><b>Наличными в магазинах «Мебель-АРТ»</b></p>
									<p>Оплата производится исключительно в национальной валюте. В подтверждение оплаты мы выдаем Вам чек</p>
								</div>
							</div>
							<div class="cell cell--4">
								<div class="block6__icon">
									<img src="images/icon-delivery-10.png" alt="">
								</div>
							</div>
							<div class="cell cell--20">
								<div class="block6__desc view-text">
                                    <p><b>Картой через терминал ПриватБанка в магазинах «Мебель-АРТ»</b></p>
									<p>Оплата производится через терминал ПриватБанка (портативный) в магазине «Мебель-АРТ» с помощью карты Visa / MasterCard любого банка.</p>
								</div>
							</div>
							<div class="cell cell--4">
								<div class="block6__icon">
									<img src="images/icon-delivery-11.png" alt="">
								</div>
							</div>
							<div class="cell cell--20">
								<div class="block6__desc view-text">
                                    <p><b>Безналичный расчет согласно выписанного счета-фактуры. Мы работает как с НДС так и без НДС</b></p>
									<p>Оплата по безналичному расчету осуществляется следующим образом: после оформления заказа, менеджер магазина факсом или электронной почтой вышлет Вам счет-фактуру, который Вы сможете оплатить в кассе отделения любого банка или с расчетного счета Вашей фирмы. Для юридических лиц пакет всех необходимых документов предоставляется вместе с товаром</p>
								</div>
							</div>
							<div class="cell cell--4">
								<div class="block6__icon">
									<img src="images/icon-delivery-12.png" alt="">
								</div>
							</div>
							<div class="cell cell--20">
								<div class="block6__desc view-text">
                                    <p><b>Через систему платежей Приват24</b></p>
									<p>Оплата производится через систему интернет платежей Приват24 на карточный счет ПриватБанка</p>
								</div>
							</div>
							<div class="cell cell--4">
								<div class="block6__icon">
									<img src="images/icon-delivery-13.png" alt="">
								</div>
							</div>
							<div class="cell cell--20">
								<div class="block6__desc view-text">
                                    <p><b>Через терминал Ibox</b></p>
									<p>Оплата на карту ПриватБанка через Ibox (терминалы систем моментальных платежей)</p>
								</div>
							</div>
						</div>
					</div>
					<div class="cell cell--8 cell--lg10 cell--ms24">
						<div class="block6__image-block block6__image-block--small">
							<img src="images/delivery-2.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>