<?php
	header('Content-Type: application/json');

	// кодируем объект данных с клиента,
	// который приходит JSON строкой
	$instance = json_decode( $_POST['instance'], true );
	// кодируем параметры, которые изменились
	$params = json_decode( $_POST['params'], true );
	$id = $_POST['id'];               // уникальный ключ товаров
	$list = $instance['list'];        // список всех товарав
	$globalsTotalPrices = array();    // для набора общих итоговых цен
	$postGlobalsTotalPrices = $instance['globals']['totalPrices'];
	$globalsQuantity = array(
		'items' => 0,
		'products' => 0
	);

	// перебираем товары
	foreach( $list as $key => $item )  {

		// итоговые цена каждого товара
		$prices = $item['prices'];

		// если ключи совпадают
		// удаляем элемент из списка
		if ( $item['id'] == $id ) {
			if ( array_key_exists( 'quantity', $params ) ) {
				$item['quantity']['selected'] = $params['quantity']['selected'];
				// var_dump($item);
			}
		}

		$globalsQuantity['items'] = $globalsQuantity['items'] + 1;
		$globalsQuantity['products'] = $globalsQuantity['products'] + $item['quantity']['selected'];

		// пересчитываем общие итоговое цены
		foreach( $prices as $priceName => $priceValue )  {
			// вырезаем лишние символы,
			// чтобы получить валидное число строкой
			$price = preg_replace( '/[^0-9\.]/', '', $priceValue );
			// приводим к числу
			$price = (float) $price;
			// итоговая цена по товару
			$totalPrice = $price * $item['quantity']['selected'];

			// форматируем итоговую цену по товару в строку
			$value = number_format( $totalPrice, 2, '.', ' ' );
			// если копеек нету - убераем (опционально)
			$value = str_replace( '.00', '', $value );
			$item['totalPrices'][$priceName] = $value;

			// общая итоговая цена на данный шаг итерации
			$total = $globalsTotalPrices[ $priceName ];
			if ( is_null($total) ) {
				// если первый цыкл - значит 0
				$total = 0;
			}

			// суммируем каждую итерацию
			$globalsTotalPrices[ $priceName ] = ( $total + $totalPrice );
		}

		$list[ $key ] = $item;
	}

	// возвращаем новый список
	// с объекта в массив
	$instance['list'] = array_values( $list );

	// если удалили все товары
	// включаем флаг пустой корзины
	if ( count( $instance['list'] ) === 0 ) {
		$instance['globals']['cartIsEmpty'] = true;
	} else {
		// форматируем итоговые цены, к примеру:
		// - 12543.50 -> "12 543.50"
		// - 1899.00 -> "1 899"
		foreach( $globalsTotalPrices as $key => $value )  {
			$value = number_format( $value, 2, '.', ' ' );
			// если копеек нету - убераем (опционально)
			$value = str_replace( '.00', '', $value );
			// вписываем новое значение
			$globalsTotalPrices[$key] = $value;
		}
	}

	// вписываем новые данные
	$instance['globals']['action'] = 'change';
	$instance['globals']['quantity'] = $globalsQuantity;
	$instance['globals']['totalPrices'] = $globalsTotalPrices;

	// возвращаем данные для рендера
	echo json_encode( $instance );
	die;
?>
