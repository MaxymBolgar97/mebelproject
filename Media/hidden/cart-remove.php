<?php
	header('Content-Type: application/json');

	// кодируем объект данных с клиента,
	// который приходит JSON строкой
	$instance = json_decode( $_POST['instance'], true );
	$id = $_POST['id'];               // уникальный ключ товаров
	$list = $instance['list'];        // список всех товарав
	$globalsTotalPrices = array();    // для набора общих итоговых цен
	$postGlobalsTotalPrices = $instance['globals']['totalPrices'];
	$globalsQuantity = array(
		'items' => 0,
		'products' => 0
	);

	// перебираем товары
	foreach( $list as $key => $item )  {

		// если ключи совпадают
		// удаляем элемент из списка
		if ( $item['id'] == $id ) {
			unset( $list[$key] );
			continue;
		}
		$globalsQuantity['items'] = $globalsQuantity['items'] + 1;
		$globalsQuantity['products'] = $globalsQuantity['products'] + $item['quantity']['selected'];

		// итоговые цена каждого товара
		$totalPrices = $item['totalPrices'];
		// пересчитываем общие итоговое цены
		foreach( $totalPrices as $priceName => $priceValue )  {
			// вырезаем лишние символы,
			// чтобы получить валидное число строкой
			$price = preg_replace( '/[^0-9\.]/', '', $priceValue );
			// приводим к числу
			$price = (float) $price;

			// собераем итоговые цены по товару
			$total = $globalsTotalPrices[ $priceName ];
			if ( is_null($total) ) {
				$total = 0;
			}

			// суммируем каждую итерацию
			$globalsTotalPrices[ $priceName ] = ( $total + $price );
		}
	}

	// возвращаем новый список
	// с объекта в массив
	$instance['list'] = array_values( $list );

	// если удалили все товары
	// включаем флаг пустой корзины
	if ( count( $instance['list'] ) === 0 ) {
		$instance['globals']['cartIsEmpty'] = true;
		foreach( $postGlobalsTotalPrices as $key => $value )  {
			$globalsTotalPrices[$key] = 0;
		}
	} else {
		// форматируем итоговые цены, к примеру:
		// - 12543.50 -> "12 543.50"
		// - 1899.00 -> "1 899"
		foreach( $globalsTotalPrices as $key => $value )  {
			$value = number_format( $value, 2, '.', ' ' );
			// если копеек нету - убераем (опционально)
			$value = str_replace( '.00', '', $value );
			// вписываем новое значение
			$globalsTotalPrices[$key] = $value;
		}
	}

	// вписываем новые данные
	$instance['globals']['action'] = 'remove';
	$instance['globals']['quantity'] = $globalsQuantity;
	$instance['globals']['totalPrices'] = $globalsTotalPrices;

	// возвращаем данные для рендера
	echo json_encode( $instance );
	die;
?>
