<?php
	$current = (isset($_POST['current'])) ? (int)$_POST['current'] : 0;
?>
<?php ob_start(); ?>
<div class="mfp-popup mfp-popup--large">
	<div class="product-image">
        <div class="grid grid--nowrap grid--mswrap grid--jbetween grid--msjcenter i-10 mb-0">
            <div class="cell cell--ms0">
                <div class="product-image__title">
                    <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                </div>
            </div>
            <div class="cell">
                <div class="product-image__price-block">
                    <div class="grid grid--nowrap i-10">
                        <div class="cell">
                            <div class="product-image__price-old">
                                <span>5614 грн</span>
                            </div>
                            <div class="product-image__price">
                                <span>5013 грн</span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="btn btn--green js-product-image-price">
                                <svg><use xlink:href="svg/sprite.svg#icon-cart" /></svg>
                                <span>Купить</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="grid i-30 i-lg15">
			<div class="cell cell--20 cell--ms18 cell--sm24">
				<div class="product-image__slider-block">
					<div class="product-image__slider js-product-image-slider">
						<?php for ($i = 1; $i <= 4; $i++) { ?>
							<div>
								<div class="product-image__item image image--contain">
									<img src="images/product-big-<?php echo $i; ?>.jpg" alt="">
								</div>
							</div>
						<?php } ?>
						<div>
							<div class="product-image__video">
								<iframe src="https://www.youtube.com/embed/sBThAV4rFNk" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell cell--4 cell--ms6 cell--sm0">
				<div class="product-image__list">
					<?php for ($i = 1; $i <= 4; $i++) { ?>
					<div class="product-image__item-small image image--contain js-product-image-list">
						<img src="images/product-big-<?php echo $i; ?>.jpg" alt="">
					</div>
					<?php } ?>
					<div class="product-image__video-small js-product-image-list">
						<svg><use xlink:href="svg/sprite.svg#icon-video" /></svg>
						<span>Видеообзор</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$('.js-product-image-price').on('click', function () {
			$.magnificPopup.close();
			setTimeout(function(){
				$('.js-prod-btn').trigger('click');
			}, 500);
		});
		if ($('.js-product-image-slider').length) {
			$('.js-product-image-slider').on('afterChange', function (e, slick, currentSlide) {
				$('.js-product-image-list').removeClass('is-active').eq(currentSlide).addClass('is-active');
			});
			$('.js-product-image-slider').slick({
				speed: 300,
				autoplay: false,
				touchMove: false,
				useTransform: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				initialSlide: <?php echo $current; ?>,
				infinite: false,
				vertical: false,
				arrows: true,
				dots: false,
				fade: true,
				cssEase: 'ease',
				prevArrow: '<div class="product-image__prev"><svg><use xlink:href="' + wConfig.svgSprite + '#arrow-left"></use></svg></div>',
				nextArrow: '<div class="product-image__next"><svg><use xlink:href="' + wConfig.svgSprite + '#arrow-right"></use></svg></div>',
			});
		}
		if ($('.js-product-image-list').length) {
			$('.js-product-image-list').eq(<?php echo $current; ?>).addClass('is-active');
			$('.js-product-image-list').on('click', function () {
				var index = $(this).parent().find('.js-product-image-list').removeClass('is-active').index(this);
				$(this).addClass('is-active');
				$('.js-product-image-slider').slick('slickGoTo', index);
			});
		}
	</script>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>