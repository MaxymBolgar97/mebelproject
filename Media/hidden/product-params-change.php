<?php
$params = [
	'productId' => 'id2221',
	'price' => 5020,
	'list' => [
		'paramId1' => [
			'itemName' => 'Кожа SPLIT',
			'valueName' => 'SP-A18',
			'valueImage' => 'images/product-param-1-8.jpg',
			'list' => [
				[
					'itemId' => '1',
					'valueId' => '18'
				]
			]
		],
		'paramId2' => [
			'itemName' => 'Дерев. элементы',
			'valueName' => 'Ясень',
			'valueImage' => 'images/product-param-2.jpg',
			'list' => [
				[
					'itemId' => '1',
					'valueId' => '13'
				]
			]
		],
		'paramId3' => [
			'itemName' => 'Механизм',
			'valueName' => 'Anyfix15',
			'valueImage' => 'images/product-param-1-5.jpg',
			'list' => [
				[
					'itemId' => '1',
					'valueId' => '15'
				]
			]
		],
		'paramId4' => [
			'itemName' => 'Тип роликов',
			'valueName' => 'Прорезиненные',
			'valueImage' => 'images/product-param-4.jpg',
			'list' => [
				[
					'itemId' => '1',
					'valueId' => '13'
				],
				[
					'itemId' => '2',
					'valueId' => '15'
				]
			]
		]
	]
];

echo json_encode(array(
	'success' => true,
	'params' => $params
));
die;