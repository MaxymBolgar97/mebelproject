<?php
	header('Content-Type: application/json');
	echo '{
	"globals": {
		"action": "update",
		"cartIsEmpty": false,
		"currency": " грн.",
		"totalPrices": {
			"retail": "20000"
		},
		"quantity": {
			"items": 2,
			"products": 3
		},
		"returnToShopping": {
			"enable": false,
			"url": "index.html"
		},
		"redirectIfEmpty": {
			"enable": false,
			"url": "index.html"
		},
		"paths": {
			"indexView": "index.html",
			"orderView": "order.html",
			"mfpOpen": "hidden/cart-mfp.php",
			"mediaFolder": ""
		},
		"shipping": "0",
		"special": "0"
	},
	"header": {
		"titles": {
			"default": "В корзине",
			"defaultEmpty": "Пусто",
			"order": "Список ваших товаров",
			"orderEmpty": "Корзина пуста",
			"ask": "Удалить?",
			"yes": "Да",
			"no": "Нет",
			"result": "ИТОГО:",
			"button": "Перейти в корзину",
			"products": "товара(ов) на сумму",
			"name": "Модель:",
			"color": "Материал и цвет обивки:",
			"price": "Цена:",
			"count": "Кол-во:",
			"total": "Сумма:",
			"gotoBtn": "Продолжить покупки",
			"orderBtn": "Оформить заказ",
			"orderTitle": "Содержимое заказа",
			"orderLink": "Редактировать заказ",
			"quantity": "шт.",
			"shipping": "Доставка",
			"special": "Скидка"
		}
	},
	"list": [
		{
			"id": "id1110",
			"url": "index.html",
			"type": "main",
			"title": "Тумба Orion Steel Chrome (SP-A, SP-I)",
			"prices": {
				"retailOld": "1240",
				"retail": "1024"
			},
			"totalPrices": {
				"retail": "1024"
			},
			"images": {
				"default": "images/product-1.jpg"
			},
			"quantity": {
				"selected": 1,
				"mincount": 1,
				"maxcount": 18,
				"step": 1,
				"gauge": "шт."
			},
			"attr": [
				{
					"key": "Материал и цвет обивки:",
					"value": "АРТ_004 (Варона Золотая/Черный)"
				}
			],
			"inCart": true
		},
		{
			"id": "id2221",
			"url": "index.html",
			"type": "main",
			"title": "Кресло руководителя Orion Steel Chrome (SP-A, SP-I)",
			"prices": {
				"retail": "2000"
			},
			"totalPrices": {
				"retail": "4000"
			},
			"images": {
				"default": "images/product-2.jpg"
			},
			"quantity": {
				"selected": 2,
				"mincount": 1,
				"maxcount": 25,
				"step": 1,
				"gauge": "шт."
			},
			"attr": [
				{
					"key": "Материал и цвет обивки:",
					"value": "АРТ_004 (Варона Золотая/Черный)"
				},
				{
					"key": "Тип роликов:",
					"value": "Прорезиненые"
				}
			],
			"inCart": true
		}
	],
	"footer": {}
}';
	die;
?>
