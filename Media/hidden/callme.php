<?php ob_start(); ?>
<div class="mfp-popup">
	<div class="popup-block">
		<div class="popup-block__head">
			<div class="grid grid--jcenter">
				<div class="cell">
					<div class="logo">
						<img src="pic/logo-white.png" alt="Мебель 048">
					</div>
				</div>
			</div>
		</div>
		<div class="popup-block__body">
			<div class="popup-block__title mb-10">
				<span>Оставьте заявку</span>
			</div>
			<div class="popup-block__text mb-20">
				<span>И наш специалист перезвонит Вам</span>
			</div>
			<div class="form js-form" data-form="true" data-ajax="hidden/response.php">
				<div class="grid grid--jcenter i-10">
					<div class="cell cell--20">
						<div class="form__label mb-5">
							<span>Имя<i>*</i></span>
						</div>
						<div class="control-holder control-holder--text">
							<input type="text" name="name" data-name="name" required data-rule-word="true">
						</div>
					</div>
					<div class="cell cell--20">
						<div class="form__label mb-5">
							<span>Телефон<i>*</i></span>
						</div>
						<div class="control-holder control-holder--text">
							<input type="tel" name="tel" data-name="tel" required data-rule-phone="true">
						</div>
					</div>
					<div class="cell cell--20">
						<div class="btn btn--green btn--full js-form-submit">
							Отправить заявку
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>