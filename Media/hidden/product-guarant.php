<?php ob_start(); ?>
	<div class="mfp-popup mfp-popup--bigest">
		<div class="popup-block">
			<div class="popup-block__body">
				<div class="popup-block__title popup-block__title--left mb-30">
					<span>Гарантируем 100% качества</span>
				</div>
				<div class="block6">
					<div class="grid i-50 i-lg20">
						<div class="cell cell--16 cell--lg14 cell--ms24">
							<div class="block6__bold mb-20">
								<span>Гарантийный срок эксплуатации мебели</span>
							</div>
							<div class="grid ig-10 iv-20 iv-lg10">
								<div class="cell cell--4">
									<div class="block6__icon">
										<img src="images/icon-delivery-14.png" alt="">
									</div>
								</div>
								<div class="cell cell--20">
									<div class="block6__desc">
										<p>Детской мебели и для общественных помещений</p>
                                        <p><b>18 месяцев</b></p>
									</div>
								</div>
								<div class="cell cell--4">
									<div class="block6__icon">
										<img src="images/icon-delivery-14.png" alt="">
									</div>
								</div>
								<div class="cell cell--20">
									<div class="block6__desc">
										<p>Бытовой мебели</p>
                                        <p><b>24 месяца с момента поставки</b></p>
									</div>
								</div>
								<div class="cell cell--6">
									<div class="block6__icon">
										<img src="pic/guarant-2.png" alt="">
									</div>
								</div>
								<div class="cell cell--18">
									<div class="block6__desc view-text">
										<p>За исключением случаев неосторожной эксплуатации или преднамеренного повреждения по вине Покупателя.</p>
										<p>В случае выявления Покупателем каких-либо дефектов, компания Мебель-АРТ в Николаеве обязуется устранить недостатки в течение 30 дней.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="cell cell--8 cell--lg10 cell--ms24">
							<div class="block6__image-block block6__image-block--small">
								<img src="images/guarant-1.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>