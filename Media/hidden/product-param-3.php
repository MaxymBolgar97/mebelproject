<?php ob_start(); ?>
<div class="mfp-popup mfp-popup--large">
    <!-- @TODO param-id - это id параметра товара (материал, дополнительно, модификация или особенность) -->
	<div class="product-param js-product-param" data-param-id="3">
		<div class="grid i-20">
			<div class="cell cell--24">
                <div class="product-param__title">
                    <span>Выберите вариант модификации</span>
                </div>
				<div class="grid grid--jbetween mt-10">
					<div class="cell">
						<div class="btn btn--green js-mfp-ajax js-prod-param-item" data-url="hidden/product-param-2.php" data-param=''>
							<svg><use xlink:href="svg/sprite.svg#icon-arrow-left" /></svg>
							<span>Назад</span>
						</div>
					</div>
					<div class="cell">
						<div class="btn btn--green js-mfp-ajax js-prod-param-item" data-url="hidden/product-param-4.php" data-param=''>
							<span>Далее</span>
							<svg><use xlink:href="svg/sprite.svg#icon-arrow-right" /></svg>
						</div>
					</div>
				</div>
			</div>
			<?php for ($j = 1; $j <= 1; $j++) { ?>
			<div class="cell cell--24">
                <!-- @TODO item-id - это id типа параметра (например, если есть 2 вида кожи или дерева) -->
				<div class="grid grid--acenter i-10 js-product-param-item" data-item-id="<?php echo $j; ?>" data-total="550<?php echo $j; ?>">
					<div class="cell">
						<div class="product-param__name js-product-param-name">Механизм</div>
					</div>
                    <div class="cell">
                        <div class="product-param__price-block">
                            <div class="product-param__price">
                                <span>550<?php echo $j; ?></span> <span>грн</span>
                            </div>
                        </div>
                    </div>
					<div class="cell cell--24">
						<div class="product-param__text">
							<span>Здесь должно быть краткое описание модификации</span>
						</div>
					</div>
					<div class="cell cell--24">
						<div class="grid grid--jcenter">
							<?php for ($i = 1; $i <= 7; $i++) { ?>
							<div class="cell">
                                <!-- @TODO value-id - это id значения параметра (например, цвет выбранной кожи) -->
								<div class="product-param__item js-product-param-value <?php if ($j == 1 && $i == 3) { ?>is-active<?php } ?>" data-value-id="<?php echo $j . $i; ?>">
									<img src="images/product-param-1-<?php echo $i; ?>.jpg" alt="" class="js-product-value-image">
									<span class="js-product-value-name">Anyfix<?php echo $j . $i; ?></span>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="cell cell--24">
				<div class="hr"></div>
				<div class="grid grid--nowrap grid--smwrap grid--jbetween i-10 i-ms5 mt-20">
					<div class="cell cell--msostart">
						<div class="product-param__btn btn btn--trans" onclick="$.magnificPopup.close()">
							<svg><use xlink:href="svg/sprite.svg#icon-arrow-left" /></svg>
							<span>Вернуться</span>
						</div>
					</div>
					<div class="cell cell--msoend cell--ms24 ms-gcenter">
						<div class="product-param__text">
							<span>Выбрано:</span> <span class="product-param__select js-product-param-select">Механизм</span>
						</div>
						<div class="product-param__total-text">
							<span>Цена:</span> <span class="product-param__total"><span class="js-product-param-total">5300</span> <span>грн</span></span>
						</div>
					</div>
					<div class="cell cell--msostart">
						<div class="product-param__btn btn js-product-param-btn" data-url="hidden/product-params-change.php">
							<span>Подтвердить выбор</span>
							<svg><use xlink:href="svg/sprite.svg#icon-arrow-right" /></svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>