<?php
$page = (isset($_POST['page'])) ? (int)$_POST['page'] : 1;
?>
<?php ob_start(); ?>
<div class="cell cell--8 cell--ms12 cell--sm24">
	<div class="index-know">
		<a href="index.html" class="index-know__image image image--cover">
			<img src="images/index-know-1.jpg" alt=""> </a>
		<div class="grid i-10">
			<div class="cell cell--7 gright">
				<div class="index-know__date">
					<span>18.01.2017</span>
				</div>
			</div>
			<div class="cell cell--17">
				<a href="index.html" class="index-know__name">
					<span>Как выбрать кресло: ключевые особенности и рекомендации специалистов</span>
				</a>
				<div class="index-know__desc">
					<p>При работе за столом в кресле основная нагрузка ложится на позвоночник так как большинство времени при работе мы проводим сидя. Поэтому по возможности выбирайте кресло с хорошей эргономикой…</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="cell cell--8 cell--ms12 cell--sm24">
	<div class="index-know">
		<a href="index.html" class="index-know__image image image--cover">
			<img src="images/index-know-2.jpg" alt=""> </a>
		<div class="grid i-10">
			<div class="cell cell--7 gright">
				<div class="index-know__date">
					<span>18.01.2017</span>
				</div>
			</div>
			<div class="cell cell--17">
				<a href="index.html" class="index-know__name">
					<span>Как выбрать матрас: ортопедические особенности, жесткость, наполнение, отзывы</span>
				</a>
				<div class="index-know__desc">
					<p>Как выбрать матрас? Данным вопросом задаются многие и в этом нет ничего удивительного. Согласитесь, ведь перед там, как отправится в магазин необходимо иметь хотя бы базовые знания о типах...</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="cell cell--8 cell--ms12 cell--sm24">
	<div class="index-know">
		<a href="index.html" class="index-know__image image image--cover">
			<img src="images/index-know-3.jpg" alt=""> </a>
		<div class="grid i-10">
			<div class="cell cell--7 gright">
				<div class="index-know__date">
					<span>18.01.2017</span>
				</div>
			</div>
			<div class="cell cell--17">
				<a href="index.html" class="index-know__name">
					<span>Как правильно выбрать кухню: советы профессионалов</span>
				</a>
				<div class="index-know__desc">
					<p>Как выбрать кухню? Рано или поздно у подавляющего большинства людей возникает этот вопрос. В этом нет ничего удивительного, кухня — это незаменимая часть любого дома, или квартиры, а, следовательно, требова...</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="cell cell--8 cell--ms12 cell--sm24">
	<div class="index-know">
		<a href="index.html" class="index-know__image image image--cover">
			<img src="images/index-know-4.jpg" alt=""> </a>
		<div class="grid i-10">
			<div class="cell cell--7 gright">
				<div class="index-know__date">
					<span>18.01.2017</span>
				</div>
			</div>
			<div class="cell cell--17">
				<a href="index.html" class="index-know__name">
					<span>Как выбрать кресло: ключевые особенности и рекомендации специалистов</span>
				</a>
				<div class="index-know__desc">
					<p>При работе за столом в кресле основная нагрузка ложится на позвоночник так как большинство времени при работе мы проводим сидя. Поэтому по возможности выбирайте кресло с хорошей эргономикой...</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo json_encode(array(
    'success' => true,
    'html' => $content,
    'count' => 3,
    'end' => ($page >= 4) ? true : false
));
die;
?>
