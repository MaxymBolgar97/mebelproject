<?php ob_start(); ?>
<div class="mfp-popup mfp-popup--bigest">
	<div class="popup-block">
		<div class="popup-block__body">
			<div class="popup-block__title popup-block__title--left mb-30">
				<span>Доставка и сборка</span>
			</div>
			<div class="block6">
				<div class="grid i-30 i-lg15">
                    <div class="cell cell--24">
                        <div class="block6__bold mb-20">
                            <span>Время доставки мебели в Одессе:</span>
                        </div>
                        <div class="block6__attrs">
                            <div class="grid i-10">
                                <div class="cell--8 cell--ms24">
                                    <div class="block6__attr block6__attr--select">
                                        <span>Время продажи</span>
                                    </div>
                                    <div class="block6__attr">
                                        <span>с 9-00 до 14-00</span>
                                    </div>
                                    <div class="block6__attr">
                                        <span>с 14-00 до 18-00</span>
                                    </div>
                                </div>
                                <div class="cell--8 cell--ms24">
                                    <div class="block6__attr block6__attr--select">
                                        <span>Время доставки по Одессе</span>
                                    </div>
                                    <div class="block6__attr">
                                        <span>с 14-00 до 18-00</span>
                                    </div>
                                    <div class="block6__attr">
                                        <span>с 9-00 до 18-00 следующего дня</span>
                                    </div>
                                </div>
                                <div class="cell--8 cell--ms24">
                                    <div class="block6__attr block6__attr--select">
                                        <span>Время доставки за пределы города</span>
                                    </div>
                                    <div class="block6__attr">
                                        <span>По договоренности</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="cell cell--10 cell--lg8 cell--ms24">
						<div class="block6__bold mb-20">
							<span>Стоимость доставки:</span>
						</div>
						<div class="grid i-10">
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-4.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Доставка мебели до подъезда</p>
                                    <p><b>150грн</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-5.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Подъём на этаж платный</p>
                                    <p><b>5-40 грн/этаж</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-6.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Занос в частный дом и 1-й этаж –</p>
                                    <p><b>10-25 грн.</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-7.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>За пределами Николаева доставка мебели -</p>
                                    <p><b>10,0 грн/км.</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-8.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Доставка и сборка мебели по Украине</p>
                                    <p><b>оговаривается индивидуально</b></p>
                                </div>
                            </div>
						</div>
					</div>
                    <div class="cell cell--6 cell--lg8 cell--ms24">
                        <div class="block6__desc view-text">
                            <p>Также мы доставляем в пригород:</p>
                            <p>Терновка - <b>160 грн</b></p>
                            <p>Мешково-Погорелово, Октябрьское, Балабановка, Б.Корениха – <b>190 грн</b></p>
                            <p>Матвеевка, Кульбакино – <b>180 грн</b></p>
                            <p>М.Корениха – <b>260 грн</b></p>
                        </div>
                    </div>
					<div class="cell cell--8 cell--ms24">
						<div class="block6__image-block block6__image-block--small">
							<img src="images/delivery-1.jpg" alt="">
						</div>
					</div>
                    <div class="cell cell--8 cell--lg10 cell--ms24">
                        <div class="block6__image-block block6__image-block--small">
                            <img src="images/delivery-3.jpg" alt="">
                        </div>
                    </div>
                    <div class="cell cell--16 cell--lg14 cell--ms24">
                        <div class="block6__bold mb-20">
                            <span>Сборка мебели:</span>
                        </div>
                        <div class="grid i-10">
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-1.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Сборка корпусной мебели на дому у заказчика –</p>
                                    <p><b>БЕСПЛАТНО</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-2.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>Сборка/разборка мягкой мебели:</p>
                                    <p>Диван – <b>20 грн.</b></p>
                                    <p>Мягкий уголок – <b>30 грн</b></p>
                                </div>
                            </div>
                            <div class="cell cell--4">
                                <div class="block6__icon">
                                    <img src="images/icon-delivery-3.png" alt="">
                                </div>
                            </div>
                            <div class="cell cell--20">
                                <div class="block6__desc view-text">
                                    <p>При покупке выставочного образца</p>
                                    <p>стоимость разборки/сборки:</p>
                                    <p>Кровать -  <b>50 грн.</b></p>
                                    <p>Шкаф - <b>100 грн.</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$content = ob_get_contents();
ob_end_clean();
echo $content;
die;
?>