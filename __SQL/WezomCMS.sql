-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 23 2017 г., 16:42
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `WezomCMS`
--

-- --------------------------------------------------------

--
-- Структура таблицы `access`
--

CREATE TABLE `access` (
  `id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `controller` varchar(64) DEFAULT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `edit` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `access`
--

INSERT INTO `access` (`id`, `role_id`, `controller`, `view`, `edit`) VALUES
(2, 5, 'index', 1, 0),
(3, 5, 'content', 0, 0),
(4, 5, 'control', 0, 0),
(5, 5, 'news', 0, 0),
(6, 5, 'articles', 1, 0),
(7, 5, 'menu', 0, 0),
(8, 5, 'slider', 0, 0),
(9, 5, 'gallery', 0, 0),
(10, 5, 'banners', 0, 0),
(11, 5, 'questions', 0, 0),
(12, 5, 'comments', 0, 0),
(13, 5, 'groups', 0, 0),
(14, 5, 'items', 0, 0),
(15, 5, 'brands', 0, 0),
(16, 5, 'models', 0, 0),
(17, 5, 'specifications', 0, 0),
(18, 5, 'orders', 0, 0),
(19, 5, 'simple', 0, 0),
(20, 5, 'users', 0, 0),
(21, 5, 'subscribers', 0, 0),
(22, 5, 'subscribe', 0, 0),
(23, 5, 'contacts', 0, 0),
(24, 5, 'callback', 0, 0),
(25, 5, 'mailTemplates', 0, 0),
(26, 5, 'config', 0, 0),
(27, 5, 'templates', 0, 0),
(28, 5, 'links', 0, 0),
(29, 5, 'scripts', 0, 0),
(30, 5, 'redirects', 0, 0),
(31, 6, 'button', 0, 0),
(32, 6, 'index', 0, 0),
(33, 6, 'content', 0, 0),
(34, 6, 'control', 0, 0),
(35, 6, 'news', 0, 0),
(36, 6, 'articles', 0, 0),
(37, 6, 'menu', 0, 0),
(38, 6, 'slider', 0, 0),
(39, 6, 'gallery', 0, 0),
(40, 6, 'banners', 0, 0),
(41, 6, 'questions', 0, 0),
(42, 6, 'comments', 0, 0),
(43, 6, 'groups', 0, 0),
(44, 6, 'items', 0, 0),
(45, 6, 'brands', 0, 0),
(46, 6, 'models', 0, 0),
(47, 6, 'specifications', 0, 0),
(48, 6, 'orders', 0, 0),
(49, 6, 'simple', 0, 0),
(50, 6, 'users', 0, 0),
(51, 6, 'subscribers', 0, 0),
(52, 6, 'subscribe', 0, 0),
(53, 6, 'contacts', 0, 0),
(54, 6, 'callback', 0, 0),
(55, 6, 'mailTemplates', 0, 0),
(56, 6, 'config', 0, 0),
(57, 6, 'templates', 0, 0),
(58, 6, 'links', 0, 0),
(59, 6, 'scripts', 0, 0),
(60, 6, 'redirects', 0, 0),
(121, 7, 'button', 0, 0),
(122, 7, 'index', 1, 0),
(123, 7, 'content', 1, 1),
(124, 7, 'control', 1, 1),
(125, 7, 'news', 1, 1),
(126, 7, 'articles', 1, 1),
(127, 7, 'menu', 1, 1),
(128, 7, 'slider', 1, 1),
(129, 7, 'gallery', 1, 1),
(130, 7, 'banners', 1, 1),
(131, 7, 'questions', 1, 1),
(132, 7, 'comments', 1, 1),
(133, 7, 'groups', 1, 1),
(134, 7, 'items', 1, 1),
(135, 7, 'brands', 1, 1),
(136, 7, 'models', 1, 1),
(137, 7, 'specifications', 1, 1),
(138, 7, 'orders', 1, 1),
(139, 7, 'simple', 1, 1),
(140, 7, 'users', 1, 1),
(141, 7, 'subscribers', 1, 1),
(142, 7, 'subscribe', 1, 1),
(143, 7, 'contacts', 1, 1),
(144, 7, 'callback', 1, 1),
(145, 7, 'mailTemplates', 1, 1),
(146, 7, 'config', 1, 1),
(147, 7, 'templates', 1, 1),
(148, 7, 'links', 1, 1),
(149, 7, 'scripts', 1, 1),
(150, 7, 'redirects', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  `show_image` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `created_at`, `updated_at`, `status`, `name`, `alias`, `text`, `h1`, `title`, `description`, `keywords`, `image`, `show_image`, `views`) VALUES
(5, 1447090773, 1492433938, 1, 'Что такое «Пантон»?', 'chto-takoe-panton', '<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Знали ли вы, что цвета коллекций дизайнеры не выбирают самостоятельно? Модные тенденции формируются не на подиумах, а в офисах исследовательского центра. Знакомьтесь, Pantone &ndash; институт цвета, который каждый декабрь провозглашает тренды будущих сезонов.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Pantone&nbsp; - авторитетный источник не только для fashion-индустрии. Его наработки используют графические и интерьерные дизайнеры, модные цвета отражаются в ювелирных украшениях и оттенках губной помады, которыми мы будем краситься. И так уже 40 лет.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp;</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Специалисты Pantone изучают философию цвета. Каждый год они анализируют источники вдохновения дизайнеров, что бы понять &laquo;чем живет&raquo; мировая общественность.</span></p>\r\n<p style=\"text-align: left;\"><em><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Оттенки 2015</span></em></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">2015 год &ndash; это период бурного индустриального и технического развития. Но делать акценты на искусственности &ndash; это не для Pantone. Поэтому все трендовые оттенки были почерпнуты из природы. Они мягкие, теплые и нежные &ndash; в стилистике плэнер.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp;</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Кстати, пленэр &ndash; это техника в живописи. Слово создано от французского &laquo;plein&raquo; и &laquo;air&raquo;.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Для пленер характерна натуральность, без прикрас, без постановочного освещения и надуманных образов. Как будто художник вышел утром в лес, прихватив кисточки, краски, мольберт, и начал рисовать.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Проще говоря, все трендовые оттенки года &ndash; очень натуральные и естественные. Они отражают буйство цветущего сада, мгновения закатов, прозрачность воды.</span></p>\r\n<p style=\"text-align: left;\"><em><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Список цветов</span></em></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Главный оттенок &ndash; Marsala &ndash; и по цвету, и по названию напоминающий вино из Сицилии. Не менее актуальны:</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp; Аквамарин;</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp; Ледниковый серый;</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp; Подводный синий;</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">&nbsp; Зеленый акриловый (мятный).</span></p>\r\n<p style=\"text-align: left;\"><span style=\"font-family: arial, helvetica, sans-serif; font-size: 12pt;\">Есть и вкусные цвета: &laquo;Клубничное мороженное&raquo; или &laquo;Мандарин&raquo;. В общем, если хотите быть в тренде, просто придерживайтесь палитры. Это совсем не сложно, ведь в каждой модной коллекции обязательно найдутся вещи в гамме от Пантон.</span></p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>', 'Что такое «Пантон»?', 'Что такое «Пантон»?', 'Что такое «Пантон»?', 'Что такое «Пантон»?', 'c7c2a25a62c6ef509aee1798ae2ee5f2.jpg', 1, 111);

-- --------------------------------------------------------

--
-- Структура таблицы `banners`
--

CREATE TABLE `banners` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `text` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `banners`
--

INSERT INTO `banners` (`id`, `created_at`, `updated_at`, `status`, `text`, `url`, `image`) VALUES
(6, 1447092731, 1462880266, 0, 'Баннер 1', '', '6a20c9cb45e3728f888c2ae727bb15b5.jpg'),
(7, 1447092766, 1462880258, 0, 'Баннер 2', '', '9c1294724ccdb29ab2848450178c9ec5.jpg'),
(8, 1447094167, 1483519425, 0, '', '', 'c49bfe228855a3fd5f5cd0b63350b500.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `date` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `text` text,
  `image` varchar(64) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `rubric_id` int(10) DEFAULT NULL,
  `views` int(10) DEFAULT '0',
  `likes` int(10) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0' COMMENT '0 - доступно всем, 1 - только обычным зарегистрированным пользователям, 2 - партнерам'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `created_at`, `updated_at`, `status`, `date`, `name`, `alias`, `text`, `image`, `h1`, `title`, `description`, `keywords`, `rubric_id`, `views`, `likes`, `type`) VALUES
(46, 1447091074, 1447778212, 1, 1446933600, 'Как ухаживать за одеждой', 'kak-uhazhivat-za-odezhdoj', '<p>У каждой современной модницы есть свои &laquo;любимчики&raquo; - это может быть кашемировый свитер, элегантное вечернее платье, комплект шелкового белья и многое другое. С этими вещами никогда не хочется расставаться, но, к сожалению, век одежды не долгосрочен.</p>\r\n<p>Для того, чтобы продлить удовольствие от любимых нарядов, давайте вспомним правила ухода за ними. Вы удивитесь, насколько можно продлить &laquo;жизнь&raquo; одежде, если придерживаться определенных правил.</p>\r\n<h2>Три главных составляющих долголетия вашей одежды это:</h2>\r\n<ul>\r\n<li>&nbsp; стирка;</li>\r\n<li>&nbsp; глажка;</li>\r\n<li>&nbsp; хранение.</li>\r\n</ul>\r\n<p>Азы ухода за одеждой должна знать каждая девушка. Начнем со стирки: прежде, чем бросать платье в стиральную машину, внимательно изучите информацию на ярлыке. Его ведь не зря туда прикрепили, а для того, чтобы модницы следовали технической инструкции. Если какие-то символы на ярлыке вам непонятны, то Google всегда рад помочь. Глажка так же должна быть строго по инструкции. Не пытайтесь перехитрить производителя и экономить на химчистке и правильной стирке.</p>\r\n<p>&nbsp;</p>\r\n<p>Хранение одежды &ndash; важнейший фактор, влияющий на ее долголетие. Прощаясь со своим пальто или платьем до следующего сезона, не забудьте убедиться, что вещь чистая. Ничего смешного в этом нет! Оправдание типа &laquo;наступит зима, тогда и почищу&raquo; приведет к &laquo;гибели&raquo; ваших любимых вещей. Если не верите, спросите у любого работника химчистки. Хранить одежду с пятнами в шкафу почти год &ndash; это худшее, что можно быть с ней. Пятна могут въесться настолько, что вывести их потом будет практически невозможно. К тому же вряд ли кому-то понравится с наступлением нового сезона отчищать куртку или пальто, вместо того, чтобы красоваться в чистом наряде среди друзей.</p>\r\n<h2>Сушка и хранение трикотажа</h2>\r\n<p>Трикотажные вещи, как и все другие, нужно сушить с умом. Главное правило здесь &ndash; защита от солнечных лучей. Если вы сушите вещи на балконе под лучами солнца, чтобы сэкономить место в квартире, то вы просто убиваете их. Ткань быстро выцветает и теряет прочность. Просто вспомните свой купальник из предыдущего отпуска, вряд ли он такой же, как был до поездки.</p>\r\n<p>&nbsp;</p>\r\n<p>В зависимости от типа ткани, одежда хранится по-разному. Трикотажные вещи нужно хранить в сложенном виде, а не в висячем. В противном случае вытянутся плечики и рукава, и любимый свитер или платье потеряют свою привлекательность.</p>\r\n<p>Храните и ухаживайте за одеждой правильно, тогда любимые вещи будут радовать вас очень долго.</p>', '2063536ccc4a51989df713f159258c2c.jpg', 'Как ухаживать за одеждой', 'Как ухаживать за одеждой', 'Как ухаживать за одеждой', 'Как ухаживать за одеждой', 12, 0, 0, 0),
(47, 1447091221, 1483517566, 1, 1447016400, 'Лето в деталях. Главные пляжные аксессуары 2015', 'leto-v-detaljah-glavnye-pljazhnye-aksessuary-2015', '<h2 style=\"text-align: justify;\">Цветная соломенная шляпа</h2>\r\n<p style=\"text-align: justify;\">Первое место в аксессуарах для пляжа &ndash; защита от солнца. Практичная соломенная шляпа справляется с этой функцией и с другой &ndash; та, то её носит, прослывет модницей. С узкими и широкими краями, с ободками и без &ndash; выбирайте любую, но толкьо не натурально бежевую. Соломенная шляпа ярких цветов - отличный способ выделиться на желто-голубом фоне курорта.</p>\r\n<h2 style=\"text-align: justify;\">Очки в пластиковой оправе</h2>\r\n<p style=\"text-align: justify;\">Ослепляющее солнце будет не по чем, если вы запасетесь солнцезащитным кремом и крупными очками в яркой пластиковой оправе. Кому нравятся файфареры, кто тяготит к округлым формам, как у кота Базилио, кто предпочитает необычную геометрическую форму &ndash; все найдут нечто для себя. И на макияже какая экономия!</p>\r\n<h3 style=\"text-align: justify;\">Туника</h3>\r\n<p style=\"text-align: justify;\">Если у вас слишком светлая кожа или вы не настолько смелая, чтобы разгуливать по побережью в бикини, туника вам непременно пригодиться. Самые модные варианты &ndash; светлый однотонный хлопок и расцветки в стиле хиппи. Самые интересные варианты сделаны из ткани с перфорацией, украшены бахромой. Лето становится еще горячее.</p>\r\n<h4 style=\"text-align: justify;\">Слитный купальник</h4>\r\n<p style=\"text-align: justify;\">В погоне за сексуальностью девушки отказались от таких моделей, и совершенно зря. Как утверждают мужчины, в слитных купальниках женская фигура смотрится гораздо аппетитнее. Для любительниц пляжного волейбола и безудержного веселья на волнах хорошая новость - какой бы интенсивной не была активность, ничего не слетит и не задерется.</p>\r\n<h5 style=\"text-align: justify;\">Пляжная сумка</h5>\r\n<p style=\"text-align: justify;\">Модницы несут с собой на пляж все &ndash; от необходимых бутылки воды и солнцезащитного крема до маникюрного набора на всякий случай. Кроме того, если не удастся вытянуть на пляж бойфренда, нести полотенце и подстилку придется самой. Куда это положить? Во вместительную пляжную сумку. Дизайнеры подготовили такое разнообразие, что определенных критериев выбора просто нет. Подберите сумки под купальник или шлепки. Один совет &ndash; чем больше деталей на сумке, тем лучше. Бахрома помпоны, брелоки &ndash; одним словом, детали в моде!</p>', '06d9aee86cb9e4bbcb9c98c0e8086256.jpg', 'Лето в деталях. Главные пляжные аксессуары 2015', 'Лето в деталях. Главные пляжные аксессуары 2015', 'Лето в деталях. Главные пляжные аксессуары 2015', 'Лето в деталях. Главные пляжные аксессуары 2015', 10, 0, 0, 0),
(48, 1447091289, NULL, 1, 1441746000, 'Как правильно хранить зимние вещи?', 'kak-pravilno-hranit-zimnie-veschi', '<p style=\"text-align: justify;\">Эх, как сладко с первыми лучами теплого солнышка закинуть наскучившие свитера да куртки на верхние полки и вдохнуть в гардероб летнее настроение топами и шортиками! Сие почти священное действо наполняет энергией от макушки до пяточек, ноги вальсируют, легкомыслие приходит на смену серым мыслям. Ну, здравствуй, лето, 90 дней счастья!</p>\r\n<p style=\"text-align: justify;\">И все бы хорошо, только вот на смену лету все равно придет осень с её дождями и заморозками. Это, конечно, будет еще не скоро. Но чтобы не сокрушаться над теплой одеждой потом, давайте правильно хранить её уже сейчас.</p>\r\n<h2 style=\"text-align: justify;\">Уход за зимними вещами</h2>\r\n<p style=\"text-align: justify;\">Ну, еще чуточку подумаем о ненастной зиме &ndash; и сразу на пляж, договорились? Перед тем, как сложить теплые вещи в шкаф, их нужно почистить. Конечно, всегда лучше воспользоваться услугами химчистки, но вполне можно справиться и собственными силами. Предварительно изучите информацию на этикетке, опустошите карманы &ndash; не очень-то приятно будет одевать куртку с растаявшей конфетой внутри или джинсы с жвачкой в заднем кармане.</p>\r\n<p style=\"text-align: justify;\">Не стоит складывать вещи в пакеты &ndash; полиэтилен создает благоприятную среду для моли и неприятного запаха. Идеально, если помещение периодически проветривается &ndash; свитера тоже хотят &laquo;дышать&raquo;. Если вы решительно настроены запихнуть вещи в пакет, то лучше используйте тару из натуральных материалов &ndash; хлопковые чехлы, льняные контейнеры и т.д.</p>\r\n<p style=\"text-align: justify;\">Ловите несколько секретов:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>Вещам не должно быть тесно. Пусть между ними будет пара сантиметров;</li>\r\n<li>Используйте мешочки с лавандой или апельсиновой цедрой. Пусть вещи пахнут летом, а не старым шкафом;</li>\r\n<li>Храните мех в максимально прохладном месте;</li>\r\n<li>Перед тем, как отправить шерстяное пальто на антресоли, хорошенько проветрите его.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">И еще &ndash; солнышка хочется, но и летом бывают дождливые деньки. Чтобы не тянуться каждый раз до антресолей, составьте капсулу одежды для прохладной погоды.</p>', 'faa03358852f3fdad99aedf329f34e77.jpg', 'Как правильно хранить зимние вещи?', 'Как правильно хранить зимние вещи?', 'Как правильно хранить зимние вещи?', 'Как правильно хранить зимние вещи?', 12, 0, 0, 0),
(49, 1447091355, NULL, 1, 1445374800, 'Джинсы-«мамики»: модная дерзость 2015', 'dzhinsy-mamiki-modnaja-derzost-2015', '<p style=\"text-align: justify;\">Не первый год дизайнеры декламируют с высоты своих подиумов о прекрасной возможности бесплатного обновления гардероба. Просто стащите пару вещей у бойфренда, дополните аксессуарами &ndash; и пара образов готова. В дело идет всё: от рубашек до смокингов. Эх, раздевать своих любимых так приятно &ndash; во всех планах! Овер-сайз нынче в тренде, да и в кризис решение как никогда актуально.</p>\r\n<p style=\"text-align: justify;\">В 2015 году величественный перст дизайнеров указал на гардероб&hellip;наших мам. Да-да, теперь и их можно &laquo;грабить&raquo; со спокойной душой. Что из этого вышло &ndash; читайте дальше.</p>\r\n<h2 style=\"text-align: justify;\">Джинсы-&laquo;мамики&raquo;</h2>\r\n<p style=\"text-align: justify;\">Возвращение трендов 70-х и хиппанской культуры сделали свое дело: в жизнь модниц нагрянули mom-джинсы. Помните ранние клипы Мадонны и мелодрамы 2000-х? Модницы гордо и повсеместно вышагивали в джинсах-клеш, а серые мышки предпочитали мешковатые модели с низкой посадкой. Многие из нас буквально на коленях умоляли своих мам выкинуть это чудо-чудное. И хорошо, что не уговорили &ndash; тренд снова в моде!</p>\r\n<p style=\"text-align: justify;\">Mom-джинсы насквозь пропитаны духом обезбашенной культуры феминизма, травки, секса и рок-н-рола. Легендарные женщины носили именно их &ndash; андрогинные, кричащие, как будто снятые с чужого бедра джинсы. Мэрилин Монро, Кейт Мосс, Дрю Бэрримор&hellip;.а &laquo;Друзья&raquo;? А &laquo;Секс в большом городе&raquo;? Все оттуда, из милого сердцу детства.</p>\r\n<p style=\"text-align: justify;\">Носить &laquo;мамики&raquo; можно по-разному. Вдохновитесь легендарными кумирами &ndash; и создайте свою интерпретацию, добавив в образ:</p>\r\n<ul style=\"text-align: justify;\">\r\n<li>Ретро-детали &ndash; собираясь на фэшн-пати;</li>\r\n<li>Свежие тренды 21 века.</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Беспроигрышный вариант &ndash; голубые, слега потертые mom-джинсы прямого кроя и простая майка. Можно разбавить образ винтажной обувью и аксессуарами. Накиньте шляпу &ndash; и начинайте хулиганить!</p>', '43021cad52b98c36fe77fffcbab5538e.jpg', 'Джинсы-«мамики»: модная дерзость 2015', 'Джинсы-«мамики»: модная дерзость 2015', 'Джинсы-«мамики»: модная дерзость 2015', 'Джинсы-«мамики»: модная дерзость 2015', 11, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `date` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `text` text,
  `answer` text,
  `blog_id` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `date_answer` int(10) DEFAULT NULL,
  `watched` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `created_at`, `updated_at`, `status`, `date`, `user_id`, `text`, `answer`, `blog_id`, `ip`, `name`, `date_answer`, `watched`) VALUES
(20, 1447091463, 1483518523, 1, 1447016400, NULL, 'Спасибо!\r\nВаша статья мне очень помогла! =) Теперь обязательно мои футболочки будут жить вечно!!!', '', 46, NULL, 'Василиса Петровна', 0, 1),
(21, 1447091514, 1483517918, 1, 1447016400, NULL, 'Никогда не понимал таких джинс =\\', 'Петр! Вы никогда и не поймете!\r\nДля этого Вам нужно быть женщиной ;)', 49, NULL, 'Петр', 1447016400, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `blog_rubrics`
--

CREATE TABLE `blog_rubrics` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blog_rubrics`
--

INSERT INTO `blog_rubrics` (`id`, `created_at`, `updated_at`, `sort`, `name`, `alias`, `text`, `h1`, `title`, `description`, `keywords`, `status`) VALUES
(10, 1447090962, 1458160121, 1, 'Мода', 'moda', '<p>Мода - совокупность привычек и вкусов (в отношении одежды, туалета), господствующих в определённой общественной среде в определённое время.</p>', 'Мода', 'Мода', 'Мода', 'Мода', 1),
(11, 1447090973, 1458160121, 2, 'Стиль', 'stil', '', 'Стиль', 'Стиль', 'Стиль', 'Стиль', 1),
(12, 1447091013, 1483517879, 0, 'Домашние дела', 'domashnie-dela', '', 'Домашние дела', 'Домашние дела', 'Домашние дела', 'Домашние дела', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE `brands` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `text` text,
  `alias` varchar(255) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `views` int(10) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `text`, `alias`, `h1`, `title`, `description`, `keywords`, `views`, `image`) VALUES
(29, 1447092819, NULL, 1, 0, 'Nike', '', 'nike', 'Nike', 'Nike', 'Nike', 'Nike', 0, NULL),
(30, 1447092830, NULL, 1, 0, 'New Balance', '', 'newbalance', 'New Balance', 'New Balance', 'New Balance', 'New Balance', 0, NULL),
(31, 1447092841, NULL, 1, 0, 'Benetton', '', 'benetton', 'Benetton', 'Benetton', 'Benetton', 'Benetton', 0, NULL),
(32, 1447092852, NULL, 1, 0, 'H&M', '', 'hm', 'H&M', 'H&M', 'H&M', 'H&M', 0, NULL),
(33, 1447092863, NULL, 1, 0, 'Next', '', 'next', 'Next', 'Next', 'Next', 'Next', 0, NULL),
(34, 1453976736, 1492434270, 1, 0, 'Aldo Brue', '', 'aldobrue', '', '', '', '', 0, NULL),
(35, 1453976759, NULL, 1, 0, 'Fabric Frontline', '', 'fabricfrontline', '', '', '', '', 0, NULL),
(36, 1453976774, NULL, 1, 0, 'Caf', '', 'caf', '', '', '', '', 0, NULL),
(37, 1453978318, NULL, 1, 0, 'Dolce & Gabbana', '', 'dolcegabbana', '', '', '', '', 0, NULL),
(38, 1453978328, NULL, 1, 0, 'Erdem', '', 'erdem', '', '', '', '', 0, NULL),
(39, 1453978341, NULL, 1, 0, 'Lancome', '', 'lancome', '', '', '', '', 0, NULL),
(40, 1453978382, NULL, 1, 0, 'Moorer', '', 'moorer', '', '', '', '', 0, NULL),
(41, 1453978395, NULL, 1, 0, 'Passarella Death Squad', '', 'passarelladeathsquad', '', '', '', '', 0, NULL),
(42, 1453978406, NULL, 1, 0, 'Valentino', '', 'valentino', '', '', '', '', 0, NULL),
(43, 1453978419, NULL, 1, 0, 'Victoria Beckham', '', 'victoriabeckham', '', '', '', '', 0, NULL),
(44, 1453978442, NULL, 1, 0, 'Stizzoli', '', 'stizzoli', '', '', '', '', 0, NULL),
(45, 1453978456, 1453982939, 1, 0, 'Two Women In The World', '', 'twowomenintheworld', 'Two Women In The World', 'Two Women In The World', 'Two Women In The World', 'Two Women In The World', 0, NULL),
(46, 1462882036, 1462882121, 1, 0, 'Puma ', '', 'puma', '', '', '', '', 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `callback`
--

CREATE TABLE `callback` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `callback`
--

INSERT INTO `callback` (`id`, `created_at`, `updated_at`, `status`, `name`, `phone`, `ip`) VALUES
(10, 1508505079, NULL, 0, 'Болгар Максим Сергеевич', '+3803323231222', NULL),
(13, 1508505253, NULL, 0, 'Болгар Максим Сергеевич', '+380952015405', NULL),
(17, 1508505470, NULL, 0, 'Болгар Максим Сергеевич', '+3803323231222', NULL),
(18, 1508505470, NULL, 0, 'Болгар Максим Сергеевич', '+3803323231222', NULL),
(75, 1508508712, NULL, 0, 'ТЕСТССССС', '+3801111111', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `carts`
--

CREATE TABLE `carts` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `carts`
--

INSERT INTO `carts` (`id`, `created_at`, `updated_at`, `hash`) VALUES
(8, 1505463590, NULL, 'e8e56a27460906ec07cc9cb20727ce95d99d355a'),
(12, 1508411497, NULL, '5ceb46ea601bca8425a9b60569975d67d66ffba1'),
(13, 1508485885, NULL, 'd38eced8ab65ce97b1bc860d6bf8d8bd843f26b1');

-- --------------------------------------------------------

--
-- Структура таблицы `carts_items`
--

CREATE TABLE `carts_items` (
  `id` int(10) NOT NULL,
  `cart_id` int(10) NOT NULL,
  `catalog_id` int(10) NOT NULL,
  `count` int(6) DEFAULT NULL,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `carts_items`
--

INSERT INTO `carts_items` (`id`, `cart_id`, `catalog_id`, `count`, `params`) VALUES
(7, 8, 1, 3, '{\"productID\":\"1\",\"modificationID\":\"1\",\"materialID\":\"242\",\"colorID\":\"413\",\"additionalID\":\"1\",\"additionalValueID\":\"1\",\"features\":[{\"featureID\":\"1\",\"featureValueID\":\"1\"},{\"featureID\":\"2\",\"featureValueID\":\"3\"}],\"cost\":\"230\",\"count\":\"1\"}'),
(8, 8, 2, 2, '{\"productID\":\"2\",\"modificationID\":\"\",\"materialID\":\"\",\"colorID\":\"\",\"additionalID\":\"1\",\"additionalValueID\":\"1\",\"features\":[{\"featureID\":\"1\",\"featureValueID\":\"1\"},{\"featureID\":\"2\",\"featureValueID\":\"3\"}],\"cost\":\"11237\",\"count\":\"1\"}'),
(13, 12, 2, 2, '{\"productID\":\"2\",\"cost\":\"11237\",\"modificationID\":null,\"materialID\":null,\"colorID\":null,\"additionalID\":\"1\",\"additionalValueID\":\"1\",\"features\":[{\"featureID\":\"1\",\"featureValueID\":\"1\"},{\"featureID\":\"2\",\"featureValueID\":\"3\"}]}');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `sort` int(10) DEFAULT '0',
  `artikul` varchar(128) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `desc` text,
  `charecter` text,
  `parent_id` int(10) DEFAULT '3',
  `sale` tinyint(1) DEFAULT '0',
  `new` tinyint(1) DEFAULT NULL,
  `top` tinyint(1) DEFAULT '0',
  `available` tinyint(1) DEFAULT '1',
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `cost` int(6) DEFAULT '0',
  `cost_old` int(6) DEFAULT '0',
  `views` int(10) DEFAULT '0',
  `brand_alias` varchar(255) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `specifications` text,
  `percent` int(128) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `min_cost` int(10) DEFAULT NULL,
  `min_cost_old` int(10) DEFAULT NULL,
  `max_cost` int(10) DEFAULT NULL,
  `rate` int(2) DEFAULT NULL,
  `text` text,
  `model_alias` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `created_at`, `updated_at`, `status`, `sort`, `artikul`, `name`, `alias`, `desc`, `charecter`, `parent_id`, `sale`, `new`, `top`, `available`, `h1`, `title`, `keywords`, `description`, `cost`, `cost_old`, `views`, `brand_alias`, `image`, `specifications`, `percent`, `video`, `min_cost`, `min_cost_old`, `max_cost`, `rate`, `text`, `model_alias`) VALUES
(31, 1508159795, NULL, 1, 0, '12314', 'Стул для персонала', 'stul-dlja-personala', 'Описание для Стул для персонала', 'Характеристика для Стул для персонала', 2, 0, 0, 1, 2, NULL, NULL, NULL, NULL, 100, 110, 0, NULL, 'absolyut.jpg', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 1508160495, 1508241816, 1, 0, '', 'Диван угловой Монако', 'divan-uglovoj-monako', 'Описание для Диван угловой Монако', 'Характеристика для Диван угловой Монако', 68, 1, 1, 0, 1, '', '', '', '', 4000, 5000, 0, NULL, '3b268a946fc9fc3a7c325ec5113d3339.jpg', NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1508160591, 1508329909, 1, 1, '12512', 'Мини-диван Барон', 'mini-divan-baron', 'Описание для Мини-диван Барон', 'Характеристика для Мини-диван Барон', 69, 1, 1, 0, 2, '', '', '', '', 900, 930, 0, NULL, 'SHIRSH.064.JPG', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 1508245238, NULL, 1, 5, 'AA12412VD', 'Офисный шкаф Ника ОН 9-3', 'ofisnyj-shkaf-nika-on-9-3', 'Описание для Офисный шкаф Ника ОН 9-3', 'Характеристика для Офисный шкаф Ника ОН 9-3', 47, 0, 0, 1, 1, '', '', '', '', 3086, 0, 0, '', 'nika-on-9-3.jpg', '', NULL, '', NULL, NULL, NULL, NULL, '', ''),
(50, NULL, NULL, 1, 0, NULL, ' Мини-диван Барон2', 'mini-divan-baron2', 'Описание для  Мини-диван Барон2', 'Характеристика для  Мини-диван Барон2', 69, 0, 1, 1, 1, NULL, NULL, NULL, NULL, 1414, 0, 0, NULL, 'nika-on-9-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, NULL, NULL, 1, 0, '124214', 'Стул', 'stul', 'stul', 'stul', 69, 0, NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 1508507362, NULL, 1, 1, '11231241', 'Сушилка для белья', 'sushilka-dlja-belja', NULL, NULL, 160, 0, 1, 1, 1, '', '', '', '', 789, 0, 0, NULL, 'sushilka_vannaya_komnata2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 1508746462, NULL, 1, 1, '1241251', 'Стенка Линда 2 (Венге цаво\\Дуб мадура )', 'stenka-linda-2-venge-tsavodub-madura-', '<p><strong>Стенка \"Линда 2\" является одновременно и&nbsp;тумбой под телевизор, и витриной, и шкафом для хранения вещей. Впишется в интерьер любой квартиры.</strong></p>\r\n<p><strong><img class=\"\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/test-images/stenka_0026.jpg\" alt=\"\" width=\"355\" height=\"254\" /></strong></p>\r\n<p><strong>В них можно хранить белье, книги, канцелярские принадлежности, мягкие игрушки, аксессуары, фоторамки, статуэтки.</strong></p>', '<ul>\r\n<li><strong>Модульная система прямой формы включает следующие элементы: модули, шкаф, шкаф угловой, стеллаж.</strong></li>\r\n<li><strong>Мебель изготовлена из ДСП, в фасадах применяется ламинированный МДФ.&nbsp;</strong></li>\r\n<li><strong>Модули состоят из множества вместительных секций, полочек открытого и закрытого типа, отделений, выдвижных ящиков.</strong></li>\r\n</ul>', 161, 1, 1, 0, 1, '', '', '', '', 3569, 4000, 0, NULL, 'linda-wenge-1_beldrev.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 1508747296, NULL, 1, 2, '12512513', 'Зеркало настенное Виктория №3', 'zerkalo-nastennoe-viktorija-3', '<p><strong>Набор корпусной мебели для спальни &laquo;Виктория в.3&raquo; включает в себя: кровать двойную, шкаф для одежды трехстворчатый, комод, тумбу прикроватную 2 шт., зеркало. Любой элемент комплекта Вы также можете приобрести отдельно.</strong></p>\r\n<p>&nbsp;</p>\r\n<p><strong><img class=\"\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/test-images/viktoriya_v3_zerkalo_nastennoe.jpg\" alt=\"\" width=\"255\" height=\"255\" /></strong></p>', '<p><strong>Материалы:</strong></p>\r\n<ul>\r\n<li><strong>фасад&nbsp;&ndash; &nbsp;МДФ</strong></li>\r\n<li><strong>корпус &ndash; ламинированное ДСП</strong></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p style=\"text-align: center;\"><strong>Примечание: цена указана за набор мебели; цветовые решения могут несущественно отличаться от представленных на сайте.</strong></p>', 57, 1, 0, 1, 2, '', '', '', '', 1280, 2000, 0, NULL, 'viktoriya_v3_zerkalo_nastennoe.jpg', NULL, 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 1508748227, 1508748767, 1, 2, '139035', 'Шкаф угловой S320-SZFN5D Кентаки (KENTAKI) (белый)', 'shkaf-uglovoj-s320-szfn5d-kentaki-kentaki-belyj', '<p><strong>Кентаки (KENTAKI)</strong>&nbsp;- элегантная коллекция мебели, выполненная в классическом стиле. Отличительными особенностями набора являются витиеватые изящные ручки. Широкий выбор элементов данной коллекции позволит обустроить Ваш дом в едином неповторимом стиле.</p>\r\n<p>В состав набора модульной мебели&nbsp;<strong>KENTAKI&nbsp;</strong>входят&nbsp;37 элементов в белом цвете&nbsp;&nbsp;для создания <strong>стильного интерьера&nbsp;гостиной,&nbsp;спальни, прихожей.</strong></p>', '<p><strong>Габаритные размеры, мм:</strong></p>\r\n<ul>\r\n<li><strong>Высота - 2250</strong></li>\r\n<li><strong>Ширина - 1365</strong></li>\r\n<li><strong>Глубина - 1365</strong></li>\r\n</ul>', 50, 0, 0, 0, 2, '', '', '', '', 2500, 0, 0, NULL, 'shkaf_uglovoj_s320-szfn5d_kentaki_belyj_black_red_white.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 1508748997, NULL, 1, 0, '372233', 'Шкаф S131-REG1W Арека (AREKA)', 'shkaf-s131-reg1w-areka-areka', '<p><strong>AREKA&nbsp;</strong>- набор модульной мебели, способный угодить самому современному вкусу. Для набора характерны прямые линии, простые формы, функциональные элементы, стильное сочетание цветов, минимальное количество декора. Функциональность обеспечивают глубокие выдвижные ящики и вместительные шкафы. Солидные профилированные фасады и отсутствие ручек в их прямом понимании основные характерные особенности набора.&nbsp;</p>\r\n<p><img class=\"\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/test-images/shkaf_s131-reg1w_areka_black_red_white.jpg\" alt=\"\" width=\"201\" height=\"151\" /></p>\r\n<div class=\"desk-block\">\r\n<div><em>В состав набора модульной мебели&nbsp;<strong>AREKA&nbsp;</strong>входят&nbsp;26 элементов в цвете дуб венге / дуб венге Магия&nbsp;для создания стильного интерьера&nbsp;гостиной,&nbsp;спальни&nbsp;и&nbsp;прихожей.&nbsp;</em></div>\r\n<p>&nbsp;</p>\r\n</div>', '<p><strong>Габаритные размеры, мм:</strong></p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Высота - 1995</li>\r\n<li>Ширина - 560</li>\r\n<li>Глубина - 430</li>\r\n</ul>', 48, 1, 0, 0, 1, '', '', '', '', 1500, 2300, 0, NULL, 'shkaf_s131-reg1w_areka_black_red_white.jpg', NULL, 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 1508749445, 1508756234, 1, 0, '897586', 'Шкаф-купе ШК05', 'shkaf-kupe-shk05', '<p><strong>Шкаф-купе ШК05 от мебельной фабрики \"КОРТЕКС-МЕБЕЛЬ\"</strong> - это 2-х дверный шкаф с двумя отсеками, оборудованный встроенными светильниками. Исполняется с левой или правой консолью.<br /><strong>Может быть изготовлен в 3-х исполнениях:</strong></p>\r\n<ul>\r\n<li>без зеркал (ШК05-00);</li>\r\n<li>с одним зеркалом (ШК05-01);</li>\r\n<li>с двумя зеркалами (ШК05-02)</li>\r\n</ul>\r\n<p style=\"text-align: center;\"><strong><em>Наполнение шкафа&nbsp;</em></strong></p>\r\n<p style=\"text-align: left;\"><strong><em><img class=\"\" src=\"http://мебельплюс.бел/images/kortex/shkafy_kupe/shk05_sxema.png\" alt=\"\" width=\"354\" height=\"266\" /></em></strong></p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><strong>Цветовые решения</strong></p>\r\n<p>Шкаф-купе ШК05 может быть изготовлен на заказ в следующих&nbsp;<br />цветовых решениях:</p>\r\n<table class=\"table-textures\">\r\n<tbody>\r\n<tr>\r\n<td><img src=\"http://xn--90ahbyabzr0hwa.xn--90ais/images/kortex/textures/venge_kombinirovannyj.jpg\" alt=\"венге комбинированный\" width=\"65\" height=\"65\" /></td>\r\n<td><img src=\"http://xn--90ahbyabzr0hwa.xn--90ais/images/kortex/textures/venge.jpg\" alt=\"венге\" width=\"65\" height=\"65\" /></td>\r\n<td><img src=\"http://xn--90ahbyabzr0hwa.xn--90ais/images/kortex/textures/svenge.jpg\" alt=\"венге светлый\" width=\"65\" height=\"65\" /></td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<p>венге&nbsp;<br />комбинированный</p>\r\n</td>\r\n<td>венге</td>\r\n<td>\r\n<p>венге&nbsp;<br />светлый</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><img src=\"http://xn--90ahbyabzr0hwa.xn--90ais/images/kortex/textures/yablonya_lokarno_temnaya.jpg\" alt=\"яблоня локарно\" width=\"65\" height=\"65\" /></td>\r\n<td><img src=\"http://xn--90ahbyabzr0hwa.xn--90ais/images/kortex/textures/olha.jpg\" alt=\"ольха\" width=\"65\" height=\"65\" /></td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p><em>Примечание:&nbsp;цветовые решения могут несущественно отличаться от представленных на сайте; шкаф-купе поставляется в разобранном и упакованном виде, в фабричной упаковке.</em></p>', '<p><strong>Габаритные размеры, мм:</strong></p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li>Высота -&nbsp;2200</li>\r\n<li>Ширина -&nbsp;1850</li>\r\n<li>Глубина -&nbsp;600</li>\r\n</ul>', 162, 0, 1, 1, 1, '', '', '', '', 3578, 0, 0, NULL, '5_30.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 1508754112, NULL, 1, 1, '2113531', 'ПОЛУБАРНЫЙ СТУЛ CONNUBIA CB/1427 LED', 'polubarnyj-stul-connubia-cb1427-led', '<p><strong>Оригинальные итальянские барные&nbsp;стулья CB/1427 Led&nbsp;</strong>прекрасно подходят не только для бара, но и для домашнего применения.<br />Если интерьер Вашей столовой или studio выдержан в стилях &laquo;модерн&raquo;, hi-tech, fusion или в других авангардных направлениях дизайна, небольшой барный уголок будет выглядеть там вполне уместно.</p>\r\n<p><strong>Стильный хромированный или сатиновый каркас&nbsp;стула Led</strong>, изготовленный из металла, устойчив и надёжен. Сидение из высокопрочного пластика (цвет можно подобрать на своё усмотрение) имеет физиологически правильную форму. На таком стуле можно не только расслабляться, глядя в окно и попивая мохито, но также и заниматься домашней работой &ndash; например, готовить обед сидя.<br />Высота&nbsp;стула Led&nbsp;83 см, ширина 41 см, глубина 42 см, высота сидения 65 см. Полубарные стулья Led комплектуются со столами высотой до 90 см над уровнем пола.</p>\r\n<p><img class=\"\" src=\"https://resolute.com.ua/uploads/shop/products/additional/b4d17b8c7eb19483cd4a0d0c84882a89.jpg\" alt=\"\" width=\"558\" height=\"453\" /></p>', '<ul>\r\n<li><strong>Цвет</strong> на выбор</li>\r\n<li><strong>Материал основания (рамы)</strong> Металл</li>\r\n<li><strong>Габариты (ВхШхГ)</strong> см 83х41х42</li>\r\n<li><strong>Высота до сидения (см)</strong> 65</li>\r\n<li><strong>Материал Сиденье:</strong> Пластик</li>\r\n</ul>', 14, 0, 0, 0, 1, '', '', '', '', 3944, 0, 0, NULL, '57b8-cb-1427-led-connubia-italyanskaya-mebel-barnie-stulya.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 1508754402, NULL, 1, 0, '431242', 'ПОЛУБАРНЫЙ СТУЛ CONNUIA CB/1140 EVERGREEN', 'polubarnyj-stul-connuia-cb1140-evergreen', '<p>Симпатичные полубарные стулья CB/1140 Evergreen станут стильным элементом и оригинальным украшением Вашей столовой, кухни или studio. Классическая форма стула и возможность подобрать цвет под уже имеющийся ансамбль по своему усмотрению, открывают широкие возможности применения этого изящного образца современного итальянского мебельного искусства в различных стилях интерьера.</p>\r\n<p>Полубарные стулья &ndash; очень удобный вид мебели, поскольку по высоте они несколько выше обеденных (высота сидения 65 см), но ниже классических барных. Соответственно, стул Evergreen подходит и для использования у барной стойки и у обычного обеденного стола.</p>\r\n<p>При создании собственного интерьера стулья Evergreen можно комбинировать с родственной им моделью&nbsp;</p>\r\n<p><img class=\"\" src=\"https://resolute.com.ua/uploads/shop/products/additional/d1c4bcbdcae1fd6dedfa9b424302dc18.jpg\" alt=\"\" width=\"1500\" height=\"1003\" /></p>', '<ul>\r\n<li><strong>Цвет</strong> венге, серый, графит, белый, бук, горчичный, голубой, белый</li>\r\n<li><strong>Материал основания (рамы)</strong> дерево</li>\r\n<li><strong>Cиденье</strong> Дерево</li>\r\n<li><strong>Габариты (ВхШхГ) см</strong> 95х44х46</li>\r\n<li><strong>Высота до сидения (см)</strong> 65</li>\r\n<li><strong>Материал</strong> Дерево</li>\r\n</ul>', 14, 0, 1, 1, 1, '', '', '', '', 5056, 0, 0, NULL, '100f90372fd8f888c69e3b823c6f77be.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 1508755336, NULL, 1, 3, '938632', 'СТУЛ ПОЛУБАРНЫЙ CONNUBIA CB/1190 SUITE', 'stul-polubarnyj-connubia-cb1190-suite', '<p>Барный уголок становится почти обязательным атрибутом современного дома. Однако, высокие барные стулья могут выглядеть громоздко в наших малогабаритных квартирах. В таком случае их можно заменить стульями промежуточной высоты &ndash; так называемые полубарные стулья. Пара очаровательных барных стульчиков CB/1190 Suite&nbsp;станет настоящим украшением Вашей кухни-studio, столовой и даже гостиной!<br />Каркас стула Suite и его спинка изготавливаются из массива натуральной древесины (бук), окрашенной в глубокий коричневый цвет (&laquo;венге&raquo;). Сидение с мягким наполнителем обито качественной искусственной кожей темно-коричневого цвета. Декор в виде разбитой на квадраты спинки (&laquo;витражный рисунок&raquo;) придает конструкции стула визуальную лёгкость и негромоздкость.<br />Высота стула Suite 99 см, ширина 45 см, глубина 49 см, высота сидения 65 см. Полубарные стулья как правило комбинируются со столами высотой 90 см.</p>', '<ul>\r\n<li><strong>Цвет</strong> Венге</li>\r\n<li><strong>Материал обивки</strong> кож зам</li>\r\n<li><strong>Материал основания (рамы)</strong> Дерево</li>\r\n<li><strong>Цвет обивки</strong> Коричневый</li>\r\n<li><strong>Габариты (ВхШхГ) см</strong> 99х45х49</li>\r\n<li><strong>Высота до сидения (см)</strong> 65</li>\r\n</ul>', 14, 1, 0, 0, 2, '', '', '', '', 3944, 4800, 0, NULL, '100f90372fd8aswehwqf888c69e3b823c6f77be.jpg', NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 1508755667, NULL, 1, 0, '90292', 'Стул школьный \"90292\" салатовый, серый', 'stul-shkolnyj-90292-salatovyj-seryj', '<p><strong>Каркас:</strong>&nbsp;металлическая труба (квадратная);<br /><strong>Сиденье и спинка:</strong>&nbsp;лакированная гнутоклеенная фанера;<br /><strong>Ростовая группа:&nbsp;</strong>№4, 5, 6 (h = 380, 420, 460 мм)<br /><strong>Цвет каркаса:</strong>&nbsp;салатовый, серый;<br /><strong>Цвет спинки и сиденья:</strong>&nbsp;натуральный.</p>', '<p><strong>Каркас:</strong>&nbsp;металлическая труба (квадратная);<br /><strong>Сиденье и спинка:</strong>&nbsp;лакированная гнутоклеенная фанера;<br /><strong>Ростовая группа:&nbsp;</strong>№4, 5, 6 (h = 380, 420, 460 мм)<br /><strong>Цвет каркаса:</strong>&nbsp;салатовый, серый;<br /><strong>Цвет спинки и сиденья:</strong>&nbsp;натуральный.</p>', 15, 0, 1, 1, 1, '', '', '', '', 309, 0, 0, NULL, '90292-Са--1-(1)-osvito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 1508755819, NULL, 1, 0, '012376', 'Стул Сильвия хром Кожзам черный', 'stul-silvija-hrom-kozhzam-chernyj', '<h1 class=\"prod-name\">Стул Сильвия хром Кожзам черный</h1>', '<h1 class=\"prod-name\">Стул Сильвия хром Кожзам черный</h1>', 16, 0, 1, 0, 2, '', '', '', '', 559, 0, 0, NULL, '012376-osvito.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 1508756119, NULL, 1, 0, '674134', 'Стеллаж для документов', 'stellazh-dlja-dokumentov', '<ul>\r\n<li><strong>Ширина</strong>&nbsp;640/740 мм.</li>\r\n<li><strong>Глубина</strong>&nbsp;320/370 мм.</li>\r\n<li><strong>Высота</strong>&nbsp;1850 мм.</li>\r\n<li><strong>Толщина</strong>&nbsp;ДСП 16 мм.</li>\r\n<li><strong>Толщина пластиковой кромки ПВХ</strong>&nbsp;0,5 мм.</li>\r\n<li><strong>Вес</strong>&nbsp;32/38 кг.</li>\r\n<li><strong>Гарантийный срок эксплуатации</strong>&nbsp;1 год.</li>\r\n</ul>', '<ul>\r\n<li><strong>Ширина</strong> 640/740 мм.</li>\r\n<li><strong>Глубина</strong> 320/370 мм.</li>\r\n<li><strong>Высота</strong> 1850 мм.</li>\r\n<li><strong>Толщина</strong> ДСП 16 мм.</li>\r\n<li><strong>Толщина пластиковой кромки ПВХ</strong> 0,5 мм.</li>\r\n<li><strong>Вес</strong> 32/38 кг.</li>\r\n<li><strong>Гарантийный срок эксплуатации</strong> 1 год.</li>\r\n</ul>', 49, 0, 0, 1, 1, '', '', '', '', 692, 0, 0, NULL, 'ШКФ5.00-246x300.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 1508756818, NULL, 1, 0, '2435762', 'Стол с тумбой левой Helios HL-300', 'stol-s-tumboj-levoj-helios-hl-300', '<p>Helios - кабинет для сильных духом и уверенных в себе людей!</p>\r\n<p>Твердость характера, выражается в каждом силуэте мебели, стабильность и размеренность линий наполняет собой рабочее пространство.</p>\r\n<p>Теплота и натуральность дерева создадут гармонию атмосферы и образуют все условия для реализации Ваших лучших идей.</p>\r\n<p><img class=\"\" src=\"https://amf.com.ua/upload/photos/b/147636816419969.jpg\" alt=\"\" width=\"800\" height=\"460\" /></p>', '<ul>\r\n<li><strong>Коллекция</strong>: Helios</li>\r\n<li><strong>Модель изделия</strong>: Стол с тумбой левой Helios HL-300</li>\r\n<li><strong>Цвет изделия</strong>: Орех американский</li>\r\n<li><strong>Ширина столешницы</strong>: 1800 мм</li>\r\n<li><strong>Глубина столешницы</strong>: 800 мм</li>\r\n<li><strong>Высота изделия</strong>: 780 мм</li>\r\n<li><strong>Ширина тумбы</strong>: 1800 мм</li>\r\n<li><strong>Глубина тумбы</strong>: 600 мм</li>\r\n<li><strong>Высота тумбы</strong>: 600 мм</li>\r\n<li><strong>Материал изделия</strong>: Пластик</li>\r\n<li><strong>Толщина материала изделия</strong>: 32 мм</li>\r\n<li><strong>Вес изделия</strong>: 150 кг</li>\r\n</ul>', 17, 1, 0, 0, 1, '', '', '', '', 32526, 35000, 0, NULL, '159390.jpg', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 1508757051, NULL, 1, 0, '357433', 'Офисный стол FLASHNIKA С-18', 'ofisnyj-stol-flashnika-s-18', '<p><strong>Стол офисный угловой&nbsp; С-18</strong>&nbsp;прекрасно совмещает&nbsp; в себе функциональность и оригинальность. Наличие выдвижных ящиков поможет Вам в хранении документации и канцелярии. На столешнице достаточно места для компьютерной техники и прочих принадлежностей.</p>\r\n<p>&nbsp;</p>', '<p><strong>Материал</strong>&nbsp;&mdash; ламинированная&nbsp; ДСП &laquo;Swiss Krono&raquo;&nbsp; 16мм, имеет высокую плотность, разрешено использовать в производстве детской мебели, торгового оборудования и др., экологическая безопасность класс Е1. Фурнитура изделия &mdash; \"DC\", \"Linken\" Кромка столешницы &mdash; противоударная,&nbsp; ПВХ, толщиной 2 мм;<br />Кромка&nbsp; корпуса &mdash; противоударная,&nbsp; ПВХ , толщиной 0,5 мм;<span id=\"clipboard\"></span></p>', 18, 0, 0, 0, 1, '', '', '', '', 1897, 0, 0, NULL, 'th_stol_s_18_54bfa5e846fc1.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 1508757319, NULL, 1, 0, '943941', 'СТУРНЭС Раздвижной стол, морилка,антик', 'sturnes-razdvizhnoj-stol-morilkaantik', '<p>Для обеспечения устойчивости через две недели после сборки еще раз затяните шурупы.<br />На 4-6 человек.</p>', '<p><strong>Размеры товара</strong></p>\r\n<ul>\r\n<li id=\"metric\" class=\"texts\"><strong>Длина: 147 см</strong></li>\r\n<li class=\"texts\"><strong>Макс длина: 204 см</strong></li>\r\n<li class=\"texts\"><strong>Ширина: 95 см</strong></li>\r\n<li class=\"texts\"><strong>Высота: 74 см</strong></li>\r\n</ul>', 70, 1, 0, 0, 1, '', '', '', '', 6800, 7000, 0, NULL, 'sturnes-razdviznoj-stol__0113311_PE265459_S4.JPG', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_comments`
--

CREATE TABLE `catalog_comments` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date` int(10) DEFAULT NULL,
  `catalog_id` int(10) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `city` varchar(64) DEFAULT NULL,
  `text` text,
  `ip` varchar(16) DEFAULT NULL,
  `rate` double(2,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_comments`
--

INSERT INTO `catalog_comments` (`id`, `created_at`, `updated_at`, `status`, `date`, `catalog_id`, `name`, `city`, `text`, `ip`, `rate`) VALUES
(1, 1453192114, NULL, 1, 1453192114, 48, 'Darya', 'Kherson', 'testttt', '178.136.229.251', 5),
(2, 1453375172, NULL, 1, 1453375172, 48, 'Darya', 'Kherson', 'testttt', '178.136.229.251', 4),
(3, 1453375419, NULL, 1, 1453375419, 48, 'Darya', 'Kherson', 'testttt', '178.136.229.251', 4),
(4, 1453378494, NULL, 1, 1453378494, 48, 'Darya', 'Kherson', 'testttt', '178.136.229.251', 5),
(5, 1453446803, NULL, 1, 1453446803, 48, 'Darya', 'Kherson', 'testttt', '178.136.229.251', 3),
(6, 1460985324, NULL, 0, 1460985324, 52, 'кк', 'кк', 'ккп  п п п', '178.136.229.251', 2),
(7, 1461879659, NULL, 0, 1461879659, 43, 'cxzcz', 'cxzczczx', 'cxzcxzcz', '78.137.5.210', 4),
(8, 1462879614, 1483518762, 0, 1462827600, 155, 'впвпфвп', '4145tfdgdf', 'отзыв 22993\r\n4', '178.136.229.251', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_images`
--

CREATE TABLE `catalog_images` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(10) NOT NULL DEFAULT '0',
  `catalog_id` int(10) NOT NULL DEFAULT '0',
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_images`
--

INSERT INTO `catalog_images` (`id`, `created_at`, `updated_at`, `sort`, `catalog_id`, `main`, `image`) VALUES
(611, 1447094678, NULL, 0, 40, 1, '31c79213d5aceed90eed954307c44416.jpg'),
(612, 1447094678, NULL, 0, 40, 0, '38ff50f13f667a0e6500555f58db75e0.jpg'),
(613, 1447094683, NULL, 0, 40, 0, 'a8939e7b09f8347786e0f3e830c3a94a.jpg'),
(614, 1447094684, NULL, 0, 40, 0, 'c4513d5088f6374a2df90dc6a7a76ac1.jpg'),
(615, 1447094778, NULL, 0, 41, 0, 'c65bc1609165bf2eaf28fb76755cfb3a.jpg'),
(616, 1447094778, NULL, 0, 41, 0, '9c20b99496ac618fd064561234fdf753.jpg'),
(617, 1447094778, 1447094785, 0, 41, 1, '3faf38fccbe35f31b603141be04317f3.jpg'),
(618, 1447094779, NULL, 0, 41, 0, '7fd7e096c6eb0e37c2ba1322f84a404f.jpg'),
(619, 1447094884, NULL, 0, 42, 0, '8c7705ee980b733f90bebc2e15637494.jpg'),
(620, 1447094884, 1447094889, 0, 42, 1, 'fe75cb26c4ae4c06af7d7fdef455fe1b.jpg'),
(621, 1447094884, NULL, 0, 42, 0, 'ec61f00800b0aeaf06758204adaa1107.jpg'),
(622, 1447094884, NULL, 0, 42, 0, '52380165b1463bad08ad2c201a010f24.jpg'),
(623, 1447095185, NULL, 0, 43, 0, '06f084f113e5b5248af77fc0afb53656.jpg'),
(624, 1447095185, 1447095192, 0, 43, 0, 'e8e63648d7a99d6ac02973c1f79fdc20.jpg'),
(625, 1447095186, NULL, 0, 43, 0, '2401a36f9678329c6a799b52845614d6.jpg'),
(626, 1447095186, 1447095197, 0, 43, 1, '21f15ca55231a0db69dad9a2f1c6d2cd.jpg'),
(627, 1447095290, NULL, 0, 44, 0, '46e3ad3bb52ad060888db5a26cf38fb0.jpg'),
(628, 1447095290, 1447095295, 0, 44, 1, '43c658475e0a09cb4fc1c63b46bdc5a9.jpg'),
(629, 1447095290, NULL, 0, 44, 0, '593c5d47b1233bf928d841e35ceda328.jpg'),
(630, 1447095291, NULL, 0, 44, 0, '104e058a23b1aeeded9aa4ba16b3613c.jpg'),
(631, 1447095375, NULL, 0, 45, 0, '7aaf1cc025af46186ef6105af07302aa.jpg'),
(632, 1447095375, NULL, 0, 45, 0, 'c6ed2a324ba41120cde5b43691e6fa1c.jpg'),
(633, 1447095375, 1453983879, 0, 45, 1, '85b47014b4b719452a06dc924426491f.jpg'),
(634, 1447095376, NULL, 0, 45, 0, '30e1b40a46d741c377846b089dc1b5d2.jpg'),
(647, 1447095935, NULL, 0, 49, 1, 'd56dbeb19c27a828e5762b1aabd956a2.jpg'),
(648, 1447095935, NULL, 0, 49, 0, '794e27e91e3827d06c9a275f2d5fdf67.jpg'),
(649, 1447095935, NULL, 0, 49, 0, '0c2a3494e44ff805f0aa0101acc731b7.jpg'),
(650, 1447095936, NULL, 0, 49, 0, '890457273def0c8f792aea62fb9e024b.jpg'),
(651, 1447096008, NULL, 0, 50, 0, '782c69989db1782087e4db39a775339d.jpg'),
(652, 1447096009, NULL, 0, 50, 0, '4fc5db935e0b553e9313f41a6039f38f.jpg'),
(653, 1447096009, 1453988344, 0, 50, 1, '147378e984f87fbc42a0b80bd52899a2.jpg'),
(654, 1447096009, NULL, 0, 50, 0, 'e5ba68f0c3d4204607f810388584dffc.jpg'),
(655, 1447096112, NULL, 0, 51, 0, '1547105c5adc86058857a80c8c0c9385.jpg'),
(656, 1447096112, NULL, 0, 51, 0, '17bd4f3fb5b9ebb4b440aa93ea501ce5.jpg'),
(657, 1447096113, 1447096118, 0, 51, 1, '528d7c85efd8247ba056e4a1c29b4bca.jpg'),
(658, 1447096113, NULL, 0, 51, 0, '9913d50a7fc4f0647673024fcc1520e4.jpg'),
(659, 1447096192, NULL, 0, 52, 1, '3e402891cdc71ca41bc7f7f09e042d4b.jpg'),
(660, 1447096192, NULL, 0, 52, 0, '15fe0291226b53ccf22139b82ea0e3b3.jpg'),
(661, 1447096192, NULL, 0, 52, 0, '5143399809e92177a6b245a13eaf1040.jpg'),
(662, 1447096193, NULL, 0, 52, 0, 'd82dfae402a5be04f1e5428418b03899.jpg'),
(663, 1447096193, NULL, 0, 52, 0, 'fc6d6e8fa2573e57820c6f9831589c70.jpg'),
(664, 1447096257, NULL, 0, 53, 0, '5570dd2844ef1b6537ef54cf8201d017.jpg'),
(665, 1447096258, NULL, 0, 53, 0, '4021ff43abd896ed381a93e3f20d2eee.jpg'),
(666, 1447096258, 1447096264, 0, 53, 1, 'ffeee2baa0eb681477b1b4ae95c92e79.jpg'),
(667, 1447096258, NULL, 0, 53, 0, '695335f5e51e38b722e7245e7e5e7418.jpg'),
(668, 1447096351, 1448356268, 1, 54, 0, '834c53733e652ec1f70cd1317ef82139.jpg'),
(669, 1447096351, 1449923786, 4, 54, 1, '3b3c8e2b8b4c89d1fbb12c3e12de9d5a.jpg'),
(670, 1447096351, NULL, 2, 54, 0, '7a6d9f600684aa0b7d8131e2788bc1b5.jpg'),
(671, 1447096352, NULL, 5, 54, 0, 'eba225f85fd9ab2132e0ba7fb68efa36.jpg'),
(672, 1447096352, 1447096358, 6, 54, 0, 'fd180a4c7b269eff9987c17be93ed57f.jpg'),
(673, 1447096440, NULL, 0, 55, 0, '535707cf9761cf5dda7b4b8cfa8412d9.jpg'),
(674, 1447096441, NULL, 0, 55, 0, 'bc45391efa8abb4ad22e6b03675b0687.jpg'),
(675, 1447096441, NULL, 0, 55, 0, '25bf84f8b8034c3ed6f12634955ef9ee.jpg'),
(676, 1447096441, 1454062939, 0, 55, 1, 'ff92a4371fdb1b55022573290fc5ad2f.jpg'),
(677, 1447096442, NULL, 0, 55, 0, '27c09b1972e05bab1a05bd8a6c644e77.jpg'),
(678, 1448355514, 1448356114, 3, 54, 0, '1eef990b83ea781b7259171943f6a5d0.jpg'),
(707, 1453982710, NULL, 0, 84, 0, '03a4bd46da830c79d48098bc30f410ec.jpg'),
(708, 1453982710, NULL, 0, 84, 0, '2c036c65e187abac51b1839ca115b7dd.jpg'),
(709, 1453982710, 1453982715, 0, 84, 1, '878a7e4e4ce127d11d0f01e49b165543.jpg'),
(710, 1453983182, NULL, 0, 56, 0, 'aabe0e6e9da54870d2b850f5bc3100a1.jpg'),
(711, 1453983184, NULL, 0, 56, 0, 'bbcf6f4835aefdfe6cab1ea8fa5c4cb5.jpg'),
(712, 1453983184, 1453983188, 0, 56, 1, '9bd9e2c0cb862c21ba8b33160fa544b7.jpg'),
(713, 1453983301, NULL, 0, 57, 1, '72db86cef6b5e1901bd0dabf72b1735e.jpg'),
(714, 1453983302, NULL, 0, 57, 0, 'cced5c99377923d74a7a88b8f0841e62.jpg'),
(715, 1453983303, NULL, 0, 57, 0, '0f8f7897e053de651ea53b5a02c00165.jpg'),
(716, 1453983415, NULL, 0, 58, 1, '46d9d8f8e3be8cb4f5049aa0b11b796c.jpg'),
(717, 1453983416, NULL, 0, 58, 0, '8b1a1e324010527b44a7fb2f371fab0f.jpg'),
(718, 1453983417, NULL, 0, 58, 0, 'da7f86fdc6b745745f2421deb46bc851.jpg'),
(719, 1453983538, NULL, 0, 59, 0, 'd49e029935ae7db636ccb2841cc077db.jpg'),
(720, 1453983539, NULL, 0, 59, 0, 'a88a4f24315ce916d1eaa9da5d723797.jpg'),
(721, 1453983540, NULL, 0, 59, 0, '72b78db192ee58dcde018a4d7f312b57.jpg'),
(722, 1453983540, 1453983544, 0, 59, 1, 'feee3908006fdc4ab715cd327d00a745.jpg'),
(723, 1453983822, NULL, 0, 60, 0, '12dc48c811c05fc2c3d0347727f914f5.jpg'),
(724, 1453983823, 1453983892, 0, 60, 1, '371f58a9be9b8faba677741ac6b12050.jpg'),
(725, 1453983824, NULL, 0, 60, 0, '2973ce21c82746f252ac45c693d6fd90.jpg'),
(726, 1453983824, NULL, 0, 60, 0, 'c88a9dffe14254ee22e1a18f1afd811f.jpg'),
(727, 1453985374, NULL, 0, 85, 1, '1e45fd1d34c616984a94829c43c9d251.jpg'),
(728, 1453985374, NULL, 0, 85, 0, '0ecedd75804257dde4437de71425ce49.jpg'),
(729, 1453985375, NULL, 0, 85, 0, '767100b541d222386b6908d7b7962c2c.jpg'),
(730, 1453985375, NULL, 0, 85, 0, 'a77ae6f3635bb1bb07c0814c32e2134c.jpg'),
(731, 1453985627, NULL, 0, 86, 1, '8de18a7355287270b3df43c05a1048c0.jpg'),
(732, 1453985628, NULL, 0, 86, 0, 'f5cead36ddfd1bb67a551d21a4a189bf.jpg'),
(733, 1453985628, NULL, 0, 86, 0, 'c302f303c96788afa04ca5f37a9f4996.jpg'),
(734, 1453985737, NULL, 0, 87, 0, '9c780810973a8c9934fd5b152e3eb678.jpg'),
(735, 1453985738, 1453985742, 0, 87, 1, '2130622858813fec5259b99f8b452e75.jpg'),
(736, 1453985738, NULL, 0, 87, 0, '5dd556a5eb29a31bfac08bd48a34231a.jpg'),
(737, 1453987094, NULL, 0, 61, 0, '7a97c8e3764b1f7f8926b61b1299963a.jpg'),
(738, 1453987095, NULL, 0, 61, 0, '048b13f12356c4c5e9e262ca6deb8ad8.jpg'),
(739, 1453987096, 1453987101, 0, 61, 1, '744f8a702159b78c379ed35dc8200eb9.jpg'),
(740, 1453987222, NULL, 0, 62, 1, '8aafb240eb2626fb201353890a8d9035.jpg'),
(741, 1453987222, NULL, 0, 62, 0, '2acd1acec7117ac8e0cd66f46bcc89fa.jpg'),
(742, 1453987223, NULL, 0, 62, 0, 'e5e1db7303758f31dcc458ba512a4f70.jpg'),
(743, 1453987340, NULL, 0, 63, 1, '69fb49c55e1dbae657920116c3121aca.jpg'),
(744, 1453987340, NULL, 0, 63, 0, '43a255e3e909090a1fac9373c2fc397e.jpg'),
(745, 1453987341, NULL, 0, 63, 0, '351457c1b5cc6b56b3ebbf7f555f008f.jpg'),
(746, 1453987425, NULL, 0, 64, 1, 'e88ca78f3d83a09ec4d9a087f7488465.jpg'),
(747, 1453987425, NULL, 0, 64, 0, '0978227f164919ba8f133902efc80082.jpg'),
(748, 1453987426, NULL, 0, 64, 0, 'b644bb174c4ae4927f95ada221fbc89f.jpg'),
(749, 1453987538, NULL, 0, 65, 1, '343f2ed7a53c89842f76680def0ff912.jpg'),
(750, 1453987539, NULL, 0, 65, 0, '57e65d5a9b2fa85373eb495875815dce.jpg'),
(751, 1453987539, NULL, 0, 65, 0, 'e30a0bdda65f94b6b1a783cf3836032e.jpg'),
(752, 1453987606, 1453987619, 0, 66, 1, '4000f9750e94224c62a0c5a4e155a753.jpg'),
(753, 1453987608, 1453987613, 0, 66, 0, '342aa1e8a73c09e0eea9567c78513f98.jpg'),
(754, 1453987608, NULL, 0, 66, 0, 'af7d547f0e53fee0e3cf9315d6120633.jpg'),
(755, 1453988011, NULL, 0, 67, 1, 'cf92f6be7ef042db8506fd217337ec04.jpg'),
(756, 1453988012, NULL, 0, 67, 0, '1c955d65100250aa23c32b1251ae4172.jpg'),
(757, 1453988012, NULL, 0, 67, 0, 'ef590a4c74997d465572a9992b8e9d0e.jpg'),
(758, 1453988013, NULL, 0, 67, 0, '51abdf80e96777fe3e2942af40be5302.jpg'),
(759, 1453988030, NULL, 0, 68, 1, '9b2bd4df4c45164ff451721eec1393a4.jpg'),
(760, 1453988031, NULL, 0, 68, 0, 'c1998bb0dbfe40e6f67bc32950926ac8.jpg'),
(761, 1453988031, NULL, 0, 68, 0, '0f996bb3bf77a488209574a4778316ce.jpg'),
(762, 1453988044, NULL, 0, 69, 0, 'a8fe67dce288d4a18c8bf978c45343a0.jpg'),
(763, 1453988044, 1453988048, 0, 69, 1, 'af5e3e1d7e0cefb9d417502078673523.jpg'),
(764, 1453988045, NULL, 0, 69, 0, 'd01d39d9d4ce8d4a4cda06c13332fe75.jpg'),
(765, 1453988162, NULL, 0, 88, 1, '650e1c2c39fcd2db0fe5552ce0a96813.jpg'),
(766, 1453988163, NULL, 0, 88, 0, '3be3d76ed2062c15a1cc2f205d0d0a04.jpg'),
(767, 1453988163, NULL, 0, 88, 0, '5135307d552ab87e7572ab44441a7f17.jpg'),
(768, 1453988164, NULL, 0, 88, 0, '712a656d0502072dad0aba167bd7854c.jpg'),
(799, 1454003730, NULL, 0, 72, 1, '0e6af6118a01535c6d6b8cb9b9b1cc95.jpg'),
(800, 1454003730, NULL, 0, 72, 0, '694f6b9c879a24b47533cc84694595bf.jpg'),
(801, 1454003730, NULL, 0, 72, 0, '2a04ccf5c609406ec5b421f24f12ee6b.jpg'),
(802, 1454004414, NULL, 0, 71, 0, '220ae0756f73e333b0ca2ae962d00890.jpg'),
(803, 1454004415, NULL, 0, 71, 0, '9515a9318697032022a1c5f1e5f1a217.jpg'),
(804, 1454004415, 1454004421, 0, 71, 1, '99d4ab2baa6efb497393b8caa546b7d2.jpg'),
(805, 1454004532, NULL, 0, 70, 1, '0d8ad8265bc795e5f0b1e46f1872f01d.jpg'),
(806, 1454004533, NULL, 0, 70, 0, '83a111408b7fd7dc7278f858d89c978d.jpg'),
(807, 1454004641, NULL, 0, 89, 1, 'e16e898eaab10b2a8288cc73e4da458c.jpg'),
(808, 1454004641, NULL, 0, 89, 0, 'd72dab98811c694f9cebe61a7fec7b9b.jpg'),
(809, 1454004641, NULL, 0, 89, 0, '1237b20593ad76cb11ac52076f5ffc31.jpg'),
(810, 1454004641, NULL, 0, 89, 0, '759f19c66c4c6e2f3300360c08fa4dff.jpg'),
(811, 1454004739, NULL, 0, 90, 0, 'd6b65ca03dd2c7d5f368e31da0b76bb1.jpg'),
(812, 1454004739, 1454004744, 0, 90, 1, '996c9737bb19cec0cdf8545920a35024.jpg'),
(813, 1454004739, NULL, 0, 90, 0, '1940a37c8014ada7474000a6564813c7.jpg'),
(814, 1454004919, NULL, 0, 91, 0, '4216eeaa83749e2ea84ef3fac9b41b4a.jpg'),
(815, 1454004919, NULL, 0, 91, 0, 'b23d2cc98743135d288b59a2c6d9fcd7.jpg'),
(816, 1454004919, 1454004923, 0, 91, 1, '1a89bd77c02b30bb960a2758628ff129.jpg'),
(817, 1454004919, NULL, 0, 91, 0, 'f1029157d3165c37e5b0e1f2aa15955a.jpg'),
(818, 1454004920, NULL, 0, 91, 0, '1356f04ebf05b76e1a9891b3d995bb71.jpg'),
(819, 1454005068, NULL, 0, 92, 1, '27386468296175ab291d15df6925661d.jpg'),
(820, 1454005069, NULL, 0, 92, 0, '3bdbda6f6678aba8ac4ab48fc9396276.jpg'),
(821, 1454005351, NULL, 0, 93, 0, 'ca3d750b429a89dc44f2a0057ddb43fd.jpg'),
(822, 1454005351, 1454005358, 0, 93, 1, '222d27610fe00ae41888d76765c172b5.jpg'),
(823, 1454005351, NULL, 0, 93, 0, '07aa04206a12deac7d9af34d90bbe5ad.jpg'),
(824, 1454005352, NULL, 0, 93, 0, 'df8a7f28b68d6d22a9493103d4418a45.jpg'),
(825, 1454005352, NULL, 0, 93, 0, '2ba4f0cf91c8cbf93f1c8ac2c6213560.jpg'),
(826, 1454005518, NULL, 0, 94, 0, '3115991de405f0586cca786ed9a506f3.jpg'),
(827, 1454005518, NULL, 0, 94, 0, 'e89a43be8afdeb58c1548ab6e2bbc624.jpg'),
(828, 1454005518, 1454005524, 0, 94, 1, '0123777411cf5317bb713d9afdc3c9c1.jpg'),
(829, 1454005519, NULL, 0, 94, 0, 'a60bc82fc028454e26fbccb06032cc0b.jpg'),
(830, 1454006065, NULL, 0, 74, 1, '2b58dd8bf1316ed14ede88d7fe1a9a92.jpg'),
(831, 1454006065, NULL, 0, 74, 0, '8e41132d8c00cba37625f9a136fbb60d.jpg'),
(832, 1454006066, NULL, 0, 74, 0, '9b7a69731bf43646e1cbdb2140663231.jpg'),
(833, 1454006080, NULL, 0, 75, 1, '6acb9c32679cf8b76b7d4a1138f4ea24.jpg'),
(834, 1454006080, NULL, 0, 75, 0, 'fc8cb8f57e42d9d20d840ab99309de72.jpg'),
(835, 1454006080, NULL, 0, 75, 0, 'a86e19f0861685cfee4a182b11b579b2.jpg'),
(836, 1454006103, NULL, 0, 73, 1, 'f61df09878d457ee66fc7e1fd14addbd.jpg'),
(837, 1454006103, NULL, 0, 73, 0, '100ebfa1515dae2e87a40e2215d27cd3.jpg'),
(838, 1454006103, NULL, 0, 73, 0, 'd1b157543ec2e0536e2531c8099ca05d.jpg'),
(839, 1454006176, NULL, 0, 95, 0, 'a17d7ce694cb0c2fcc7304f8c829030e.jpg'),
(840, 1454006176, NULL, 0, 95, 0, '706140e5d025c19f1d273deeaf757e7c.jpg'),
(841, 1454006176, 1454006179, 0, 95, 1, 'aedca7492b0710de73e4d3553a0b317c.jpg'),
(842, 1454006176, NULL, 0, 95, 0, '9e7f08b943a5925c2aa3937d8ead6e40.jpg'),
(843, 1454006245, NULL, 0, 96, 0, 'b575c6391044b7d0a45f041c6ead7163.jpg'),
(844, 1454006246, NULL, 0, 96, 0, 'ee4095167f127a5938daa8315ddf12ad.jpg'),
(845, 1454006246, 1454006252, 0, 96, 1, '296c2f7b532ab1905d9813cd4843d955.jpg'),
(846, 1454006246, NULL, 0, 96, 0, 'd98d23d8342ece8fe8a9a3da2b95f736.jpg'),
(847, 1454006529, NULL, 0, 97, 1, 'f733af1bc7c73d6def2e19d94fcebd15.jpg'),
(848, 1454006529, NULL, 0, 97, 0, 'ddac1fb506514965e7c818ea145c9d54.jpg'),
(849, 1454006584, NULL, 0, 98, 1, 'ce1d58dc0da917077043c1edf6de746a.jpg'),
(850, 1454006585, NULL, 0, 98, 0, 'f4005f006cd1b560458a5e56ce6a000e.jpg'),
(851, 1454006586, NULL, 0, 98, 0, '34382630bd380991c61b913778c75747.jpg'),
(852, 1454006659, NULL, 0, 99, 1, 'eb148d52f4cc6c272ee88bacf2776732.jpg'),
(853, 1454006660, NULL, 0, 99, 0, '7e3939220d61f4fe145869f7ebe3ddad.jpg'),
(854, 1454058548, NULL, 0, 100, 1, '400f456673efc583825c497288dbdca7.jpg'),
(855, 1454058548, NULL, 0, 100, 0, '6430eebeec685dfd9e0377e4a4b1ca42.jpg'),
(856, 1454058548, NULL, 0, 100, 0, '69f85a02d7e557cfce2803831b8a8bbb.jpg'),
(857, 1454058725, NULL, 0, 101, 0, '91fc0afa9fde681971476b2f569a90f4.jpg'),
(858, 1454058725, NULL, 0, 101, 0, '9ac1437c3d25cd9f1172eccdd710944a.jpg'),
(859, 1454058725, NULL, 0, 101, 0, '82ad5be4bed112cc6d2d23dd72718dd7.jpg'),
(860, 1454058726, 1454058729, 0, 101, 1, 'bd1b148c4d131aaa82c04b236c01de3c.jpg'),
(861, 1454058987, NULL, 0, 102, 0, '5eb6d9d837cb2cb4f855feb94bd3c37d.jpg'),
(862, 1454058987, NULL, 0, 102, 0, '2fe2af665db7a16a74fc53b737da9da5.jpg'),
(863, 1454058987, 1454058992, 0, 102, 1, 'c75370b832affc3be9d433178f864ead.jpg'),
(864, 1454058988, NULL, 0, 102, 0, 'd531b27fef7c738e88924427a446b806.jpg'),
(865, 1454058988, NULL, 0, 102, 0, '2f92add341e2bba7a50c7bc2bc95d7e4.jpg'),
(866, 1454058988, NULL, 0, 102, 0, 'e03de269b92e127c5676a8a50a8c8945.jpg'),
(867, 1454059085, NULL, 2, 103, 0, 'ff99a21eb210baf5b737a1118a6f1732.jpg'),
(868, 1454059086, 1454059089, 1, 103, 1, '73487bc7d72b9ca7f581b1e2c87ed78d.jpg'),
(869, 1454059190, NULL, 0, 104, 0, '79716a60d4dc7b2cd2ce6f7f2944d7a8.jpg'),
(870, 1454059190, NULL, 0, 104, 0, 'ed678a262c53b7d59de4008ad8a1d61e.jpg'),
(871, 1454059190, NULL, 0, 104, 0, '681363239b8832a7b025394d7727e9e2.jpg'),
(872, 1454059191, 1454059195, 0, 104, 1, '348b0a5a662583ff1543ba0703034249.jpg'),
(873, 1454059499, NULL, 0, 105, 0, '6f1f2bda9b0c537fb2fbba398f846fb9.jpg'),
(874, 1454059500, NULL, 0, 105, 0, 'f0697a66790f884a48ea377ab78c91db.jpg'),
(875, 1454059500, 1454059504, 0, 105, 1, '3ee3635b29378b2c1b92d0ee8a8262ae.jpg'),
(876, 1454059500, NULL, 0, 105, 0, '310d75d92bff31e1b2ed9cddafd6a4f2.jpg'),
(877, 1454059805, NULL, 0, 76, 1, 'aa1d555bcf57d51b72b13f70e12d5036.png'),
(878, 1454059806, NULL, 0, 76, 0, 'f8abb9f1d2c623ce76fa99015f02ed2d.png'),
(879, 1454059807, NULL, 0, 76, 0, '2e2b87b1db1abfbb238315250efe109c.png'),
(880, 1454059932, NULL, 0, 77, 1, 'ac95a9367b31d93cf5c8f6e5bf1a34de.jpg'),
(881, 1454059933, NULL, 0, 77, 0, '29dcefd737992e68577493f2fc25bd1b.jpg'),
(882, 1454059933, NULL, 0, 77, 0, 'a084f9c45351e90ae273078e2c9ad7ae.jpg'),
(883, 1454059933, NULL, 0, 77, 0, 'c034e66061a0ea06df768ef31a715136.jpg'),
(884, 1454059934, NULL, 0, 77, 0, '23ac4dc4e934985790049d92413f3f1f.jpg'),
(885, 1454060055, NULL, 0, 78, 1, 'f30fe5c63fd4f0f9e382e275606aa7e5.jpg'),
(886, 1454060055, NULL, 0, 78, 0, '2177dcf84396e89a84ec03c9ca7fe947.jpg'),
(887, 1454060055, NULL, 0, 78, 0, '82e7948122a5339eafdf39c1984f0c4e.jpg'),
(888, 1454060055, NULL, 0, 78, 0, '9e00e5d1fd378ffa2d33377c098035b1.jpg'),
(889, 1454060290, NULL, 0, 46, 1, '6891ab8f29e5eeded563f514d42279f4.jpg'),
(890, 1454060291, NULL, 0, 46, 0, '6f792362ea19c0293b3f8964e8e2744e.jpg'),
(891, 1454060291, NULL, 0, 46, 0, '2ca9b31103dd3841c9d2ed6620748b9e.jpg'),
(892, 1454060427, NULL, 0, 47, 1, '4454777def4b6cf80459a15566e23770.jpg'),
(893, 1454060428, NULL, 0, 47, 0, '658a009a62674b13832e088c81fc3c56.jpg'),
(894, 1454060519, NULL, 0, 48, 1, 'c2c9d0b441f241040ff329880f7840e5.jpg'),
(895, 1454060520, NULL, 0, 48, 0, '72acee67ac7d096953ed7e80e9c19091.jpg'),
(896, 1454060641, NULL, 0, 106, 0, 'c42d9959125e80eb70759301e84a2f87.jpg'),
(897, 1454060641, 1454060646, 0, 106, 1, 'bcedcb826fce171cb73c27dcf27e2b85.jpg'),
(898, 1454060641, 1454060645, 0, 106, 0, 'fde09d1c8dde81cd1a97b982939d7831.jpg'),
(899, 1454060765, NULL, 0, 107, 0, 'b8dd61fda4136456a950e3588db2725d.jpg'),
(900, 1454060766, NULL, 0, 107, 0, 'c35cdae2bdd3bcd425326c3d14d0bb97.jpg'),
(901, 1454060766, 1454060773, 0, 107, 1, '1e2e2f992f6e9d1aad557278cd9d56b7.jpg'),
(902, 1454060896, NULL, 0, 108, 0, '9c08b55f586bed4b24c4db5f3940face.jpg'),
(903, 1454060896, NULL, 0, 108, 0, '5114f3b8896e1cd6f8589c06ee2b82f4.jpg'),
(904, 1454060897, 1454060902, 0, 108, 1, 'a4211640da4bb4ad88bf464938bdda63.jpg'),
(905, 1454060897, NULL, 0, 108, 0, 'c3f02f658d19e6277079eba3c830dea5.jpg'),
(906, 1454061009, NULL, 0, 109, 0, '9c62729b8903346f47fbadec2a603d54.jpg'),
(907, 1454061010, 1454061016, 0, 109, 0, 'cbe0b1362992f9a611732fa3c628a283.jpg'),
(908, 1454061010, 1454061017, 0, 109, 1, '40d2333010aa10e1f683eaee65ffeb1f.jpg'),
(909, 1454061010, NULL, 0, 109, 0, '7cf2bb2259fc83105dd54b5f45164e7c.jpg'),
(910, 1454061480, NULL, 0, 82, 0, 'c137b3097e8e27c8b37ddb50ba4f5d60.jpg'),
(911, 1454061486, 1454061492, 0, 82, 1, '81734926419a4401f8322ece3a5fe691.jpg'),
(912, 1454061488, NULL, 0, 82, 0, 'de6139486400f05812ff930e070a7219.jpg'),
(913, 1454061664, NULL, 0, 83, 0, '45b1ee668c6b32211024e653592d6eab.jpg'),
(914, 1454061664, NULL, 0, 83, 0, 'd4a752509012e86a1eba61ddbcd56ac2.jpg'),
(915, 1454061664, 1454061668, 0, 83, 1, '06020e5f548b14bbe0d3cddd1ddb9609.jpg'),
(916, 1454061665, NULL, 0, 83, 0, '7f7308b85cf8860bf87688003c341c4d.jpg'),
(917, 1454061805, NULL, 0, 110, 1, '8243a32288f42683b19c2e6246d32434.jpg'),
(918, 1454061805, NULL, 0, 110, 0, '5e8803702804b5191526f617a9872bfc.jpg'),
(919, 1454061805, NULL, 0, 110, 0, 'cf13c4865c9d525707be1380fa12fee9.jpg'),
(920, 1454061918, NULL, 0, 111, 0, '3bc233bb01df0372a9bfec43bc40808b.jpg'),
(921, 1454061919, 1454061923, 0, 111, 1, '2bc9719c017326e92508f46b404f342f.jpg'),
(922, 1454061920, NULL, 0, 111, 0, 'cc18d065868523ddbaecfea60658a927.jpg'),
(923, 1454061920, NULL, 0, 111, 0, '14ad7b75a49aaa94e79498940e95c46e.jpg'),
(924, 1454061920, NULL, 0, 111, 0, 'a73c11aa2c911000dad0adbe67389636.jpg'),
(925, 1454062059, NULL, 0, 112, 0, 'e3331e4ecff91a020e6faf5855f6c106.jpg'),
(926, 1454062059, 1454062062, 0, 112, 1, '82a09f8eae1e2e17f6f4d38a116fe1f9.jpg'),
(927, 1454062059, NULL, 0, 112, 0, 'c7b1f5eede7218e7ce3928ce75bebf80.jpg'),
(928, 1454062149, NULL, 0, 113, 0, '862bbc95fd9d5931cc45224a5594912b.jpg'),
(929, 1454062150, NULL, 0, 113, 0, 'c04388b2a78d26f58494f64bb85a20a2.jpg'),
(930, 1454062150, 1454062155, 0, 113, 1, '027e8a2609c1af3e188c8638f016fe97.jpg'),
(931, 1454062150, NULL, 0, 113, 0, '3decac2f8a418b001541276a997e4449.jpg'),
(932, 1454062267, NULL, 0, 114, 0, '39a508af70aec16c9ecdbd65332851f6.jpg'),
(933, 1454062268, NULL, 0, 114, 0, '18de15e953baa59ad1f7c1b346816baf.jpg'),
(934, 1454062268, NULL, 0, 114, 0, '3690cf2b90c165992677c2d3d61dbad0.jpg'),
(935, 1454062268, NULL, 0, 114, 0, 'b55851bfba00ea6741ca568034728662.jpg'),
(936, 1454062268, 1454062272, 0, 114, 1, '992e36604939e00adee689ee475add0a.jpg'),
(937, 1454062268, NULL, 0, 114, 0, '6a7dc3478f86309394a89873b4d743ea.jpg'),
(938, 1454062618, NULL, 0, 79, 1, '9286138925af6f01592d369df039cffc.jpg'),
(939, 1454062618, NULL, 0, 79, 0, '7ab297364a9518ea16a6b030e9ab7b8a.jpg'),
(940, 1454062618, NULL, 0, 79, 0, '19140df14a0a5ceb42b42f80764fc355.jpg'),
(941, 1454062618, NULL, 0, 79, 0, '9c4ecd02c5ed6b28563d844ca9b07c9b.jpg'),
(942, 1454062773, NULL, 0, 80, 0, '0c9f7afa3eb6e0f2a3d65e471d5da307.jpg'),
(943, 1454062774, NULL, 0, 80, 0, '9b2f1cf3b00966ff3111dbc7b4021fe6.jpg'),
(944, 1454062774, NULL, 0, 80, 0, '50a6b10f77b582361cbdfb1496daf1a0.jpg'),
(945, 1454062774, 1454062778, 0, 80, 1, '27a7915a23137d61f99bc2a20df0dc61.jpg'),
(946, 1454062891, NULL, 0, 81, 1, '3e7b9e0e1683ca0fe9a851c4c3051520.jpg'),
(947, 1454062891, NULL, 0, 81, 0, 'e3a6bd35af72c12bc71ee2a54a1a3de1.jpg'),
(948, 1454062891, NULL, 0, 81, 0, 'dd3352263b67f9a846b282d93e050701.jpg'),
(949, 1454063107, NULL, 0, 115, 0, '250a8fe994e364a3773d7a37c57c9985.jpg'),
(950, 1454063107, 1454063112, 0, 115, 1, '5bb962a05bf5a36940a77ba77e496ac6.jpg'),
(951, 1454063107, NULL, 0, 115, 0, '672a7e3dda7c1c8d497aab89cfbcb35c.jpg'),
(952, 1454063263, NULL, 0, 116, 0, 'ca3de76337a3902847896ed0a1b06d64.jpg'),
(953, 1454063264, 1454063268, 0, 116, 1, 'bf2ae8500081b45c16b214f51e50d39e.jpg'),
(954, 1454063264, NULL, 0, 116, 0, '486702ea00e43dd359416babcc76d2da.jpg'),
(955, 1454063264, NULL, 0, 116, 0, '3e15bc76d50ac4b91791d5decf6eeda7.jpg'),
(956, 1454063363, NULL, 0, 117, 0, 'f013ce4cf791d03679469aa718f6f849.jpg'),
(957, 1454063363, NULL, 0, 117, 0, '1178795b54c1a91f9daef568e5bbc4ec.jpg'),
(958, 1454063363, 1454063367, 0, 117, 1, '3d087c18ee28a88fc99dc78933d842a1.jpg'),
(959, 1454063363, NULL, 0, 117, 0, '5642ce231fadd8fdc28d4d0a3d0b3eea.jpg'),
(960, 1454063483, NULL, 0, 118, 0, 'bf00cbf5b566f593ae21362a13b0305a.jpg'),
(961, 1454063483, 1454063486, 0, 118, 1, '69efd778c57c7f5e8561eec011853107.jpg'),
(962, 1454063483, NULL, 0, 118, 0, '8eae61a75de204bd069f97771d9ce798.jpg'),
(963, 1454063563, NULL, 0, 119, 0, 'f37787bc2ccff45d2112176b593251a0.jpg'),
(964, 1454063564, 1454063568, 0, 119, 1, '92f3b935559751b2265a53c2fecf62ef.jpg'),
(965, 1454063564, NULL, 0, 119, 0, '5e2b688fc6f3264f69f0a35dfce03bcf.jpg'),
(966, 1454319171, NULL, 0, 120, 1, 'c0fec459280ee93b12b4e4a711705309.jpg'),
(967, 1454319171, NULL, 0, 120, 0, 'ff807ce787c61c6f1986c3673f181e97.jpg'),
(968, 1454319171, NULL, 0, 120, 0, 'b71ca9198c831d5bbb7aa7313f96432b.jpg'),
(969, 1454319242, NULL, 0, 121, 0, '1015f5cb50794306d2b4125ca59f5584.jpg'),
(970, 1454319243, 1454319246, 0, 121, 0, '2a9e943fa03374c77eea2d57ab7bea8b.jpg'),
(971, 1454319243, 1454319248, 0, 121, 1, '971c2e8906aa445de06e2a0bf33f0b87.jpg'),
(972, 1454319243, NULL, 0, 121, 0, 'f9a8f692a43aa33d4a34398966ec8efb.jpg'),
(973, 1454319244, NULL, 0, 121, 0, 'ae75b887c7022d28ae4c68fd3a05c566.jpg'),
(974, 1454319314, NULL, 0, 122, 0, 'e189a03cc2b6174e231a98d96aece6d0.jpg'),
(975, 1454319314, 1454319318, 0, 122, 1, '8c7b7cecca6ff092982454f7334ab879.jpg'),
(976, 1454319315, NULL, 0, 122, 0, '4ccf2c4bb581f46f9e536c824fb1456f.jpg'),
(977, 1454319365, NULL, 0, 123, 0, '0c6d6feef5e9c20657a192dd10236dbe.jpg'),
(978, 1454319366, NULL, 0, 123, 0, '5302e9694024fe5558e1146096437aae.jpg'),
(979, 1454319367, NULL, 0, 123, 0, '4238115c559a911f53334943e9227da6.jpg'),
(980, 1454319367, 1454319371, 0, 123, 1, 'a0977a205a70991491100a55f09d1368.jpg'),
(981, 1454319412, NULL, 0, 124, 0, '2b8bc8adcaef88284d0b14e634ae7a45.jpg'),
(982, 1454319412, NULL, 0, 124, 0, '48bef535720a1f08c4bce4cf9490bc6d.jpg'),
(983, 1454319412, NULL, 0, 124, 0, 'c5bda8b9fb179cdeec5c7eb74f389c08.jpg'),
(984, 1454319412, NULL, 0, 124, 0, '24d0cf99fcccb969d5aecf34116109cc.jpg'),
(985, 1454319412, NULL, 0, 124, 0, '81f02fbc87f9c57d1e5249ca5fb5f2e4.jpg'),
(986, 1454319412, 1454319415, 0, 124, 1, '00538151c9f5b0c2548a2dd7aff6339e.jpg'),
(987, 1454319856, NULL, 0, 125, 1, '59d38fdc0ca405a5b33d3c7841fd842b.jpg'),
(988, 1454319857, NULL, 0, 125, 0, 'ea4af92df0545e742442fd9521557295.jpg'),
(989, 1454319857, NULL, 0, 125, 0, '8e2d0cec4b76fefc8612be3cba8f68c4.jpg'),
(990, 1454319905, NULL, 0, 126, 0, '6ce02f5777176954b1ffc94d953d65ec.jpg'),
(991, 1454319905, 1454319908, 0, 126, 1, '654581ae81effacc3a69f675c1fe3a94.jpg'),
(992, 1454319905, NULL, 0, 126, 0, 'c82f273fd9e369474092ae0a9c462962.jpg'),
(993, 1454319950, NULL, 0, 127, 0, 'dd1880d4a59d9bc70554db980450f937.jpg'),
(994, 1454319950, NULL, 0, 127, 0, '85fcae91f7f12197f0d6098462d727b3.jpg'),
(995, 1454319951, 1454319954, 0, 127, 1, '720d0d3efa105c644950af32522f4045.jpg'),
(996, 1454320009, NULL, 0, 128, 0, 'a9578d07b90f63897ee57d84785341b1.jpg'),
(997, 1454320009, 1454320016, 0, 128, 1, 'b22809828871f8e09526177f0baed2b1.jpg'),
(998, 1454320010, 1454320013, 0, 128, 0, '242d03aca626cf78fbca10d7bb14afc8.jpg'),
(999, 1454320070, NULL, 0, 129, 0, 'f414790001dee545a59e82d57c48e446.jpg'),
(1000, 1454320070, NULL, 0, 129, 0, 'ab943c7b21343b3e5269ecb6d4e2be8f.jpg'),
(1001, 1454320070, NULL, 0, 129, 0, '18b8a15810f4557c7e42175a12bf0683.jpg'),
(1002, 1454320070, 1454320074, 0, 129, 1, 'b872e6cbc18e01ce6047b418aa0b2f88.jpg'),
(1003, 1454320433, NULL, 0, 130, 0, 'e0846d35b79b97059ac928e8784759af.jpg'),
(1004, 1454320434, 1454320437, 0, 130, 1, '4c81d2327a33a5aa58e18501eccf17a5.jpg'),
(1005, 1454320492, NULL, 0, 131, 0, 'be913b0dc5694f62c5f4326f5ce17d7e.jpg'),
(1006, 1454320492, NULL, 0, 131, 0, 'c62608b28945e55efad0c9318ad97844.jpg'),
(1007, 1454320493, 1454320496, 0, 131, 1, '2e49d83ce27e526f8e7cc578406fb5a8.jpg'),
(1008, 1454320557, NULL, 0, 132, 0, 'e210d5846ba2eceec2ba942d4002a5ea.jpg'),
(1009, 1454320557, NULL, 0, 132, 0, '095b4891b67b0872d09dcfdea6cf2a62.jpg'),
(1010, 1454320557, 1454320560, 0, 132, 1, 'cb0ab2f6740708dff3d7607f41163153.jpg'),
(1011, 1454320558, NULL, 0, 132, 0, 'c59d05b415af1a10a7b35f26e84fdbeb.jpg'),
(1012, 1454320615, NULL, 0, 133, 0, '26222e1e97d9b8873897856e6938dbef.jpg'),
(1013, 1454320616, 1454320619, 0, 133, 1, '7991833d4e097829b93dd9f278b40c14.jpg'),
(1014, 1454320668, NULL, 0, 134, 0, '46906e45201ceb883ca4b3e43fb0a08b.jpg'),
(1015, 1454320668, NULL, 0, 134, 0, '6614b56452da935d34639892ad2328d5.jpg'),
(1016, 1454320668, NULL, 0, 134, 0, '890c520e810507ccd696bb43f1f699e2.jpg'),
(1017, 1454320668, 1454320672, 0, 134, 1, '857b1bc39a911a1a24ae239ee59d16e2.jpg'),
(1018, 1454322295, NULL, 0, 135, 0, '17086e50d45528d8c80a0bad0d36d003.jpg'),
(1019, 1454322295, NULL, 0, 135, 0, 'e6f10d735dc72e567c9e93d0d8339744.jpg'),
(1020, 1454322295, 1454322299, 0, 135, 1, '3bfdb6e9c6abd9d58741a9afd69a73d1.jpg'),
(1021, 1454322346, NULL, 0, 136, 0, '0b49df8cb67727af6eb26fdb795415a3.jpg'),
(1022, 1454322346, 1454322350, 0, 136, 1, '98a88f9a611f19fda6d0507bfeb84ef4.jpg'),
(1023, 1454322347, NULL, 0, 136, 0, '62b142faa5eb460446b7435d05b7dd5f.jpg'),
(1024, 1454322395, NULL, 0, 137, 0, '53ff3671332a1d6662d7097ef02447d6.jpg'),
(1025, 1454322395, 1454322399, 0, 137, 1, '2ab31caa02f13ca7fe793fc6ffde3935.jpg'),
(1026, 1454322396, NULL, 0, 137, 0, '8ea3fa1cb1e70c61d7699e32dd62543f.jpg'),
(1027, 1454322444, NULL, 0, 138, 0, '5f93fd8536213e1b493fba0c0cb1128b.jpg'),
(1028, 1454322444, NULL, 0, 138, 0, 'f82cea06efc222807a7c4c818b2c53f0.jpg'),
(1029, 1454322444, NULL, 0, 138, 0, '4f939283bcfade8414883cd63f9ed2b1.jpg'),
(1030, 1454322444, 1454322448, 0, 138, 1, 'dd84e3af3ac388c165830cf5c2a99be1.jpg'),
(1031, 1454322445, NULL, 0, 138, 0, 'c1a9e596b0f7f1be6cd0327f2f2803d8.jpg'),
(1032, 1454322445, NULL, 0, 138, 0, 'c5575242fd29e5361bec9af5654d22d4.jpg'),
(1033, 1454322495, NULL, 0, 139, 0, '7383116a90c0ffc6a3b3879afd21c9db.jpg'),
(1034, 1454322495, NULL, 0, 139, 0, 'ae1cce1c0b3063dcad4aaea5e97c4d24.jpg'),
(1035, 1454322495, NULL, 0, 139, 0, '851352032583039a485fc68b17fb9605.jpg'),
(1036, 1454322495, 1454322499, 0, 139, 1, '33d7d2b06133a49ca5fe0f732ebf5d5b.jpg'),
(1037, 1454323558, NULL, 0, 140, 1, 'ce8d895d2c71cfb9a98da843140265a7.jpg'),
(1038, 1454323558, NULL, 0, 140, 0, 'e7c130e6bc23cc2e6909dd44a2b8edd8.jpg'),
(1039, 1454323597, NULL, 0, 141, 1, 'da41ddcc27ee136176fdce29738682b2.jpg'),
(1040, 1454323597, NULL, 0, 141, 0, '40438702f208dc7a846de7fd044fdc3f.jpg'),
(1041, 1454323638, NULL, 0, 142, 1, '0677e1ad1f6541eee5747d28058c8180.jpg'),
(1042, 1454323638, NULL, 0, 142, 0, 'e9ed81d1338738690da8a66ccfe64caa.jpg'),
(1043, 1454323638, NULL, 0, 142, 0, '8d12653145ce327fd9fd529bf97ed832.jpg'),
(1044, 1454323638, NULL, 0, 142, 0, '575f35dc33c347d187b07d0ba140483c.jpg'),
(1045, 1454323690, NULL, 0, 143, 0, '326971d7fdcea2ebc896edba1289c5b8.jpg'),
(1046, 1454323691, NULL, 0, 143, 0, 'fea9c60dc43c4d63e41564e3f5195222.jpg'),
(1047, 1454323691, 1454323695, 0, 143, 1, '68d868374b6db3d1cf9bea2f72027e31.jpg'),
(1048, 1454323732, NULL, 0, 144, 0, '5c982d0f22ca28a981b1279b07071224.jpg'),
(1049, 1454323732, NULL, 0, 144, 0, 'f5912eca33cdd38c8eb926dfc3a4a515.jpg'),
(1050, 1454323733, 1454323736, 0, 144, 1, '43de9d0f84d292f30c5ac0451faff7de.jpg'),
(1051, 1454324150, NULL, 0, 145, 0, '45353260ac145a792c6473702dc3a245.jpg'),
(1052, 1454324150, NULL, 0, 145, 0, '0014bf9b863376bac33ba1cf220ef9c1.jpg'),
(1053, 1454324221, 1454324224, 0, 145, 1, '2c57611d39d0339269ecba567a749967.jpg'),
(1054, 1454324435, NULL, 0, 146, 0, 'bb0fb34187c83bc9bce342222d5e6761.jpg'),
(1055, 1454324436, NULL, 0, 146, 0, '521f766892d6953a9be16b9ba10b016c.jpg'),
(1056, 1454324436, 1454324439, 0, 146, 1, '120a2393910ce4cad4acd9300c124ca4.jpg'),
(1057, 1454324483, NULL, 0, 147, 0, 'a09d61fb3daa2187d6b0a973be323fae.jpg'),
(1058, 1454324483, NULL, 0, 147, 0, '962c84e6f34d053265bde8e9ee17cebc.jpg'),
(1059, 1454324483, NULL, 0, 147, 0, 'c61fa782753805642c5bbc08772aec33.jpg'),
(1060, 1454324483, 1454324486, 0, 147, 1, '2e70fbd4a6418b0d25f58157243ec680.jpg'),
(1061, 1454324549, NULL, 0, 148, 0, '0e21d9a8d4d8443d5121adb22c7a3c48.jpg'),
(1062, 1454324550, 1454324554, 0, 148, 1, '7e6ed9ff208f16a45ab64257ba9f3a72.jpg'),
(1063, 1454324550, NULL, 0, 148, 0, '113cdbec9b7ebb3e1bc063ca79d47584.jpg'),
(1064, 1454324595, NULL, 0, 149, 0, '9840481770e3971b1e3eb0602e4ef8fa.jpg'),
(1065, 1454324596, 1454324599, 0, 149, 1, '8b80482102762d94fb58b6db7e57550c.jpg'),
(1066, 1454324596, NULL, 0, 149, 0, 'c90cb4ce6c0405c147528e3f54e05a15.jpg'),
(1067, 1454324992, NULL, 0, 150, 1, 'f7202f3d2445ba1f4fa2839437e2b9b9.jpg'),
(1068, 1454324992, NULL, 0, 150, 0, 'a11e493b6e6499bed3eefea74fa7b8eb.jpg'),
(1069, 1454324992, NULL, 0, 150, 0, '9b1c229f467cf4fd476137d50ab7621f.jpg'),
(1070, 1454324992, NULL, 0, 150, 0, 'f77d7fcd5276da5d38b69dd2ecc946ef.jpg'),
(1071, 1454325041, NULL, 0, 151, 0, 'b13b198a8f241222e11d3dbf1f39945d.jpg'),
(1072, 1454325041, 1454325045, 0, 151, 1, '07ee6659e7a415e9d34f6a93b540908e.jpg'),
(1073, 1454325041, NULL, 0, 151, 0, '3aa351779f28a84504dff5cbc609d13a.jpg'),
(1074, 1454325042, NULL, 0, 151, 0, 'a8b5fd53b719c7d91db04e164bacfc09.jpg'),
(1075, 1454325090, NULL, 0, 152, 0, 'f51fa7d3e0c760a5261a21d55b9043bb.jpg'),
(1076, 1454325090, 1454325095, 0, 152, 1, '0a4eff388b67a57a083f654ac8149b0b.jpg'),
(1077, 1454325091, NULL, 0, 152, 0, '76617532a08b25cc659055fac6e55cb9.jpg'),
(1078, 1454325091, NULL, 0, 152, 0, 'd9ddcf29ba1a7f649373f5b979c58301.jpg'),
(1079, 1454325146, NULL, 0, 153, 0, 'd871aa2b96326c15066e08b700517ed3.jpg'),
(1080, 1454325146, 1454325149, 0, 153, 1, '698e3ecaee23d4e5f2f1530d0c240ad8.jpg'),
(1081, 1454325146, NULL, 0, 153, 0, '73bfd89a892a80532ca90638490b6310.jpg'),
(1082, 1454325188, 1455271808, 0, 154, 1, 'a1263630a87a89b5226af70435ba8254.jpg'),
(1083, 1454325188, 1455271807, 0, 154, 0, 'acbc57eebac7833ff4b8b9922cccfa83.jpg'),
(1084, 1454325188, 1454325191, 0, 154, 0, '0ab43126cd4348da5a5c2cf762f680e8.jpg'),
(1085, 1455806196, NULL, 0, 154, 0, 'ec8c6a0a41584b9206686503bbf15a4b.jpg'),
(1086, 1462879188, NULL, 0, 155, 1, '8d2ec33b46ebc352b67098759eebe5fc.jpg'),
(1087, 1462879776, NULL, 0, 156, 1, 'eab6d6b4851e790b52d71813e8537b8c.jpg'),
(1088, 1463996837, NULL, 0, 157, 1, '0cdf3a8e39c4cfb545c4e0da3a719c6f.jpg'),
(1089, 1464162034, NULL, 0, 156, 0, 'd55ccad693f675098834733e4ff31e57.JPG'),
(1090, 1464612672, NULL, 0, 158, 1, '876558828e8bf44fd34c1638e82bfdb4.jpg'),
(1091, 1464684600, NULL, 0, 161, 1, 'c6121f9633e13ce34552ba96c741d096.jpg'),
(1092, 1464685042, NULL, 0, 160, 1, '1b22ffdbad1330328b791e4a253e6d97.jpg'),
(1093, 1464685157, NULL, 0, 162, 1, 'ae7cd5e751f1f6c5305e881d8a38622d.jpg'),
(1094, 1465035038, NULL, 0, 163, 1, 'db6bf874a183ef6709366777e38b5f8b.jpg'),
(1095, 1485243789, NULL, 0, 163, 0, '78913ab853a7861d461bd1cb2660244f.jpg'),
(1096, 1485346633, NULL, 0, 48, 0, 'a051147ac0abb55ecf2fb0573ca1d767.jpg'),
(1097, 1492005412, NULL, 0, 163, 0, '70097c952dff895f9ed113ff205ed87d.jpg'),
(1098, 1492005432, NULL, 0, 163, 0, '2b26bbd625f28853c8c72e539dc56108.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_questions`
--

CREATE TABLE `catalog_questions` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `text` text,
  `catalog_id` int(10) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_questions`
--

INSERT INTO `catalog_questions` (`id`, `created_at`, `updated_at`, `status`, `name`, `email`, `text`, `catalog_id`, `ip`) VALUES
(1, 1453192288, NULL, 0, 'Darya', 'test.wezom@mail.ru', 'test question', 44, '178.136.229.251'),
(2, 1453375179, NULL, 0, 'Darya', 'test.wezom@mail.ru', 'test question', 44, '178.136.229.251'),
(3, 1453375179, NULL, 0, 'Darya', 'test.wezom@mail.ru', 'test question', 44, '178.136.229.251'),
(4, 1453375425, NULL, 0, 'Darya', 'test.wezom@mail.ru', 'test question', 44, '178.136.229.251'),
(5, 1453446848, NULL, 0, 'Darya', 'test.wezom@mail.ru', 'test question', 44, '178.136.229.251'),
(6, 1461879707, 1483518738, 0, 'gfgfgd', 'gfdgfdg@mail.ru', 'gfgdg', 43, '78.137.5.210');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_related`
--

CREATE TABLE `catalog_related` (
  `id` int(10) NOT NULL,
  `who_id` int(10) NOT NULL,
  `with_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_related`
--

INSERT INTO `catalog_related` (`id`, `who_id`, `with_id`) VALUES
(41, 41, 40),
(42, 42, 40),
(43, 42, 41),
(44, 54, 40),
(45, 54, 46),
(46, 54, 48),
(47, 154, 57),
(48, 154, 59),
(49, 154, 84),
(50, 154, 60),
(51, 154, 56),
(52, 154, 45),
(53, 154, 58),
(54, 154, 85),
(55, 154, 86),
(56, 154, 87),
(57, 154, 120),
(58, 154, 124),
(59, 154, 123),
(60, 154, 121),
(61, 154, 122);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_specifications_values`
--

CREATE TABLE `catalog_specifications_values` (
  `id` int(11) NOT NULL,
  `catalog_id` int(10) NOT NULL,
  `specification_value_alias` varchar(255) NOT NULL,
  `specification_alias` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_specifications_values`
--

INSERT INTO `catalog_specifications_values` (`id`, `catalog_id`, `specification_value_alias`, `specification_alias`) VALUES
(822, 81, 'kozha', 'material'),
(823, 81, 'nejlon', 'material'),
(824, 81, 'nubuk', 'material'),
(825, 81, 'polimer', 'material'),
(826, 81, 'uniseks', 'sex'),
(827, 81, 'kitaj', 'proizvodstvo'),
(828, 81, 'vesna', 'sezon'),
(829, 81, 'leto', 'sezon'),
(830, 81, 'osen', 'sezon'),
(831, 81, 'chernyj', 'tsvet'),
(1304, 60, 'lateks', 'material'),
(1305, 60, 'nejlon', 'material'),
(1306, 60, 'zhenskij', 'sex'),
(1307, 60, 'italija', 'proizvodstvo'),
(1308, 60, 'vesna', 'sezon'),
(1309, 60, 'zima', 'sezon'),
(1310, 60, 'leto', 'sezon'),
(1311, 60, 'osen', 'sezon'),
(1320, 56, 'lateks', 'material'),
(1321, 56, 'nejlon', 'material'),
(1322, 56, 'muzhskoj', 'sex'),
(1323, 56, 'italija', 'proizvodstvo'),
(1324, 56, 'vesna', 'sezon'),
(1325, 56, 'osen', 'sezon'),
(1326, 56, 'sinij', 'tsvet'),
(1327, 45, 'nubuk', 'material'),
(1328, 45, 'poliestr', 'material'),
(1329, 45, 'tekstil', 'material'),
(1330, 45, 'muzhskoj', 'sex'),
(1331, 45, 'kitaj', 'proizvodstvo'),
(1332, 45, 'vesna', 'sezon'),
(1333, 45, 'zima', 'sezon'),
(1334, 45, 'osen', 'sezon'),
(1335, 45, 'korichnevyj', 'tsvet'),
(1336, 84, 'hlopok', 'material'),
(1337, 84, 'zhenskij', 'sex'),
(1338, 84, 'italija', 'proizvodstvo'),
(1339, 84, 'vesna', 'sezon'),
(1340, 84, 'zima', 'sezon'),
(1341, 84, 'leto', 'sezon'),
(1342, 84, 'osen', 'sezon'),
(1343, 84, 'sinij', 'tsvet'),
(1344, 87, 'poliestr', 'material'),
(1345, 87, 'hlopok', 'material'),
(1346, 87, 'zhenskij', 'sex'),
(1347, 87, 'vesna', 'sezon'),
(1348, 87, 'zima', 'sezon'),
(1349, 87, 'leto', 'sezon'),
(1350, 87, 'osen', 'sezon'),
(1351, 87, 'seryj', 'tsvet'),
(1352, 86, 'poliestr', 'material'),
(1353, 86, 'hlopok', 'material'),
(1354, 86, 'zhenskij', 'sex'),
(1355, 86, 'polsha', 'proizvodstvo'),
(1356, 86, 'vesna', 'sezon'),
(1357, 86, 'zima', 'sezon'),
(1358, 86, 'leto', 'sezon'),
(1359, 86, 'osen', 'sezon'),
(1412, 61, 'lateks', 'material'),
(1413, 61, 'polimer', 'material'),
(1414, 61, 'hlopok', 'material'),
(1415, 61, 'muzhskoj', 'sex'),
(1416, 61, 'kitaj', 'proizvodstvo'),
(1417, 61, 'vesna', 'sezon'),
(1418, 61, 'zima', 'sezon'),
(1419, 61, 'leto', 'sezon'),
(1420, 61, 'osen', 'sezon'),
(1421, 61, 'white', 'tsvet'),
(1422, 43, 'poliestr', 'material'),
(1423, 43, 'polimer', 'material'),
(1424, 43, 'sintetika', 'material'),
(1425, 43, 'muzhskoj', 'sex'),
(1426, 43, 'ukraina', 'proizvodstvo'),
(1427, 43, 'vesna', 'sezon'),
(1428, 43, 'leto', 'sezon'),
(1429, 43, 'osen', 'sezon'),
(1430, 43, 'chernyj', 'tsvet'),
(1431, 62, 'nubuk', 'material'),
(1432, 62, 'silikon', 'material'),
(1433, 62, 'tekstil', 'material'),
(1434, 62, 'muzhskoj', 'sex'),
(1435, 62, 'italija', 'proizvodstvo'),
(1436, 62, 'vesna', 'sezon'),
(1437, 62, 'zima', 'sezon'),
(1438, 62, 'leto', 'sezon'),
(1439, 62, 'osen', 'sezon'),
(1440, 62, 'sinij', 'tsvet'),
(1441, 44, 'lateks', 'material'),
(1442, 44, 'nubuk', 'material'),
(1443, 44, 'hlopok', 'material'),
(1444, 44, 'zhenskij', 'sex'),
(1445, 44, 'indonezija', 'proizvodstvo'),
(1446, 44, 'leto', 'sezon'),
(1447, 44, 'white', 'tsvet'),
(1448, 63, 'nejlon', 'material'),
(1449, 63, 'poliestr', 'material'),
(1450, 63, 'silikon', 'material'),
(1451, 63, 'muzhskoj', 'sex'),
(1452, 63, 'ssha', 'proizvodstvo'),
(1453, 63, 'vesna', 'sezon'),
(1454, 63, 'zima', 'sezon'),
(1455, 63, 'leto', 'sezon'),
(1456, 63, 'osen', 'sezon'),
(1457, 63, 'seryj', 'tsvet'),
(1458, 64, 'nejlon', 'material'),
(1459, 64, 'poliestr', 'material'),
(1460, 64, 'polimer', 'material'),
(1461, 64, 'uniseks', 'sex'),
(1462, 64, 'indonezija', 'proizvodstvo'),
(1463, 64, 'vesna', 'sezon'),
(1464, 64, 'zima', 'sezon'),
(1465, 64, 'leto', 'sezon'),
(1466, 64, 'osen', 'sezon'),
(1467, 64, 'light_blue', 'tsvet'),
(1468, 65, 'iskusstvennajakozha', 'material'),
(1469, 65, 'nejlon', 'material'),
(1470, 65, 'polimer', 'material'),
(1471, 65, 'tekstil', 'material'),
(1472, 65, 'uniseks', 'sex'),
(1473, 65, 'polsha', 'proizvodstvo'),
(1474, 65, 'vesna', 'sezon'),
(1475, 65, 'zima', 'sezon'),
(1476, 65, 'leto', 'sezon'),
(1477, 65, 'osen', 'sezon'),
(1478, 65, 'green', 'tsvet'),
(1479, 66, 'iskusstvennajakozha', 'material'),
(1480, 66, 'kozha', 'material'),
(1481, 66, 'polimer', 'material'),
(1482, 66, 'zhenskij', 'sex'),
(1483, 66, 'italija', 'proizvodstvo'),
(1484, 66, 'vesna', 'sezon'),
(1485, 66, 'zima', 'sezon'),
(1486, 66, 'leto', 'sezon'),
(1487, 66, 'osen', 'sezon'),
(1488, 66, 'light_blue', 'tsvet'),
(1489, 41, 'kozha', 'material'),
(1490, 41, 'silikon', 'material'),
(1491, 41, 'hlopok', 'material'),
(1492, 41, 'uniseks', 'sex'),
(1493, 41, 'indonezija', 'proizvodstvo'),
(1494, 41, 'zima', 'sezon'),
(1495, 41, 'sinij', 'tsvet'),
(1496, 40, 'nejlon', 'material'),
(1497, 40, 'poliestr', 'material'),
(1498, 40, 'silikon', 'material'),
(1499, 40, 'uniseks', 'sex'),
(1500, 40, 'italija', 'proizvodstvo'),
(1501, 40, 'zima', 'sezon'),
(1502, 40, 'chernyj', 'tsvet'),
(1503, 42, 'nejlon', 'material'),
(1504, 42, 'sintetika', 'material'),
(1505, 42, 'hlopok', 'material'),
(1506, 42, 'muzhskoj', 'sex'),
(1507, 42, 'indonezija', 'proizvodstvo'),
(1508, 42, 'zima', 'sezon'),
(1509, 42, 'green', 'tsvet'),
(1510, 67, 'lateks', 'material'),
(1511, 67, 'nejlon', 'material'),
(1512, 67, 'polimer', 'material'),
(1513, 67, 'silikon', 'material'),
(1514, 67, 'tekstil', 'material'),
(1515, 67, 'hlopok', 'material'),
(1516, 67, 'uniseks', 'sex'),
(1517, 67, 'vetnam', 'proizvodstvo'),
(1518, 67, 'vesna', 'sezon'),
(1519, 67, 'zima', 'sezon'),
(1520, 67, 'leto', 'sezon'),
(1521, 67, 'osen', 'sezon'),
(1522, 67, 'seryj', 'tsvet'),
(1523, 68, 'nejlon', 'material'),
(1524, 68, 'poliestr', 'material'),
(1525, 68, 'polimer', 'material'),
(1526, 68, 'sintetika', 'material'),
(1527, 68, 'tekstil', 'material'),
(1528, 68, 'hlopok', 'material'),
(1529, 68, 'zhenskij', 'sex'),
(1530, 68, 'kitaj', 'proizvodstvo'),
(1531, 68, 'vesna', 'sezon'),
(1532, 68, 'zima', 'sezon'),
(1533, 68, 'leto', 'sezon'),
(1534, 68, 'osen', 'sezon'),
(1535, 68, 'chernyj', 'tsvet'),
(1536, 69, 'zamsha', 'material'),
(1537, 69, 'kozha', 'material'),
(1538, 69, 'nejlon', 'material'),
(1539, 69, 'nubuk', 'material'),
(1540, 69, 'sintetika', 'material'),
(1541, 69, 'hlopok', 'material'),
(1542, 69, 'zhenskij', 'sex'),
(1543, 69, 'italija', 'proizvodstvo'),
(1544, 69, 'vesna', 'sezon'),
(1545, 69, 'zima', 'sezon'),
(1546, 69, 'leto', 'sezon'),
(1547, 69, 'osen', 'sezon'),
(1548, 69, 'seryj', 'tsvet'),
(1563, 50, 'zamsha', 'material'),
(1564, 50, 'muzhskoj', 'sex'),
(1565, 50, 'indonezija', 'proizvodstvo'),
(1566, 50, 'leto', 'sezon'),
(1567, 50, 'sinij', 'tsvet'),
(1637, 72, 'kozha', 'material'),
(1638, 72, 'muzhskoj', 'sex'),
(1639, 72, 'ssha', 'proizvodstvo'),
(1640, 72, 'vesna', 'sezon'),
(1641, 72, 'leto', 'sezon'),
(1642, 72, 'osen', 'sezon'),
(1643, 72, 'seryj', 'tsvet'),
(1644, 71, 'zamsha', 'material'),
(1645, 71, 'nubuk', 'material'),
(1646, 71, 'muzhskoj', 'sex'),
(1647, 71, 'italija', 'proizvodstvo'),
(1648, 71, 'vesna', 'sezon'),
(1649, 71, 'leto', 'sezon'),
(1650, 70, 'iskusstvennajazamsha', 'material'),
(1651, 70, 'muzhskoj', 'sex'),
(1652, 70, 'kitaj', 'proizvodstvo'),
(1653, 70, 'vesna', 'sezon'),
(1654, 70, 'leto', 'sezon'),
(1655, 70, 'turquoise', 'tsvet'),
(1656, 89, 'zamsha', 'material'),
(1657, 89, 'nubuk', 'material'),
(1658, 89, 'muzhskoj', 'sex'),
(1659, 89, 'indonezija', 'proizvodstvo'),
(1660, 89, 'vesna', 'sezon'),
(1661, 89, 'leto', 'sezon'),
(1662, 89, 'chernyj', 'tsvet'),
(1663, 90, 'kozha', 'material'),
(1664, 90, 'polimer', 'material'),
(1665, 90, 'uniseks', 'sex'),
(1666, 90, 'polsha', 'proizvodstvo'),
(1667, 90, 'vesna', 'sezon'),
(1668, 90, 'leto', 'sezon'),
(1669, 90, 'osen', 'sezon'),
(1670, 90, 'light_blue', 'tsvet'),
(1678, 91, 'kozha', 'material'),
(1679, 91, 'sintetika', 'material'),
(1680, 91, 'tekstil', 'material'),
(1681, 91, 'muzhskoj', 'sex'),
(1682, 91, 'vetnam', 'proizvodstvo'),
(1683, 91, 'leto', 'sezon'),
(1684, 91, 'sinij', 'tsvet'),
(1691, 92, 'zamsha', 'material'),
(1692, 92, 'muzhskoj', 'sex'),
(1693, 92, 'ukraina', 'proizvodstvo'),
(1694, 92, 'vesna', 'sezon'),
(1695, 92, 'osen', 'sezon'),
(1696, 92, 'coral', 'tsvet'),
(1704, 93, 'zamsha', 'material'),
(1705, 93, 'nubuk', 'material'),
(1706, 93, 'uniseks', 'sex'),
(1707, 93, 'italija', 'proizvodstvo'),
(1708, 93, 'vesna', 'sezon'),
(1709, 93, 'leto', 'sezon'),
(1710, 93, 'korichnevyj', 'tsvet'),
(1711, 94, 'iskusstvennajazamsha', 'material'),
(1712, 94, 'poliestr', 'material'),
(1713, 94, 'polimer', 'material'),
(1714, 94, 'muzhskoj', 'sex'),
(1715, 94, 'kitaj', 'proizvodstvo'),
(1716, 94, 'leto', 'sezon'),
(1717, 94, 'beige', 'tsvet'),
(1859, 99, 'zamsha', 'material'),
(1860, 99, 'iskusstvennajazamsha', 'material'),
(1861, 99, 'polimer', 'material'),
(1862, 99, 'sintetika', 'material'),
(1863, 99, 'muzhskoj', 'sex'),
(1864, 99, 'vetnam', 'proizvodstvo'),
(1865, 99, 'vesna', 'sezon'),
(1866, 99, 'zima', 'sezon'),
(1867, 99, 'leto', 'sezon'),
(1868, 99, 'osen', 'sezon'),
(1869, 98, 'iskusstvennajazamsha', 'material'),
(1870, 98, 'kozha', 'material'),
(1871, 98, 'nubuk', 'material'),
(1872, 98, 'muzhskoj', 'sex'),
(1873, 98, 'vesna', 'sezon'),
(1874, 98, 'zima', 'sezon'),
(1875, 98, 'leto', 'sezon'),
(1876, 98, 'osen', 'sezon'),
(1877, 97, 'kozha', 'material'),
(1878, 97, 'nejlon', 'material'),
(1879, 97, 'poliestr', 'material'),
(1880, 97, 'muzhskoj', 'sex'),
(1881, 97, 'ssha', 'proizvodstvo'),
(1882, 97, 'vesna', 'sezon'),
(1883, 97, 'zima', 'sezon'),
(1884, 97, 'leto', 'sezon'),
(1885, 97, 'osen', 'sezon'),
(1886, 97, 'chernyj', 'tsvet'),
(1887, 96, 'zamsha', 'material'),
(1888, 96, 'poliestr', 'material'),
(1889, 96, 'silikon', 'material'),
(1890, 96, 'zhenskij', 'sex'),
(1891, 96, 'polsha', 'proizvodstvo'),
(1892, 96, 'zima', 'sezon'),
(1893, 96, 'chernyj', 'tsvet'),
(1894, 95, 'zamsha', 'material'),
(1895, 95, 'poliestr', 'material'),
(1896, 95, 'sintetika', 'material'),
(1897, 95, 'zhenskij', 'sex'),
(1898, 95, 'italija', 'proizvodstvo'),
(1899, 95, 'leto', 'sezon'),
(1900, 95, 'sinij', 'tsvet'),
(1901, 73, 'nejlon', 'material'),
(1902, 73, 'nubuk', 'material'),
(1903, 73, 'zhenskij', 'sex'),
(1904, 73, 'italija', 'proizvodstvo'),
(1905, 73, 'vesna', 'sezon'),
(1906, 73, 'leto', 'sezon'),
(1907, 73, 'osen', 'sezon'),
(1908, 73, 'krasnyj', 'tsvet'),
(1909, 49, 'kozha', 'material'),
(1910, 49, 'muzhskoj', 'sex'),
(1911, 49, 'italija', 'proizvodstvo'),
(1912, 49, 'vesna', 'sezon'),
(1913, 49, 'leto', 'sezon'),
(1914, 49, 'osen', 'sezon'),
(1915, 49, 'chernyj', 'tsvet'),
(1916, 74, 'zamsha', 'material'),
(1917, 74, 'nubuk', 'material'),
(1918, 74, 'zhenskij', 'sex'),
(1919, 74, 'kitaj', 'proizvodstvo'),
(1920, 74, 'vesna', 'sezon'),
(1921, 74, 'zima', 'sezon'),
(1922, 74, 'leto', 'sezon'),
(1923, 74, 'osen', 'sezon'),
(1924, 74, 'seryj', 'tsvet'),
(1925, 75, 'kozha', 'material'),
(1926, 75, 'zhenskij', 'sex'),
(1927, 75, 'italija', 'proizvodstvo'),
(1928, 75, 'vesna', 'sezon'),
(1929, 75, 'zima', 'sezon'),
(1930, 75, 'leto', 'sezon'),
(1931, 75, 'osen', 'sezon'),
(1932, 75, 'beige', 'tsvet'),
(1940, 100, 'tekstil', 'material'),
(1941, 100, 'hlopok', 'material'),
(1942, 100, 'muzhskoj', 'sex'),
(1943, 100, 'indonezija', 'proizvodstvo'),
(1944, 100, 'vesna', 'sezon'),
(1945, 100, 'leto', 'sezon'),
(1946, 100, 'white', 'tsvet'),
(1959, 101, 'polimer', 'material'),
(1960, 101, 'sintetika', 'material'),
(1961, 101, 'hlopok', 'material'),
(1962, 101, 'muzhskoj', 'sex'),
(1963, 101, 'ukraina', 'proizvodstvo'),
(1964, 101, 'osen', 'sezon'),
(1973, 102, 'kozha', 'material'),
(1974, 102, 'poliestr', 'material'),
(1975, 102, 'tekstil', 'material'),
(1976, 102, 'zhenskij', 'sex'),
(1977, 102, 'vetnam', 'proizvodstvo'),
(1978, 102, 'vesna', 'sezon'),
(1979, 102, 'osen', 'sezon'),
(1980, 102, 'chernyj', 'tsvet'),
(1988, 103, 'zamsha', 'material'),
(1989, 103, 'kozha', 'material'),
(1990, 103, 'poliestr', 'material'),
(1991, 103, 'muzhskoj', 'sex'),
(1992, 103, 'italija', 'proizvodstvo'),
(1993, 103, 'vesna', 'sezon'),
(1994, 103, 'chernyj', 'tsvet'),
(2003, 104, 'iskusstvennajakozha', 'material'),
(2004, 104, 'lateks', 'material'),
(2005, 104, 'silikon', 'material'),
(2006, 104, 'muzhskoj', 'sex'),
(2007, 104, 'kitaj', 'proizvodstvo'),
(2008, 104, 'vesna', 'sezon'),
(2009, 104, 'osen', 'sezon'),
(2010, 104, 'chernyj', 'tsvet'),
(2011, 88, 'zamsha', 'material'),
(2012, 88, 'hlopok', 'material'),
(2013, 88, 'uniseks', 'sex'),
(2014, 88, 'kitaj', 'proizvodstvo'),
(2015, 88, 'zima', 'sezon'),
(2016, 88, 'osen', 'sezon'),
(2017, 88, 'sinij', 'tsvet'),
(2025, 105, 'kozha', 'material'),
(2026, 105, 'nubuk', 'material'),
(2027, 105, 'sintetika', 'material'),
(2028, 105, 'muzhskoj', 'sex'),
(2029, 105, 'polsha', 'proizvodstvo'),
(2030, 105, 'vesna', 'sezon'),
(2031, 105, 'korichnevyj', 'tsvet'),
(2174, 109, 'kozha', 'material'),
(2175, 109, 'nubuk', 'material'),
(2176, 109, 'polimer', 'material'),
(2177, 109, 'muzhskoj', 'sex'),
(2178, 109, 'vetnam', 'proizvodstvo'),
(2179, 109, 'vesna', 'sezon'),
(2180, 109, 'osen', 'sezon'),
(2181, 109, 'chernyj', 'tsvet'),
(2182, 108, 'kozha', 'material'),
(2183, 108, 'polimer', 'material'),
(2184, 108, 'uniseks', 'sex'),
(2185, 108, 'italija', 'proizvodstvo'),
(2186, 108, 'leto', 'sezon'),
(2187, 108, 'osen', 'sezon'),
(2188, 108, 'seryj', 'tsvet'),
(2189, 107, 'zamsha', 'material'),
(2190, 107, 'nejlon', 'material'),
(2191, 107, 'polimer', 'material'),
(2192, 107, 'muzhskoj', 'sex'),
(2193, 107, 'polsha', 'proizvodstvo'),
(2194, 107, 'zima', 'sezon'),
(2195, 107, 'osen', 'sezon'),
(2196, 107, 'sinij', 'tsvet'),
(2197, 106, 'lateks', 'material'),
(2198, 106, 'nejlon', 'material'),
(2199, 106, 'silikon', 'material'),
(2200, 106, 'uniseks', 'sex'),
(2201, 106, 'kitaj', 'proizvodstvo'),
(2202, 106, 'vesna', 'sezon'),
(2203, 106, 'white', 'tsvet'),
(2204, 76, 'zamsha', 'material'),
(2205, 76, 'iskusstvennajazamsha', 'material'),
(2206, 76, 'iskusstvennajakozha', 'material'),
(2207, 76, 'kozha', 'material'),
(2208, 76, 'lateks', 'material'),
(2209, 76, 'nejlon', 'material'),
(2210, 76, 'nubuk', 'material'),
(2211, 76, 'poliestr', 'material'),
(2212, 76, 'polimer', 'material'),
(2213, 76, 'silikon', 'material'),
(2214, 76, 'sintetika', 'material'),
(2215, 76, 'tekstil', 'material'),
(2216, 76, 'hlopok', 'material'),
(2217, 76, 'muzhskoj', 'sex'),
(2218, 76, 'kitaj', 'proizvodstvo'),
(2219, 76, 'vesna', 'sezon'),
(2220, 76, 'zima', 'sezon'),
(2221, 76, 'leto', 'sezon'),
(2222, 76, 'osen', 'sezon'),
(2223, 76, 'light_blue', 'tsvet'),
(2224, 46, 'zamsha', 'material'),
(2225, 46, 'iskusstvennajakozha', 'material'),
(2226, 46, 'uniseks', 'sex'),
(2227, 46, 'vetnam', 'proizvodstvo'),
(2228, 46, 'vesna', 'sezon'),
(2229, 46, 'zima', 'sezon'),
(2230, 46, 'leto', 'sezon'),
(2231, 46, 'chernyj', 'tsvet'),
(2232, 77, 'nejlon', 'material'),
(2233, 77, 'silikon', 'material'),
(2234, 77, 'tekstil', 'material'),
(2235, 77, 'uniseks', 'sex'),
(2236, 77, 'kitaj', 'proizvodstvo'),
(2237, 77, 'vesna', 'sezon'),
(2238, 77, 'zima', 'sezon'),
(2239, 77, 'leto', 'sezon'),
(2240, 77, 'osen', 'sezon'),
(2241, 77, 'white', 'tsvet'),
(2242, 47, 'kozha', 'material'),
(2243, 47, 'sintetika', 'material'),
(2244, 47, 'uniseks', 'sex'),
(2245, 47, 'ukraina', 'proizvodstvo'),
(2246, 47, 'vesna', 'sezon'),
(2247, 47, 'osen', 'sezon'),
(2248, 47, 'white', 'tsvet'),
(2249, 78, 'kozha', 'material'),
(2250, 78, 'nejlon', 'material'),
(2251, 78, 'silikon', 'material'),
(2252, 78, 'muzhskoj', 'sex'),
(2253, 78, 'polsha', 'proizvodstvo'),
(2254, 78, 'vesna', 'sezon'),
(2255, 78, 'zima', 'sezon'),
(2256, 78, 'leto', 'sezon'),
(2257, 78, 'osen', 'sezon'),
(2258, 78, 'chernyj', 'tsvet'),
(2259, 48, 'kozha', 'material'),
(2260, 48, 'uniseks', 'sex'),
(2261, 48, 'ukraina', 'proizvodstvo'),
(2262, 48, 'vesna', 'sezon'),
(2263, 48, 'osen', 'sezon'),
(2264, 48, 'green', 'tsvet'),
(2265, 51, 'nejlon', 'material'),
(2266, 51, 'nubuk', 'material'),
(2267, 51, 'poliestr', 'material'),
(2268, 51, 'polimer', 'material'),
(2269, 51, 'uniseks', 'sex'),
(2270, 51, 'ssha', 'proizvodstvo'),
(2271, 51, 'vesna', 'sezon'),
(2272, 51, 'zima', 'sezon'),
(2273, 51, 'leto', 'sezon'),
(2274, 51, 'osen', 'sezon'),
(2275, 51, 'korichnevyj', 'tsvet'),
(2276, 52, 'kozha', 'material'),
(2277, 52, 'uniseks', 'sex'),
(2278, 52, 'kitaj', 'proizvodstvo'),
(2279, 52, 'vesna', 'sezon'),
(2280, 52, 'zima', 'sezon'),
(2281, 52, 'leto', 'sezon'),
(2282, 52, 'osen', 'sezon'),
(2283, 52, 'fioletovyj', 'tsvet'),
(2284, 53, 'iskusstvennajakozha', 'material'),
(2285, 53, 'muzhskoj', 'sex'),
(2286, 53, 'italija', 'proizvodstvo'),
(2287, 53, 'vesna', 'sezon'),
(2288, 53, 'zima', 'sezon'),
(2289, 53, 'leto', 'sezon'),
(2290, 53, 'osen', 'sezon'),
(2291, 53, 'chernyj', 'tsvet'),
(2292, 82, 'iskusstvennajakozha', 'material'),
(2293, 82, 'kozha', 'material'),
(2294, 82, 'poliestr', 'material'),
(2295, 82, 'zhenskij', 'sex'),
(2296, 82, 'polsha', 'proizvodstvo'),
(2297, 82, 'vesna', 'sezon'),
(2298, 82, 'zima', 'sezon'),
(2299, 82, 'leto', 'sezon'),
(2300, 82, 'osen', 'sezon'),
(2301, 82, 'yellow', 'tsvet'),
(2302, 83, 'iskusstvennajakozha', 'material'),
(2303, 83, 'lateks', 'material'),
(2304, 83, 'polimer', 'material'),
(2305, 83, 'zhenskij', 'sex'),
(2306, 83, 'ukraina', 'proizvodstvo'),
(2307, 83, 'vesna', 'sezon'),
(2308, 83, 'zima', 'sezon'),
(2309, 83, 'leto', 'sezon'),
(2310, 83, 'osen', 'sezon'),
(2311, 83, 'white', 'tsvet'),
(2320, 110, 'kozha', 'material'),
(2321, 110, 'zhenskij', 'sex'),
(2322, 110, 'italija', 'proizvodstvo'),
(2323, 110, 'vesna', 'sezon'),
(2324, 110, 'zima', 'sezon'),
(2325, 110, 'leto', 'sezon'),
(2326, 110, 'osen', 'sezon'),
(2327, 110, 'chernyj', 'tsvet'),
(2338, 111, 'lateks', 'material'),
(2339, 111, 'nejlon', 'material'),
(2340, 111, 'nubuk', 'material'),
(2341, 111, 'zhenskij', 'sex'),
(2342, 111, 'vetnam', 'proizvodstvo'),
(2343, 111, 'vesna', 'sezon'),
(2344, 111, 'zima', 'sezon'),
(2345, 111, 'leto', 'sezon'),
(2346, 111, 'osen', 'sezon'),
(2347, 111, 'rozovyj', 'tsvet'),
(2358, 112, 'iskusstvennajakozha', 'material'),
(2359, 112, 'nejlon', 'material'),
(2360, 112, 'poliestr', 'material'),
(2361, 112, 'muzhskoj', 'sex'),
(2362, 112, 'indonezija', 'proizvodstvo'),
(2363, 112, 'vesna', 'sezon'),
(2364, 112, 'zima', 'sezon'),
(2365, 112, 'leto', 'sezon'),
(2366, 112, 'osen', 'sezon'),
(2367, 112, 'korichnevyj', 'tsvet'),
(2378, 113, 'kozha', 'material'),
(2379, 113, 'poliestr', 'material'),
(2380, 113, 'hlopok', 'material'),
(2381, 113, 'muzhskoj', 'sex'),
(2382, 113, 'kitaj', 'proizvodstvo'),
(2383, 113, 'vesna', 'sezon'),
(2384, 113, 'zima', 'sezon'),
(2385, 113, 'leto', 'sezon'),
(2386, 113, 'osen', 'sezon'),
(2387, 113, 'seryj', 'tsvet'),
(2399, 114, 'nubuk', 'material'),
(2400, 114, 'polimer', 'material'),
(2401, 114, 'sintetika', 'material'),
(2402, 114, 'tekstil', 'material'),
(2403, 114, 'muzhskoj', 'sex'),
(2404, 114, 'polsha', 'proizvodstvo'),
(2405, 114, 'vesna', 'sezon'),
(2406, 114, 'zima', 'sezon'),
(2407, 114, 'leto', 'sezon'),
(2408, 114, 'osen', 'sezon'),
(2409, 114, 'chernyj', 'tsvet'),
(2410, 54, 'polimer', 'material'),
(2411, 54, 'uniseks', 'sex'),
(2412, 54, 'vetnam', 'proizvodstvo'),
(2413, 54, 'leto', 'sezon'),
(2414, 54, 'korichnevyj', 'tsvet'),
(2420, 79, 'kozha', 'material'),
(2421, 79, 'nejlon', 'material'),
(2422, 79, 'poliestr', 'material'),
(2423, 79, 'silikon', 'material'),
(2424, 79, 'muzhskoj', 'sex'),
(2425, 79, 'italija', 'proizvodstvo'),
(2426, 79, 'vesna', 'sezon'),
(2427, 79, 'zima', 'sezon'),
(2428, 79, 'leto', 'sezon'),
(2429, 79, 'osen', 'sezon'),
(2430, 79, 'chernyj', 'tsvet'),
(2431, 80, 'lateks', 'material'),
(2432, 80, 'nubuk', 'material'),
(2433, 80, 'sintetika', 'material'),
(2434, 80, 'zhenskij', 'sex'),
(2435, 80, 'polsha', 'proizvodstvo'),
(2436, 80, 'vesna', 'sezon'),
(2437, 80, 'zima', 'sezon'),
(2438, 80, 'leto', 'sezon'),
(2439, 80, 'osen', 'sezon'),
(2440, 80, 'chernyj', 'tsvet'),
(2441, 55, 'polimer', 'material'),
(2442, 55, 'uniseks', 'sex'),
(2443, 55, 'vetnam', 'proizvodstvo'),
(2444, 55, 'leto', 'sezon'),
(2445, 55, 'sinij', 'tsvet'),
(2456, 115, 'iskusstvennajazamsha', 'material'),
(2457, 115, 'kozha', 'material'),
(2458, 115, 'poliestr', 'material'),
(2459, 115, 'muzhskoj', 'sex'),
(2460, 115, 'ukraina', 'proizvodstvo'),
(2461, 115, 'vesna', 'sezon'),
(2462, 115, 'zima', 'sezon'),
(2463, 115, 'leto', 'sezon'),
(2464, 115, 'osen', 'sezon'),
(2465, 115, 'chernyj', 'tsvet'),
(2476, 116, 'kozha', 'material'),
(2477, 116, 'lateks', 'material'),
(2478, 116, 'nubuk', 'material'),
(2479, 116, 'muzhskoj', 'sex'),
(2480, 116, 'ssha', 'proizvodstvo'),
(2481, 116, 'vesna', 'sezon'),
(2482, 116, 'zima', 'sezon'),
(2483, 116, 'leto', 'sezon'),
(2484, 116, 'osen', 'sezon'),
(2485, 116, 'chernyj', 'tsvet'),
(2497, 117, 'kozha', 'material'),
(2498, 117, 'nubuk', 'material'),
(2499, 117, 'poliestr', 'material'),
(2500, 117, 'sintetika', 'material'),
(2501, 117, 'muzhskoj', 'sex'),
(2502, 117, 'kitaj', 'proizvodstvo'),
(2503, 117, 'vesna', 'sezon'),
(2504, 117, 'zima', 'sezon'),
(2505, 117, 'leto', 'sezon'),
(2506, 117, 'osen', 'sezon'),
(2507, 117, 'chernyj', 'tsvet'),
(2519, 118, 'kozha', 'material'),
(2520, 118, 'lateks', 'material'),
(2521, 118, 'polimer', 'material'),
(2522, 118, 'silikon', 'material'),
(2523, 118, 'uniseks', 'sex'),
(2524, 118, 'indonezija', 'proizvodstvo'),
(2525, 118, 'vesna', 'sezon'),
(2526, 118, 'zima', 'sezon'),
(2527, 118, 'leto', 'sezon'),
(2528, 118, 'osen', 'sezon'),
(2529, 118, 'chernyj', 'tsvet'),
(2541, 119, 'lateks', 'material'),
(2542, 119, 'nubuk', 'material'),
(2543, 119, 'silikon', 'material'),
(2544, 119, 'tekstil', 'material'),
(2545, 119, 'uniseks', 'sex'),
(2546, 119, 'polsha', 'proizvodstvo'),
(2547, 119, 'vesna', 'sezon'),
(2548, 119, 'zima', 'sezon'),
(2549, 119, 'leto', 'sezon'),
(2550, 119, 'osen', 'sezon'),
(2551, 119, 'korichnevyj', 'tsvet'),
(2561, 120, 'lateks', 'material'),
(2562, 120, 'nubuk', 'material'),
(2563, 120, 'poliestr', 'material'),
(2564, 120, 'hlopok', 'material'),
(2565, 120, 'muzhskoj', 'sex'),
(2566, 120, 'indonezija', 'proizvodstvo'),
(2567, 120, 'zima', 'sezon'),
(2568, 120, 'osen', 'sezon'),
(2569, 120, 'sinij', 'tsvet'),
(2578, 121, 'zamsha', 'material'),
(2579, 121, 'poliestr', 'material'),
(2580, 121, 'tekstil', 'material'),
(2581, 121, 'hlopok', 'material'),
(2582, 121, 'muzhskoj', 'sex'),
(2583, 121, 'italija', 'proizvodstvo'),
(2584, 121, 'vesna', 'sezon'),
(2585, 121, 'leto', 'sezon'),
(2610, 123, 'lateks', 'material'),
(2611, 123, 'nejlon', 'material'),
(2612, 123, 'poliestr', 'material'),
(2613, 123, 'muzhskoj', 'sex'),
(2614, 123, 'kitaj', 'proizvodstvo'),
(2615, 123, 'vesna', 'sezon'),
(2616, 123, 'leto', 'sezon'),
(2617, 123, 'seryj', 'tsvet'),
(2625, 124, 'lateks', 'material'),
(2626, 124, 'silikon', 'material'),
(2627, 124, 'tekstil', 'material'),
(2628, 124, 'zhenskij', 'sex'),
(2629, 124, 'polsha', 'proizvodstvo'),
(2630, 124, 'zima', 'sezon'),
(2631, 124, 'osen', 'sezon'),
(2641, 125, 'lateks', 'material'),
(2642, 125, 'nubuk', 'material'),
(2643, 125, 'tekstil', 'material'),
(2644, 125, 'muzhskoj', 'sex'),
(2645, 125, 'vetnam', 'proizvodstvo'),
(2646, 125, 'vesna', 'sezon'),
(2647, 125, 'zima', 'sezon'),
(2648, 125, 'leto', 'sezon'),
(2649, 125, 'osen', 'sezon'),
(2650, 125, 'krasnyj', 'tsvet'),
(2661, 126, 'kozha', 'material'),
(2662, 126, 'nubuk', 'material'),
(2663, 126, 'poliestr', 'material'),
(2664, 126, 'muzhskoj', 'sex'),
(2665, 126, 'indonezija', 'proizvodstvo'),
(2666, 126, 'vesna', 'sezon'),
(2667, 126, 'zima', 'sezon'),
(2668, 126, 'leto', 'sezon'),
(2669, 126, 'osen', 'sezon'),
(2670, 126, 'chernyj', 'tsvet'),
(2671, 127, 'nejlon', 'material'),
(2672, 127, 'poliestr', 'material'),
(2673, 127, 'polimer', 'material'),
(2674, 127, 'muzhskoj', 'sex'),
(2675, 127, 'italija', 'proizvodstvo'),
(2676, 127, 'vesna', 'sezon'),
(2677, 127, 'zima', 'sezon'),
(2678, 127, 'leto', 'sezon'),
(2679, 127, 'osen', 'sezon'),
(2680, 127, 'chernyj', 'tsvet'),
(2691, 128, 'nejlon', 'material'),
(2692, 128, 'nubuk', 'material'),
(2693, 128, 'polimer', 'material'),
(2694, 128, 'muzhskoj', 'sex'),
(2695, 128, 'kitaj', 'proizvodstvo'),
(2696, 128, 'vesna', 'sezon'),
(2697, 128, 'zima', 'sezon'),
(2698, 128, 'leto', 'sezon'),
(2699, 128, 'osen', 'sezon'),
(2700, 128, 'white', 'tsvet'),
(2711, 129, 'poliestr', 'material'),
(2712, 129, 'sintetika', 'material'),
(2713, 129, 'hlopok', 'material'),
(2714, 129, 'uniseks', 'sex'),
(2715, 129, 'ssha', 'proizvodstvo'),
(2716, 129, 'vesna', 'sezon'),
(2717, 129, 'zima', 'sezon'),
(2718, 129, 'leto', 'sezon'),
(2719, 129, 'osen', 'sezon'),
(2720, 129, 'white', 'tsvet'),
(2729, 130, 'nejlon', 'material'),
(2730, 130, 'nubuk', 'material'),
(2731, 130, 'sintetika', 'material'),
(2732, 130, 'tekstil', 'material'),
(2733, 130, 'muzhskoj', 'sex'),
(2734, 130, 'polsha', 'proizvodstvo'),
(2735, 130, 'zima', 'sezon'),
(2736, 130, 'fioletovyj', 'tsvet'),
(2743, 131, 'kozha', 'material'),
(2744, 131, 'muzhskoj', 'sex'),
(2745, 131, 'ssha', 'proizvodstvo'),
(2746, 131, 'vesna', 'sezon'),
(2747, 131, 'osen', 'sezon'),
(2748, 131, 'chernyj', 'tsvet'),
(2755, 132, 'kozha', 'material'),
(2756, 132, 'muzhskoj', 'sex'),
(2757, 132, 'ukraina', 'proizvodstvo'),
(2758, 132, 'vesna', 'sezon'),
(2759, 132, 'osen', 'sezon'),
(2760, 132, 'chernyj', 'tsvet'),
(2769, 133, 'nejlon', 'material'),
(2770, 133, 'nubuk', 'material'),
(2771, 133, 'polimer', 'material'),
(2772, 133, 'zhenskij', 'sex'),
(2773, 133, 'indonezija', 'proizvodstvo'),
(2774, 133, 'vesna', 'sezon'),
(2775, 133, 'osen', 'sezon'),
(2776, 133, 'seryj', 'tsvet'),
(2785, 134, 'lateks', 'material'),
(2786, 134, 'nejlon', 'material'),
(2787, 134, 'poliestr', 'material'),
(2788, 134, 'sintetika', 'material'),
(2789, 134, 'muzhskoj', 'sex'),
(2790, 134, 'polsha', 'proizvodstvo'),
(2791, 134, 'zima', 'sezon'),
(2792, 134, 'chernyj', 'tsvet'),
(2804, 135, 'lateks', 'material'),
(2805, 135, 'nubuk', 'material'),
(2806, 135, 'silikon', 'material'),
(2807, 135, 'sintetika', 'material'),
(2808, 135, 'zhenskij', 'sex'),
(2809, 135, 'vetnam', 'proizvodstvo'),
(2810, 135, 'vesna', 'sezon'),
(2811, 135, 'zima', 'sezon'),
(2812, 135, 'leto', 'sezon'),
(2813, 135, 'osen', 'sezon'),
(2814, 135, 'white', 'tsvet'),
(2824, 136, 'kozha', 'material'),
(2825, 136, 'nejlon', 'material'),
(2826, 136, 'polimer', 'material'),
(2827, 136, 'zhenskij', 'sex'),
(2828, 136, 'indonezija', 'proizvodstvo'),
(2829, 136, 'zima', 'sezon'),
(2830, 136, 'leto', 'sezon'),
(2831, 136, 'osen', 'sezon'),
(2832, 136, 'chernyj', 'tsvet'),
(2838, 137, 'zamsha', 'material'),
(2839, 137, 'zhenskij', 'sex'),
(2840, 137, 'vesna', 'sezon'),
(2841, 137, 'leto', 'sezon'),
(2842, 137, 'seryj', 'tsvet'),
(2851, 138, 'iskusstvennajakozha', 'material'),
(2852, 138, 'poliestr', 'material'),
(2853, 138, 'polimer', 'material'),
(2854, 138, 'muzhskoj', 'sex'),
(2855, 138, 'kitaj', 'proizvodstvo'),
(2856, 138, 'leto', 'sezon'),
(2857, 138, 'osen', 'sezon'),
(2858, 138, 'chernyj', 'tsvet'),
(2867, 139, 'nubuk', 'material'),
(2868, 139, 'polimer', 'material'),
(2869, 139, 'silikon', 'material'),
(2870, 139, 'muzhskoj', 'sex'),
(2871, 139, 'polsha', 'proizvodstvo'),
(2872, 139, 'leto', 'sezon'),
(2873, 139, 'osen', 'sezon'),
(2874, 139, 'sinij', 'tsvet'),
(2881, 140, 'kozha', 'material'),
(2882, 140, 'zhenskij', 'sex'),
(2883, 140, 'italija', 'proizvodstvo'),
(2884, 140, 'vesna', 'sezon'),
(2885, 140, 'leto', 'sezon'),
(2886, 140, 'beige', 'tsvet'),
(2895, 141, 'kozha', 'material'),
(2896, 141, 'silikon', 'material'),
(2897, 141, 'sintetika', 'material'),
(2898, 141, 'zhenskij', 'sex'),
(2899, 141, 'vetnam', 'proizvodstvo'),
(2900, 141, 'vesna', 'sezon'),
(2901, 141, 'leto', 'sezon'),
(2902, 141, 'chernyj', 'tsvet'),
(2911, 142, 'zamsha', 'material'),
(2912, 142, 'lateks', 'material'),
(2913, 142, 'poliestr', 'material'),
(2914, 142, 'muzhskoj', 'sex'),
(2915, 142, 'polsha', 'proizvodstvo'),
(2916, 142, 'vesna', 'sezon'),
(2917, 142, 'osen', 'sezon'),
(2918, 142, 'chernyj', 'tsvet'),
(2927, 143, 'lateks', 'material'),
(2928, 143, 'poliestr', 'material'),
(2929, 143, 'silikon', 'material'),
(2930, 143, 'muzhskoj', 'sex'),
(2931, 143, 'ukraina', 'proizvodstvo'),
(2932, 143, 'vesna', 'sezon'),
(2933, 143, 'osen', 'sezon'),
(2934, 143, 'korichnevyj', 'tsvet'),
(2943, 144, 'kozha', 'material'),
(2944, 144, 'lateks', 'material'),
(2945, 144, 'poliestr', 'material'),
(2946, 144, 'muzhskoj', 'sex'),
(2947, 144, 'ukraina', 'proizvodstvo'),
(2948, 144, 'vesna', 'sezon'),
(2949, 144, 'leto', 'sezon'),
(2950, 144, 'chernyj', 'tsvet'),
(2965, 145, 'kozha', 'material'),
(2966, 145, 'nejlon', 'material'),
(2967, 145, 'polimer', 'material'),
(2968, 145, 'muzhskoj', 'sex'),
(2969, 145, 'italija', 'proizvodstvo'),
(2970, 145, 'osen', 'sezon'),
(2971, 145, 'chernyj', 'tsvet'),
(2980, 146, 'iskusstvennajakozha', 'material'),
(2981, 146, 'lateks', 'material'),
(2982, 146, 'poliestr', 'material'),
(2983, 146, 'muzhskoj', 'sex'),
(2984, 146, 'indonezija', 'proizvodstvo'),
(2985, 146, 'leto', 'sezon'),
(2986, 146, 'osen', 'sezon'),
(2987, 146, 'chernyj', 'tsvet'),
(2995, 147, 'lateks', 'material'),
(2996, 147, 'nubuk', 'material'),
(2997, 147, 'tekstil', 'material'),
(2998, 147, 'muzhskoj', 'sex'),
(2999, 147, 'ukraina', 'proizvodstvo'),
(3000, 147, 'osen', 'sezon'),
(3001, 147, 'yellow', 'tsvet'),
(3009, 148, 'zamsha', 'material'),
(3010, 148, 'nubuk', 'material'),
(3011, 148, 'muzhskoj', 'sex'),
(3012, 148, 'ssha', 'proizvodstvo'),
(3013, 148, 'leto', 'sezon'),
(3014, 148, 'osen', 'sezon'),
(3015, 148, 'krasnyj', 'tsvet'),
(3024, 149, 'lateks', 'material'),
(3025, 149, 'nejlon', 'material'),
(3026, 149, 'poliestr', 'material'),
(3027, 149, 'muzhskoj', 'sex'),
(3028, 149, 'polsha', 'proizvodstvo'),
(3029, 149, 'leto', 'sezon'),
(3030, 149, 'osen', 'sezon'),
(3031, 149, 'chernyj', 'tsvet'),
(3042, 150, 'lateks', 'material'),
(3043, 150, 'nubuk', 'material'),
(3044, 150, 'silikon', 'material'),
(3045, 150, 'zhenskij', 'sex'),
(3046, 150, 'italija', 'proizvodstvo'),
(3047, 150, 'vesna', 'sezon'),
(3048, 150, 'zima', 'sezon'),
(3049, 150, 'leto', 'sezon'),
(3050, 150, 'osen', 'sezon'),
(3051, 150, 'krasnyj', 'tsvet'),
(3059, 151, 'kozha', 'material'),
(3060, 151, 'silikon', 'material'),
(3061, 151, 'zhenskij', 'sex'),
(3062, 151, 'ssha', 'proizvodstvo'),
(3063, 151, 'vesna', 'sezon'),
(3064, 151, 'osen', 'sezon'),
(3065, 151, 'chernyj', 'tsvet'),
(3074, 152, 'nejlon', 'material'),
(3075, 152, 'nubuk', 'material'),
(3076, 152, 'polimer', 'material'),
(3077, 152, 'zhenskij', 'sex'),
(3078, 152, 'ukraina', 'proizvodstvo'),
(3079, 152, 'leto', 'sezon'),
(3080, 152, 'osen', 'sezon'),
(3081, 152, 'chernyj', 'tsvet'),
(3090, 153, 'kozha', 'material'),
(3091, 153, 'nubuk', 'material'),
(3092, 153, 'silikon', 'material'),
(3093, 153, 'muzhskoj', 'sex'),
(3094, 153, 'ukraina', 'proizvodstvo'),
(3095, 153, 'leto', 'sezon'),
(3096, 153, 'osen', 'sezon'),
(3097, 153, 'chernyj', 'tsvet'),
(3126, 154, 'lateks', 'material'),
(3127, 154, 'nejlon', 'material'),
(3128, 154, 'sintetika', 'material'),
(3129, 154, 'zhenskij', 'sex'),
(3130, 154, 'italija', 'proizvodstvo'),
(3131, 154, 'zima', 'sezon'),
(3132, 154, 'leto', 'sezon'),
(3133, 154, 'chernyj', 'tsvet'),
(3144, 155, 'kozha', 'material'),
(3145, 155, 'nejlon', 'material'),
(3146, 155, 'nubuk', 'material'),
(3147, 155, 'tekstil', 'material'),
(3148, 155, 'uniseks', 'sex'),
(3149, 155, 'vetnam', 'proizvodstvo'),
(3150, 155, 'vesna', 'sezon'),
(3151, 155, 'leto', 'sezon'),
(3152, 155, 'osen', 'sezon'),
(3153, 155, 'chernyj', 'tsvet'),
(3172, 156, 'zamsha', 'material'),
(3173, 156, 'iskusstvennajazamsha', 'material'),
(3174, 156, 'poliestr', 'material'),
(3175, 156, 'zhenskij', 'sex'),
(3176, 156, 'indonezija', 'proizvodstvo'),
(3177, 156, 'vesna', 'sezon'),
(3178, 156, 'leto', 'sezon'),
(3179, 156, 'osen', 'sezon'),
(3180, 156, 'chernyj', 'tsvet'),
(3190, 85, 'poliestr', 'material'),
(3191, 85, 'hlopok', 'material'),
(3192, 85, 'zhenskij', 'sex'),
(3193, 85, 'ssha', 'proizvodstvo'),
(3194, 85, 'vesna', 'sezon'),
(3195, 85, 'zima', 'sezon'),
(3196, 85, 'leto', 'sezon'),
(3197, 85, 'osen', 'sezon'),
(3260, 159, 'zamsha', 'material'),
(3261, 159, 'iskusstvennajazamsha', 'material'),
(3262, 159, 'iskusstvennajakozha', 'material'),
(3263, 159, 'kozha', 'material'),
(3264, 159, 'lateks', 'material'),
(3265, 159, 'nejlon', 'material'),
(3266, 159, 'nubuk', 'material'),
(3267, 159, 'poliestr', 'material'),
(3268, 159, 'polimer', 'material'),
(3269, 159, 'silikon', 'material'),
(3270, 159, 'sintetika', 'material'),
(3271, 159, 'tekstil', 'material'),
(3272, 159, 'hlopok', 'material'),
(3273, 159, 'ukraina', 'proizvodstvo'),
(3292, 158, 'zamsha', 'material'),
(3293, 158, 'iskusstvennajazamsha', 'material'),
(3294, 158, 'iskusstvennajakozha', 'material'),
(3295, 158, 'kozha', 'material'),
(3296, 158, 'lateks', 'material'),
(3297, 158, 'nejlon', 'material'),
(3298, 158, 'nubuk', 'material'),
(3299, 158, 'poliestr', 'material'),
(3300, 158, 'polimer', 'material'),
(3301, 158, 'silikon', 'material'),
(3302, 158, 'tekstil', 'material'),
(3303, 158, 'hlopok', 'material'),
(3304, 158, 'polsha', 'proizvodstvo'),
(3305, 158, 'vesna', 'sezon'),
(3306, 158, 'osen', 'sezon'),
(3307, 158, 'fioletovyj', 'tsvet'),
(3397, 161, 'seryj', 'tsvet'),
(3398, 160, 'kitaj', 'proizvodstvo'),
(3417, 163, 'beige', 'tsvet'),
(3418, 163, 'zhenskij', 'sex'),
(3419, 163, 'italija', 'proizvodstvo'),
(3428, 122, 'zima', 'sezon'),
(3429, 122, 'osen', 'sezon'),
(3430, 122, 'muzhskoj', 'sex'),
(3431, 122, 'vetnam', 'proizvodstvo'),
(3432, 122, 'iskusstvennajazamsha', 'material'),
(3433, 122, 'iskusstvennajakozha', 'material'),
(3434, 122, 'nejlon', 'material'),
(3435, 122, 'sintetika', 'material'),
(3457, 58, 'sinij', 'tsvet'),
(3458, 58, 'leto', 'sezon'),
(3459, 58, 'vesna', 'sezon'),
(3460, 58, 'zhenskij', 'sex'),
(3461, 58, 'indonezija', 'proizvodstvo'),
(3462, 58, 'polimer', 'material'),
(3463, 58, 'silikon', 'material');

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_tree`
--

CREATE TABLE `catalog_tree` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` int(255) DEFAULT '0',
  `image` varchar(64) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `views` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `catalog_tree`
--

INSERT INTO `catalog_tree` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `alias`, `parent_id`, `image`, `h1`, `title`, `keywords`, `description`, `text`, `views`) VALUES
(2, 1504522041, 1508752582, 1, 2, 'Офисные кресла для персонала', 'ofisnye-kresla-dlja-personala', 137, 'KO-1_300h300_2008140352.png', '1', '1', 'Офисные кресла для персонала', '37', '<p>ofisnye-kresla-dlja-personala</p>', NULL),
(12, NULL, 1508752552, 1, 1, 'Компьютерные кресла', 'comp-kresla', 137, '1648067564543.jpg', '1', '0', 'Компьютерные кресла', '37', '<p>comp-kresla</p>', NULL),
(13, NULL, 1508752509, 1, 0, 'Конференц кресла', 'conference-kresla', 137, '_b22897.jpg', '1', '0', 'Конференц кресла', '37', '<p>conference-kresla</p>', NULL),
(14, NULL, 1508749972, 1, 0, 'Стулья для кабаре', 'stulya-dlya-kabare', 138, 'paged_h-5245_228_228_5.jpg', '1', '0', 'Стулья для кабаре', '38', '<p>stulya-dlya-kabare</p>', NULL),
(15, NULL, 1508750703, 1, 2, 'Cтулья школьные', 'stylia-shkolnue', 138, '983_2055.jpg', '1', '0', 'Cтулья школьные', '38', '<p>stylia-shkolnue</p>', NULL),
(16, NULL, 1508750416, 1, 1, 'Стулья офисные', 'stylia-offisnue', 138, 'h1jgr1vhv1121m1.jpg', '1', '0', 'Стулья офисные', '38', '<p>stylia-offisnue</p>', NULL),
(17, NULL, 1508751636, 1, 14, 'Столы руководителя  ', 'stolu-rykovoditela', 128, 'ydk-606-kabinet-rukovoditelya-433-product-10000-10000.jpeg', '1', '0', 'Столы руководителя  ', '28', '<p>stolu-rykovoditela</p>', NULL),
(18, NULL, 1508752051, 1, 13, 'Офисные столы для персонала', 'ofissnue-stolu-dlya-personala', 128, 'офисные-столы-для-персонала.jpg', '1', '0', 'Офисные столы для персонала', '28', '<p>ofissnue-stolu-dlya-personala</p>', NULL),
(19, NULL, 1508752009, 1, 12, 'Письменные столы', 'pissmenue-stolu', 128, 'BIU_2D2S_160_120_gl-800x600.jpg', '1', '0', 'Письменные столы', '28', '<p>pissmenue-stolu</p>', NULL),
(20, NULL, 1508751978, 1, 11, 'Приставные столы', 'pristavnue-ctolu', 128, '1364574321_1-3000x2000.png', '1', '0', 'Приставные столы', '28', '<p>pristavnue-ctolu</p>', NULL),
(21, NULL, 1508751892, 1, 10, 'Конференц столы', 'conference-stolu', 128, '2079.jpg', '1', '0', 'Конференц столы', '28', '<p>conference-stolu</p>', NULL),
(22, NULL, 1508751761, 1, 9, 'Компьютерные столы', 'comp-stolu', 128, 'p_icon-pryamye-kompyuternye-stoly-s-nadstavkoy.jpg', '1', '0', 'Компьютерные столы', '28', '<p>comp-stolu</p>', NULL),
(23, NULL, 1508751707, 1, 8, 'Стол папка', 'stol-papka', 128, '1720125536.jpg', '1', '0', 'Стол папка', '28', '<p>stol-papka</p>', NULL),
(24, NULL, 1508751546, 1, 7, 'Стол книжка', 'stol-knizhka', 128, 'Osobennosti-stola-knizhki-na-kolesikah-890x395_c.jpg', '1', '0', 'Стол книжка', '28', '<p>stol-knizhka</p>', NULL),
(25, NULL, 1508751494, 1, 6, 'Парты школьные', 'partu-shkolnue', 128, 'Parta157_0292.jpg', '1', '0', 'Парты школьные', '28', '<p>partu-shkolnue</p>', NULL),
(26, NULL, 1508751440, 1, 5, 'Журнальные столы', 'zhurnalnie-stolu', 128, 'zhurnalnyiy_stolik_olimp_steklo.jpg', '1', '0', 'Журнальные столы', '28', '<p>zhurnalnie-stolu</p>', NULL),
(27, NULL, 1508751388, 1, 4, 'Столы для кабаре', 'stolu-dlya-cabare', 128, 'large_tables_CaBaRe.jpg', '1', '0', 'Столы для кабаре', '28', '<p>stolu-dlya-cabare</p>', NULL),
(28, NULL, 1508751348, 1, 3, 'Базы, основания, опоры для столов', 'bazu-osnovu-oporu', 128, '9045_skm.jpg', '1', '0', 'Базы, основания, опоры для столов', '28', '<p>bazu-osnovu-oporu</p>', NULL),
(29, NULL, 1508751285, 1, 2, 'Столешницы столов', 'stoleshnizu-stolov', 128, 'pvsmb_config_a.jpg', '1', '0', 'Столешницы столов', '28', '<p>stoleshnizu-stolov</p>', NULL),
(30, NULL, 1508751216, 1, 1, 'Стойки ресепшн', 'stoiki-recepshn', 128, 'stmeb-fotx011.jpg', '1', '0', 'Стойки ресепшн', '28', '<p>stoiki-recepshn</p>', NULL),
(31, NULL, 1508752915, 1, 6, 'Кровати односпальные', 'krovati-odnospalnie', 139, '2434681098.jpg', '1', '0', 'Кровати односпальные', '39', '<p>krovati-odnospalnie</p>', NULL),
(32, NULL, 1508752863, 1, 5, 'Кровати двуспальные', 'krovati-dvuspalnie', 139, 'tokio_krovat_1600_venge_sonoma.jpeg', '1', '0', 'Кровати двуспальные', '39', '<p>krovati-dvuspalnie</p>', NULL),
(33, NULL, 1508752810, 1, 4, 'Кровати детские', 'krovati-detskie', 139, '617271101_w640_h640_konfetti_1.jpg', '1', '0', 'Кровати детские', '39', '<p>krovati-detskie</p>', NULL),
(34, NULL, 1508752778, 1, 3, 'Кровати для гостиниц', 'krovati-dlya-gostiniz', 139, '13243543224.jpg', '1', '0', 'Кровати для гостиниц', '39', '<p>krovati-dlya-gostiniz</p>', NULL),
(35, NULL, 1508752722, 1, 2, 'Двухярусные кровати', 'dvuhyarusnue-krovati', 139, 'dvuhyarusnye_krovati.jpg', '1', '0', 'Двухярусные кровати', '39', '<p>dvuhyarusnue-krovati</p>', NULL),
(36, NULL, 1508752668, 1, 1, 'Деревяные кровати', 'derevianue-krovati', 139, 'krovat-liza2.jpg', '1', '0', 'Деревяные кровати', '39', '<p>derevianue-krovati</p>', NULL),
(37, NULL, 1508752635, 1, 0, 'Каркасы кроватей', 'karkasu-krovatei', 139, 'ka-1.jpg', '1', '0', 'Каркасы кроватей', '39', '<p>karkasu-krovatei</p>', NULL),
(38, NULL, 1508753165, 1, 5, 'Ортопедические', 'ortopedicheskie', 140, '346463.jpg', '1', '0', 'Ортопедические', '40', '<p>ortopedicheskie</p>', NULL),
(39, NULL, 1508753131, 1, 4, 'Футоны', 'futonu', 140, '228250.jpg', '1', '0', 'Футоны', '40', '<p>futonu</p>', NULL),
(40, NULL, 1508753094, 1, 3, 'Детские матрасы', 'detskie-matrasu', 140, '308485.jpg', '1', '0', 'Детские матрасы', '40', '<p>detskie-matrasu</p>', NULL),
(41, NULL, 1508753053, 1, 2, 'Латексные', 'lateksnue', 140, 'lateksniy matras1.jpg', '1', '0', 'Латексные', '40', '<p>lateksnue</p>', NULL),
(42, NULL, 1508753009, 1, 1, 'Пружиные', 'pryzhnue', 140, 'pruzhynnye2-310x270.jpg', '1', '0', 'Пружиные', '40', '<p>pryzhnue</p>', NULL),
(43, NULL, 1508752982, 1, 0, 'Беспружиные', 'bespruzhunue', 140, 'bespruzhinnii.jpg', '1', '0', 'Беспружиные', '40', '<p>bespruzhunue</p>', NULL),
(44, NULL, 1508753339, 1, 2, 'Офисные диваны', 'oficcesnue-divanu', 141, 'CHernyiy-divan.jpg', '1', '0', 'Офисные диваны', '41', '<p>oficcesnue-divanu</p>', NULL),
(45, NULL, 1508753298, 1, 1, 'Мягкая мебель для КаБаРе', 'myhkaia-mebel-dlya-kabare', 141, '156454312.jpg', '1', '0', 'Мягкая мебель для КаБаРе', '41', '<p>myhkaia-mebel-dlya-kabare</p>', NULL),
(46, NULL, 1508753238, 1, 0, 'Мягкая мебель для зон ожидания', 'myahkaya-mebel-dlya-zon-ozhidaniyz', 141, '617910b2b2e59ef7f3bfa46ecf9ecf43_387x290x-1.jpg', '1', '0', 'Мягкая мебель для зон ожидания', '41', '<p>myahkaya-mebel-dlya-zon-ozhidaniyz</p>', NULL),
(47, NULL, 1508751077, 1, 3, 'Шкафы офисные', 'shkafu-offisnue', 133, '923.jpg', '1', '0', 'Шкафы офисные', '33', '<p>shkafu-offisnue</p>', NULL),
(48, NULL, 1508751018, 1, 2, 'Пеналы офисные', 'penalu-ofisnue', 133, 'ofisnyij-penal-model-evita.jpg', '1', '0', 'Пеналы офисные', '33', '<p>penalu-ofisnue</p>', NULL),
(49, NULL, 1508750914, 1, 1, 'Стелажи для документов', 'stelazhu-dlya-documentov', 133, 'stellazh-dokumenty-01.jpg', '1', '0', 'Стелажи для документов', '33', '<p>stelazhu-dlya-documentov</p>', NULL),
(50, NULL, 1508750828, 1, 0, 'Угловые секции', 'uhlovie-seczii', 133, 'modulnye_shkafy_1_2.jpg', '1', '0', 'Угловые секции', '33', '<p>uhlovie-seczii</p>', NULL),
(51, NULL, 1508752444, 1, 5, 'Комоды', 'komodu', 129, 'komod_vega_k_04_mebeli_96.jpg', '1', '0', 'Комоды', '29', '<p>komodu</p>', NULL),
(52, NULL, 1508752408, 1, 4, 'Тумбы для офиса', 'tymbu-dlya-ofisa', 129, 'office_tumba_001.jpg', '1', '0', 'Тумбы для офиса', '29', '<p>tymbu-dlya-ofisa</p>', NULL),
(53, NULL, 1508752355, 1, 3, 'Тумбы для обуви', 'tymbu-dlya-obyvi', 129, 'nKgX99XXfe.jpg', '1', '0', 'Тумбы для обуви', '29', '<p>tymbu-dlya-obyvi</p>', NULL),
(54, NULL, 1508752322, 1, 2, 'Туалетные столики', 'tyaletnie-stoliki', 129, '1241аыфа1к.jpg', '1', '0', 'Туалетные столики', '29', '<p>tyaletnie-stoliki</p>', NULL),
(55, NULL, 1508752265, 1, 1, 'ТВ тумбы под телевизор', 'tv-tumby-pod-televizor', 129, 'tv-tumba-d311-81.jpg', '1', '0', 'ТВ тумбы под телевизор', '29', '<p>TV-tymbu-pod-televizor</p>', NULL),
(56, NULL, 1508752191, 1, 0, 'Прикроватные тумбы', 'prikrovatnie-tymbu', 129, 'tumba-prikrovatnaya-m-105-belaya-foto-0.jpg', '1', '0', 'Прикроватные тумбы', '29', '<p>prikrovatnie-tymbu</p>', NULL),
(57, NULL, 1508753471, 1, 0, 'Зеркала', 'zerkala', 143, 'zerkala.jpg', '1', '0', 'Зеркала', '43', '<p>zerkala</p>', NULL),
(62, NULL, 1508753539, 1, 1, 'Кабинеты руководителя', 'cabinetu-rukovoditelya', 148, '75355342356.jpg', '1', '0', 'Кабинеты руководителя', '48', '<p>cabinetu-rukovoditelya</p>', NULL),
(63, NULL, 1508753504, 1, 0, 'Мебель для персонала', 'mebel-dlya-personala', 148, '9876543468.jpg', '1', '0', 'Мебель для персонала', '48', '<p>mebel-dlya-personala</p>', NULL),
(64, NULL, 1508753426, 1, 0, 'Вешалки для дома', 'test-veshalok', 142, '13987609737.jpg', '1', '0', 'Тест вешалок', '42', '<p>test-veshalok</p>', NULL),
(68, 1508156793, 1508234699, 1, 2, 'Угловые диваны', 'uglovye-divany', 152, '0debd8ef317e47e2dd14e8b9e5adaca7.jpg', '1', '0', 'Угловые диваны', '52', '<p>uglovye-divany</p>', NULL),
(69, 1508157052, 1508234672, 1, 1, 'Мини диваны', 'mini-divany', 152, '959c3cd2dbdb04c4e5997ba4467ed2c2.JPG', '1', '0', 'Мини диваны', '52', '<p>mini-divany</p>', NULL),
(70, 1508164120, 1508232033, 1, 0, 'Раздвижные столы', 'razdvizhnye-stoly', 128, 'razdvizhnieStolu.jpg', '1', '0', 'Раздвижные столы', '28', 'razdvizhnye-stoly', NULL),
(71, 1508165329, 1508234657, 1, 0, 'Большие диваны', 'bolshie-divany', 152, '59f1118930af3f1a3fc2c2a3d5b2aad2.jpg', '1', '0', 'Большие диваны', '52', '<p>bolshie-divany</p>', NULL),
(128, NULL, 1508234267, 1, 2, 'Столы', 'tables', 0, '27beea1673a92a94470957adb0b7313c.png', '', '', '', '', '', 0),
(129, NULL, 1508234301, 1, 3, 'Комоды и тумбы', 'comods', 0, '3f8d0bab5fe6e1a83452b570aa31c222.png', '', '', '', '', '', 0),
(133, NULL, 1508234230, 1, 1, 'Шкафы', 'shkafu', 0, '37c817c1386df1ec107a7b508483b567.png', '', '', '', '', '', 0),
(137, NULL, 1508234333, 1, 4, 'Кресла', 'kresla', 0, '9a205cb36e372eb5ade9d3e2b0419e4c.png', '', '', '', '', '', 0),
(138, NULL, 1508234170, 1, 0, 'Стулья', 'stylia', 0, 'c844091619f3305be27eb4cb6d32d7db.png', '', '', '', '', '', 0),
(139, NULL, 1508234359, 1, 5, 'Кровати', 'krovati', 0, 'cb69659a5ff0c4fd8a9a9e494d4e3c92.png', '', '', '', '', '', 0),
(140, NULL, 1508234376, 1, 6, 'Матрасы', 'matrasu', 0, '63ad4b6450a38ab8a26c6da434ae0bcb.png', '', '', '', '', '', 0),
(141, NULL, 1508234398, 1, 7, 'Мягкая мебель', 'myahkaya-mebel', 0, 'ddb6dde6f630a273362f07bb2a9f5099.png', '', '', '', '', '', 0),
(142, NULL, 1508234422, 1, 8, 'Вешалки', 'veshalku', 0, '9635f0c93b67f412aea68a9d7a631a20.png', '', '', '', '', '', 0),
(143, NULL, 1508234598, 1, 9, 'Прихожие', 'prihozhii', 0, 'afe15c27419a0dc111fd06ccdcc2def1.png', '', '', '', '', '', 0),
(144, NULL, 1508234443, 1, 10, 'Гостинные', 'gostinue', 0, 'ed593efd8da3910cd41cbbc5b9c781aa.png', '', '', '', '', '', 0),
(145, NULL, 1508234501, 1, 11, 'Детские', 'detskie', 0, 'cab37aade24926eabd8eb1386470c15b.png', '', '', '', '', '', 0),
(146, NULL, 1508234520, 1, 12, 'Спальни', 'spalni', 0, '8cb463ebf27e50bb3f780065770bb894.png', '', '', '', '', '', 0),
(147, NULL, 1508234536, 1, 13, 'Модульная мебель', 'modulnaya-mebel', 0, '805ae5b26227deab642889949d6baec3.png', '', '', '', '', '', 0),
(148, NULL, 1508234552, 1, 14, 'Комплекты офисной мебели', 'komplektu-office', 0, '3f21558f4c4e5edb18bcabaf467cedac.png', '', '', '', '', '', 0),
(152, NULL, 1508234563, 1, 15, 'Диваны', 'divany', 0, 'bdfe0089aa13b2d4d82034a69372a2b8.png', '', '', '', '', '', 0),
(159, 1508233875, NULL, 1, 0, 'Ванная', 'vannaja', 0, '6bdda5c4cb97a326585669d95a5467ae.png', '', '', '', '', '', 0),
(160, 1508506984, NULL, 1, 0, 'Сушилки', 'sushilki', 159, '5bd0988c942e3c0d6bc649c81442ea40.jpg', '', '', '', '', '', 0),
(161, 1508746258, NULL, 1, 0, 'Стенки', 'stenki', 133, '3e74a1ea3e0cea480f5be7fce668a829.jpg', '', '', '', '', '', 0),
(162, 1508749151, NULL, 1, 0, 'Шкафы-купе', 'shkafy-kupe', 133, '8af85a74b0c72ad478dca61e462e21a1.jpg', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_tree_brands`
--

CREATE TABLE `catalog_tree_brands` (
  `id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `catalog_tree_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `catalog_tree_specifications`
--

CREATE TABLE `catalog_tree_specifications` (
  `id` int(10) NOT NULL,
  `catalog_tree_id` int(10) NOT NULL,
  `specification_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `parent_id` int(128) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `views` int(10) DEFAULT '0',
  `markup` double(6,2) DEFAULT NULL,
  `image` varchar(255) DEFAULT 'no-image.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `parent_id`, `alias`, `h1`, `title`, `keywords`, `description`, `text`, `views`, `markup`, `image`) VALUES
(1, 1504522010, 1507816507, 1, 0, 'Кресла для руководителей', 37, 'kresla-dlja-rukovoditelej', '', '', '', '', '', 1, 20.00, 'no-image.png'),
(2, 1504522041, 1504522065, 1, 1, 'Офисные кресла для персонала', 37, 'ofisnye-kresla-dlja-personala', '', '', '', '', '', 0, 10.00, 'no-image.png'),
(12, NULL, NULL, 1, 0, 'Компьютерные кресла', 37, 'comp-kresla', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(13, NULL, NULL, 1, 0, 'Конференц кресла', 37, 'conference-kresla', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(14, NULL, NULL, 1, 0, 'Стулья для кабаре', 38, 'stulya-dlya-kabare', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(15, NULL, NULL, 1, 0, 'Cтулья школьные', 38, 'stylia-shkolnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(16, NULL, NULL, 1, 0, 'Стулья офисные', 38, 'stylia-offisnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(17, NULL, NULL, 1, 0, 'Столы руководителя  ', 28, 'stolu-rykovoditela', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(18, NULL, NULL, 1, 0, 'Офисные столы для персонала', 28, 'ofissnue-stolu-dlya-personala', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(19, NULL, NULL, 1, 0, 'Письменные столы', 28, 'pissmenue-stolu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(20, NULL, NULL, 1, 0, 'Приставные столы', 28, 'pristavnue-ctolu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(21, NULL, NULL, 1, 0, 'Конференц столы', 28, 'conference-stolu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(22, NULL, NULL, 1, 0, 'Компьютерные столы', 28, 'comp-stolu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(23, NULL, NULL, 1, 0, 'Стол папка', 28, 'stol-papka', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(24, NULL, NULL, 1, 0, 'Стол книжка', 28, 'stol-knizhka', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(25, NULL, NULL, 1, 0, 'Парты школьные', 28, 'partu-shkolnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(26, NULL, NULL, 1, 0, 'Журнальные столы', 28, 'zhurnalnie-stolu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(27, NULL, NULL, 1, 0, 'Столы для кабаре', 28, 'stolu-dlya-cabare', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(28, NULL, NULL, 1, 0, 'Базы, основания, опоры для столов', 28, 'bazu-osnovu-oporu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(29, NULL, NULL, 1, 0, 'Столешницы столов', 28, 'stoleshnizu-stolov', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(30, NULL, NULL, 1, 0, 'Стойки ресепшн', 28, 'stoiki-recepshn', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(31, NULL, NULL, 1, 0, 'Кровати односпальные', 39, 'krovati-odnospalnie', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(32, NULL, NULL, 1, 0, 'Кровати двуспальные', 39, 'krovati-dvuspalnie', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(33, NULL, NULL, 1, 0, 'Кровати детские', 39, 'krovati-detskie', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(34, NULL, NULL, 1, 0, 'Кровати для гостиниц', 39, 'krovati-dlya-gostiniz', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(35, NULL, NULL, 1, 0, 'Двухярусные кровати', 39, 'dvuhyarusnue-krovati', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(36, NULL, NULL, 1, 0, 'Деревяные кровати', 39, 'derevianue-krovati', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(37, NULL, NULL, 1, 0, 'Каркасы кроватей', 39, 'karkasu-krovatei', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(38, NULL, NULL, 1, 0, 'Ортопедические', 40, 'ortopedicheskie', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(39, NULL, NULL, 1, 0, 'Футоны', 40, 'futonu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(40, NULL, NULL, 1, 0, 'Детские матрасы', 40, 'detskie-matrasu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(41, NULL, NULL, 1, 0, 'Латексные', 40, 'lateksnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(42, NULL, NULL, 1, 0, 'Пружиные', 40, 'pryzhnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(43, NULL, NULL, 1, 0, 'Беспружиные', 40, 'bespruzhunue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(44, NULL, NULL, 1, 0, 'Офисные диваны', 41, 'oficcesnue-divanu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(45, NULL, NULL, 1, 0, 'Мягкая мебель для КаБаРе', 41, 'myhkaia-mebel-dlya-kabare', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(46, NULL, NULL, 1, 0, 'Мягкая мебель для зон ожидания', 41, 'myahkaya-mebel-dlya-zon-ozhidaniyz', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(47, NULL, NULL, 1, 0, 'Шкафы офисные', 33, 'shkafu-offisnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(48, NULL, NULL, 1, 0, 'Пеналы офисные', 33, 'penalu-ofisnue', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(49, NULL, NULL, 1, 0, 'Стелажи для документов', 33, 'stelazhu-dlya-documentov', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(50, NULL, NULL, 1, 0, 'Угловые секции', 33, 'uhlovie-seczii', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(51, NULL, NULL, 1, 0, 'Комоды', 29, 'komodu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(52, NULL, NULL, 1, 0, 'Тумбы для офиса', 29, 'tymbu-dlya-ofisa', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(53, NULL, NULL, 1, 0, 'Тумбы для обуви', 29, 'tymbu-dlya-obyvi', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(54, NULL, NULL, 1, 0, 'Туалетные столики', 29, 'tyaletnie-stoliki', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(55, NULL, NULL, 1, 0, 'ТВ тумбы под телевизор', 29, 'TV-tymbu-pod-televizor', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(56, NULL, NULL, 1, 0, 'Прикроватные тумбы', 29, 'prikrovatnie-tymbu', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(57, NULL, NULL, 1, 0, 'Зеркала', 43, 'zerkala', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(62, NULL, NULL, 1, 0, 'Кабинеты руководителя', 48, 'cabinetu-rukovoditelya', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(63, NULL, NULL, 1, 0, 'Мебель для персонала', 48, 'mebel-dlya-personala', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(64, NULL, NULL, 1, 0, 'Тест вешалок', 42, 'test-veshalok', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'no-image.png'),
(68, 1508156793, NULL, 1, 0, 'Угловые диваны', 52, 'uglovye-divany', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'uglovoiDivan.jpg'),
(69, 1508157052, NULL, 1, 0, 'Мини диваны', 52, 'mini-divany', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'miniDivan.jpg'),
(70, 1508164120, NULL, 1, 0, 'Раздвижные столы', 28, 'razdvizhnye-stoly', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'razdvizhnieStolu.jpg'),
(71, 1508165329, NULL, 1, 0, 'Большие диваны', 52, 'bolshie-divany', NULL, NULL, NULL, NULL, NULL, 0, NULL, 'maxresdefault.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `config`
--

CREATE TABLE `config` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `zna` text,
  `updated_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(32) DEFAULT NULL,
  `values` text COMMENT 'Возможные значения в json массиве ключ => значение. Для селекта и радио',
  `group` varchar(128) DEFAULT NULL COMMENT 'Группа настроек'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `config`
--

INSERT INTO `config` (`id`, `name`, `zna`, `updated_at`, `status`, `sort`, `key`, `valid`, `type`, `values`, `group`) VALUES
(1, 'E-Mail администратора сайта (отправитель по умолчанию)', 'veselovskaya.wezom@gmail.com', 1434885560, 1, 1, 'admin_email', 1, 'input', NULL, 'mail'),
(2, 'Название сайта', 'CMS v4.0', 1434885560, 1, 1, 'name_site', 1, 'input', NULL, 'basic'),
(4, 'Copyright', '2014 © Интернет магазин спортивной обуви и одежды', 1434885560, 1, 4, 'copyright', 1, 'input', NULL, 'static'),
(5, 'Отображение всплывающих сообщений на сайте', 'topRight', 1434835221, 0, 2000, 'message_output', 1, 'radio', '[{\"key\":\"Отображать вверху\",\"value\":\"top\"},{\"key\":\"Отображать вверху слева\",\"value\":\"topLeft\"},{\"key\":\"Отображать вверху по центру\",\"value\":\"topCenter\"},{\"key\":\"Отображать вверху справа\",\"value\":\"topRight\"},{\"key\":\"Отображать по центру слева\",\"value\":\"centerLeft\"},{\"key\":\"Отображать по центру\",\"value\":\"center\"},{\"key\":\"Отображать по центру справа\",\"value\":\"centerRight\"},{\"key\":\"Отображать внизу слева\",\"value\":\"bottomLeft\"},{\"key\":\"Отображать внизу по центру\",\"value\":\"bottomCenter\"},{\"key\":\"Отображать внизу справа\",\"value\":\"bottomRight\"},{\"key\":\"Отображать внизу\",\"value\":\"bottom\"}]', 'basic'),
(6, 'Номер телефона в шапке сайта', '+38990909239', 1434885560, 1, 6, 'phone', 0, 'input', NULL, 'static'),
(7, 'Количество товаров на странице', '15', 1434885560, 1, 7, 'limit', 1, 'input', NULL, 'basic'),
(8, 'Количество статей на главной странице', '1', 1434885560, 1, 8, 'limit_articles_main_page', 1, 'input', NULL, 'basic'),
(9, 'Количество строк в админ-панели', '10', 1434885560, 1, 9, 'limit_backend', 1, 'input', NULL, 'basic'),
(10, 'VK.com', 'http://vk.com/airpacgroup', 1434885560, 1, 10, 'vk', 0, 'input', NULL, 'socials'),
(11, 'FB.com', 'https://www.facebook.com', 1434885560, 1, 11, 'fb', 0, 'input', NULL, 'socials'),
(12, 'Instagram', 'http://instagram.com', 1434885560, 1, 12, 'instagram', 0, 'input', NULL, 'socials'),
(13, 'Надпись в подвале сайта для подписчиков', 'Хочешь быть в числе первых, кому мы сообщим об акциях и новинках?!', 1434885560, 1, 5, 'subscribe_text', 0, 'input', NULL, 'static'),
(17, 'Количество новостей / статей на странице', '1', 1434885560, 1, 7, 'limit_articles', 1, 'input', NULL, 'basic'),
(18, 'Количество групп товаров на странице', '5', 1434885560, 1, 7, 'limit_groups', 1, 'input', NULL, 'basic'),
(19, 'Использовать СМТП', '1', 1434885560, 1, 3, 'smtp', 1, 'radio', '[{\"key\":\"Да\",\"value\":1},{\"key\":\"Нет\",\"value\":0}]', 'mail'),
(20, 'SMTP server', '', 1434885560, 1, 4, 'host', 0, 'input', NULL, 'mail'),
(22, 'Логин', '1111', 1434885560, 1, 5, 'username', 0, 'input', NULL, 'mail'),
(23, 'Пароль', '112233', 1434885560, 1, 6, 'pass', 0, 'password', NULL, 'mail'),
(24, 'Тип подключения', '', 1434885560, 1, 7, 'secure', 0, 'select', '[{\"key\":\"TLS\",\"value\":\"tls\"},{\"key\":\"SSL\",\"value\":\"ssl\"},{\"key\":\"Отсутствует\",\"value\":\"\"}]', 'mail'),
(25, 'Порт. Например 465 или 587. (587 по умолчанию)', '587', 1434885560, 1, 8, 'port', 0, 'input', NULL, 'mail'),
(26, 'Имя латинницей (отображается в заголовке письма)', 'Info', 1434885560, 1, 2, 'name', 1, 'input', NULL, 'mail'),
(27, 'Запаролить сайт', '0', 1434885560, 1, 0, 'auth', 1, 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'security'),
(28, 'Логин', '1', 1434885560, 1, 2, 'username', 0, 'input', NULL, 'security'),
(29, 'Пароль', '1', 1434885560, 1, 3, 'password', 0, 'password', NULL, 'security'),
(30, 'Сократить CSS u JavaScript', '1', 1434885561, 1, 1, 'minify', 1, 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'speed'),
(31, 'Сократить HTML', '1', 1434885561, 1, 2, 'compress', 1, 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'speed'),
(32, 'Кеширование размера изображений', '0', NULL, 1, 3, 'cache_images', 1, 'select', '[{\"key\":\"Выключить\",\"value\":\"0\"},{\"key\":\"12 часов\",\"value\":\"0.5\"},{\"key\":\"День\",\"value\":\"1\"},{\"key\":\"3 дня\",\"value\":\"3\"},{\"key\":\"Неделя\",\"value\":\"7\"},{\"key\":\"2 недели\",\"value\":\"14\"},{\"key\":\"Месяц\",\"value\":\"30\"},{\"key\":\"Год\",\"value\":\"365\"}]', 'speed'),
(33, 'Наименование организации', 'wezom', NULL, 1, 1, 'organization', 1, 'input', NULL, 'microdata'),
(34, 'Автор статей', 'wezom author', NULL, 1, 2, 'author', 1, 'input ', NULL, 'microdata'),
(35, 'Количество отзывов на странице', '10', NULL, 1, 8, 'limit_reviews', 1, 'input', NULL, 'basic'),
(36, 'Наименование магазина', 'cms4', NULL, 1, 1, 'name', 1, 'input', NULL, 'export'),
(37, 'Наименование компании', 'wezom', NULL, 1, 2, 'company', 1, 'input', NULL, 'export'),
(38, 'Email разработчиков CMS или агентства, осуществляющего техподдержку.', 'alyohina.i.wezom@gmail.com', NULL, 1, 3, 'email', 1, 'input', NULL, 'export'),
(40, 'Использовать защищённое ssl соединение?', '0', NULL, 1, 5, 'ssl', 1, 'radio', '[{\"key\":\"Да\",\"value\":\"1\"},{\"key\":\"Нет\",\"value\":\"0\"}]', 'security'),
(41, 'Ключ googlemaps', 'AIzaSyDGMr7U3w0bDthcw1jqIQRLbz-zXJmd0K4', 1505459047, 1, 100, 'googlemapskey', 0, 'input', NULL, 'basic');

-- --------------------------------------------------------

--
-- Структура таблицы `config_groups`
--

CREATE TABLE `config_groups` (
  `id` int(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(128) NOT NULL,
  `side` varchar(16) NOT NULL DEFAULT 'left' COMMENT 'left, right',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `config_groups`
--

INSERT INTO `config_groups` (`id`, `name`, `alias`, `side`, `status`, `sort`) VALUES
(1, 'Почта', 'mail', 'right', 1, 1),
(2, 'Базовые', 'basic', 'left', 1, 1),
(3, 'Статика', 'static', 'left', 1, 2),
(4, 'Соц. сети', 'socials', 'left', 1, 3),
(5, 'Безопасность', 'security', 'right', 1, 2),
(6, 'Быстродействие', 'speed', 'right', 1, 3),
(7, 'Микроразметка', 'microdata', 'left', 1, 4),
(8, 'Выгрузки', 'export', 'left', 1, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `config_types`
--

CREATE TABLE `config_types` (
  `id` int(4) NOT NULL,
  `name` varchar(128) NOT NULL,
  `alias` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `config_types`
--

INSERT INTO `config_types` (`id`, `name`, `alias`) VALUES
(1, 'Однострочное текстовое поле', 'input'),
(2, 'Текстовое поле', 'textarea'),
(3, 'Выбор значения из списка', 'select'),
(4, 'Пароль', 'password'),
(5, 'Радиокнопка', 'radio'),
(6, 'Текстовое поле с редактором', 'tiny');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `text` text,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `created_at`, `updated_at`, `status`, `name`, `email`, `text`, `ip`) VALUES
(13, 1508505405, NULL, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `content`
--

CREATE TABLE `content` (
  `id` int(10) NOT NULL,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `alias` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `title` text CHARACTER SET cp1251,
  `description` text CHARACTER SET cp1251,
  `keywords` text CHARACTER SET cp1251,
  `text` text CHARACTER SET cp1251,
  `status` int(1) DEFAULT '1',
  `created_at` int(10) DEFAULT NULL,
  `h1` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `class` varchar(64) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `content`
--

INSERT INTO `content` (`id`, `name`, `alias`, `title`, `description`, `keywords`, `text`, `status`, `created_at`, `h1`, `updated_at`, `sort`, `parent_id`, `class`, `views`) VALUES
(19, 'О нас', 'o-nas', 'О нас', 'О нас', 'О нас', '<p><span style=\"color: #000000;\">&nbsp;&nbsp; <strong>Дорогие посетители и клиенты нашего сайта!</strong><br /> </span><br /><span style=\"color: #000000;\">&nbsp; <br />&nbsp;Интернет магазин одежды и обуви является на данный момент одним из самых перспективных и быстроразвивающихся магазинов молодежной одежды и обуви на территории Украины. В нашем ассортименте представлено более 4000 пар обуви, а также множество курток, пуховиков, футболок, толстовок, кепок и прочих аксессуаров. С каждым днем этот ассортимент увеличивается, в продажу входят все новые и новые модели, а вместе с ним растет и наш уровень обслуживания клиентов. <br /></span><br /><span style=\"color: #000000;\">&nbsp; &nbsp;Начиная с 2012-го года мы успели добиться ошеломительных результатов, расширив ассортимент до максимума и сделав его одним из самых крупных в Украине. За это время мы показали себя с лучшей стороны и накопили добротную базу постоянных клиентов. Изо дня в день мы стараемся стать лучше, наш сервис и обслуживание клиентов становится качественнее, риски связанные с покупкой товара в интернете становятся меньше, а цены и обилие моделей склоняют на свою сторону ежедневно десятки новых клиентов. </span></p>\r\n<p>&nbsp;</p>\r\n<p><br /><span style=\"color: #000000;\">&nbsp; &nbsp;Мы уверены, что вместе с вами, с вашей обоснованной критикой и заслуженной нами похвалой, мы достигнем огромных результатов, и с каждым днем будем баловать вас еще большим количеством качественной обуви и одежды! <br /></span><br /><span style=\"color: #000000;\">&nbsp; &nbsp;С Ув. персонал интернет магазина одежды и обуви!<br /><br /><br /><br /></span></p>\r\n<p style=\"text-align: left;\"><a href=\"http://blog.wezom.ua/2011/11/blog-post.html\" target=\"_blank\">&nbsp;test</a></p>', 1, 1447089913, 'О нас', 1508251581, 1, 0, NULL, 32),
(20, 'FAQ', 'faq', 'FAQ', 'FAQ', 'FAQ', '<h1>Часто задаваемые вопросы</h1>\r\n<p><strong><span style=\"text-decoration: underline;\"><span style=\"color: #000000; text-decoration: underline;\">Гарантия и возврат</span></span></strong></p>\r\n<p><br /><strong><span style=\"color: #000000;\">Есть ли гарантия на товар?</span></strong><br /><span style=\"color: #000000;\"><em>- В нашем интернет магазине предоставляется гарантия 2 месяца на весь ассортимент женской и мужской обуви.</em> </span><br /><br /><strong><span style=\"color: #000000;\">Что делать если обувь пришла бракованная?</span></strong><br /><span style=\"color: #000000;\"><em>- В случае если обувь, которую вы получили, имеет брак, обязательно свяжитесь с нами по одному из этих номеров <span style=\"text-decoration: underline;\">+380(66)915-25-45</span>; <span style=\"text-decoration: underline;\">+380(93)285-75-05</span> или напишите нам на e-mail: <span style=\"text-decoration: underline;\">office@wezom.com.ua</span>. Мы постараемся в кротчайшие сроки сделать возврат или обмен товара.</em> </span><br /><br /><strong><span style=\"color: #000000;\">Что делать если в период гарантийного срока на обуви стали проявляться скрытые дефекты?</span></strong><br /><span style=\"color: #000000;\"><em>- В таком случае обязательно свяжитесь с нами по одному из этих номеров <span style=\"text-decoration: underline;\">+380(66)915-25-45</span>;&nbsp;<span style=\"text-decoration: underline;\">+380(93)285-75-05</span>&nbsp;или напишите нам на e-mail: <span style=\"text-decoration: underline;\">office@wezom.com.ua</span>. Мы постараемся в кротчайшие сроки сделать возврат или обмен товара.</em> </span><br /><br /><strong><span style=\"color: #000000;\">Есть ли гарантия на одежду?</span></strong><br /><em><span style=\"color: #000000;\">- Наш интернет магазин не предоставляет гарантию на одежду и головные уборы. </span></em><br /><br /><strong><span style=\"color: #000000;\">Что делать если обувь или одежда не подошли по размеру?</span></strong><br /><em><span style=\"color: #000000;\">-Если случилось так, что придя домой, вы поняли, что обувь или одежда не подходит вам по размеру, свяжитесь обязательно с нами, и мы осуществим возврат или обмен товара.</span></em><br /><br /><strong><span style=\"color: #000000;\">Как вернуть товар?</span></strong><br /><span style=\"color: #000000;\"><em>-Вы идете в отделение новой почты и высылаете товар на указанные реквизиты. При этом все затраты связанные с доставкой товара вы берете на себя. Дабы минимизировать эти затраты, мы предлагаем отправлять товар без наложенного платежа, и при получении его нами, деньги в полном объеме будут перечислены на указанный вами номер банковской карточки.</em> </span><br /><br /><strong><span style=\"color: #000000;\">В какой период я могу вернуть или обменять товар?</span></strong><br /><span style=\"color: #000000;\"><em>-Если вещь, которую вы хотите вернуть сохранила товарный вид, а так же тару (коробка, пакет) в которой она поставлялась, вы можете вернуть ее в течение 14 дней с момента получения на почте. Если же это обмен, после получения нами товара, мы в этот же день высылаем вам другой надлежащего качества и размера.</em> </span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"text-decoration: underline;\"><strong><span style=\"color: #000000; text-decoration: underline;\">Оплата</span></strong></span></p>\r\n<p><br /><strong><span style=\"color: #000000;\">Можно ли оплатить товар при получении?</span></strong><br /><em><span style=\"color: #000000;\">-Так как мы высылаем товар наложенным платежом, вы оплачиваете его при получении на почте. Если же это курьерская доставка, оплата производится при личной встрече.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Могу ли я оплатить полностью всю стоимость товара на карточку?</span></strong><br /><em><span style=\"color: #000000;\">-Вы можете оплатить полностью всю стоимость товара на карточку до того как он будет отправлен вам. Просим заранее сообщать об этом.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Какие способы электронной оплаты вы поддерживаете?</span></strong><br /><em><span style=\"color: #000000;\">-Вы можете оплатить товар прямо на сайте через систему электронных платежей Приват24 или же через терминал по номеру банковской карточки.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Могу ли я примерять товар перед оплатой?</span></strong><br /><em><span style=\"color: #000000;\">-Да, вы можете примерять товар перед оплатой на почте. При этом вы оставляете задаток оператору в размере полной стоимости товара, после чего идете делать осмотр и примерку. Если товар вам подходит, вы оплачиваете доставку и забираете его. Если же нет, пишите заявление на возврат и возвращаете товар обратно нам.</span></em><br /><br /><br /><strong><span style=\"text-decoration: underline;\"><span style=\"color: #000000; text-decoration: underline;\">Доставка</span></span></strong></p>\r\n<p><br /><br /><strong><span style=\"color: #000000;\">Как происходит доставка товара?</span></strong><br /><em><span style=\"color: #000000;\">-Доставка товара осуществляется службой экспресс доставки Новая почта.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Если я с Днепропетровска, могу ли я забрать товар лично при встрече?</span></strong><br /><em><span style=\"color: #000000;\">-Да, вы можете забрать товар лично при встрече, заранее оговорив с оператором место и время.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Сколько по времени занимает доставка товара?</span></strong><br /><em><span style=\"color: #000000;\">-Срок доставки товара в наличии составляет 1-3 дня по территории Украины. Если же товара в наличии нет, срок доставки увеличивается до 14-28 дней.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Какими курьерскими службами вы отправляете товар?</span></strong><br /><em><span style=\"color: #000000;\">-В данный момент доставка осуществляется только компанией Новая почта.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Как я узнаю о том, что товар пришел?</span></strong><br /><em><span style=\"color: #000000;\">-Мы обязательно связываемся с клиентом за 1-2 дня до того как товар поступит на наш склад в Днепропетровск. Когда товар будет доставлен на отделение Новой почты в вашем городе, на ваш номер придет sms оповещение о том, что товар прибыл.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Кто оплачивает доставку?</span></strong><br /><em><span style=\"color: #000000;\">-Доставку оплачивает клиент. В стоимость товара не входит сума доставки.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Cколько стоит доставка?</span></strong><br /><em><span style=\"color: #000000;\">-Стоимость доставки рассчитывается индивидуально, и зависит от объема и стоимости товара, а так же от отдаленности между городом отправителя и городом получателя. В среднем стоимость доставки одной пары кроссовок или одной футболки составляет от 25 до 35 грн. Так же клиент оплачивает обратную доставку денег 30-40 грн. В общем, средняя стоимость доставки составляет 55-75 грн.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Нужны ли документы для получения товара?</span></strong><br /><em><span style=\"color: #000000;\">-При получении товара вы обязаны предъявить документы оператору, подтверждающие вашу личность. Это может быть как паспорт, так и водительские права.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Если я несовершеннолетний?</span></strong><br /><em><span style=\"color: #000000;\">-Если вы еще не достигли совершеннолетия, ваши родители могут получить товар за вас, при этом в заказе следует указывать Фамилию и Имя одного из родителей.</span></em><br /><br /><br /><span style=\"text-decoration: underline;\"><strong><span style=\"color: #000000; text-decoration: underline;\">Качество товара и производство </span></strong></span></p>\r\n<p><br /><br /><strong><span style=\"color: #000000;\">Товар оригинальный?</span></strong><br /><em><span style=\"color: #000000;\">-Мы занимаемся исключительно качественными копиями, товар не оригинальный. На всю обувь предоставляется гарантия 2 месяца, а так же право возврата и обмена товара.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Качество хорошее?</span></strong><br /><em><span style=\"color: #000000;\">-Так как у каждого человека свое субъективное понимание качественного товара, мы не вправе внушать ему свою точку зрения и свое мнение. Поэтому, мы предоставляем клиенту возможность перед оплатой тщательно осмотреть товар и примерять его. Мы занимаемся высококачественными копиями и предоставляем гарантию на всю обувь.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Кто производитель?</span></strong><br /><em><span style=\"color: #000000;\">-Весь товар производится в Китае.</span></em></p>\r\n<p><strong><span style=\"color: #000000;\">Чем отличается оригинальные модели от копий (реплик)?</span></strong><br /><em><span style=\"color: #000000;\">-В отличие от оригинальных моделей в копиях используются искусственные материалы, искусственная кожа и искусственная замша. По всем же остальным параметрам они полностью соответствуют оригинальным моделям.</span></em></p>\r\n<p style=\"text-align: left;\"><strong><span style=\"color: #000000;\">Есть ли у вас оригинал?</span></strong><br /><em><span style=\"color: #000000;\">-Мы занимаемся исключительно качественными копиями, совмещая доступную цену с хорошим качеством товара. Возможно, в ближайшем будущем у нас появятся и оригинальные модели.</span></em><br /><br /><span style=\"text-decoration: underline;\"><strong><span style=\"color: #000000; text-decoration: underline;\"><br />Размеры</span></strong></span></p>\r\n<p style=\"text-align: left;\"><strong><span style=\"color: #000000;\"><br />Как правильно подобрать размер обуви?</span></strong><br /><em><span style=\"color: #000000;\">-Приоритетом при выборе размера является длина стопы в сантиметрах. Но так как при замере часто возникают погрешности, мы ориентируемся на обозначения с размерами, которые указаны на ваших повседневных кроссовках с внутренней стороны язычка.&nbsp;</span></em></p>\r\n<p style=\"text-align: left;\"><strong><span style=\"color: #000000;\">У вас обувь маломерная?</span></strong><br /><em><span style=\"color: #000000;\">-Да, все наша обувь маломерная, поэтому советуем вам, выбирать из двух размеров которые вы носите, самый большой. Это и будет европейский размер. Например, если вам подходит и 37-й и 38-й размеры, значит заказывать нужно 38-й.</span></em></p>\r\n<p style=\"text-align: left;\"><strong><span style=\"color: #000000;\">Как подобрать размер куртки или футболки?</span></strong><br /><em><span style=\"color: #000000;\">-Так как у нас все куртки и футболки маломерные, и буквенные обозначения не совпадают с повседневными, размер подбирать следует исходя из роста и веса. Зная два этих параметра, мы можем более точно выбрать вам подходящий размер.&nbsp;<br /><br /></span></em></p>', 1, 1447090050, 'FAQ', 1508251581, 2, 0, NULL, 37),
(21, 'Доставка', 'dostavka', 'Доставка', 'Доставка', 'Доставка', '<p><span style=\"color: #000000;\"><strong>Сроки доставки</strong></span><br /><br /><span style=\"color: #000000;\">&nbsp; <em>Доставка товара под заказ</em> &ndash; подразумевает под сбой то, что товара в данный момент нет в наличии, он находится за границей, и срок его доставки составит от 14&nbsp;до 28&nbsp;дней. При этом время, затрачиваемое на доставку товара по территории Украины не учитывается.<br /></span><br /><span style=\"color: #000000;\">&nbsp; <em>Доставка товара в наличии</em> &ndash; самый быстрый способ, так как товар находится на складе в Днепропетровске, и срок его доставки составит 1-3 дня.</span></p>\r\n<p><br /><span style=\"color: #000000;\"><strong>Способы доставки</strong></span><br /><br /><span style=\"color: #000000;\">&nbsp; <em>Курьерская доставка</em> &ndash; осуществляется курьером интернет магазина, и подразумевает под собой передачу товара непосредственно в руки клиенту при личной встрече. Время и место оговариваются после того, как товар поступил на склад в город Днепропетровск.<br /></span><br /><span style=\"color: #000000;\">&nbsp; <em>Доставка Новой почтой</em> - осуществляется лидером экспресс доставки в Украине, компанией &laquo;Нова пошта&raquo; и составляет 1-3 дня с момента поступления товара на склад в город Днепропетровск. Все адреса и номера отделений вы можете посмотреть непосредственно на сайте компании отправите<img class=\"\" src=\"http://www.kitco.com/images/live/silver.gif\" alt=\"\" width=\"630\" height=\"400\" />ля: http://novaposhta.ua/</span><br /><br /></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #000000;\"><strong>Стоимость доставки</strong></span><br /><br /><span style=\"color: #000000;\">&nbsp; <em>Курьерская доставка</em> по городу Днепропетровск бесплатная.</span><br /><br /><span style=\"color: #000000;\">&nbsp; <em>Доставка Новой почтой</em> рассчитывается индивидуально, и зависит от объема и стоимости товара, а так же от отдаленности между городом отправителя и городом получателя. В среднем стоимость доставки одной пары кроссовок или одной футболки составляет от 25 до 35 грн. Так же клиент оплачивает обратную доставку денег 30-40 грн. В общем, средняя стоимость доставки составляет 55-75 грн.</span><br /><br /><span style=\"color: #000000;\"><em><strong>Важно!</strong></em></span><br /><br /><span style=\"color: #000000;\">&nbsp; 1.&nbsp;<em>В связи с непредвиденными обстоятельствами, такими как пожары, наводнения, или другие стихийные бедствия на территории страны производителя, а так же в связи с праздниками или затрудненной работой таможенной службы, возможны задержки товара на незначительный срок. <br />&nbsp; 2. Доставка осуществляется только по территории Украины и не распространяется на территорию России, Белоруссии, а так же территории других стран пост советского пространства.</em></span></p>', 1, 1447090182, 'Доставка', 1508251581, 5, 0, NULL, 28),
(22, 'Оплата', 'oplata', 'Оплата', 'Оплата', 'Оплата', '<p style=\"text-align: left;\"><span style=\"color: #000000;\">&nbsp; Ни для кого не секрет, что при покупке в интернет магазине, человек подвергается определенным рискам, одним из которых является оплата товара. Во многих интернет магазинах она осуществляется еще до того как клиент получает свой заказ. При этом он может ожидать от продавца чего угодно: получения некачественного товара, несоответствие размеров или цветов, а в худшем случае продавец вообще пропадет сразу же после получении 100%-й оплаты товара.<br /></span><br /><span style=\"color: #000000;\">&nbsp; &nbsp;Дабы минимизировать эти риски и сделать покупку товара в интернет магазине более безопасной, мы предоставляем несколько способов оплаты товара.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #000000;\">&nbsp; <span style=\"text-decoration: underline;\">Наложенным платежом (более безопасный)</span> &ndash; осуществляется при встрече с курьером интернет магазина или же в отделении Новой почты при получении товара, при этом, клиент оплачивает обратную доставку денег, величина которой зависит от стоимости товара и в среднем составляет 30-40 грн.</span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #000000;\">&nbsp; <span style=\"text-decoration: underline;\">Электронный перевод (менее затратный)</span> &ndash; осуществляется на карточку Приватбанка через терминал или же через систему электронных платежей Приват24. В случае перевода на карточку Приватбанка через терминал, реквизиты для оплаты высылаются интернет магазином на e-mail клиента или же на мобильный номер. После перевода средств, клиент обязуется, сообщит продавцу точное время и суму зачисления. Выбрав этот способ, покупатель экономит примерно 30-40 грн., на обратной доставке денег.<br /></span></p>', 1, 1447090201, 'Оплата', 1508251581, 4, 0, NULL, 1),
(23, 'Гарантия', 'garantija', 'Гарантия', 'Гарантия', 'Гарантия', '<p style=\"text-align: left;\"><span style=\"color: #000000;\">&nbsp; По статистике, при выборе интернет магазина, 70% потенциальных покупателей уделяют особое внимание наличию предоставления интернет магазином гарантии на товар. Это очень важный&nbsp;момент и мы уделяем ему особое внимание.&nbsp;<br /><br />&nbsp; Гарантия предоставляется на весь сегмент обуви в нашем интернет магазине и составляет 2 месяца с момента получения товара клиентом.<br /></span><br /><span style=\"color: #000000;\">&nbsp; Если в период гарантийного срока, в процессе носки появились непредвиденные дефекты (отрывы по шву или отклеивание), вы имеете право на обмен товара или возврат денег. В случае возникновения такой ситуации, вы должны как можно быстрее сообщить об этом продавцу, предоставив несколько фотографий места дефекта, а так же общего вида обуви.<br /></span><br /><span style=\"color: #000000;\">&nbsp; После проведенной оценки и просмотра фотографий, мы принимаем решение о возврате или же обмене товара.</span></p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p style=\"text-align: left;\">&nbsp;</p>\r\n<p><br /><br /></p>', 1, 1447090225, 'Гарантия', 1508251581, 3, 0, NULL, 1),
(24, 'Возврат', 'vozvrat', 'Возврат', 'Возврат', 'Возврат', '<p style=\"text-align: left;\">&nbsp;<span style=\"color: #000000;\">&nbsp; В случае если вам не подошел размер, не понравилась сама модель или же имеют место быть причины, о которых вы не считаете нужным уведомить продавца, вы имеете право вернуть товар в течение 14-ти дней с момента его получения. При этом вещь должна быть неношеной, иметь товарный вид и тару (коробку, пакет) в котором изначально поставлялась вам.</span><br /><br /><br /><span style=\"color: #000000;\"><strong><span style=\"text-decoration: underline;\">Процесс возврата и обмена товара</span></strong></span><br /><br /><span style=\"color: #000000;\">&nbsp; Уведомив продавца о возврате и согласовав с ним все моменты, клиент возвращает товар на полученные им реквизиты, новой почтой. При этом все затраты связанные с доставкой он берет на себя. Рекомендуем высылать товар без наложенного платежа, дабы минимизировать затраты связанные с обратной доставкой денег, предварительно указав продавцу ваши реквизиты (Приватбанк). После получения товара, в течение двух дней продавец обязуется перечислить деньги в полном объеме на номер карты, указанной покупателем.<br /></span></p>\r\n<p style=\"text-align: left;\"><span style=\"text-decoration: underline; color: #000000;\"><em><strong>Важно!</strong></em></span></p>\r\n<p style=\"text-align: left;\"><span style=\"color: #000000;\"><em><span style=\"text-decoration: underline;\">Возврату и обмену не подлежит товар, если:</span> <br />- имеет царапины и порезы, связанные с неправильной его ноской; <br />- бывший в ремонте; <br />- имеет потертый или грязный вид; <br />- не имеет тары (коробка, пакет), в которой изначально поставлялся покупателю.</em></span></p>', 1, 1447090248, 'Возврат', 1508251581, 0, 0, NULL, 1),
(25, 'Bogdan', 'b', '', '', '', '<p><a title=\"Для того чтобы заменить фото в галерее нужно\" href=\"http://cms.wezom.ks.ua/Media/files/filemanager/%D0%94%D0%BB%D1%8F%20%D1%82%D0%BE%D0%B3%D0%BE%20%D1%87%D1%82%D0%BE%D0%B1%D1%8B%20%D0%B7%D0%B0%D0%BC%D0%B5%D0%BD%D0%B8%D1%82%D1%8C%20%D1%84%D0%BE%D1%82%D0%BE%20%D0%B2%20%D0%B3%D0%B0%D0%BB%D0%B5%D1%80%D0%B5%D0%B5%20%D0%BD%D1%83%D0%B6%D0%BD%D0%BE.docx\">Для того чтобы заменить фото в галерее нужно</a></p>', 0, 1458158661, '', 1508251581, 0, 22, NULL, 1),
(26, 'bogdan2', 'b2', 'zagotitle', '', 'ada ada adad   ', '<p>dasdasd</p>\r\n<h1 style=\"text-align: center;\"><span style=\"font-family: \'times new roman\', times; font-size: 18pt;\">asdassad</span></h1>\r\n<p>dasd</p>\r\n<p>asd</p>', 1, 1458158994, 'zagolovok', 1508251581, 0, 25, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `control`
--

CREATE TABLE `control` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `alias` varchar(32) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `other` text,
  `updated_at` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `short_text` varchar(255) DEFAULT NULL,
  `image2` varchar(255) DEFAULT NULL,
  `image3` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `control`
--

INSERT INTO `control` (`id`, `name`, `h1`, `title`, `keywords`, `description`, `text`, `alias`, `status`, `other`, `updated_at`, `image`, `short_text`, `image2`, `image3`) VALUES
(1, 'Главная страница', 'Главная страница h1', 'Главная страница title', 'Главная страница keywords', 'Главная страница description', '<p><big>Мы готовы предоставить широкий ассортимент продукции, предназначенной для различных потребностей и типов помещений:</big></p>\r\n<p><a href=\"/\"> <strong>Мягкая мебель</strong> </a> &ndash; как никакая другая, поможет расслабиться после тяжелого трудового дня и добавит немного дополнительного комфорта для семейного времяпровождения за просмотром кино. В нашем каталоге представлены модели, способные удовлетворить даже самых требовательных покупателей;</p>\r\n<p><a href=\"/\"> <strong>Кабинеты руководителей</strong> </a> &ndash; представлены в виде линейки брендовых многофункциональных и презентабельных модульных конструкций, из качественных и долговечных материалов. Такая система станет отличными дополнением для каждого помещения;</p>\r\n<p><a href=\"/\"> <strong>Офисная мебель</strong> </a> &ndash; всегда солидная и лаконичная продукция. Характеризуется легкостью в обслуживании, низкой ценой, высокой функциональностью, длительным эксплуатационным периодом. У нас, Вы всегда сможете подобрать офисных стол, шкаф, тумбу, комод и прочие атрибуты современного офиса;</p>\r\n<p><a href=\"/\"> <strong>Компьютерные столы</strong> </a> &ndash; разработаны с учетом анатомический особенностей человека с применением эргономичных элементов для организации удобной и эффективной рабочей зоны сотрудника. Комплектуются дополнительными элементами различной направленности: полочками, надстройками, выдвижными ящиками и прочим;</p>\r\n<p><a href=\"/\"> <strong>Шкафы-купе</strong> </a> &ndash; качественные и вместительные шкафы от производителя по самым низким в городе ценам. Выполнены из влагостойких и надежных материалов на основе плит ДСП/МДФ. Комплектуются системой полочек для хранения вещей, а также различными нишами и ящиками. В наличии модификации с пескоструйными и пленочными рисунками;</p>\r\n<p><a href=\"/\"> <strong>Кухни</strong> </a> &ndash; представлена готовыми, а также модульными функциональными конструкциями, отличительной особенностью которых является применение материалов, устойчивых к воздействию агрессивных веществ (вода, моющие средства, ультрафиолет и прочие). Наши кухни, обязательно станут отличным компаньоном и незаменимым инструментом в приготовлении еды;</p>\r\n<p><a href=\"/\"> <strong>Школьная мебель</strong> </a> &ndash; широкий ассортимент недорогой и качественной мебели для образовательных учреждений. Одноместные и двухместные школьные парты, стулья на основе металлокаркаса;</p>\r\n<p><a href=\"/\"> <strong>Детские</strong> </a> &ndash; все изделия выполнены из экологически чистых и противоаллергенных материалов. Как правило, выполнены в красочной цветовой гамме, имеют необычную форму, которая часто напоминает детям любимых персонажей из мультфильмов, сказок и игр;</p>\r\n<p><a href=\"/\"> <strong>Гостиные</strong> </a> &ndash; большой выбор готовых и модульных систем для обустройства гостиной комнаты по низким ценам. Популярные стили, а также цветовые решения никого не оставят равнодушным;</p>\r\n<p><a href=\"/\"> <strong>Спальни</strong> </a> &ndash; модульная мебель для спальной комнаты обязательно создаст тонкую атмосферу, для комфортного отдыха после тяжелого рабочего дня;</p>\r\n<p><a href=\"/\"> <strong>Прихожие</strong> </a> &ndash; в нашем каталоге, каждый сможет подобрать вариант, который будет выполнять возложенную на него функцию: от навесной полки с крючками для хранения одежды, до мебельной стенки;</p>\r\n<p><a href=\"/\"> <strong>Кресла и стулья</strong> </a> &ndash; различаются по своему предназначению, конструктивными особенностям, а также материалам применимых при изготовлении. Наиболее популярные следующие подгруппы для: директора, персонала, ребенка, посетителя;</p>\r\n<p><a href=\"/\"> <strong>Каркас кровати</strong> </a> &ndash; изделия с металлической, или деревянной основной. В качестве опорной части выступают ламели. Являются незаменимым продуктом при приобретении кровати;</p>\r\n<p><a href=\"/\"> <strong>Матрасы</strong> </a> &ndash; недороги и качественные изделия с ортопедическими свойствами для комфортного отдыха и здорового сна.</p>', 'index', 1, NULL, 1504005323, '4d78ce39ea8e0376376780897fb4763f.jpg', NULL, NULL, NULL),
(2, 'Контакты', '', 'Контакты', 'Контакты', 'Контакты', '<p><strong>Контактная информация компании Интернет-магазин \"Airpac\"</strong></p>\r\n<p><br />Название:&nbsp;&nbsp; &nbsp;Интернет-магазин одежды и обуви<br />Контактное лицо:&nbsp;&nbsp;&nbsp; Администратор<br />Адрес:&nbsp;&nbsp; &nbsp;Только online-магазин, Украина<br />Телефон:&nbsp;&nbsp; &nbsp;<br />+38(XXX)XXX-XX-XX<br />+38(XXX)XXX-XX-XX<br />Email:&nbsp;&nbsp;&nbsp; <a href=\"mailto:office@wezom.com.u\">office@wezom.com.ua</a></p>', 'contacts', 1, '{\"latitude\":\"46.96647\",\"longitude\":\"31.961573999999928\"}', 1504253587, NULL, NULL, NULL, NULL),
(4, 'Корзина', '', 'Корзина', 'Корзина', 'Корзина', NULL, 'cart', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Новости', '', 'Новости', 'Новости', 'Новости', NULL, 'news', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Поиск', 'Поиск', 'Поиск', 'Поиск', 'Поиск', NULL, 'search', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', 'Карта сайта', NULL, 'sitemap', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Каталог продукции', 'Каталог', 'Каталог', 'Каталог', 'Каталог', NULL, 'products', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Популярные', 'Популярные товары', 'Популярные товары', 'Популярные товары', 'Популярные товары', NULL, 'popular', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Акции', '', 'Акции', 'Акции', 'Акции', '<p>fhfhfghfghfhfh</p>', 'sale', 1, NULL, 1483519150, NULL, NULL, NULL, NULL),
(13, 'Просмотренные', 'Просмотренные', 'Просмотренные', 'Просмотренные', 'Просмотренные', NULL, 'viewed', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Отзывы', '', 'Отзывы', 'Отзывы', 'Отзывы', NULL, 'reviews', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Условия использования', 'Условия использования', 'Условия использования', 'Условия использования', 'Условия использования', 'Условия использования', 'terms-of-use', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Мебель на заказ', 'Мебель на заказ', 'Мебель на заказ', 'Мебель на заказ', 'Мебель на заказ', '<p>Продукция нашей компании всегда отличалась высоким уровнем качества, стильным внешним видом и широкой функциональностью. Мебель индивидуальным размерам изготавливается в кратчайшие сроки, а собранный за многие годы автопарк позволяет безопасно и быстро доставлять товар в любою точку города и области.</p>\r\n<p>Что бы обсудить особенности процесса изготовления корпусной мебели, а также получить ответы на возникшие вопросы, Вам следует оставить заявку в &ldquo;контактной форме&rdquo;, или же позвонить по телефону, указанному в шапке сайта. Наши технологи и дизайнеры приедут в удобное для Вас время, для того, чтобы снять замеры помещения, а также составить первичный эскиз.</p>', 'for-order', 1, NULL, 1504007970, '4015eaafc6979de05a4e91dc7e999f3d.jpg', '<p>Мебель на заказ порой становится единственным верным и возможным решением, ведь фабричные изделия изготавливаются по стандарту, не учитывая всех особенностей планировки помещения. В таких ситуациях, индивидуальная мебель приходит на помощь.</p>', NULL, NULL),
(17, 'О компании', 'О компании', 'О компании', 'О компании', 'О компании', '<p>Компания \"МЕБЕЛЬ-АРТ\" &ndash; это профессионалы мебельного искусства, это искусство делать мебель! Всё началось в 2000 году с обычного гаража - тщательность сборки, требовательность к качеству исходных материалов и к готовой продукции с первого дня стали основными принципами компании, остаются ими и сейчас. В процессе роста и развития компании появился опыт, пришли надёжные поставщики, совершенствовались технологии производства и оборудование, появилось умение экономить, сохраняя высокое качество.</p>', 'about', 1, '[{\"name\":\"\\u0420\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435\",\"text\":\"\\u041d\\u0430 \\u0441\\u0435\\u0433\\u043e\\u0434\\u043d\\u044f - \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\\"\\u041c\\u0415\\u0411\\u0415\\u041b\\u042c-\\u0410\\u0420\\u0422\\\" \\u0438\\u043c\\u0435\\u0435\\u0442 \\u0441\\u0430\\u043c\\u043e\\u0435 \\u0441\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u043e\\u0435 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u043e \\u043f\\u043e \\u0438\\u0437\\u0433\\u043e\\u0442\\u043e\\u0432\\u043b\\u0435\\u043d\\u0438\\u044e \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u043d\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0434\\u043b\\u044f \\u0434\\u043e\\u043c\\u0430 \\u0438 \\u043e\\u0444\\u0438\\u0441\\u0430, \\u0441\\u0435\\u0442\\u044c \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u043e\\u0432 \\u043f\\u043e \\u043e\\u043f\\u0442\\u043e\\u0432\\u043e\\u0439 \\u0438 \\u0440\\u043e\\u0437\\u043d\\u0438\\u0447\\u043d\\u043e\\u0439 \\u043f\\u0440\\u043e\\u0434\\u0430\\u0436\\u0435, \\u0440\\u0430\\u0441\\u043f\\u043e\\u043b\\u043e\\u0436\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043d\\u0435\\u043f\\u043e\\u0441\\u0440\\u0435\\u0434\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u043e \\u0440\\u044f\\u0434\\u043e\\u043c \\u0441 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0441\\u0442\\u0432\\u043e\\u043c, \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d-\\u0441\\u0442\\u0443\\u0434\\u0438\\u044e, \\u0438\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442-\\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0432 \\u041d\\u0438\\u043a\\u043e\\u043b\\u0430\\u0435\\u0432\\u0435 \\u0438 \\u0434\\u0440\\u0443\\u0433\\u0438\\u0445 \\u0433\\u043e\\u0440\\u043e\\u0434\\u0430\\u0445 \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u044b.\\u0412\\u043e \\u0432\\u0441\\u0435\\u0445 \\u044d\\u0442\\u0438\\u0445 \\u043f\\u043e\\u0434\\u0440\\u0430\\u0437\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f\\u0445 \\u0442\\u0440\\u0443\\u0434\\u044f\\u0442\\u0441\\u044f \\u043f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044b \\u0432\\u044b\\u0441\\u043e\\u043a\\u043e\\u0433\\u043e \\u0443\\u0440\\u043e\\u0432\\u043d\\u044f, \\u0441\\u043f\\u043e\\u0441\\u043e\\u0431\\u043d\\u044b\\u0435 \\u0440\\u0435\\u0430\\u043b\\u0438\\u0437\\u043e\\u0432\\u0430\\u0442\\u044c \\u0441\\u0430\\u043c\\u044b\\u0435 \\u0441\\u043c\\u0435\\u043b\\u044b\\u0435 \\u043f\\u043e\\u0436\\u0435\\u043b\\u0430\\u043d\\u0438\\u044f \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u043c\\u043e\\u0433\\u0443\\u0442 \\u0440\\u0435\\u0448\\u0430\\u0442\\u044c \\u0441\\u043b\\u043e\\u0436\\u043d\\u044b\\u0435 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0435 \\u0437\\u0430\\u0434\\u0430\\u0447\\u0438. \\u0411\\u043b\\u0438\\u0437\\u043e\\u0441\\u0442\\u044c \\u0444\\u0430\\u0431\\u0440\\u0438\\u043a\\u0438 \\u0438 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u043e\\u0432 \\u0434\\u0430\\u0451\\u0442 \\u044d\\u043a\\u043e\\u043d\\u043e\\u043c\\u0438\\u044e \\u0442\\u0440\\u0430\\u043d\\u0441\\u043f\\u043e\\u0440\\u0442\\u043d\\u044b\\u0445 \\u0440\\u0430\\u0441\\u0445\\u043e\\u0434\\u043e\\u0432, \\u044d\\u0442\\u043e \\u0434\\u0435\\u043b\\u0430\\u0435\\u0442 \\u043d\\u0430\\u0448\\u0443 \\u043c\\u0435\\u0431\\u0435\\u043b\\u044c \\u0431\\u043e\\u043b\\u0435\\u0435 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0439 \\u043f\\u043e \\u0446\\u0435\\u043d\\u0435.\"},{\"name\":\"\\u0426\\u0435\\u043b\\u044c\",\"text\":\"\\u0426\\u0435\\u043b\\u044c \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 - \\u043f\\u043e\\u0432\\u044b\\u0448\\u0435\\u043d\\u0438\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439, \\u0440\\u0430\\u0441\\u0448\\u0438\\u0440\\u0435\\u043d\\u0438\\u0435 \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442\\u0430, \\u0437\\u0430\\u0432\\u043e\\u0435\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u0443\\u0434\\u0435\\u0440\\u0436\\u0430\\u043d\\u0438\\u0435 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u043a \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\\"\\u041c\\u0415\\u0411\\u0415\\u041b\\u042c-\\u0410\\u0420\\u0422\\\", \\u0443\\u0432\\u0435\\u043b\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0447\\u0438\\u0441\\u043b\\u0430 \\u043f\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u043d\\u044b\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432, \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u0435 \\u043d\\u0430\\u0438\\u043b\\u0443\\u0447\\u0448\\u0435\\u0433\\u043e \\u0441\\u043e\\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u044f \\u0446\\u0435\\u043d\\u044b \\u0438 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u0438.\"},{\"name\":\"\\u041c\\u0438\\u0441\\u0441\\u0438\\u044f\",\"text\":\"\\u041c\\u044b \\u0432\\u0438\\u0434\\u0438\\u043c \\u0441\\u0432\\u043e\\u0451 \\u043f\\u0440\\u0435\\u0434\\u043d\\u0430\\u0437\\u043d\\u0430\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0432 \\u0442\\u043e\\u043c, \\u0447\\u0442\\u043e\\u0431\\u044b \\u043a\\u0430\\u0436\\u0434\\u044b\\u0439 \\u043d\\u0430\\u0448 \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044c \\u0432\\u043c\\u0435\\u0441\\u0442\\u0435 \\u0441 \\u043c\\u0435\\u0431\\u0435\\u043b\\u044c\\u044e \\u043f\\u0440\\u0438\\u043e\\u0431\\u0440\\u0435\\u0442\\u0430\\u043b \\u043a\\u043e\\u043c\\u0444\\u043e\\u0440\\u0442, \\u0443\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432\\u043e \\u0438 \\u043d\\u0430\\u0434\\u0451\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u043f\\u043e \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0439 \\u0446\\u0435\\u043d\\u0435.\"},{\"name\":\"\\u041f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u044f\",\"text\":\"\\u041c\\u044b \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c \\u043a\\u0443\\u043f\\u0438\\u0442\\u044c \\u043c\\u0435\\u0431\\u0435\\u043b\\u044c \\u0434\\u043b\\u044f \\u0434\\u043e\\u043c\\u0430: \\u0432 \\u0433\\u043e\\u0441\\u0442\\u0438\\u043d\\u0443\\u044e, \\u0432 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u044e, \\u0432 \\u0434\\u0435\\u0442\\u0441\\u043a\\u0443\\u044e \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0443, \\u0432 \\u043a\\u0443\\u0445\\u043d\\u044e, \\u0432 \\u043f\\u0440\\u0438\\u0445\\u043e\\u0436\\u0443\\u044e. \\u041c\\u0435\\u0431\\u0435\\u043b\\u044c \\u0434\\u043b\\u044f \\u043e\\u0444\\u0438\\u0441\\u0430: \\u043f\\u0438\\u0441\\u044c\\u043c\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0438 \\u043a\\u043e\\u043c\\u043f\\u044c\\u044e\\u0442\\u0435\\u0440\\u043d\\u044b\\u0435 \\u0441\\u0442\\u043e\\u043b\\u044b, \\u0441\\u0442\\u0443\\u043b\\u044c\\u044f \\u0438 \\u043a\\u0440\\u0435\\u0441\\u043b\\u0430, \\u0448\\u043a\\u0430\\u0444\\u044b \\u0438 \\u0441\\u0442\\u0435\\u043b\\u043b\\u0430\\u0436\\u0438, \\u043a\\u0430\\u0431\\u0438\\u043d\\u0435\\u0442\\u044b \\u0440\\u0443\\u043a\\u043e\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u0435 \\u0434\\u0440\\u0443\\u0433\\u043e\\u0435. \\u041d\\u0430 \\u0432\\u0441\\u044e \\u043c\\u0435\\u0431\\u0435\\u043b\\u044c \\u0440\\u0430\\u0441\\u043f\\u0440\\u043e\\u0441\\u0442\\u0440\\u0430\\u043d\\u044f\\u0435\\u0442\\u0441\\u044f \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430, \\u043c\\u044b \\u043e\\u0431\\u0435\\u0441\\u043f\\u0435\\u0447\\u0438\\u0432\\u0430\\u0435\\u043c \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0443 \\u0438 \\u0441\\u0431\\u043e\\u0440\\u043a\\u0443 \\u043a\\u0443\\u043f\\u043b\\u0435\\u043d\\u043d\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0434\\u043e\\u043c\\u043e\\u0439 \\u0438 \\u043d\\u0430 \\u043f\\u0440\\u0435\\u0434\\u043f\\u0440\\u0438\\u044f\\u0442\\u0438\\u044f.\"},{\"name\":\"\\u041f\\u043b\\u0430\\u043d\\u044b\",\"text\":\"\\u041c\\u044b \\u043f\\u043b\\u0430\\u043d\\u0438\\u0440\\u0443\\u0435\\u043c \\u043f\\u0440\\u043e\\u0434\\u043e\\u043b\\u0436\\u0430\\u0442\\u044c \\u0440\\u0430\\u0431\\u043e\\u0442\\u0443 \\u043f\\u043e \\u043f\\u043e\\u0432\\u044b\\u0448\\u0435\\u043d\\u0438\\u044e \\u043a\\u043e\\u043d\\u0442\\u0440\\u043e\\u043b\\u044f \\u043d\\u0430\\u0434 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e\\u043c \\u0432\\u044b\\u043f\\u0443\\u0441\\u043a\\u0430\\u0435\\u043c\\u043e\\u0439 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u0438, \\u0443\\u043b\\u0443\\u0447\\u0448\\u0430\\u0442\\u044c \\u0443\\u0440\\u043e\\u0432\\u0435\\u043d\\u044c \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439, \\u0441\\u043e\\u0437\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c \\u043d\\u043e\\u0432\\u044b\\u0435, \\u0441\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u044b\\u0435 \\u044d\\u043b\\u0435\\u043c\\u0435\\u043d\\u0442\\u044b \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438. \\u0414\\u043b\\u044f \\u0440\\u0430\\u0441\\u0448\\u0438\\u0440\\u0435\\u043d\\u0438\\u044f \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442\\u0430 \\u2013 \\u043f\\u0440\\u0438\\u0432\\u043b\\u0435\\u043a\\u0430\\u0442\\u044c \\u043a \\u0441\\u043e\\u0442\\u0440\\u0443\\u0434\\u043d\\u0438\\u0447\\u0435\\u0441\\u0442\\u0432\\u0443 \\u0434\\u0440\\u0443\\u0433\\u0438\\u0445\\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u0435\\u0439.\"},{\"name\":\"\\u0412 \\u0437\\u0430\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435\",\"text\":\"\\u041d\\u0430\\u0448\\u0438 \\u0434\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u044b \\u0438 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0430\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438 \\u043f\\u043e\\u0441\\u0435\\u0449\\u0430\\u044e\\u0442 \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u043d\\u044b\\u0435 \\u0432\\u044b\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438, \\u0438\\u0437\\u0443\\u0447\\u0430\\u044e\\u0442 \\u043d\\u043e\\u0432\\u044b\\u0435 \\u0442\\u0435\\u043d\\u0434\\u0435\\u043d\\u0446\\u0438\\u0438 \\u0438\\u0441\\u043a\\u0443\\u0441\\u0441\\u0442\\u0432\\u0430 \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u044f \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438, \\u044d\\u0442\\u043e \\u043e\\u0442\\u0440\\u0430\\u0436\\u0430\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u043c\\u043d\\u043e\\u0433\\u043e\\u043e\\u0431\\u0440\\u0430\\u0437\\u0438\\u0438 \\u0432\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442\\u043e\\u0432 \\u043f\\u0440\\u0435\\u0434\\u043b\\u0430\\u0433\\u0430\\u0435\\u043c\\u043e\\u0439 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u0438. \\u041a\\u0440\\u043e\\u043c\\u0435 \\u0433\\u043e\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438, \\u0432\\u044b \\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u0441\\u0434\\u0435\\u043b\\u0430\\u0442\\u044c \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437 \\u2013 \\u0432 \\u044d\\u0442\\u043e\\u043c \\u0441\\u043b\\u0443\\u0447\\u0430\\u0435 \\u043d\\u0430\\u0448\\u0438 \\u043c\\u0435\\u043d\\u0435\\u0434\\u0436\\u0435\\u0440\\u044b \\u0432\\u0441\\u0451 \\u0440\\u0430\\u0441\\u0441\\u0447\\u0438\\u0442\\u0430\\u044e\\u0442, \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438 \\u0441\\u0434\\u0435\\u043b\\u0430\\u044e\\u0442 \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u044b\\u0435 \\u0437\\u0430\\u043c\\u0435\\u0440\\u044b.\\r\\n\\u0414\\u0438\\u0437\\u0430\\u0439\\u043d\\u0435\\u0440\\u044b \\u0441\\u043e\\u0437\\u0434\\u0430\\u0434\\u0443\\u0442 \\u043d\\u0435\\u0441\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u0432\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442\\u043e\\u0432 \\u0440\\u0430\\u0441\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0438 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0438 \\u0435\\u0451 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432\\u043e\\u0433\\u043e \\u0440\\u0435\\u0448\\u0435\\u043d\\u0438\\u044f, \\u043f\\u0440\\u0435\\u0434\\u0441\\u0442\\u0430\\u0432\\u044f\\u0442 3D \\u043f\\u043b\\u0430\\u043d \\u0412\\u0430\\u0448\\u0435\\u0433\\u043e \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430, \\u043e\\u0441\\u0442\\u0430\\u043d\\u0435\\u0442\\u0441\\u044f \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u043e\\u043f\\u043b\\u0430\\u0442\\u0438\\u0442\\u044c \\u0438 \\u0443\\u0431\\u0435\\u0434\\u0438\\u0442\\u044c\\u0441\\u044f, \\u0447\\u0442\\u043e \\u0441 \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0435\\u0439 \\u00ab\\u041c\\u0415\\u0411\\u0415\\u041b\\u042c-\\u0410\\u0420\\u0422\\u00bb \\u043f\\u043e\\u043c\\u0435\\u043d\\u044f\\u0442\\u044c \\u043e\\u0431\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043a\\u0443 \\u043b\\u0435\\u0433\\u043a\\u043e \\u0438 \\u043f\\u0440\\u0438\\u044f\\u0442\\u043d\\u043e. \\u041c\\u044b \\u0437\\u0430\\u043d\\u0438\\u043c\\u0430\\u0435\\u043c\\u0441\\u044f \\u0438\\u0441\\u043a\\u0443\\u0441\\u0441\\u0442\\u0432\\u043e\\u043c \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u044f \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0412\\u0430\\u0441 \\u043e\\u043a\\u0440\\u0443\\u0436\\u0430\\u043b\\u0438 \\u043a\\u043e\\u043c\\u0444\\u043e\\u0440\\u0442\\u043d\\u044b\\u0435 \\u0443\\u0441\\u043b\\u043e\\u0432\\u0438\\u044f \\u043d\\u0430 \\u0440\\u0430\\u0431\\u043e\\u0442\\u0435 \\u0438 \\u0434\\u043e\\u043c\\u0430, \\u0447\\u0442\\u043e\\u0431\\u044b \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u043e \\u0412\\u0430\\u0448\\u0435\\u0439 \\u0436\\u0438\\u0437\\u043d\\u0438 \\u0432\\u043e\\u0437\\u0440\\u0430\\u0441\\u0442\\u0430\\u043b\\u043e. \\u041c\\u044b \\u0438 \\u0432 \\u0434\\u0430\\u043b\\u044c\\u043d\\u0435\\u0439\\u0448\\u0435\\u043c, \\u0431\\u0443\\u0434\\u0435\\u043c \\u0441\\u0442\\u0440\\u0435\\u043c\\u0438\\u0442\\u044c\\u0441\\u044f \\u0441\\u043e\\u0437\\u0434\\u0430\\u0432\\u0430\\u0442\\u044c \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u0443\\u044e, \\u0443\\u0434\\u043e\\u0431\\u043d\\u0443\\u044e \\u043c\\u0435\\u0431\\u0435\\u043b\\u044c, \\u0434\\u0435\\u043b\\u0430\\u044f \\u0435\\u0451 \\u0434\\u043e\\u0441\\u0442\\u0443\\u043f\\u043d\\u043e\\u0439 \\u0434\\u043b\\u044f \\u043a\\u0430\\u0436\\u0434\\u043e\\u0433\\u043e \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044f, \\u0438 \\u043c\\u044b \\u0432\\u0441\\u0435\\u0433\\u0434\\u0430 \\u0431\\u0443\\u0434\\u0435\\u043c \\u0440\\u0430\\u0434\\u044b \\u0432\\u0438\\u0434\\u0435\\u0442\\u044c \\u0412\\u0430\\u0441 \\u0432 \\u0447\\u0438\\u0441\\u043b\\u0435 \\u043d\\u0430\\u0448\\u0438\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432.\"}]', 1504165312, '10caf95fbd844389edb9bc6201ebd6c2.jpg', NULL, '1911f58e7c6ea185a8a86339aba0035a.jpg', NULL),
(18, 'Доставка и оплата', '', 'Доставка и оплата', 'Доставка и оплата', 'Доставка и оплата', '<p>Заказывая доставку товара сторонними курьерскими службами (Нова пошта, Мист Экспресс ) - при получении обязательно проверяйте наличие всего товара в заказе, а так же его внешний вид.</p>', 'delivery', 1, '{\"install\":[{\"text\":\"<p>\\u0421\\u0431\\u043e\\u0440\\u043a\\u0430 \\u043a\\u043e\\u0440\\u043f\\u0443\\u0441\\u043d\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u043d\\u0430 \\u0434\\u043e\\u043c\\u0443 \\u0443 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0447\\u0438\\u043a\\u0430 &ndash;<\\/p>\\r\\n<p><strong>\\u0411\\u0415\\u0421\\u041f\\u041b\\u0410\\u0422\\u041d\\u041e<\\/strong><\\/p>\"},{\"text\":\"<p>\\u0421\\u0431\\u043e\\u0440\\u043a\\u0430\\/\\u0440\\u0430\\u0437\\u0431\\u043e\\u0440\\u043a\\u0430 \\u043c\\u044f\\u0433\\u043a\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438:<\\/p>\\r\\n<p>\\u0414\\u0438\\u0432\\u0430\\u043d &ndash;&nbsp;<strong>20 \\u0433\\u0440\\u043d.<\\/strong><\\/p>\\r\\n<p>\\u041c\\u044f\\u0433\\u043a\\u0438\\u0439 \\u0443\\u0433\\u043e\\u043b\\u043e\\u043a &ndash;&nbsp;<strong>30 \\u0433\\u0440\\u043d<\\/strong><\\/p>\"},{\"text\":\"<p>\\u041f\\u0440\\u0438 \\u043f\\u043e\\u043a\\u0443\\u043f\\u043a\\u0435 \\u0432\\u044b\\u0441\\u0442\\u0430\\u0432\\u043e\\u0447\\u043d\\u043e\\u0433\\u043e \\u043e\\u0431\\u0440\\u0430\\u0437\\u0446\\u0430<\\/p>\\r\\n<p>\\u0441\\u0442\\u043e\\u0438\\u043c\\u043e\\u0441\\u0442\\u044c \\u0440\\u0430\\u0437\\u0431\\u043e\\u0440\\u043a\\u0438\\/\\u0441\\u0431\\u043e\\u0440\\u043a\\u0438:<\\/p>\\r\\n<p>\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c -&nbsp;<strong>50 \\u0433\\u0440\\u043d.<\\/strong><\\/p>\\r\\n<p>\\u0428\\u043a\\u0430\\u0444 -&nbsp;<strong>100 \\u0433\\u0440\\u043d.<\\/strong><\\/p>\"}],\"delivery\":[{\"text1\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0434\\u043e \\u043f\\u043e\\u0434\\u044a\\u0435\\u0437\\u0434\\u0430\",\"text2\":\"150\\u0433\\u0440\\u043d\"},{\"text1\":\"\\u041f\\u043e\\u0434\\u044a\\u0451\\u043c \\u043d\\u0430 \\u044d\\u0442\\u0430\\u0436 \\u043f\\u043b\\u0430\\u0442\\u043d\\u044b\\u0439\",\"text2\":\"5-40 \\u0433\\u0440\\u043d\\/\\u044d\\u0442\\u0430\\u0436\"},{\"text1\":\"\\u0417\\u0430\\u043d\\u043e\\u0441 \\u0432 \\u0447\\u0430\\u0441\\u0442\\u043d\\u044b\\u0439 \\u0434\\u043e\\u043c \\u0438 1-\\u0439 \\u044d\\u0442\\u0430\\u0436 \\u2013\",\"text2\":\"10-25 \\u0433\\u0440\\u043d.\"},{\"text1\":\"\\u0417\\u0430 \\u043f\\u0440\\u0435\\u0434\\u0435\\u043b\\u0430\\u043c\\u0438 \\u041d\\u0438\\u043a\\u043e\\u043b\\u0430\\u0435\\u0432\\u0430 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 -\",\"text2\":\"10,0 \\u0433\\u0440\\u043d\\/\\u043a\\u043c.\"},{\"text1\":\"\\u0414\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u0438 \\u0441\\u0431\\u043e\\u0440\\u043a\\u0430 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u043f\\u043e \\u0423\\u043a\\u0440\\u0430\\u0438\\u043d\\u0435\",\"text2\":\"\\u043e\\u0433\\u043e\\u0432\\u0430\\u0440\\u0438\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u043e\"},{\"text\":\"<p>\\u0422\\u0430\\u043a\\u0436\\u0435 \\u043c\\u044b \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u043c \\u0432 \\u043f\\u0440\\u0438\\u0433\\u043e\\u0440\\u043e\\u0434:<\\/p>\\r\\n<p>\\u0428. \\u0411\\u0430\\u043b\\u043a\\u0430, \\u0422\\u0435\\u0440\\u043d\\u043e\\u0432\\u043a\\u0430 - 160 \\u0433\\u0440\\u043d \\u041c\\u0435\\u0448\\u043a\\u043e\\u0432\\u043e-\\u041f\\u043e\\u0433\\u043e\\u0440\\u0435\\u043b\\u043e\\u0432\\u043e, \\u041e\\u043a\\u0442\\u044f\\u0431\\u0440\\u044c\\u0441\\u043a\\u043e\\u0435, \\u0411\\u0430\\u043b\\u0430\\u0431\\u0430\\u043d\\u043e\\u0432\\u043a\\u0430, \\u0411.\\u041a\\u043e\\u0440\\u0435\\u043d\\u0438\\u0445\\u0430 &ndash; 190 \\u0433\\u0440\\u043d \\u041c\\u0430\\u0442\\u0432\\u0435\\u0435\\u0432\\u043a\\u0430, \\u041a\\u0443\\u043b\\u044c\\u0431\\u0430\\u043a\\u0438\\u043d\\u043e &ndash; 180 \\u0433\\u0440\\u043d \\u041c.\\u041a\\u043e\\u0440\\u0435\\u043d\\u0438\\u0445\\u0430 &ndash; 260 \\u0433\\u0440\\u043d<\\/p>\"}],\"payment\":[{\"name\":\"\\u041d\\u0430\\u043b\\u0438\\u0447\\u043d\\u044b\\u043c\\u0438 \\u0432 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0430\\u0445 \\u00ab\\u041c\\u0435\\u0431\\u0435\\u043b\\u044c-\\u0410\\u0420\\u0422\\u00bb\",\"text\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0438\\u0441\\u043a\\u043b\\u044e\\u0447\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u043e \\u0432 \\u043d\\u0430\\u0446\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0432\\u0430\\u043b\\u044e\\u0442\\u0435. \\u0412 \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043e\\u043f\\u043b\\u0430\\u0442\\u044b \\u043c\\u044b \\u0432\\u044b\\u0434\\u0430\\u0435\\u043c \\u0412\\u0430\\u043c \\u0447\\u0435\\u043a\"},{\"name\":\"\\u041a\\u0430\\u0440\\u0442\\u043e\\u0439 \\u0447\\u0435\\u0440\\u0435\\u0437 \\u0442\\u0435\\u0440\\u043c\\u0438\\u043d\\u0430\\u043b \\u041f\\u0440\\u0438\\u0432\\u0430\\u0442\\u0411\\u0430\\u043d\\u043a\\u0430 \\u0432 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0430\\u0445 \\u00ab\\u041c\\u0435\\u0431\\u0435\\u043b\\u044c-\\u0410\\u0420\\u0422\\u00bb\",\"text\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0447\\u0435\\u0440\\u0435\\u0437 \\u0442\\u0435\\u0440\\u043c\\u0438\\u043d\\u0430\\u043b \\u041f\\u0440\\u0438\\u0432\\u0430\\u0442\\u0411\\u0430\\u043d\\u043a\\u0430 (\\u043f\\u043e\\u0440\\u0442\\u0430\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439) \\u0432 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0435 \\u00ab\\u041c\\u0435\\u0431\\u0435\\u043b\\u044c-\\u0410\\u0420\\u0422\\u00bb \\u0441 \\u043f\\u043e\\u043c\\u043e\\u0449\\u044c\\u044e \\u043a\\u0430\\u0440\\u0442\\u044b Visa \\/ MasterCard \\u043b\\u044e\\u0431\\u043e\\u0433\\u043e \\u0431\\u0430\\u043d\\u043a\\u0430.\"},{\"name\":\"\\u0411\\u0435\\u0437\\u043d\\u0430\\u043b\\u0438\\u0447\\u043d\\u044b\\u0439 \\u0440\\u0430\\u0441\\u0447\\u0435\\u0442 \\u0441\\u043e\\u0433\\u043b\\u0430\\u0441\\u043d\\u043e \\u0432\\u044b\\u043f\\u0438\\u0441\\u0430\\u043d\\u043d\\u043e\\u0433\\u043e \\u0441\\u0447\\u0435\\u0442\\u0430-\\u0444\\u0430\\u043a\\u0442\\u0443\\u0440\\u044b. \\u041c\\u044b \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430\\u0435\\u0442 \\u043a\\u0430\\u043a \\u0441 \\u041d\\u0414\\u0421 \\u0442\\u0430\\u043a \\u0438 \\u0431\\u0435\\u0437 \\u041d\\u0414\\u0421\",\"text\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u043f\\u043e \\u0431\\u0435\\u0437\\u043d\\u0430\\u043b\\u0438\\u0447\\u043d\\u043e\\u043c\\u0443 \\u0440\\u0430\\u0441\\u0447\\u0435\\u0442\\u0443 \\u043e\\u0441\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0438\\u043c \\u043e\\u0431\\u0440\\u0430\\u0437\\u043e\\u043c: \\u043f\\u043e\\u0441\\u043b\\u0435 \\u043e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u0438\\u044f \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430, \\u043c\\u0435\\u043d\\u0435\\u0434\\u0436\\u0435\\u0440 \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0430 \\u0444\\u0430\\u043a\\u0441\\u043e\\u043c \\u0438\\u043b\\u0438 \\u044d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u043d\\u043d\\u043e\\u0439 \\u043f\\u043e\\u0447\\u0442\\u043e\\u0439 \\u0432\\u044b\\u0448\\u043b\\u0435\\u0442 \\u0412\\u0430\\u043c \\u0441\\u0447\\u0435\\u0442-\\u0444\\u0430\\u043a\\u0442\\u0443\\u0440\\u0443, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0439 \\u0412\\u044b \\u0441\\u043c\\u043e\\u0436\\u0435\\u0442\\u0435 \\u043e\\u043f\\u043b\\u0430\\u0442\\u0438\\u0442\\u044c \\u0432 \\u043a\\u0430\\u0441\\u0441\\u0435 \\u043e\\u0442\\u0434\\u0435\\u043b\\u0435\\u043d\\u0438\\u044f \\u043b\\u044e\\u0431\\u043e\\u0433\\u043e \\u0431\\u0430\\u043d\\u043a\\u0430 \\u0438\\u043b\\u0438 \\u0441 \\u0440\\u0430\\u0441\\u0447\\u0435\\u0442\\u043d\\u043e\\u0433\\u043e \\u0441\\u0447\\u0435\\u0442\\u0430 \\u0412\\u0430\\u0448\\u0435\\u0439 \\u0444\\u0438\\u0440\\u043c\\u044b. \\u0414\\u043b\\u044f \\u044e\\u0440\\u0438\\u0434\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u043b\\u0438\\u0446 \\u043f\\u0430\\u043a\\u0435\\u0442 \\u0432\\u0441\\u0435\\u0445 \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u044b\\u0445 \\u0434\\u043e\\u043a\\u0443\\u043c\\u0435\\u043d\\u0442\\u043e\\u0432 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0432\\u043c\\u0435\\u0441\\u0442\\u0435 \\u0441 \\u0442\\u043e\\u0432\\u0430\\u0440\\u043e\\u043c\"},{\"name\":\"\\u0427\\u0435\\u0440\\u0435\\u0437 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0443 \\u043f\\u043b\\u0430\\u0442\\u0435\\u0436\\u0435\\u0439 \\u041f\\u0440\\u0438\\u0432\\u0430\\u044224\",\"text\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0447\\u0435\\u0440\\u0435\\u0437 \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c\\u0443 \\u0438\\u043d\\u0442\\u0435\\u0440\\u043d\\u0435\\u0442 \\u043f\\u043b\\u0430\\u0442\\u0435\\u0436\\u0435\\u0439 \\u041f\\u0440\\u0438\\u0432\\u0430\\u044224 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u043e\\u0447\\u043d\\u044b\\u0439 \\u0441\\u0447\\u0435\\u0442 \\u041f\\u0440\\u0438\\u0432\\u0430\\u0442\\u0411\\u0430\\u043d\\u043a\\u0430\"},{\"name\":\"\\u0427\\u0435\\u0440\\u0435\\u0437 \\u0442\\u0435\\u0440\\u043c\\u0438\\u043d\\u0430\\u043b Ibox\",\"text\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u043d\\u0430 \\u043a\\u0430\\u0440\\u0442\\u0443 \\u041f\\u0440\\u0438\\u0432\\u0430\\u0442\\u0411\\u0430\\u043d\\u043a\\u0430 \\u0447\\u0435\\u0440\\u0435\\u0437 Ibox (\\u0442\\u0435\\u0440\\u043c\\u0438\\u043d\\u0430\\u043b\\u044b \\u0441\\u0438\\u0441\\u0442\\u0435\\u043c \\u043c\\u043e\\u043c\\u0435\\u043d\\u0442\\u0430\\u043b\\u044c\\u043d\\u044b\\u0445 \\u043f\\u043b\\u0430\\u0442\\u0435\\u0436\\u0435\\u0439)\"}],\"times\":{\"sale\":[\"\\u0441 9-00 \\u0434\\u043e 14-00\",\"\\u0441 14-00 \\u0434\\u043e 18-00\"],\"delivery\":{\"0\":\"\\u0441 14-00 \\u0434\\u043e 18-00\",\"1\":\"\\u0441 9-00 \\u0434\\u043e 18-00 \\u0441\\u043b\\u0435\\u0434\\u0443\\u044e\\u0449\\u0435\\u0433\\u043e \\u0434\\u043d\\u044f\",\"3\":\"\\u041f\\u043e \\u0434\\u043e\\u0433\\u043e\\u0432\\u043e\\u0440\\u0435\\u043d\\u043d\\u043e\\u0441\\u0442\\u0438\"}}}', 1505310891, '350717542439b5d6f0ab998e56db9593.jpg', NULL, '9072ebc85170a1f45393ef90c7de1c9b.jpg', 'd4cb20f92743facafd48ac45d7a5d797.jpg'),
(19, 'Гарантия', '', 'Гарантия', 'Гарантия', 'Гарантия', '<div class=\"block6__title block6__title--left mb-50 mb-lg30\">Гарантийный срок <br />эксплуатации мебели</div>\r\n<div class=\"block6__text view-text mb-20 ml-50 ml-md0\">\r\n<p>Детской мебели и для общественных помещений &ndash; <strong>18 месяцев</strong></p>\r\n<p>Бытовой мебели &ndash; <strong>24 месяца</strong> с момента поставки</p>\r\n</div>\r\n<div class=\"block6__desc view-text mb-50 ml-50 ml-md0 mb-lg30\">\r\n<p>За исключением случаев неосторожной эксплуатации или преднамеренного повреждения по вине Покупателя.</p>\r\n<p>В случае выявления Покупателем каких-либо дефектов, компания Мебель-АРТ в Николаеве обязуется устранить недостатки в течение <strong>30 дней</strong>.</p>\r\n</div>', 'garant', 1, '{\"subtitle\":\"\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u0439 \\u0441\\u0440\\u043e\\u043a  \\u044d\\u043a\\u0441\\u043f\\u043b\\u0443\\u0430\\u0442\\u0430\\u0446\\u0438\\u0438 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438\",\"text1\":\"\\u0414\\u0435\\u0442\\u0441\\u043a\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u0438 \\u0434\\u043b\\u044f \\u043e\\u0431\\u0449\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u044b\\u0445 \\u043f\\u043e\\u043c\\u0435\\u0449\\u0435\\u043d\\u0438\\u0439 \\u2013 18 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432\",\"text2\":\"\\u0411\\u044b\\u0442\\u043e\\u0432\\u043e\\u0439 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438 \\u2013 24 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0430 \\u0441 \\u043c\\u043e\\u043c\\u0435\\u043d\\u0442\\u0430 \\u043f\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0438\",\"text3\":\"<p>\\u0417\\u0430 \\u0438\\u0441\\u043a\\u043b\\u044e\\u0447\\u0435\\u043d\\u0438\\u0435\\u043c \\u0441\\u043b\\u0443\\u0447\\u0430\\u0435\\u0432 \\u043d\\u0435\\u043e\\u0441\\u0442\\u043e\\u0440\\u043e\\u0436\\u043d\\u043e\\u0439 \\u044d\\u043a\\u0441\\u043f\\u043b\\u0443\\u0430\\u0442\\u0430\\u0446\\u0438\\u0438 \\u0438\\u043b\\u0438 \\u043f\\u0440\\u0435\\u0434\\u043d\\u0430\\u043c\\u0435\\u0440\\u0435\\u043d\\u043d\\u043e\\u0433\\u043e \\u043f\\u043e\\u0432\\u0440\\u0435\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f \\u043f\\u043e \\u0432\\u0438\\u043d\\u0435 \\u041f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u044f.<\\/p>\\r\\n<p>\\u0412 \\u0441\\u043b\\u0443\\u0447\\u0430\\u0435 \\u0432\\u044b\\u044f\\u0432\\u043b\\u0435\\u043d\\u0438\\u044f \\u041f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u0435\\u043c \\u043a\\u0430\\u043a\\u0438\\u0445-\\u043b\\u0438\\u0431\\u043e \\u0434\\u0435\\u0444\\u0435\\u043a\\u0442\\u043e\\u0432, \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\u041c\\u0435\\u0431\\u0435\\u043b\\u044c-\\u0410\\u0420\\u0422 \\u0432 \\u041d\\u0438\\u043a\\u043e\\u043b\\u0430\\u0435\\u0432\\u0435 \\u043e\\u0431\\u044f\\u0437\\u0443\\u0435\\u0442\\u0441\\u044f \\u0443\\u0441\\u0442\\u0440\\u0430\\u043d\\u0438\\u0442\\u044c \\u043d\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0442\\u043a\\u0438 \\u0432 \\u0442\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435&nbsp;30 \\u0434\\u043d\\u0435\\u0439.<\\/p>\"}', 1505310841, '767045d211bbb2c13d7943d5d0493651.jpg', NULL, 'fa88c252eb07f3b0ae0a10fe189ac91d.png', NULL),
(20, 'Покупателю', '', 'Покупателю', 'Покупателю', 'Покупателю', '<p>Мы заботимся о наших клиентах. Предлагаем профессиональные консультации и практические решения, и мы выполняем наши обещания.</p>', 'faq', 1, '[{\"name\":\"\\u0427\\u0430\\u0441\\u0442\\u043e \\u0437\\u0430\\u0434\\u0430\\u0432\\u0430\\u0435\\u043c\\u044b\\u0435 \\u0432\\u043e\\u043f\\u0440\\u043e\\u0441\\u044b \\u043f\\u0440\\u0438 \\u043f\\u043e\\u043a\\u0443\\u043f\\u043a\\u0435 \\u043c\\u0435\\u0431\\u0435\\u043b\\u0438\"},{\"name\":\"\\u0426\\u0435\\u043b\\u044c\",\"text\":\"<p>\\u0426\\u0435\\u043b\\u044c \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 - \\u043f\\u043e\\u0432\\u044b\\u0448\\u0435\\u043d\\u0438\\u0435 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u044f \\u043f\\u043e\\u043a\\u0443\\u043f\\u0430\\u0442\\u0435\\u043b\\u0435\\u0439, \\u0440\\u0430\\u0441\\u0448\\u0438\\u0440\\u0435\\u043d\\u0438\\u0435 \\u0430\\u0441\\u0441\\u043e\\u0440\\u0442\\u0438\\u043c\\u0435\\u043d\\u0442\\u0430, \\u0437\\u0430\\u0432\\u043e\\u0435\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0438 \\u0443\\u0434\\u0435\\u0440\\u0436\\u0430\\u043d\\u0438\\u0435 \\u0434\\u043e\\u0432\\u0435\\u0440\\u0438\\u044f \\u043a \\u043a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u0438 \\\"\\u041c\\u0415\\u0411\\u0415\\u041b\\u042c-\\u0410\\u0420\\u0422\\\", \\u0443\\u0432\\u0435\\u043b\\u0438\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0447\\u0438\\u0441\\u043b\\u0430 \\u043f\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u043d\\u044b\\u0445 \\u043a\\u043b\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432, \\u0441\\u043e\\u0437\\u0434\\u0430\\u043d\\u0438\\u0435 \\u043d\\u0430\\u0438\\u043b\\u0443\\u0447\\u0448\\u0435\\u0433\\u043e \\u0441\\u043e\\u043e\\u0442\\u043d\\u043e\\u0448\\u0435\\u043d\\u0438\\u044f \\u0446\\u0435\\u043d\\u044b \\u0438 \\u043a\\u0430\\u0447\\u0435\\u0441\\u0442\\u0432\\u0430 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0446\\u0438\\u0438.<\\/p>\"},{\"name\":\"\\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u0438 \\u0441\\u0435\\u0440\\u0432\\u0438\\u0441\\u043d\\u043e\\u0435 \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435\",\"text\":\"<p><a href=\\\"http:\\/\\/inkubator.ks.ua\\/html\\/mebel048\\/guarant.html\\\">\\u041d\\u0430 \\u043a\\u0430\\u043a\\u0438\\u0435 \\u0442\\u043e\\u0432\\u0430\\u0440\\u044b \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f?<\\/a><\\/p>\\r\\n<p style=\\\"margin-left: 20px;\\\"><strong>\\u041d\\u0430 \\u0442\\u043e\\u0432\\u0430\\u0440\\u044b \\u0432 \\u043d\\u0430\\u0448\\u0435\\u043c \\u043c\\u0430\\u0433\\u0430\\u0437\\u0438\\u043d\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f, \\u043f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0430\\u044e\\u0449\\u0430\\u044f \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432\\u0430 \\u043f\\u043e \\u043e\\u0442\\u0441\\u0443\\u0442\\u0441\\u0442\\u0432\\u0438\\u044e \\u0432 \\u0442\\u043e\\u0432\\u0430\\u0440\\u0435 \\u0437\\u0430\\u0432\\u043e\\u0434\\u0441\\u043a\\u0438\\u0445 \\u0434\\u0435\\u0444\\u0435\\u043a\\u0442\\u043e\\u0432. \\u0413\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f \\u043d\\u0430 \\u0441\\u0440\\u043e\\u043a \\u043e\\u0442 2-\\u0445 \\u043d\\u0435\\u0434\\u0435\\u043b\\u044c \\u0434\\u043e 36 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432 \\u0432 \\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438 \\u043e\\u0442 \\u0441\\u0435\\u0440\\u0432\\u0438\\u0441\\u043d\\u043e\\u0439 \\u043f\\u043e\\u043b\\u0438\\u0442\\u0438\\u043a\\u0438 \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f. \\u0421\\u0440\\u043e\\u043a \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0438 \\u0443\\u043a\\u0430\\u0437\\u0430\\u043d \\u0432 \\u043e\\u043f\\u0438\\u0441\\u0430\\u043d\\u0438\\u0438 \\u043a\\u0430\\u0436\\u0434\\u043e\\u0433\\u043e \\u0442\\u043e\\u0432\\u0430\\u0440\\u0430 \\u043d\\u0430 \\u043d\\u0430\\u0448\\u0435\\u043c \\u0441\\u0430\\u0439\\u0442\\u0435. \\u041f\\u043e\\u0434\\u0442\\u0432\\u0435\\u0440\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435\\u043c \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u0445 \\u043e\\u0431\\u044f\\u0437\\u0430\\u0442\\u0435\\u043b\\u044c\\u0441\\u0442\\u0432 \\u0441\\u043b\\u0443\\u0436\\u0438\\u0442 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u0439 \\u0442\\u0430\\u043b\\u043e\\u043d \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u043e\\u0434\\u0438\\u0442\\u0435\\u043b\\u044f, \\u0438\\u043b\\u0438 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u0439 \\u0442\\u0430\\u043b\\u043e\\u043d<\\/strong><\\/p>\\r\\n<p>\\u041a\\u0443\\u0434\\u0430 \\u043e\\u0431\\u0440\\u0430\\u0449\\u0430\\u0442\\u044c\\u0441\\u044f \\u0437\\u0430 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u043c \\u043e\\u0431\\u0441\\u043b\\u0443\\u0436\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435\\u043c?<\\/p>\\r\\n<p>\\u042f \\u043c\\u043e\\u0433\\u0443 \\u043e\\u0431\\u043c\\u0435\\u043d\\u044f\\u0442\\u044c \\u0438\\u043b\\u0438 \\u0432\\u0435\\u0440\\u043d\\u0443\\u0442\\u044c \\u0442\\u043e\\u0432\\u0430\\u0440?<\\/p>\\r\\n<p>\\u0413\\u0434\\u0435 \\u0438 \\u043a\\u0430\\u043a \\u043c\\u043e\\u0436\\u043d\\u043e \\u043f\\u0440\\u043e\\u0438\\u0437\\u0432\\u0435\\u0441\\u0442\\u0438 \\u043e\\u0431\\u043c\\u0435\\u043d \\u0438\\u043b\\u0438 \\u0432\\u043e\\u0437\\u0432\\u0440\\u0430\\u0442?<\\/p>\\r\\n<p>\\u0421\\u0435\\u0440\\u0432\\u0438\\u0441\\u043d\\u044b\\u0439 \\u0446\\u0435\\u043d\\u0442\\u0440 \\u043d\\u0435 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043e\\u0442\\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\\u0438\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c \\u043c\\u043e\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440 \\u0432 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u0439\\u043d\\u044b\\u0439 \\u043f\\u0435\\u0440\\u0438\\u043e\\u0434<\\/p>\\r\\n<p>\\u0412 \\u043a\\u0430\\u043a\\u0438\\u0445 \\u0441\\u043b\\u0443\\u0447\\u0430\\u044f\\u0445 \\u0433\\u0430\\u0440\\u0430\\u043d\\u0442\\u0438\\u044f \\u043d\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u0435\\u0442\\u0441\\u044f?<\\/p>\"},{\"name\":\"\\u041e\\u043f\\u043b\\u0430\\u0442\\u0430 \\u0438 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430\",\"text\":\"<p>\\u041e\\u0441\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043b\\u044f\\u0435\\u0442\\u0435 \\u043b\\u0438 \\u0412\\u044b \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u0443 \\u0434\\u043e \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440\\u044b \\u0438 \\u043f\\u043e\\u0434\\u044a\\u0435\\u043c \\u043d\\u0430 \\u044d\\u0442\\u0430\\u0436?<\\/p>\\r\\n<p>\\u0415\\u0441\\u0442\\u044c \\u043b\\u0438 \\u0432\\u043e\\u0437\\u043c\\u043e\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430\\u0442\\u044c \\u0442\\u043e\\u0432\\u0430\\u0440 \\u0441 \\u0434\\u043e\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0432 \\u0434\\u0440\\u0443\\u0433\\u0443\\u044e \\u0441\\u0442\\u0440\\u0430\\u043d\\u0443?<\\/p>\\r\\n<p>\\u0421\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u0434\\u043d\\u0435\\u0439 \\u0442\\u043e\\u0432\\u0430\\u0440 \\u043d\\u0430\\u0445\\u043e\\u0434\\u0438\\u0442\\u0441\\u044f \\u0432 \\u043f\\u0443\\u043d\\u043a\\u0442\\u0435 \\u0432\\u044b\\u0434\\u0430\\u0447\\u0438?<\\/p>\"}]', 1504604642, 'a7e8e513fc64cf77cd973165738c1828.jpg', NULL, NULL, NULL),
(21, 'gallery', NULL, 'gallery', 'gallery', 'gallery', 'gallery', 'gallery', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Admin', NULL, 'Admin', 'admin', 'admin', 'admin', 'admin', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'TopCategory', NULL, 'TopCategory', 'TopCategory', 'TopCategory', 'TopCategory', 'topCategory', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL, NULL, 'wezom', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Товары категории', NULL, 'Товары категории', 'Товары категории', 'Товары категории', 'Товары категории', 'category', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'product', NULL, 'product', 'product', 'product', 'product', 'product', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL, NULL, 'auth', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL, NULL, 'user', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, NULL, 'tops', 1, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `cron`
--

CREATE TABLE `cron` (
  `id` int(10) NOT NULL,
  `email` varchar(64) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery`
--

CREATE TABLE `gallery` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  `image` varchar(128) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `alias` varchar(255) NOT NULL,
  `text` text,
  `sort` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery`
--

INSERT INTO `gallery` (`id`, `created_at`, `updated_at`, `status`, `name`, `image`, `h1`, `title`, `keywords`, `description`, `alias`, `text`, `sort`) VALUES
(5, 1447092527, 1458160573, 1, 'Мужская обувь', 'f35ba04c4c18ed371a206975ff16ce90.jpg', 'Мужская обувь', 'Мужская обувь', 'Мужская обувь', 'Мужская обувь', 'muzhskaja-obuv', '', NULL),
(6, 1447092625, 1483519411, 1, 'Женская обувь', 'e85a9a463467065e7693edbca081158b.jpg', 'Женская обувь', 'Женская обувь', 'Женская обувь', 'Женская обувь', 'zhenskaja-obuv', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `image` varchar(128) NOT NULL,
  `main` tinyint(1) NOT NULL DEFAULT '0',
  `gallery_id` int(10) NOT NULL,
  `sort` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `created_at`, `updated_at`, `image`, `main`, `gallery_id`, `sort`) VALUES
(23, 1447092584, 1458160549, '46ba420360f4d0730ad1ecf4e15dd68f.jpg', 0, 5, 1),
(24, 1447092584, 1458160549, '29e55409c6b13fcde501c502f2f51a07.jpg', 0, 5, 2),
(25, 1447092585, 1458160549, '5e854470fc6c466b41e9cea23e206e59.jpeg', 0, 5, 3),
(26, 1447092588, 1458160570, '9d57b47e499dd50f5d0807578538390f.jpg', 1, 5, 4),
(27, 1447092588, 1458160549, 'd0d2de40646c11689d81701ea49cc0f6.jpg', 0, 5, 6),
(28, 1447092589, 1458160549, '10925b5fe707498f26f448f5f9d51601.jpg', 0, 5, 7),
(29, 1447092672, 1461223401, 'b6aff3b3f2be1ee75c59aedbb6422579.jpg', 0, 6, 3),
(30, 1447092673, 1461223401, 'a7efa2943ae2420469b08becd0032e92.jpg', 0, 6, 1),
(31, 1447092675, 1461223401, 'a9d881476d8fb2b8400b568435ebcafd.jpg', 0, 6, 4),
(32, 1447092677, 1461223401, 'c5ca07d88ecc2a6519292d6ac2bbb5c5.jpg', 0, 6, 5),
(33, 1447092686, 1461223401, '0ed24eedef4b158126be3a171be4dcd9.jpg', 0, 6, 6),
(34, 1447092687, 1461223401, 'cc27d790bba59d1f63d6fb170cd4df89.jpg', 1, 6, 2),
(35, 1458160542, 1458160562, 'd998c755dc8384ba861cb31459eccc1d.jpg', 0, 5, 5),
(36, 1485243751, NULL, 'a42b2b590a90b8c9a8505b7bec406ba9.jpg', 0, 6, 7),
(37, 1492005452, NULL, '7c584b432d25360cea8d980ee1b11006.jpg', 0, 6, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `i18n`
--

CREATE TABLE `i18n` (
  `id` int(2) NOT NULL,
  `alias` varchar(3) NOT NULL,
  `name` varchar(32) NOT NULL,
  `short_name` varchar(16) NOT NULL,
  `locale` varchar(8) NOT NULL,
  `default` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `i18n`
--

INSERT INTO `i18n` (`id`, `alias`, `name`, `short_name`, `locale`, `default`) VALUES
(1, 'ru', 'Русский', 'РУС', 'ru-ru', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `instructions`
--

CREATE TABLE `instructions` (
  `id` int(5) NOT NULL,
  `module` varchar(75) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `instructions`
--

INSERT INTO `instructions` (`id`, `module`, `link`) VALUES
(1, 'config', 'https://www.youtube.com/embed/ei0jwyvSwnI'),
(2, 'MailTemplates', 'https://www.youtube.com/embed/Ysiua0gHyK4'),
(3, 'log', 'https://www.youtube.com/embed/_BCCa7I8RS4'),
(4, 'callback', 'https://www.youtube.com/embed/KYVLmS8npXw'),
(5, 'blacklist', 'https://www.youtube.com/embed/8mRSL9kCfVU'),
(6, 'reviews', 'https://www.youtube.com/embed/dD_Xu8QokB8'),
(7, 'subscribe', 'https://www.youtube.com/embed/u97oKxJi-fs'),
(8, 'subscribers', 'https://www.youtube.com/embed/u97oKxJi-fs');

-- --------------------------------------------------------

--
-- Структура таблицы `log`
--

CREATE TABLE `log` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `log`
--

INSERT INTO `log` (`id`, `created_at`, `updated_at`, `name`, `link`, `ip`, `deleted`, `type`, `status`) VALUES
(1, 1447096883, NULL, 'Новый заказ', '/wezom/orders/edit/53', '127.0.0.1', 0, 8, 0),
(2, 1447096946, NULL, 'Заказ в один клик', '/wezom/simple/edit/20', '127.0.0.1', 0, 7, 0),
(3, 1447515976, NULL, 'Новый заказ', '/wezom/orders/edit/54', '109.86.230.160', 0, 8, 0),
(4, 1448259837, NULL, 'Заказ звонка', '/wezom/callback/edit/1', '178.136.229.251', 0, 3, 0),
(5, 1452758290, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/1', '178.136.229.251', 0, 2, 0),
(6, 1453192114, NULL, 'Отзыв к товару', '/wezom/comments/edit/1', '178.136.229.251', 0, 6, 0),
(7, 1453192176, NULL, 'Заказ в один клик', '/wezom/simple/edit/21', '178.136.229.251', 0, 7, 0),
(8, 1453192223, NULL, 'Новый заказ', '/wezom/orders/edit/55', '178.136.229.251', 0, 8, 0),
(9, 1453192288, NULL, 'Вопрос о товаре', '/wezom/questions/edit/1', '178.136.229.251', 0, 5, 0),
(10, 1453192341, NULL, 'Заказ звонка', '/wezom/callback/edit/2', '178.136.229.251', 0, 3, 0),
(11, 1453192417, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/2', '178.136.229.251', 0, 2, 0),
(12, 1453375172, NULL, 'Отзыв к товару', '/wezom/comments/edit/2', '178.136.229.251', 0, 6, 0),
(13, 1453375174, NULL, 'Заказ в один клик', '/wezom/simple/edit/22', '178.136.229.251', 0, 7, 0),
(14, 1453375179, NULL, 'Вопрос о товаре', '/wezom/questions/edit/2', '178.136.229.251', 0, 5, 0),
(15, 1453375179, NULL, 'Вопрос о товаре', '/wezom/questions/edit/3', '178.136.229.251', 0, 5, 0),
(16, 1453375180, NULL, 'Заказ звонка', '/wezom/callback/edit/4', '178.136.229.251', 0, 3, 0),
(17, 1453375180, NULL, 'Заказ звонка', '/wezom/callback/edit/3', '178.136.229.251', 0, 3, 0),
(18, 1453375181, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/3', '178.136.229.251', 0, 2, 0),
(19, 1453375419, NULL, 'Отзыв к товару', '/wezom/comments/edit/3', '178.136.229.251', 0, 6, 0),
(20, 1453375421, NULL, 'Заказ в один клик', '/wezom/simple/edit/23', '178.136.229.251', 0, 7, 0),
(21, 1453375426, NULL, 'Вопрос о товаре', '/wezom/questions/edit/4', '178.136.229.251', 0, 5, 0),
(22, 1453375428, NULL, 'Заказ звонка', '/wezom/callback/edit/5', '178.136.229.251', 0, 3, 0),
(23, 1453375429, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/4', '178.136.229.251', 0, 2, 0),
(24, 1453378494, NULL, 'Отзыв к товару', '/wezom/comments/edit/4', '178.136.229.251', 0, 6, 0),
(25, 1453378496, NULL, 'Заказ в один клик', '/wezom/simple/edit/24', '178.136.229.251', 0, 7, 0),
(26, 1453446803, NULL, 'Отзыв к товару', '/wezom/comments/edit/5', '178.136.229.251', 0, 6, 0),
(27, 1453446814, NULL, 'Заказ в один клик', '/wezom/simple/edit/25', '178.136.229.251', 0, 7, 0),
(28, 1453446848, NULL, 'Вопрос о товаре', '/wezom/questions/edit/5', '178.136.229.251', 0, 5, 0),
(29, 1455810028, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/5', '178.136.229.251', 0, 2, 0),
(30, 1455973056, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/6', '178.136.229.251', 0, 2, 0),
(31, 1455973493, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/7', '178.136.229.251', 0, 2, 0),
(32, 1455974746, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/8', '178.136.229.251', 0, 2, 0),
(33, 1455975369, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/9', '178.136.229.251', 0, 2, 0),
(34, 1455976611, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/10', '178.136.229.251', 0, 2, 0),
(35, 1456738529, NULL, 'Новый заказ', '/wezom/orders/edit/56', '178.136.229.251', 0, 8, 0),
(36, 1456738741, NULL, 'Новый заказ', '/wezom/orders/edit/57', '178.136.229.251', 0, 8, 0),
(37, 1460022269, NULL, 'Новый заказ', '/wezom/orders/edit/57', '85.105.102.56', 0, 8, 0),
(38, 1460975021, NULL, 'Заказ в один клик', '/wezom/simple/edit/26', '178.136.229.251', 0, 7, 0),
(39, 1460975331, NULL, 'Новый заказ', '/wezom/orders/edit/58', '178.136.229.251', 0, 8, 0),
(40, 1460975391, NULL, 'Новый заказ', '/wezom/orders/edit/59', '178.136.229.251', 0, 8, 0),
(41, 1460985084, NULL, 'Заказ звонка', '/wezom/callback/edit/6', '178.136.229.251', 0, 3, 0),
(42, 1460985324, NULL, 'Отзыв к товару', '/wezom/comments/edit/6', '178.136.229.251', 0, 6, 0),
(43, 1461525516, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/11', '213.227.252.130', 0, 2, 0),
(44, 1461526290, NULL, 'Сообщение из контактной формы', '/wezom/contacts/edit/12', '213.227.252.130', 0, 2, 0),
(45, 1461527716, NULL, 'Подписчик', '/wezom/subscribers/edit/1', '213.227.252.130', 0, 4, 0),
(46, 1461527997, NULL, 'Заказ звонка', '/wezom/callback/edit/7', '213.227.252.130', 0, 3, 0),
(47, 1461879659, NULL, 'Отзыв к товару', '/wezom/comments/edit/7', '78.137.5.210', 0, 6, 0),
(48, 1461879708, NULL, 'Вопрос о товаре', '/wezom/questions/edit/6', '78.137.5.210', 0, 5, 0),
(49, 1462879614, NULL, 'Отзыв к товару', '/wezom/comments/edit/8', '178.136.229.251', 0, 6, 0),
(50, 1485440439, NULL, 'Отзыв', '/wezom/reviews/edit/2', '127.0.0.1', 0, 5, 0),
(51, 1485440507, NULL, 'Отзыв', '/wezom/reviews/edit/3', '127.0.0.1', 0, 5, 0),
(52, 1493295046, NULL, 'Заказ звонка', '/wezom/callback/edit/8', '127.0.0.1', 0, 3, 0),
(53, 1493298079, NULL, 'Заказ звонка', '/wezom/callback/edit/9', '127.0.0.1', 0, 3, 0),
(54, 1503916924, NULL, 'Подписчик', '/wezom/subscribers/edit/2', '127.0.0.1', 0, 4, 0),
(55, 1503917384, NULL, 'Подписчик', '/wezom/subscribers/edit/2', '127.0.0.1', 0, 4, 0),
(56, 1503917419, NULL, 'Подписчик', '/wezom/subscribers/edit/2', '127.0.0.1', 0, 4, 0),
(57, 1503917517, NULL, 'Подписчик', '/wezom/subscribers/edit/2', '127.0.0.1', 0, 4, 0),
(58, 1503917532, NULL, 'Подписчик', '/wezom/subscribers/edit/4', '127.0.0.1', 0, 4, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `mail_templates`
--

CREATE TABLE `mail_templates` (
  `id` int(10) NOT NULL,
  `name` varchar(64) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `mail_templates`
--

INSERT INTO `mail_templates` (`id`, `name`, `subject`, `text`, `updated_at`, `status`, `sort`) VALUES
(1, 'Контактная форма ( Администратору )', 'Новое сообщение из контактной формы - сайт {{site}}', '<p>Вам пришло новое письмо из контактной формы с сайта {{site}}!</p>\r\n<p>Имя отправителя: {{name}} ( {{ip}} ).</p>\r\n<p>E-Mail отправителя: {{email}}.</p>\r\n<p>IP отправителя: {{ip}}.</p>\r\n<p>Дата сообщения: {{date}}.</p>\r\n<p>Текст сообщения: {{text}}.</p>\r\n<p>&nbsp;</p>\r\n<p>Письмо сгенерировано автоматически. Пожалуйста не отвечайте на него!</p>', 1483519243, 1, 0),
(2, 'Письмо после подписки ( Пользователю )', 'Спасибо за подписку! Сайт {{site}}', '<p>Спасибо за подписку на новости и акции на сайте {{site}}.</p>\r\n<p>&nbsp;</p>\r\n<h5><span style=\"color: #999999;\">Для того, что бы отписаться, перейдите по ссылке {{link}}.</span></h5>\r\n<h5><span style=\"color: #999999;\">Письмо создано автоматически. Пожалуйста не отвечайте на него.</span></h5>', 1461528132, 1, 3),
(3, 'Заказ звонка ( Администратору )', 'Заказ звонка, сайт {{site}}', '<p>Поступил новый заказ звонка с сайта {{site}}!</p>\r\n<p>Данные заказчика:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>Имя:</td>\r\n<td>{{name}}</td>\r\n</tr>\r\n<tr>\r\n<td>Номер телефона:</td>\r\n<td>{{phone}}</td>\r\n</tr>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1461528132, 1, 1),
(4, 'Подтверждение регистрации ( Пользователю )', 'Пожалуйста, подтвердите свой почтовый адрес, сайт {{site}}', '<p>Для подтверждения регистрации на сайте {{site}}, перейдите по ссылке:</p>\r\n<p>{{link}}</p>', 1461528132, 1, 2),
(5, 'Восстановление пароля ( Пользователю )', 'Восстановление пароля', '<p>Уважаемый, пользователь. Вы воспользовались услугой восстановления пароля на сайте {{site}}.</p>\r\n<p>Ваш новый пароль для входа теперь:</p>\r\n<p>{{password}}</p>', 1461528132, 1, 5),
(6, 'Изменение пароля ( Пользователю )', 'Изменение пароля - {{site}}', '<p>Ув., пользователь! Вы только что изменили пароль на сайте {{site}} на {{password}}!</p>', 1461528132, 1, 4),
(7, 'Новый коментарий ( Администратору )', 'Новый коментарий', '<p>Оставлен новый комментарий на сайте {{site}}!</p>\r\n<p>Информация о комментарии:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n<tr>\r\n<td>Имя:</td>\r\n<td>{{name}}</td>\r\n</tr>\r\n<tr>\r\n<td>Город:</td>\r\n<td>{{city}}</td>\r\n</tr>\r\n<tr>\r\n<td>Товар:</td>\r\n<td><a href=\"{{link}}\">{{item_name}}</a></td>\r\n</tr>\r\n<tr>\r\n<td>Комментарий:</td>\r\n<td>{{text}}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>', 1461528132, 1, 14),
(8, 'Новый быстрый заказ ( Администратору )', 'Новый быстрый заказ', '<p>Только что был совершен быстрый заказ на сайте {{site}}!</p>\r\n<p>Товар: <a href=\"{{link}}\">{{item_name}}</a></p>\r\n<p>Заказчик:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>Номер телефона:</td>\r\n<td>{{phone}}</td>\r\n</tr>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n</tbody>\r\n</table>', 1461528132, 1, 8),
(9, 'Вопрос о товаре ( Администратору )', 'Вопрос о товаре, сайт {{site}}', '<p>Внимание!</p>\r\n<p>Пользователь по имени {{name}} ({{ip}}) задал вопрос по товару <strong><a href=\"{{link}}\">{{item_name}}</a></strong></p>\r\n<p>Вопрос звучит так: {{question}}.</p>\r\n<p>Ответить можно, написав e-mail на почту {{email}}.</p>', 1461528132, 1, 15),
(10, 'Вопрос о товаре ( Пользователю )', 'Вопрос о товаре, сайт {{site}}', '<p>Ув. {{name}}!</p>\r\n<p>Спасибо за проявленный Вами интерес к товару <a href=\"{{link}}\">{{item_name}}</a>.</p>\r\n<p>Ваш вопрос звучал так: {{question}}</p>\r\n<p>Вы получите ответ на этот e-mail в ближайшие сроки!</p>', 1461528132, 1, 16),
(11, 'Новый заказ ( Администратору )', 'Новый заказ', '<p>Только что был совершен заказ на сайте {{site}}!</p>\r\n<p>Информация о заказчике и способе доставки:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n<tr>\r\n<td>Имя:</td>\r\n<td>{{name}}</td>\r\n</tr>\r\n<tr>\r\n<td>Номер телефона:</td>\r\n<td>{{phone}}</td>\r\n</tr>\r\n<tr>\r\n<td>Способ оплаты</td>\r\n<td>{{payment}}</td>\r\n</tr>\r\n<tr>\r\n<td>Способ доставки</td>\r\n<td>{{delivery}}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Информация о заказе:</p>\r\n<p>{{items}}</p>\r\n<p>Для уточнения деталей перейдите по ссылке {{link_admin}}</p>\r\n<p>&nbsp;</p>', 1461528132, 1, 9),
(12, 'Новый заказ ( Пользователю )', 'Новый заказ', '<p>Вы только что оформили&nbsp; заказ на сайте {{site}}!</p>\r\n<p>Информация о заказчике и способе доставки:</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>IP:</td>\r\n<td>{{ip}}</td>\r\n</tr>\r\n<tr>\r\n<td>Имя:</td>\r\n<td>{{name}}</td>\r\n</tr>\r\n<tr>\r\n<td>Номер телефона:</td>\r\n<td>{{phone}}</td>\r\n</tr>\r\n<tr>\r\n<td>Способ оплаты</td>\r\n<td>{{payment}}</td>\r\n</tr>\r\n<tr>\r\n<td>Способ доставки</td>\r\n<td>{{delivery}}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Информация о заказе:</p>\r\n<p>{{items}}</p>\r\n<p>Для уточнения деталей перейдите по ссылке {{link_user}}</p>\r\n<p>&nbsp;</p>', 1461528132, 1, 10),
(13, 'Письмо после регистрации (Пользователю)', 'Регистрация', '<p>Спасибо за регистрацию!</p>\r\n<p>Ваши данные для входа:</p>\r\n<p>Логин - {{email}}</p>\r\n<p>Пароль - {{password}}</p>', 1461528132, 1, 6),
(15, 'Заказ изменен (Заказчику)', 'Ваш заказ {{id}} был откорректирован!', '<p><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\">{{name}}, нам очень жаль.</span></p>\r\n<p><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\">Мы вынуждены были изменить состав <span class=\"il\">ваш</span>его <span class=\"il\">заказ</span>а № {{id}}<br /></span></p>\r\n<p><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\">Вот как выглядит <span class=\"il\">ваш</span> <span class=\"il\">заказ</span> № {{id}} сейчас:</span></p>\r\n<p>{{items}}</p>\r\n<p><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\">Текущая стоимость <span class=\"il\">ваш</span>его <span class=\"il\">заказ</span>а <strong>{{amount}} грн</strong></span></p>\r\n<p><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\"><span style=\"text-align: left; font-size: 14px; color: #333333; font-family: Arial;\">Еще раз извините.</span></span></p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"text-align: left; font-size: 9px; color: #999999; font-family: Arial;\">Сообщение создано автоматически. Пожалуйста не отвечайте на него.</span></p>\r\n<p><span style=\"text-align: right; font-size: 9px; color: #333333; font-family: Arial;\"><span style=\"color: #999999;\">Служба поддержки клиентов {{site}}<br /></span></span></p>\r\n<p>&nbsp;</p>', 1461528132, 1, 11),
(20, 'Смена статуса заказа на \"Отменен\"', 'Ваш заказ отменен!', '<p>Здравствуйте, {{name}}!</p>\r\n<p>Ваш заказ №{{id}} отменен!</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #808080;\">Спасибо, что Вы есть у нас!</span></p>\r\n<p><span style=\"color: #808080;\">Письмо отправлено автоматически, пожалуйста не отвечайте на него!</span></p>', 1461528132, 1, 12),
(21, 'Смена статуса заказа на \"Выполнен\"', 'Ваш заказ выполнен!', '<p>Здравствуйте, {{name}}!</p>\r\n<p>Ваш заказ №{{id}} на сумму {{amount}} успешно выполнен!</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"color: #808080;\">Спасибо, что Вы есть у нас!</span></p>\r\n<p><span style=\"color: #808080;\">Письмо отправлено автоматически, пожалуйста не отвечайте на него!</span></p>', 1461528132, 1, 13),
(26, 'Отправка нового пароля пользователю через админ-панель', 'Изменение пароля - {{site}}', '<p>Ув., пользователь!</p>\r\n<p>Ваш пароль на сайте {{site}} был изменен администрацией на {{password}}!</p>', 1461528132, 1, 7),
(27, 'Новый отзыв (Администратору)', 'Добавлен новый отзыв', '<p>Добавлен новый отзыв</p>\r\n<p>&nbsp;</p>\r\n<p>Имя: {{name}}</p>\r\n<p>Email:&nbsp;{{email}}</p>\r\n<p>IP:&nbsp;{{ip}}</p>\r\n<p>Дата: {{date}}</p>\r\n<p>Текст:&nbsp;{{text}}</p>\r\n<p>Ссылка в админ-панель:&nbsp;{{link_admin}}</p>', 1485440344, 1, 17),
(28, 'Подтверждение подписки (Пользователю)', 'Подтверждение подписки на сайте {{site}}', '<p>Для подтверждения подписки на рассылку на сайте {{site}} для {{email}} перейдите по ссылке {{link}}</p>', 1503916467, 1, 18);

-- --------------------------------------------------------

--
-- Структура таблицы `menu`
--

CREATE TABLE `menu` (
  `id` int(10) NOT NULL,
  `id_parent` int(11) DEFAULT '0',
  `name` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `link` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `id_content` int(11) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `image` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `count` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `menu`
--

INSERT INTO `menu` (`id`, `id_parent`, `name`, `link`, `sort`, `id_content`, `created_at`, `status`, `image`, `updated_at`, `icon`, `count`) VALUES
(1, 0, 'Панель управления', 'index/index', 0, 0, NULL, 1, NULL, NULL, 'fa-dashboard', NULL),
(2, 0, 'SEO', NULL, 16, 0, NULL, 1, '', NULL, 'fa-tags', NULL),
(3, 0, 'Пользователи', 'users/index', 10, 0, NULL, 1, NULL, NULL, 'fa-users', NULL),
(4, 0, 'Настройки сайта', 'config/edit', 15, 0, NULL, 1, NULL, NULL, 'fa-cogs', NULL),
(5, 0, 'Контент', NULL, 1, 0, NULL, 1, NULL, NULL, 'fa-folder-open-o', NULL),
(6, 5, 'Список текстовых страниц', 'content/index', 1, 0, NULL, 0, NULL, NULL, NULL, NULL),
(7, 5, 'Добавить текстовую страницу', 'content/add', 2, 0, NULL, 0, NULL, NULL, NULL, NULL),
(8, 2, 'Метрика и счетчики', 'seo_scripts/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(10, 2, 'Добавить метрику или счетчик', 'seo_scripts/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(12, 2, 'Теги для конкретных ссылок', 'seo_links/index', 0, 0, NULL, 1, NULL, NULL, NULL, NULL),
(13, 2, 'Добавить теги для ссылки', 'seo_links/add', 0, 0, NULL, 1, NULL, NULL, NULL, NULL),
(14, 0, 'Шаблоны писем', 'mailTemplates/index', 14, 0, NULL, 1, NULL, NULL, 'fa-file-image-o', NULL),
(15, 0, 'Меню и категории', '', 4, 0, NULL, 1, NULL, NULL, 'fa-list-ul', NULL),
(19, 5, 'Список новостей', 'news/index', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(20, 5, 'Добавить новость', 'news/add', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(22, 5, 'Список статей', 'articles/index', 6, 0, NULL, 0, NULL, NULL, NULL, NULL),
(23, 5, 'Добавить статью', 'articles/add', 7, 0, NULL, 0, NULL, NULL, NULL, NULL),
(25, 64, 'Список слайдов', 'slider/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(26, 64, 'Добавть слайд', 'slider/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(28, 64, 'Список банеров', 'banners/index', 5, 0, NULL, 0, NULL, NULL, NULL, NULL),
(29, 64, 'Добавить банер', 'banners/add', 6, 0, NULL, 0, NULL, NULL, NULL, NULL),
(30, 0, 'Рассылка', NULL, 11, 0, NULL, 1, NULL, NULL, 'fa-rss', NULL),
(31, 30, 'Список подписчиков', 'subscribers/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(32, 30, 'Добавить подписчика', 'subscribers/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(33, 30, 'Список разосланных писем', 'subscribe/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(34, 30, 'Разослать письмо', 'subscribe/send', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(35, 0, 'Обратная связь', NULL, 12, 0, NULL, 1, NULL, NULL, 'fa-envelope-o', 'all_emails'),
(36, 35, 'Сообщения из контактной формы', 'contacts/index', 1, 0, NULL, 1, NULL, NULL, NULL, 'contacts'),
(37, 35, 'Заказы звонка', 'callback/index', 2, 0, NULL, 1, NULL, NULL, NULL, 'callbacks'),
(38, 0, 'Каталог', NULL, 7, 0, NULL, 1, NULL, NULL, 'fa-inbox', NULL),
(39, 38, 'Категории товаров', 'groups/index', 1, 0, NULL, 0, NULL, NULL, NULL, NULL),
(40, 38, 'Добавить категорию товаров', 'groups/add', 2, 0, NULL, 0, NULL, NULL, NULL, NULL),
(41, 38, 'Товары', 'items/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(42, 38, 'Добавить товар', 'items/add', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(49, 38, 'Вопросы к товарам', 'questions/index', -1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(50, 0, 'Заказы', 'simple/index', 9, 0, NULL, 1, NULL, NULL, 'fa-shopping-cart', 'all_orders'),
(53, 38, 'Отзывы к товарам', 'comments/index', -1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(54, 0, 'Лента событий', 'log/index', 13, 0, NULL, 1, NULL, NULL, 'fa-tasks', NULL),
(57, 2, 'Шаблоны', 'seo_templates/index', -2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(59, 5, 'Системные страницы', 'control/index', 3, 0, NULL, 0, NULL, NULL, NULL, NULL),
(62, 64, 'Список альбомов', 'gallery/index', 3, 0, NULL, 0, NULL, NULL, NULL, NULL),
(63, 64, 'Добавить альбом', 'gallery/add', 4, 0, NULL, 0, NULL, NULL, NULL, NULL),
(64, 0, 'Мультимедиа', NULL, 6, 0, NULL, 1, NULL, NULL, 'fa-picture-o', NULL),
(65, 3, 'Список пользователей', 'users/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(66, 3, 'Список администраторов', 'admins/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(67, 3, 'Добавить администратора', 'admins/add', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(68, 3, 'Список ролей', 'roles/index', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(69, 3, 'Добавить роль', 'roles/add', 6, 0, NULL, 1, NULL, NULL, NULL, NULL),
(70, 3, 'Добавить пользователя', 'users/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(71, 2, 'Перенаправления', 'seo_redirects/index', 6, 0, NULL, 1, NULL, NULL, NULL, NULL),
(72, 2, 'Добавить перенаправление', 'seo_redirects/add', 6, 0, NULL, 1, NULL, NULL, NULL, NULL),
(108, 0, 'Статистика', '', 999, 0, NULL, 1, '', NULL, 'fa-signal', NULL),
(109, 108, 'Посетители сайта', 'visitors/index', 1, 0, NULL, 1, '', NULL, '', NULL),
(110, 108, 'Переходы по сайту', 'hits/index', 2, 0, NULL, 1, '', NULL, '', NULL),
(111, 108, 'Рефереры', 'referers/index', 3, 0, NULL, 1, '', NULL, '', NULL),
(112, 108, 'Статистика по пользователям', 'statistic/users', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(113, 108, 'Статистика по товарам', 'statistic/items', 5, 0, NULL, 1, NULL, NULL, NULL, NULL),
(114, 0, 'Блог', NULL, 1, 0, NULL, 0, NULL, NULL, 'fa-bullhorn', 'all_blog'),
(115, 114, 'Записи в блоге', 'blog/index', 1, 0, NULL, 1, NULL, NULL, NULL, 'blog'),
(116, 114, 'Добавить запись в блог', 'blog/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(117, 114, 'Рубрики', 'blogRubrics/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(118, 114, 'Добавить рубрику', 'blogRubrics/add', 4, 0, NULL, 1, NULL, NULL, NULL, NULL),
(119, 114, 'Комментарии', 'blogComments/index', 5, 0, NULL, 1, NULL, NULL, NULL, 'blog_comments'),
(120, 114, 'Добавить комментарий', 'blogComments/add', 6, 0, NULL, 1, NULL, NULL, NULL, NULL),
(121, 0, 'Черный список', NULL, 10, 0, NULL, 0, NULL, NULL, 'fa-file', NULL),
(122, 121, 'Заблокированные адреса', 'blacklist/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(123, 121, 'Заблокировать адрес', 'blacklist/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(124, 0, 'Отзывы', NULL, 10, 0, NULL, 1, NULL, NULL, 'fa-weixin', 'reviews'),
(125, 124, 'Список отзывов', 'reviews/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(126, 124, 'Добавить отзыв', 'reviews/add', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(127, 0, 'Переводы', 'btranslates', 15, 0, NULL, 1, NULL, NULL, 'fa-language', NULL),
(128, 2, 'Файлы', 'seo_files/index', 8, 0, NULL, 1, NULL, NULL, NULL, NULL),
(129, 2, 'Добавить файл', 'seo_files/add', 9, 0, NULL, 1, NULL, NULL, NULL, NULL),
(130, 2, 'Карта сайта', 'sitemap/index', 10, 0, NULL, 1, NULL, NULL, NULL, NULL),
(131, 127, 'Переводы сайта', 'translates', NULL, 0, NULL, 1, NULL, NULL, NULL, NULL),
(132, 127, 'Переводы админки', 'btranslates', NULL, 0, NULL, 1, NULL, NULL, NULL, NULL),
(133, 15, 'Верхнее меню', 'menu/index', 3, 0, NULL, 1, NULL, NULL, NULL, NULL),
(134, 15, 'Меню над слайдером', 'menu/topMenu', 2, 0, NULL, 1, NULL, NULL, NULL, NULL),
(135, 15, 'Левое меню (<b>категории</b>)', 'groups/index', 1, 0, NULL, 1, NULL, NULL, NULL, NULL),
(136, 0, NULL, NULL, NULL, 0, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `models`
--

CREATE TABLE `models` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `brand_alias` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `models`
--

INSERT INTO `models` (`id`, `created_at`, `updated_at`, `status`, `name`, `alias`, `brand_alias`) VALUES
(90, 1447092911, NULL, 1, '1300', '1300', 'newbalance'),
(91, 1447092920, NULL, 1, '1400', '1400', 'newbalance'),
(92, 1447092926, NULL, 1, '1500', '1500', 'newbalance'),
(93, 1447092933, NULL, 1, '1600', '1600', 'newbalance'),
(94, 1447092957, 1491372178, 1, 'Air Foamposite', 'airfoamposite', 'nike'),
(95, 1447092969, NULL, 1, 'Air Force', 'airforce', 'nike'),
(96, 1447092981, NULL, 1, 'Air Huarache ', 'airhuarache', 'nike'),
(97, 1447092991, NULL, 1, 'Air Jordan ', 'airjordan', 'nike'),
(98, 1462882106, 1462882111, 1, 'Puma Creepers', 'pumacreepers', 'puma'),
(99, 1464615947, 1483518838, 1, 'одеяло', 'odejalo', 'lancome');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `text` text,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `image` varchar(255) DEFAULT NULL,
  `show_image` tinyint(1) NOT NULL DEFAULT '1',
  `views` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `created_at`, `updated_at`, `status`, `name`, `alias`, `date`, `text`, `h1`, `title`, `description`, `keywords`, `image`, `show_image`, `views`) VALUES
(24, 1447090575, 1485244986, 1, 'Статья про мебель 1', 'odezhda-podcherkivajuschaja-zagar', 1441054800, '<p style=\"text-align: justify;\">Лето &ndash; пора отпусков и поездок на море, солнечных ванн и шоколадного загара. Но рано или поздно каникулы заканчиваются, и настают серые будни. Но можно продлить воспоминания об отдыхе с помощью одежды. Правильно подобранные оттенки наряда подчеркнут красивый загар.</p>\r\n<h2 style=\"text-align: justify;\">Правильное цветовое решение</h2>\r\n<p style=\"text-align: justify;\">Грамотно выбрав оттенок одежды, можно подчеркнуть загар, сделать его зрительно более насыщенным. Итак, дамам, которые только вернулись с лазурного берега, стоит отдать предпочтение следующим цветам:</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<ul>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Белому</strong>. Он не только подчеркнет бронзовую кожу, но и освежит образ, притянет внимание. Также актуальны и его оттенки: жемчужный, молочный, ивори.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Пудровый </strong>&ndash; этот пастельный полутон отлично подойдет для летнего гардероба (юбок, брюк, блуз, платьев, босоножек). Модницам следует выбирать вещи, близкие по цвету с кожей, но немного светлее.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Розовый</strong>, но не яркий неоновый, а теплый и нежный. Модницам стоит помнить, что не стоит полностью облачаться в розовые вещи, иначе есть риск показаться куклой Барби.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Мятный</strong>. Этот бодрящий и свежий цвет модный вот уже несколько лет подряд. Он выгодно подчеркнет золотистый оттенок кожи и поднимет настроение всем окружающим. Также отлично сочетается с другими пастельными тонами.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Лиловый</strong>. Благородный цвет. Может быть как фоновым в образе, так и ведущим. Отлично подходит жгучим брюнеткам с оливковым отливом кожи.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n<li style=\"text-align: justify;\">&nbsp;&ndash;&nbsp;<strong>Голубой </strong>&ndash; отличное решение для летних рубашек, платьев, босоножек. Он уместен как в повседневной жизни, так и в праздник, подходит и для прогулки, и для похода в офис. Вещи данного цвета отлично сочетаются с одеждой в пастельной палитре.</li>\r\n<li style=\"text-align: justify;\"></li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Последний совет дизайнеров для тех, кто хочет подчеркнуть загар: выбирайте цвета, которые вам к лицу, а не только те, которые модные. Неправильно подобранный оттенок может испортить образ, и все ваши усилия будут напрасными. </p>', 'Статья про мебель 1', 'Одежда, подчеркивающая загар', 'Одежда, подчеркивающая загар', 'Одежда, подчеркивающая загар', 'block3-2.png', 0, 13),
(25, 1447090634, 1492434166, 1, ' Статья про мебель 2', 'podvernuli-molodtsy', 1447016400, '<p style=\"text-align: justify;\">Как всегда, море западной моды разбушевалось, хлынула очередная волна, неспешно прошлась по всей Земле &ndash; и обстоятельно затопила наши края. Мы находимся на самом высоком берегу, поэтому ждать прилива приходится долго. Но именно нам достаются самые сильные, жгучие, раскатистые волны. На этот раз море выкинуло на наш берег джинсы. Да не простые &ndash; подвернутые. Однажды наш дизайнер шел по пляжу, поднял морской дар, сдул песок, надел &ndash; и пошел нести тренд в массы. И носят теперь все поголовно эти джинсы: и когда надо, и когда не надо. Кстати, о &laquo;не надо&raquo;. Иногда эти милые сердцу подвороты бывают неуместны.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<blockquote>\r\n<p>asdfasdfasdfasdfas<br />asdfasdfasdfasdfasdf<br />asdfasdfasdfadsfasd</p>\r\n</blockquote>\r\n<h2>Изображения</h2>\r\n<p><img style=\"float: left; margin-bottom: 10px; margin-right: 10px;\" src=\"http://cms.wezom.ks.ua/Media/files/filemanager/images.jpg\" alt=\"\" width=\"183\" height=\"275\" />Lorem ipsum dolor sit <span style=\"color: #9e2e2e;\">amet, consectetur adipisicing elit. Debitis a necessitatibus, aut accusamus. Cum adipisci ullam alias enim culpa, quae deleniti placeat pr</span>ovident, ipsum error officiis! Debitis cumque, quae, id cum laboriosam dolores sequi minima mollitia a animi sunt. Similique, ad cumque nisi ipsum iste ex placeat unde dicta labore sequi aliquam voluptate ut enim repellendus, dolor incidunt earum laboriosam?</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis a necessitatibus, aut accusamus. Cum adipisci ullam alias enim culpa, quae deleniti placeat provident, ipsum error officiis! Debitis cumque, quae, id cum laboriosam dolores sequi minima mollitia a animi sunt. Similique, ad cumque nisi ipsum iste ex placeat unde dicta labore sequi aliquam voluptate ut enim repellendus, dolor incidunt earum laboriosam?</p>\r\n<h2>Когда не нужно подворачивать</h2>\r\n<p style=\"text-align: justify;\">Если углубляться в недра фэшн-стайлинга, то можно открыть для себя, что на работу нужно ходить с ровно подвернутыми брюками, а на пляж подойдет и нарочитый кэжуал. Но нам это особенно ни к чему &ndash; мы люди простые. Дизайнеры из народа грозятся пальцами на тех, кто подворачивает джинсы при:</p>\r\n<ul style=\"text-align: justify; list-style-type: circle;\">\r\n<li>&nbsp; &ndash;&nbsp;невысоком росте &ndash; драгоценные сантиметры в этом случае заберет не только природа, но и вы сами, собственными рученьками;</li>\r\n<li>&nbsp; &ndash;&nbsp;супероблегающих джинсах типа скинни;</li>\r\n<li>&nbsp; &ndash;&nbsp;моделях клеш (если вы, конечно, еще их не выкинули).</li>\r\n</ul>\r\n<p style=\"text-align: justify;\">Некоторые особенно чопорные мистеры, укоренившиеся в своих домах моды, пытаются ввести еще и возрастное ограничение. Но мы-то с вами знаем, что возраст &ndash; не в паспорте, возраст &ndash; в душе! И лучше укорачивать джинсы, чем свою свободу!</p>\r\n<h2 style=\"text-align: justify;\">Как правильно подворачивать?</h2>\r\n<p style=\"text-align: justify;\">Вы не поверите, но еще лет 10 назад подвернутые джинсы считались признаком лени владелицы. Мол, так не хочется шить да укорачивать, что лучше я просто заправлю эти длинные штанины. И позор с пеплом сыпались на их головушки, пока какому-то дизайнеру тоже не стало лень их постоянно подшивать. И думает он: &laquo;А дай-ка понесу сей тренд да по всему миру&raquo;. И &ndash; вуаля! &ndash; лень опять стала двигателем прогресса.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Лучший вариант для поворотов &ndash; джинсы прямого кроя, &laquo;бойфренды&raquo; и &laquo;а-ля чинос&raquo;. Ну, и куда же без наглухо вытертых варенок? А вообще, выбирайте все, что ваша душа пожелает. Главное &ndash; чтобы брючины в районе икры не были в облипку. Идеально, если между ними и обувью ничего нет &ndash; ни носков, ни гольф. Только подвороты, только хардкор!</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Способов подворачивать джинсы столько, что их откровенно лень перечислять. Просто заправьте их так, как вам удобно &ndash; и, бьемся об заклад, будете в тренде. Главное, не сильно старайтесь их сделать идеально ровными. Все-таки это кэжуал. А кэжуал &ndash; это небрежность.</p>', 'Статья про мебель 2', 'Подвернули? Молодцы!', 'Подвернули? Молодцы!', 'Подвернули? Молодцы!', 'catalog-5.jpg', 1, 46),
(26, 1508577210, NULL, 1, 'Статья про мебель 3', 'statja-pro-mebel-3', 1508619600, '<p>Статья 3</p>', '', '', 'Описание', 'ыфв фы ', 'f3b58048dcb87c391ef3ed7b5e5e426f.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `payment` int(2) DEFAULT NULL,
  `delivery` int(2) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `user_id` int(10) DEFAULT '0',
  `ip` varchar(16) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `done` int(10) DEFAULT NULL COMMENT 'Дата когда заказ был выполнен',
  `changed` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `status`, `payment`, `delivery`, `name`, `phone`, `user_id`, `ip`, `last_name`, `middle_name`, `email`, `done`, `changed`) VALUES
(53, 1447096883, 1448547910, 0, 1, 1, 'Томпсон Валера', '+38 (234) 234-23-42', 21, '127.0.0.1', NULL, NULL, NULL, NULL, 0),
(54, 1447515976, 1452862399, 3, 2, 1, 'ыфвомлфи', '+38 (211) 341-31-12', 23, '109.86.230.160', NULL, NULL, NULL, NULL, 0),
(55, 1453192223, NULL, 0, 1, 1, 'Test name', '+38 (555) 555-55-55', 0, '178.136.229.251', NULL, NULL, NULL, NULL, 0),
(56, 1456738529, 1458764177, 0, 1, 1, 'Test Wezom', '+38 (095) 740-69-57', 0, '178.136.229.251', NULL, NULL, NULL, NULL, 0),
(57, 1460022269, 1465463782, 0, 2, 1, 'sadsad', '+38 (123) 213-21-32', 0, '85.105.102.56', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `orders_items`
--

CREATE TABLE `orders_items` (
  `id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `catalog_id` int(10) DEFAULT NULL,
  `size_id` int(10) NOT NULL DEFAULT '0',
  `cost` int(10) NOT NULL DEFAULT '0',
  `count` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders_items`
--

INSERT INTO `orders_items` (`id`, `order_id`, `catalog_id`, `size_id`, `cost`, `count`) VALUES
(84, 53, 48, 0, 1450, 2),
(85, 54, 45, 0, 950, 2),
(86, 53, 45, 0, 950, 1),
(87, 55, 46, 0, 875, 1),
(88, 56, 62, 0, 670, 1),
(89, 57, 116, 0, 890, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `orders_simple`
--

CREATE TABLE `orders_simple` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `phone` varchar(64) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `text` text,
  `delivery` int(1) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `floor` int(3) DEFAULT NULL,
  `catalog_id` int(10) DEFAULT NULL,
  `counts` int(2) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders_simple`
--

INSERT INTO `orders_simple` (`id`, `created_at`, `updated_at`, `status`, `phone`, `ip`, `name`, `adress`, `text`, `delivery`, `email`, `floor`, `catalog_id`, `counts`, `user_id`) VALUES
(27, 1508502340, NULL, 0, '+3803323231222', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', 'saddsa', 2, 'admin@admin.admin', 5, 32, 0, NULL),
(28, 1508502340, NULL, 0, '+3803323231222', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', 'saddsa', 2, 'admin@admin.admin', 5, 33, 0, NULL),
(29, 1508502548, NULL, 0, '+380952015405', NULL, 'Тест заказа', 'Мира 25 кв 56', 'фывфывфывфывф', 2, 'admin@admin.admin', 33, 32, 0, NULL),
(30, 1508502548, NULL, 0, '+380952015405', NULL, 'Тест заказа', 'Мира 25 кв 56', 'фывфывфывфывф', 2, 'admin@admin.admin', 33, 33, 0, NULL),
(31, 1508502608, NULL, 0, '+3803323231222', NULL, 'Стул для персонала', 'Мира 25 кв 56', 'сччссчясчясч', 2, 'TestHost@TestHost.com', 5, 31, 0, 39),
(32, 1508502740, NULL, 1, '+3803323231222', NULL, 'фыв', 'Мира 25 кв 56', '11111111111', 1, 'test1@mail.ru', 12, 49, 0, 39),
(33, 1508503390, NULL, 0, '+380952015405', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', '123123', 2, 'Acc_Test@gmail.com', 2, 33, 0, 39),
(34, 1508507474, NULL, 1, '+3803323231222', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', 'лоолрл', 2, 'admin@admin.admin', 33, 54, 0, 39),
(35, 1508578629, NULL, 0, '+380952015405', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', 'afafaf', 2, 'Makc00766@gmail.com', 2, 49, 0, NULL),
(36, 1508758089, NULL, 0, '+380952015405', NULL, 'Болгар Максим Сергеевич', 'Мира 25 кв 56', '333efee32rwed2', 2, 'admin@admin.admin', 33, 61, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `popup_messages`
--

CREATE TABLE `popup_messages` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `ru` text,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `popup_messages`
--

INSERT INTO `popup_messages` (`id`, `name`, `ru`, `updated_at`) VALUES
(1, 'message_after_oformleniya_basket', '<p>Благодарим вас за заказ! <br /> Менеджер свяжется с вами в ближайшее время.</p>', 2013),
(2, 'message_error_captcha', 'Вы неправильно ввели код безопасности.<br> Повторите, пожалуйста, отправку данных, внимательно указав код.', NULL),
(3, 'message_after_q_about_good', 'Ваш вопрос принят. Менеджер ответит вам в ближайшее время.', NULL),
(4, 'message_add_contact', 'Благодарим вас за сообщение!', NULL),
(5, 'message_add_comment_to_guestbook', 'Благодарим вас за оставленный отзыв. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.', NULL),
(6, 'message_add_comment_to_news', 'Благодарим вас за оставленный комментарий. <br>С вашей помощью наш сайт становится интереснее и полезнее. <br>Администрация сайта обязательно рассмотрит ваши материалы и опубликует их в ближайшее время.', NULL),
(7, 'message_error_login', 'Неправильно введен логин', NULL),
(8, 'message_after_registration', 'Благодарим вас за регистрацию на нашем сайте! Приятной работы!', NULL),
(9, 'message_text_after_registration_user_at_site', 'Благодарим вас за регистрацию на нашем сайте! <br /> На ваш email, указанный при регистрации, отправлено уведомление с данными для входа в ваш личный кабинет на сайте. <br /> Приятной работы!', NULL),
(10, 'message_after_autorisation', 'Данные введены правильно! Приятной работы!', NULL),
(11, 'message_text_after_autorisation_user_at_site', 'Добро пожаловать на наш сайт! <br /> Воспользуйтесь личным кабинетом для: редактирования своих данных и просмотра истории покупок. <br /> Приятной работы!', NULL),
(12, 'message_error_autorisation', 'Данные введены неправильно! Будьте внимательны!', NULL),
(13, 'message_after_exit', 'Возвращайтесь еще!', NULL),
(14, 'message_text_after_exit', 'Администрация сайта благодарит вас за время, потраченное на нашем сайте. До скорых встреч!', NULL),
(16, 'message_after_edit_data', 'Выши данные изменены. Приятной работы.', NULL),
(17, 'message_text_after_edit_data', 'Благодарим вас внимание к нашему сайту. <br /> На ваш email, указанный при регистрации, отправлено уведомление с измененными данными. <br /> Приятной работы!', NULL),
(18, 'subscribe_refuse', 'Вы отказались от рассылки на сайте!', NULL),
(19, 'subscribe_refuse_error', 'Вы не подписывались на рассылку на сайте!', NULL),
(20, 'subscribe_already01', 'уже является подписчиком на сайте!', NULL),
(21, 'subscribe_already02', 'введите другую почту для подписки.', NULL),
(22, 'subscribe_done', 'Вы подписались на рассылку на сайте', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `ip` varchar(16) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(3) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `stars` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `created_at`, `updated_at`, `status`, `ip`, `date`, `name`, `parent_id`, `email`, `text`, `stars`) VALUES
(2, 1485440439, NULL, 1, '127.0.0.1', 1485440439, 'test', 33, 'alyohina.i.wezom@gmail.com', 'test tes test', 4),
(3, 1508407079, NULL, 1, NULL, NULL, 'Максим', 33, 'TestHost@TestHost.com', 'Нормально', 2),
(4, 1508407328, NULL, 1, NULL, NULL, 'Отзыв стула', 31, 'admin@admin.admin', 'Текст текст отзыва', 5),
(5, 1508407391, NULL, 1, NULL, NULL, 'Проверка', 31, 'TestHost@TestHost.com', 'Проверка работы системы рейтинга товара', 1),
(6, 1508407445, NULL, 1, NULL, NULL, 'Проверка', 31, 'TestHost@TestHost.com', 'Проверка работы системы рейтинга товара', 1),
(7, 1508407457, NULL, 1, NULL, NULL, 'Проверка', 31, 'TestHost@TestHost.com', 'Проверка работы системы рейтинга товара', 1),
(8, 1508408283, NULL, 1, NULL, NULL, 'sadsa', 33, 'Acc_Test@gmail.com', 'wadasdadwadadw', 5),
(9, NULL, NULL, 1, NULL, NULL, 'aasd', 33, 'asdsaadas@sads.adsd', 'adsad', 5),
(10, 1508747044, NULL, 1, NULL, NULL, 'Болгар Максим Сергеевич', 55, 'TestHost@TestHost.com', 'Отзыв', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `reviewsMain`
--

CREATE TABLE `reviewsMain` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `ip` varchar(16) DEFAULT NULL,
  `date` int(10) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) DEFAULT NULL,
  `text` text NOT NULL,
  `date_answer` varchar(255) DEFAULT NULL,
  `answer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reviewsMain`
--

INSERT INTO `reviewsMain` (`id`, `created_at`, `updated_at`, `status`, `ip`, `date`, `name`, `email`, `text`, `date_answer`, `answer`) VALUES
(1, 1485437340, NULL, 1, '', 1485291600, 'test', 'test@test.com', 'Супер сайт все дела', NULL, 'Ваш ответ'),
(9, 1485437340, NULL, 1, NULL, 1485437340, 'Максим', 'Makc00766@gmail.com', 'review by ..', NULL, 'Ваш ответ'),
(10, 1508430095, 1508504632, 1, NULL, 1508533200, 'Болгар Максим Сергеевич', 'Makc00766@gmail.com', 'saasdsa', '1507928400', 'Сложнооо');

-- --------------------------------------------------------

--
-- Структура таблицы `seo_links`
--

CREATE TABLE `seo_links` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `seo_redirects`
--

CREATE TABLE `seo_redirects` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `link_from` varchar(255) DEFAULT NULL,
  `link_to` varchar(255) DEFAULT NULL,
  `type` int(4) NOT NULL DEFAULT '300'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `seo_scripts`
--

CREATE TABLE `seo_scripts` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `script` text,
  `status` int(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `place` varchar(31) DEFAULT 'head'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `seo_templates`
--

CREATE TABLE `seo_templates` (
  `id` int(10) NOT NULL,
  `name` varchar(250) CHARACTER SET cp1251 DEFAULT NULL,
  `h1` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `keywords` text,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seo_templates`
--

INSERT INTO `seo_templates` (`id`, `name`, `h1`, `title`, `description`, `keywords`, `updated_at`) VALUES
(1, 'Шаблон для групп товаров', '{{name}}', '{{name}} в интернет магазине Airpac. Купить обувь в Украине, Киеве', '{{content:20}}', '{{name}}, Купить обувь в Украине, Киеве', 1431017014),
(2, 'Шаблон для товаров', '{{name}}', '{{name}} в интернет магазине. Купить лучшую мебель {{group}} в Украине от {{price}} грн', 'Купить {{name}} в Украине. Огромный ассортимент обуви, современные {{group}} от компании {{brand}}. Гарантия качества, своевременная доставка, любые способы оплаты от {{price}} грн', 'Купить, {{name}}, {{group}}, {{brand}}, мебель от {{price}} грн', 1508227987),
(3, 'Шаблон для производителей', '{{name}}', 'Шаблон для title производителя {{name}}', 'Шаблон для description производителя {{name}}', 'Шаблон для keywords производителя {{name}}', 1492428398);

-- --------------------------------------------------------

--
-- Структура таблицы `sitemap`
--

CREATE TABLE `sitemap` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `tpl` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sitemap`
--

INSERT INTO `sitemap` (`id`, `alias`, `name`, `parent_id`, `status`, `updated_at`, `sort`, `tpl`) VALUES
(1, 'content', 'Контентовые страницы', 0, 1, NULL, 2, 'Content'),
(2, 'contact', 'Контакты', 0, 1, NULL, 3, NULL),
(3, 'news', 'Новости', 0, 1, NULL, 4, 'News'),
(4, 'news_list', 'Список новостей', 3, 1, NULL, 1, NULL),
(5, 'articles', 'Статьи', 0, 1, NULL, 5, 'Articles'),
(6, 'articles_list', 'Список статей', 5, 1, NULL, 1, NULL),
(7, 'blog', 'Блог', 0, 1, NULL, 6, 'Blog'),
(8, 'blog_rubrics', 'Список рубрик', 7, 1, NULL, 1, NULL),
(9, 'blog_list', 'Список статей', 7, 1, NULL, 2, NULL),
(10, 'gallery', 'Фотогаллерея', 0, 1, NULL, 7, 'Gallery'),
(11, 'gallery_list', 'Список альбомов', 10, 1, NULL, 1, NULL),
(12, 'reviews', 'Отзывы', 0, 1, NULL, 8, NULL),
(13, 'catalog', 'Каталог', 0, 1, NULL, 9, 'Catalog'),
(14, 'catalog_groups', 'Список групп', 13, 1, NULL, 1, NULL),
(15, 'catalog_items', 'Список товаров', 14, 1, NULL, 1, NULL),
(16, 'catalog_sale', 'Акции', 13, 1, NULL, 3, 'Sale'),
(17, 'catalog_new', 'Новинки', 13, 1, NULL, 4, 'New'),
(18, 'catalog_popular', 'Популярные', 13, 1, NULL, 5, 'Popular'),
(19, 'catalog_brands', 'Производители', 13, 1, NULL, 6, 'Brands'),
(21, 'sitemap', 'Карта сайта', 0, 1, NULL, 10, NULL),
(22, 'index', 'Главная страница', 0, 1, NULL, 1, NULL),
(23, 'brands_list', 'Список производителей', 19, 1, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `sitemenu`
--

CREATE TABLE `sitemenu` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sitemenu`
--

INSERT INTO `sitemenu` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `url`) VALUES
(6, 1447091549, 1508251552, 1, 2, 'Галерея', '/gallery'),
(7, 1447091558, 1508251552, 1, 1, 'Гарантия', '/garant'),
(8, 1447091606, 1508251552, 1, 0, 'О компании', '/'),
(9, 1447091617, 1508251552, 1, 4, 'Покупателю', '/faq'),
(10, 1447091626, 1508251552, 1, 5, 'Контакты', '/contacts'),
(11, 1453977094, 1508251552, 1, 3, 'Доставка', '/delivery'),
(12, NULL, 1508251552, 1, 6, 'Отзывы', '/top/reviews'),
(13, NULL, 1508251552, 1, 7, 'Статьи ', '/news');

-- --------------------------------------------------------

--
-- Структура таблицы `sitemenu_catalog`
--

CREATE TABLE `sitemenu_catalog` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name_cat` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `column` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sitemenu_catalog`
--

INSERT INTO `sitemenu_catalog` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name_cat`, `url`, `parent_id`, `image`, `icon`, `column`) VALUES
(28, 1504268019, 1504269109, 1, 0, 'Столы', 'tables', 20, 'icon-menu-left-3.png', NULL, 1),
(29, 1504268088, 1504268297, 1, 1, 'Комоды и тумбы', 'comods', 20, 'icon-menu-left-8.png', NULL, 2),
(33, 1504268214, 1504268297, 1, 2, 'Шкафы', 'shkafu', 20, 'icon-menu-left-7.png', NULL, 4),
(37, NULL, NULL, 1, 3, 'Кресла', 'kresla', NULL, 'icon-menu-left-1.png', NULL, NULL),
(38, NULL, NULL, 1, 4, 'Стулья', 'stylia', NULL, 'icon-menu-left-2.png', NULL, NULL),
(39, NULL, NULL, 1, 5, 'Кровати', 'krovati', NULL, 'icon-menu-left-4.png', NULL, NULL),
(40, NULL, NULL, 1, 6, 'Матрасы', 'matrasu', NULL, 'icon-menu-left-5.png', NULL, NULL),
(41, NULL, NULL, 1, 7, 'Мягкая мебель', 'myahkaya-mebel', NULL, 'icon-menu-left-6.png', NULL, NULL),
(42, NULL, NULL, 1, 8, 'Вешалки', 'veshalku', NULL, 'icon-menu-left-11.png', NULL, NULL),
(43, NULL, NULL, 1, 9, 'Прихожие', 'prihozhii', NULL, 'icon-menu-left-9.png', NULL, NULL),
(44, NULL, NULL, 1, 10, 'Гостинные', 'gostinue', NULL, 'icon-menu-left-10.png', NULL, NULL),
(45, NULL, NULL, 1, 11, 'Детские', 'detskie', NULL, 'icon-menu-left-12.png', NULL, NULL),
(46, NULL, NULL, 1, 12, 'Спальни', 'spalni', NULL, 'icon-menu-left-13.png', NULL, NULL),
(47, NULL, NULL, 1, 13, 'Модульная мебель', 'modulnaya-mebel', NULL, 'icon-menu-left-14.png', NULL, NULL),
(48, NULL, NULL, 1, 14, 'Комплекты офисной мебели', 'komplektu-office', NULL, 'icon-menu-left-15.png', NULL, NULL),
(52, 1508153358, NULL, 1, 0, 'Диваны', 'divany', NULL, 'icon-about-4.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE `slider` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `text` text,
  `image` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `url`, `text`, `image`) VALUES
(6, NULL, 1508742643, 1, 0, 'Акция 1', 'http://wezom-cms-4.loc/top/divany', 'Текст акции 1', 'index-slider-1.jpg'),
(7, NULL, 1508742643, 1, 1, 'Акция 2', 'http://wezom-cms-4.loc/category/mini-divany', 'Текст акции 2', 'maxresdefault.jpg'),
(8, NULL, 1508742643, 1, 2, 'Акция 3', 'http://wezom-cms-4.loc/category/uglovye-divany', 'Текст акции 3', '3b268a946fc9fc3a7c325ec5113d3339.png'),
(12, 1508743726, NULL, 1, 0, 'Акция 4 ', 'http://wezom-cms-4.loc/category/ofisnye-kresla-dlja-personala', '<p><strong>Акция 4</strong></p>\r\n<p><em>Текст</em></p>\r\n<p><em><img class=\"\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/test-images/prestige_2_gtp.jpg\" alt=\"\" width=\"112\" height=\"142\" /></em></p>', 'slider_pointloma.png');

-- --------------------------------------------------------

--
-- Структура таблицы `specifications`
--

CREATE TABLE `specifications` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `type_id` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `specifications`
--

INSERT INTO `specifications` (`id`, `created_at`, `updated_at`, `status`, `name`, `sort`, `alias`, `type_id`) VALUES
(19, 1447093027, 1464684912, 1, 'Материал', 6, 'material', 3),
(20, 1447093044, NULL, 1, 'Производство', 5, 'proizvodstvo', 2),
(21, 1447093054, NULL, 1, 'Сезон', 3, 'sezon', 3),
(22, 1447093064, 1447095073, 1, 'Цвет', 2, 'tsvet', 1),
(23, 1447093519, NULL, 1, 'Половой признак', 4, 'sex', 2),
(24, 1463997483, 1464196645, 1, 'Описание', 1, 'opisanie', 2),
(25, 1465039912, 1483518854, 1, 'новинка', 0, 'novinka', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `specifications_types`
--

CREATE TABLE `specifications_types` (
  `id` int(2) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `specifications_types`
--

INSERT INTO `specifications_types` (`id`, `name`) VALUES
(1, 'Цвет'),
(2, 'Обычная'),
(3, 'Мультивыбор');

-- --------------------------------------------------------

--
-- Структура таблицы `specifications_values`
--

CREATE TABLE `specifications_values` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `specification_id` int(10) NOT NULL,
  `color` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `specifications_values`
--

INSERT INTO `specifications_values` (`id`, `created_at`, `updated_at`, `status`, `name`, `sort`, `alias`, `specification_id`, `color`) VALUES
(61, 1447093079, NULL, 1, 'Замша', 0, 'zamsha', 19, NULL),
(62, 1447093084, NULL, 1, 'Искусственная замша', 0, 'iskusstvennajazamsha', 19, NULL),
(63, 1447093089, NULL, 1, 'Искусственная кожа', 0, 'iskusstvennajakozha', 19, NULL),
(64, 1447093092, NULL, 1, 'Кожа', 0, 'kozha', 19, NULL),
(65, 1447093097, NULL, 1, 'Латекс', 0, 'lateks', 19, NULL),
(66, 1447093101, NULL, 1, 'Нейлон', 0, 'nejlon', 19, NULL),
(67, 1447093107, NULL, 1, 'Нубук', 0, 'nubuk', 19, NULL),
(68, 1447093112, NULL, 1, 'Полиестр', 0, 'poliestr', 19, NULL),
(69, 1447093116, NULL, 1, 'Полимер', 0, 'polimer', 19, NULL),
(70, 1447093120, NULL, 1, 'Силикон', 0, 'silikon', 19, NULL),
(71, 1447093125, NULL, 1, 'Синтетика', 0, 'sintetika', 19, NULL),
(72, 1447093130, NULL, 1, 'Текстиль', 0, 'tekstil', 19, NULL),
(73, 1447093142, NULL, 1, 'Хлопок', 0, 'hlopok', 19, NULL),
(74, 1447093164, NULL, 1, 'Китай', 0, 'kitaj', 20, NULL),
(75, 1447093169, NULL, 1, 'Украина', 0, 'ukraina', 20, NULL),
(76, 1447093177, NULL, 1, 'США', 0, 'ssha', 20, NULL),
(77, 1447093182, NULL, 1, 'Италия', 0, 'italija', 20, NULL),
(78, 1447093190, NULL, 1, 'Вьетнам', 0, 'vetnam', 20, NULL),
(79, 1447093196, NULL, 1, 'Индонезия', 0, 'indonezija', 20, NULL),
(80, 1447093205, NULL, 1, 'Польша', 0, 'polsha', 20, NULL),
(81, 1447093215, NULL, 1, 'Лето', 1, 'leto', 21, NULL),
(82, 1447093220, NULL, 1, 'Зима', 0, 'zima', 21, NULL),
(83, 1447093225, NULL, 1, 'Весна', 2, 'vesna', 21, NULL),
(84, 1447093230, NULL, 1, 'Осень', 3, 'osen', 21, NULL),
(85, 1447093257, NULL, 1, 'Бежевый', 1, 'beige', 22, '#f0daad'),
(86, 1447093270, NULL, 1, 'Белый', 0, 'white', 22, '#ffffff'),
(87, 1447093288, NULL, 1, 'Бирюзовый', 2, 'turquoise', 22, '#4ff58c'),
(88, 1447093303, NULL, 1, 'Бордовый', 3, 'claret', 22, '#7a0b21'),
(89, 1447093316, NULL, 1, 'Голубой', 4, 'light_blue', 22, '#00bfff'),
(90, 1447093343, NULL, 1, 'Желтый', 5, 'yellow', 22, '#f2ff00'),
(91, 1447093355, NULL, 1, 'Зеленый', 6, 'green', 22, '#0dbd00'),
(92, 1447093370, NULL, 1, 'Коралловый', 7, 'coral', 22, '#ff6666'),
(93, 1447093388, NULL, 1, 'Коричневый', 8, 'korichnevyj', 22, '#802400'),
(94, 1447093397, NULL, 1, 'Красный', 9, 'krasnyj', 22, '#ff0000'),
(95, 1447093413, NULL, 1, 'Розовый', 10, 'rozovyj', 22, '#ff0077'),
(96, 1447093429, NULL, 1, 'Серый', 11, 'seryj', 22, '#8c8c8c'),
(97, 1447093438, NULL, 1, 'Синий', 12, 'sinij', 22, '#4000ff'),
(98, 1447093453, NULL, 1, 'Фиолетовый', 13, 'fioletovyj', 22, '#9900ff'),
(99, 1447093461, NULL, 1, 'Черный', 14, 'chernyj', 22, '#000000'),
(100, 1447093527, NULL, 1, 'Мужской', 0, 'muzhskoj', 23, NULL),
(101, 1447093531, NULL, 1, 'Женский', 0, 'zhenskij', 23, NULL),
(102, 1447093536, NULL, 1, 'Унисекс', 0, 'uniseks', 23, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `ip` varchar(16) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `hash` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscribers`
--

INSERT INTO `subscribers` (`id`, `created_at`, `updated_at`, `email`, `ip`, `status`, `hash`) VALUES
(13, 1508505863, NULL, 'TestHost@TestHost.com', NULL, 1, NULL),
(17, 1508505894, NULL, 'test@test.test', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `subscribe_mails`
--

CREATE TABLE `subscribe_mails` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `text` mediumtext,
  `emails` longtext,
  `count_emails` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscribe_mails`
--

INSERT INTO `subscribe_mails` (`id`, `created_at`, `updated_at`, `subject`, `text`, `emails`, `count_emails`) VALUES
(1, 1464178072, NULL, 'лоижлио', '<p>жлдоижилдо</p>', 'veselovskaya.wezom@gmail.com', 1),
(2, 1508505938, NULL, 'adasddsada', '<p><img class=\"\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/53f7399342597_brand.jpg\" alt=\"\" width=\"650\" height=\"260\" /></p>', 'test@test.test;TestHost@TestHost.com', 2),
(3, 1508505953, NULL, 'adasddsada', '<p><img class=\"\" src=\"http://wezom-cms-4.loc/Media/files/filemanager/53f7399342597_brand.jpg\" alt=\"\" width=\"650\" height=\"260\" /></p>', 'test@test.test;TestHost@TestHost.com', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `topMenu`
--

CREATE TABLE `topMenu` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `parent_id` int(255) DEFAULT '0',
  `image` varchar(64) DEFAULT NULL,
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `text` text,
  `column` varchar(255) DEFAULT NULL,
  `views` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `topMenu`
--

INSERT INTO `topMenu` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `alias`, `parent_id`, `image`, `h1`, `title`, `keywords`, `description`, `text`, `column`, `views`) VALUES
(1, NULL, 1508347580, 1, 1, 'Для офиса', 'komplektu-office', 0, 'icon-menu-top-1', NULL, NULL, NULL, NULL, NULL, '0', 0),
(2, NULL, 1508347580, 1, 2, 'Для гостиниц', 'dlya-gostiniz', 0, 'icon-menu-top-2', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, NULL, 1508347580, 1, 3, 'Для кабаре', 'dlya-kabare', 0, 'icon-menu-top-3', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, NULL, 1508347580, 1, 4, 'Для школы', 'dlya-shkolu', 0, 'icon-menu-top-4', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, NULL, 1508347580, 1, 5, 'Для дома', 'dlya-doma', 0, 'icon-menu-top-5', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, NULL, 1508347580, 1, 6, 'Мебель на заказ', 'mebel-na-zakaz', 0, 'icon-menu-top-6', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, NULL, 1508347580, 1, 7, 'Акции', 'sale', 0, 'icon-menu-top-7', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, NULL, 1508347580, 1, 8, 'Отзывы', 'reviews', 0, 'icon-menu-top-8', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, NULL, 1508347580, 1, 0, 'Столы', 'tables', 1, 'icon-menu-left-15.png', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, NULL, 1508347580, 1, 1, 'Кресла и стулья ', 'kresla', 1, ' icon-menu-left-1.png', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, NULL, 1508347580, 1, 2, 'Тумбы и комоды', 'comods', 1, 'icon-menu-left-8.png', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, NULL, 1508347580, 1, 3, 'Шкафы', 'shkafu', 1, 'icon-menu-left-7.png', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, NULL, 1508347580, 1, 4, 'Без категории', 'bez-categorii', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, NULL, 1508347580, 1, 5, 'Подборки', 'podborki', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, NULL, 1508347580, 1, 0, 'Кровати для гостиниц', 'krovati-dlya-gostiniz', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, NULL, 1508347580, 1, 1, 'Комоды и тумбы', 'tymbu-dlya-ofisa', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, NULL, 1508347580, 1, 2, 'Каркасы кроватей', 'karkasu-krovatei', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, NULL, 1508347580, 1, 3, 'Матрасы', 'matrasu', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, NULL, 1508347580, 1, 4, 'Журнальные столики', 'zhurnalnie-stolu', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, NULL, 1508347580, 1, 5, 'Прихожие', 'prihozhii', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, NULL, 1508347580, 1, 6, 'Шкафы для одежды', 'shkafu-dlya-odezhdu', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(22, NULL, 1508347580, 1, 7, 'Вешалки', 'veshalku', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, NULL, 1508347580, 1, 8, 'Угловые секции', 'uhlovie-seczii', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(24, NULL, 1508347580, 1, 9, 'Зеркала', 'zerkala', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, NULL, 1508347580, 1, 0, 'Столы для кабаре', 'stolu-dlya-cabare', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(26, NULL, 1508347580, 1, 1, 'Стулья для кабаре', 'stulya-dlya-kabare', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(27, NULL, 1508347580, 1, 2, 'Базы, основания, опоры для столов', 'bazu-osnovu-oporu', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, NULL, 1508347580, 1, 3, 'Столешницы столов', 'stoleshnizu-stolov', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(29, NULL, 1508347580, 1, 4, 'Мягкая мебель для кабаре', 'myhkaia-mebel-dlya-kabare', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(30, NULL, 1508347580, 1, 0, 'Парты школьные', 'partu-shkolnue', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(31, NULL, 1508347580, 1, 1, 'Стулья школьные', 'stylia-shkolnue', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(32, NULL, 1508347580, 1, 0, 'Кровати', 'krovati', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(33, NULL, 1508347580, 1, 1, 'Журнальные столы', 'zhurnalnie-stolu', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(34, NULL, 1508347580, 1, 2, 'Матрасы', 'matrasu', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(35, NULL, 1508347580, 1, 3, 'Комоды и тумбы', 'comods', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(36, NULL, 1508347580, 1, 4, 'Компьютерные столы', 'comp-stolu', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(37, NULL, 1508347580, 1, 5, 'Каркасы кроватей', 'karkasu-krovatei', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(38, NULL, 1508347580, 1, 6, 'Прихожие', 'prihozhii', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(39, NULL, 1508347580, 1, 7, 'Гостинные', 'gostinue', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(40, NULL, 1508347580, 1, 8, 'Детские', 'detskie', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(41, NULL, 1508347580, 1, 9, 'Спальни', 'spalni', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(42, NULL, 1508347580, 1, 10, 'Модульная мебель', 'modulnaya-mebel', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(43, NULL, 1508347580, 1, 0, 'Кухни на заказ', 'kyhni-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(44, NULL, 1508347580, 1, 1, 'Детская мебель на заказ', 'detskaya-mebel-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(45, NULL, 1508347580, 1, 2, 'Офисная мебель на заказ', 'ofisnaya-mebel-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(46, NULL, 1508347580, 1, 3, 'Шкафы-купе на заказ', 'shkafu-kype-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(47, NULL, 1508347580, 1, 4, 'Спальни на заказ', 'spalni-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(48, NULL, 1508347580, 1, 5, 'Стенки на заказ', 'stenki-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(49, NULL, 1508347580, 1, 6, 'Мебель для гостиниц на заказ', 'mebel-dlya-hostiniz-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(50, NULL, 1508347580, 1, 7, 'Барные стойки на заказ', 'barnue-stoiki-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(51, NULL, 1508347580, 1, 8, 'Торговая мебель на заказ', 'torgovaia-mebel-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(52, NULL, 1508347580, 1, 9, 'Мебель для ванной на заказ', 'mebel-dlya-vannoi-na-zakaz', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(162, NULL, 1508347580, 1, 0, 'Столы руководителя', 'stolu-rykovoditela', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(163, NULL, 1508347580, 1, 1, 'Офисные столы для персонала', 'ofissnue-stolu-dlya-personala', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(164, NULL, 1508347580, 1, 2, 'Письменные столы', 'pissmenue-stolu', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(165, NULL, 1508347580, 1, 3, 'Приставные столы', 'pristavnue-ctolu', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(166, NULL, 1508347580, 1, 4, 'Конференц столы', 'conference-stolu', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(167, NULL, 1508347580, 1, 5, 'Компьютерные столы', 'comp-stolu', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(168, NULL, 1508347580, 1, 6, 'Журнальные столы', 'zhurnalnie-stolu', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(169, NULL, 1508347580, 1, 7, 'Стол папка', 'stol-papka', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(170, NULL, 1508347580, 1, 8, 'Стол книжка', 'stol-knizhka', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(171, NULL, 1508347580, 1, 0, 'Кресла для руководителя', 'kresla-dlya-rykovoditelya', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(172, NULL, 1508347580, 1, 1, 'Офисные кресла для персонала', 'ofisnye-kresla-dlja-personala', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(173, NULL, 1508347580, 1, 2, 'Компьютерные кресла', 'comp-kresla', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(174, NULL, 1508347580, 1, 3, 'Стулья офисные', 'stulia-ofisnue', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(175, NULL, 1508347580, 1, 4, 'Конференц кресла', 'conference-kresla', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(176, NULL, 1508347580, 1, 5, 'Мягкая мебель для зон ожидания', 'myahkaya-mebel-dlya-zon-ozhidaniyz', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(177, NULL, 1508347580, 1, 0, 'Тумбы для офиса', 'tymbu-dlya-ofisa', 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(178, NULL, 1508347580, 1, 1, 'Комоды', 'komodu', 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(179, NULL, 1508347580, 1, 0, 'Шкафы офисные', 'shkafu-offisnue', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(180, NULL, 1508347580, 1, 1, 'Пеналы офисные', 'penalu-ofisnue', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(181, NULL, 1508347580, 1, 2, 'Стелажи для документов', 'stelazhu-dlya-documentov', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(182, NULL, 1508347580, 1, 0, 'Стойки ресепшн', 'stoiki-recepshn', 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(183, NULL, 1508347580, 1, 1, 'Офисные диваны', 'oficcesnue-divanu', 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(184, NULL, 1508347580, 1, 0, ' Эконом класс', 'econom-class', 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(185, NULL, 1508347580, 1, 1, 'Премиум класс', 'premium-class', 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(186, NULL, 1508347580, 1, 2, 'Комплекты офисной мебели', 'complectu-ofisnoi-mebeli', 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(187, NULL, 1508347580, 1, 0, 'Односпальные', 'krovati-odnospalnie', 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(188, NULL, 1508347580, 1, 1, 'Двуспальные', 'krovati-dvuspalnie', 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(190, NULL, 1508347580, 1, 0, 'Комоды', 'comodu', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(192, NULL, 1508347580, 1, 1, 'Тумбы для обуви', 'tymbu-dlya-obyvi', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(193, NULL, 1508347580, 1, 2, 'Туалетные столики', 'tyaletnie-stoliki', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(194, NULL, 1508347580, 1, 3, 'ТВ тумбы под телевизор', 'TV-tymbu-pod-televizor', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(195, NULL, 1508347580, 1, 4, 'Прикроватные тумбы', 'prikrovatnie-tymbu', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(196, NULL, 1508347580, 1, 0, 'Двухспальные кровати', 'krovati-dvuspalnie', 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(197, NULL, 1508347580, 1, 1, 'Односпальные кровати', 'krovati-odnospalnie', 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(198, NULL, 1508347580, 1, 2, 'Детские кровати', 'krovati-detskie', 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(199, NULL, 1508347580, 1, 3, 'Двухярусные кровати', 'dvuhyarusnue-krovati', 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(200, NULL, 1508347580, 1, 4, 'Деревянные кровати', 'derevianue-krovati', 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(201, NULL, 1508347580, 1, 0, 'Стеклянные журнальные столы', 'zhurnalnie-stolu', 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(202, NULL, 1508347580, 1, 1, 'Деревянные журнальные столики', 'zhurnalnie-stolu', 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(203, NULL, 1508347580, 1, 0, 'Ортопедические', 'ortopedicheskie', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(204, NULL, 1508347580, 1, 1, 'Футоны', 'futonu', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(205, NULL, 1508347580, 1, 2, 'Детские матрасы', 'detskie-matrasu', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(206, NULL, 1508347580, 1, 3, 'Латексные', 'lateksnue', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(207, NULL, 1508347580, 1, 4, 'Пружинные ', 'pryzhnue', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(208, NULL, 1508347580, 1, 5, 'Беспружинные', 'bespruzhunue', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(209, NULL, 1508347580, 1, 0, 'Комоды', 'komodu', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(210, NULL, 1508347580, 1, 1, 'Тумбы для обуви', 'tymbu-dlya-obyvi', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(211, NULL, 1508347580, 1, 2, 'Туалетные столики', 'tyaletnie-stoliki', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(212, NULL, 1508347580, 1, 3, 'ТВ тумбы под телевизор', 'TV-tymbu-pod-televizor', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(213, NULL, 1508347580, 1, 4, 'Прикроватные тумбы', 'prikrovatnie-tymbu', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(214, NULL, 1508347580, 1, 0, 'Прямые', 'priamue', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(215, NULL, 1508347580, 1, 1, 'Угловые', 'yglovue', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(216, NULL, 1508347580, 1, 2, 'С надстройкой', 's-nadstroikoi', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(217, NULL, 1508347580, 1, 3, 'С полкой для клавиатуры', 's-polkoi-dlya-klaviatyru', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(218, NULL, 1508347580, 1, 4, 'Комп. столы для ноутбука', 'comp-stolu-dlya-noutbyka', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(220, 1508328902, 1508347580, 0, 0, 'Тест', '/neww', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET cp1251 DEFAULT 'Не указано',
  `login` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `password` varchar(128) CHARACTER SET cp1251 DEFAULT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `hash` varchar(255) CHARACTER SET cp1251 DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `role_id` int(2) NOT NULL DEFAULT '1',
  `ip` varchar(16) DEFAULT NULL,
  `phone` varchar(64) DEFAULT 'Не указано',
  `last_login` int(10) DEFAULT NULL,
  `logins` int(10) DEFAULT '0',
  `adress` varchar(255) DEFAULT 'Не указано',
  `last_name` varchar(64) DEFAULT 'Не указано',
  `floor` varchar(255) DEFAULT 'Не указано',
  `middle_name` varchar(64) DEFAULT 'Не указано'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `login`, `password`, `created_at`, `updated_at`, `hash`, `email`, `status`, `role_id`, `ip`, `phone`, `last_login`, `logins`, `adress`, `last_name`, `floor`, `middle_name`) VALUES
(1, 'Администратор', 'admin', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 1418300546, 1430939378, '48e00180ccc77b94d73ec413b015a4cfb9aa58ba10d8c1b63aad5c8317847d9f', 'palenaya.v.wezom111@gmail.com', 1, 4, NULL, '+38 (111) 111-11-11', NULL, 0, 'Не указано', NULL, NULL, NULL),
(2, 'weZom', 'wezom', '4958070fab7cebd8b1000c6c8cb1bca4aa23b509ee9bc4b70570b5c0e3dfe64a', NULL, 1435164507, 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 'vitaliy.demyjkane3nko.1991@gmail.com', 1, 3, NULL, '+38 (567) 567-56-75', NULL, 0, 'Не указано', NULL, NULL, NULL),
(21, 'Виталя', NULL, 'd7957677dc4ec0e9e425c8b99e477aa70244ae512fc2583af6a1ab2ee3b01fbc', 1447096765, 1448350164, 'd6fb647d44ada79149c6732227667ea44b84ab78706cffe94027f960d0bb1124', 'demyanenko.v.wezom@ukr.net', 1, 1, '127.0.0.1', '', 1448350164, 3, 'Не указано', 'Демяненко', NULL, '444'),
(22, 'Test1', 'admin1', 'c2bcb46de4d99a0ce346ee0a6530b85bb6f0fb80656406a23d3e60c1158a22e8', 1447426756, 1447426756, NULL, NULL, 1, 5, NULL, NULL, NULL, 0, 'Не указано', NULL, NULL, NULL),
(23, 'Anton', NULL, '3e61c90127ae20abbcc8b1b7cfabf301a0269de0460890d5ec33373870c4e172', 1447515858, NULL, '409fe348ba859b01e942532e108ff6479dd435feaae09d81adc9aa91871a0de0', 'crocoash@gmail.com', 1, 1, '109.86.230.160', NULL, 1447515857, 1, 'Не указано', 'Tsvetkov', NULL, NULL),
(24, 'Vitaliy', NULL, '339e548c63121ea40d9b8d9a7e6bc43279ba802991a1d2dd54ec8c080328b826', 1448910941, NULL, '3d63d55d0f1a64f2ad0bd1255b4ecbdbfc0820362d2f1456b64556439fc1e5a8', 'demyanenko.v.wezom@gmail.com', 1, 1, '46.175.163.12', NULL, 1448910941, 1, 'Не указано', 'Demianenko', NULL, NULL),
(25, 'zorya', 'zorya', '131cf002c1c268432c00b026a5182ada3c14a3cbd3ec2f112ea2aff21f4e621e', 1450795155, 1483520734, NULL, NULL, 1, 7, NULL, NULL, NULL, 0, 'Не указано', NULL, NULL, NULL),
(26, 'test', NULL, '99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f', 1461673130, NULL, '73ffbeef661a2707b9bea04b39979a2a99655a8ae4eed79a35869f20bdec40a3', 'test.wezom@mail.ru', 0, 1, NULL, '', NULL, 0, 'Не указано', '', NULL, ''),
(27, 'Екатерина', NULL, 'ce7500fb2f28f5d742c9821a1bd7847e8375065070d0e137c401cd151f8b7564', 1464541221, NULL, 'd9201dfdc8a7585db6d597b570cbaddf8e8288408e5713f887baff81ec9daadf', 'katia-katerinka_@mail.ru', 1, 1, '83.142.111.151', NULL, 1464541221, 1, 'Не указано', 'Баськова', NULL, NULL),
(28, 'test', NULL, '99bbed544406cf05b82dd1e9f2affaebaf189fca8c2cfef0ee3ffa2f5250611f', 1465467766, 1483520720, '398518fd32869473b46bb3f54c02cb85e86421138514e01b9e85a23b79edc9e9', 'test1@mail.ru', 0, 1, NULL, '', NULL, 0, 'Не указано', '', NULL, ''),
(39, 'Максим', NULL, 'ba43ff0e3ad43d81aa30a86d3951fe92edd2734a2121008c37bc8d343815ae06', 1508421110, 1508428149, 'edd0e9aa0d445e1f4a08a7f72f397422e566b8bfe61fdac684d283e291380bdc', 'Makc00766@gmail.com', 1, 1, '127.0.0.1', '+380952015405', NULL, 0, 'Мира 25 кв 56', 'Болгар', '2', 'Сергеевич');

-- --------------------------------------------------------

--
-- Структура таблицы `users_networks`
--

CREATE TABLE `users_networks` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `network` varchar(32) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `uid` varchar(64) NOT NULL,
  `profile` varchar(128) NOT NULL,
  `first_name` varchar(32) DEFAULT NULL,
  `last_name` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_networks`
--

INSERT INTO `users_networks` (`id`, `user_id`, `network`, `created_at`, `uid`, `profile`, `first_name`, `last_name`, `email`) VALUES
(7, 21, 'vkontakte', 1447096768, '22898418', 'http://vk.com/id22898418', 'Виталя', 'Демяненко', 'demyanenko.v.wezom@ukr.net'),
(8, 21, 'facebook', 1447096775, '100003261046861', 'http://www.facebook.com/100003261046861', 'Vitaliy', 'Demianenko', 'vitaliy.demyanenko.1991@gmail.com'),
(9, 23, 'facebook', 1447515858, '1628676114064038', 'https://www.facebook.com/app_scoped_user_id/1628676114064038/', 'Anton', 'Tsvetkov', 'crocoash@gmail.com'),
(10, 24, 'instagram', 1448910941, '1685676043', 'http://instagram.com/demianenko_v', 'Vitaliy', 'Demianenko', 'demyanenko.v.wezom@gmail.com'),
(11, 27, 'vkontakte', 1464541222, '3646497', 'http://vk.com/id3646497', 'Екатерина', 'Баськова', 'katia-katerinka_@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `users_roles`
--

CREATE TABLE `users_roles` (
  `id` int(2) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `alias` varchar(128) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_roles`
--

INSERT INTO `users_roles` (`id`, `name`, `description`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'Пользователь', 'Обычный пользователь, зарегистрировавшийся на сайте', 'user', NULL, NULL),
(3, 'Разработчик', 'Полный доступ ко всему + к тому к чему нет доступа у главного администратора', 'developer', NULL, NULL),
(4, 'Суперадмин', 'Доступ ко всем разделам, кроме тех, к которым имеет доступ только разработчик', 'superadmin', NULL, NULL),
(5, '1', '1', 'admin', 1447421386, NULL),
(6, 'rol1', 'запрещено все', 'admin', 1447662341, NULL),
(7, 'все разрешено', '', 'admin', 1460728748, 1483520750);

-- --------------------------------------------------------

--
-- Структура таблицы `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `country` varchar(63) DEFAULT NULL,
  `region` varchar(63) DEFAULT NULL,
  `city` varchar(63) DEFAULT NULL,
  `longitude` varchar(15) DEFAULT NULL,
  `latitude` varchar(15) DEFAULT NULL,
  `answer` text,
  `first_enter` int(10) DEFAULT NULL,
  `last_enter` int(10) DEFAULT NULL,
  `enters` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `visitors_hits`
--

CREATE TABLE `visitors_hits` (
  `id` int(10) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` varchar(32) NOT NULL DEFAULT '200 OK',
  `device` varchar(32) NOT NULL DEFAULT 'Computer',
  `useragent` varchar(255) DEFAULT NULL,
  `counter` int(10) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `visitors_referers`
--

CREATE TABLE `visitors_referers` (
  `id` int(10) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_at` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `вуд`
--

CREATE TABLE `вуд` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `sale` tinyint(1) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL DEFAULT '0',
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `h1` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `cost` int(6) NOT NULL DEFAULT '0',
  `cost_old` int(6) NOT NULL DEFAULT '0',
  `artikul` varchar(128) DEFAULT NULL,
  `views` int(10) NOT NULL DEFAULT '0',
  `brand_alias` varchar(255) DEFAULT NULL,
  `model_alias` varchar(255) DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `specifications` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `вуд`
--

INSERT INTO `вуд` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name`, `alias`, `parent_id`, `new`, `sale`, `top`, `available`, `h1`, `title`, `keywords`, `description`, `cost`, `cost_old`, `artikul`, `views`, `brand_alias`, `model_alias`, `image`, `specifications`) VALUES
(40, 1447094647, 1454059240, 1, 1, 'Куртка Blend', 'kurtka-blend', 29, 1, 0, 0, 1, '', '', '', '', 2810, 0, '1', 8, 'benetton', NULL, '31c79213d5aceed90eed954307c44416.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'),
(41, 1447094746, 1454059253, 1, 2, 'Brave Soul', 'brave-soul', 29, 1, 0, 0, 1, '', '', '', '', 2210, 0, '2', 2, 'next', NULL, '3faf38fccbe35f31b603141be04317f3.jpg', '{\"material\":[\"kozha\",\"silikon\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\"],\"tsvet\":\"sinij\"}'),
(42, 1447094856, 1454059257, 1, 3, 's.Oliver', 'soliver', 29, 0, 0, 1, 1, '', '', '', '', 2310, 0, '3', 2, 'benetton', NULL, 'fe75cb26c4ae4c06af7d7fdef455fe1b.jpg', '{\"material\":[\"nejlon\",\"sintetika\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\"],\"tsvet\":\"green\"}'),
(43, 1447095158, 1461879646, 1, 1, 'Mudo', 'mudo', 30, 1, 0, 0, 0, '', '', '', '', 312, 0, '4', 4, 'hm', NULL, '21f15ca55231a0db69dad9a2f1c6d2cd.jpg', '{\"material\":[\"poliestr\",\"polimer\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(44, 1447095265, 1453987283, 1, 2, 'Only & Sons', 'only--sons', 30, 0, 0, 1, 2, '', '', '', '', 890, 820, '5', 53, 'next', NULL, '43c658475e0a09cb4fc1c63b46bdc5a9.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\"],\"tsvet\":\"white\"}'),
(45, 1447095348, 1453986770, 1, 1, 'Top Secret', 'top-secret', 31, 0, 1, 0, 1, '', '', '', '', 850, 950, '6', 8, 'next', NULL, '85b47014b4b719452a06dc924426491f.jpg', '{\"material\":[\"nubuk\",\"poliestr\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"osen\"],\"tsvet\":\"korichnevyj\"}'),
(46, 1447095545, 1454061160, 1, 1, 'Patrol', 'patrol', 32, 0, 1, 0, 1, '', '', '', '', 875, 1030, '7', 155, 'newbalance', '1300', '6891ab8f29e5eeded563f514d42279f4.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\"],\"tsvet\":\"chernyj\"}'),
(47, 1447095635, 1491228017, 1, 2, 'Dirk Bikkembergs', 'dirk-bikkembergs', 32, 0, 0, 0, 0, '', '', '', '', 1270, 0, '8', 6, 'newbalance', '1400', '4454777def4b6cf80459a15566e23770.jpg', '{\"material\":[\"kozha\",\"sintetika\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"white\"}'),
(48, 1447095826, 1485439373, 1, 3, 'Reebok Classics', 'reebok-classics', 32, 0, 0, 0, 2, '', '', '', '', 1450, 0, '9', 335, 'nike', 'airfoamposite', 'c2c9d0b441f241040ff329880f7840e5.jpg', '{\"material\":[\"kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"green\"}'),
(49, 1447095908, 1454006747, 1, 1, 'Burton Menswear London', 'burton-menswear-london', 33, 0, 0, 0, 0, '', '', '', '', 2860, 0, '10', 4, 'benetton', NULL, 'd56dbeb19c27a828e5762b1aabd956a2.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(50, 1447095980, 1458206606, 1, 1, 'Vitacci', 'vitacci', 34, 1, 0, 0, 1, '', '', '', '', 1300, 0, '11', 8, 'nike', 'airjordan', '147378e984f87fbc42a0b80bd52899a2.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'),
(51, 1447096085, 1461223417, 1, 1, 'Quiksilver', 'quiksilver', 35, 1, 0, 0, 1, '', '', '', '', 1740, 0, '12', 4, 'hm', NULL, '528d7c85efd8247ba056e4a1c29b4bca.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'),
(52, 1447096156, 1461223431, 1, 2, 'David Jones', 'david-jones', 35, 1, 0, 0, 1, '', '', '', '', 1500, 0, '13', 5, 'hm', NULL, '3e402891cdc71ca41bc7f7f09e042d4b.jpg', '{\"material\":[\"kozha\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"fioletovyj\"}'),
(53, 1447096237, 1454061351, 1, 3, 'A-Personality', 'a-personality', 35, 0, 0, 1, 1, '', '', '', '', 400, 0, '14', 1, 'benetton', NULL, 'ffeee2baa0eb681477b1b4ae95c92e79.jpg', '{\"material\":[\"iskusstvennaja-kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(54, 1447096327, 1492428101, 1, 1, 'Oakley', 'oakley', 36, 1, 0, 0, 1, '', '', '', '', 1701, 0, '15', 9, 'hm', NULL, '3b3c8e2b8b4c89d1fbb12c3e12de9d5a.jpg', '{\"material\":[\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"korichnevyj\"}'),
(55, 1447096428, 1456335230, 1, 2, 'Polaroid', 'polaroid', 36, 1, 0, 0, 1, '', '', '', '', 756, 0, '16', 5, 'hm', NULL, 'ff92a4371fdb1b55022573290fc5ad2f.jpg', '{\"material\":[\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'),
(56, 1453972719, 1454076898, 1, 2, 'Regular Fit Jeans', 'regular-fit-jeans', 31, 0, 0, 1, 1, '', '', '', '', 1100, 0, '5', 3, 'nike', 'airforce', '9bd9e2c0cb862c21ba8b33160fa544b7.jpg', '{\"material\":[\"lateks\",\"nejlon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"sinij\"}'),
(57, 1453972822, 1453987499, 1, 3, 'Regular Slim Jeans', 'regular-slim-jeans', 31, 0, 0, 1, 1, '', '', '', '', 1450, 1200, '7', 2, 'hm', NULL, '72db86cef6b5e1901bd0dabf72b1735e.jpg', '{\"material\":[\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'),
(58, 1453972990, 1492435428, 1, 4, 'Regular Jeans', 'regular-jeans', 31, 1, 0, 0, 1, '', '', '', '', 1270, 0, '5', 5, 'next', NULL, '46d9d8f8e3be8cb4f5049aa0b11b796c.jpg', '{\"tsvet\":\"sinij\",\"sezon\":[\"leto\",\"vesna\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"material\":[\"polimer\",\"silikon\"]}'),
(59, 1453973163, 1457985925, 1, 5, 'Jeans', 'jeans', 31, 1, 0, 0, 1, '', '', '', '', 800, 0, '4', 2, 'hm', NULL, 'feee3908006fdc4ab715cd327d00a745.jpg', '{\"material\":[\"nubuk\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'),
(60, 1453973235, 1464802566, 1, 6, 'FitJeans', 'fitjeans', 31, 1, 0, 0, 0, '', '', '', '', 980, 0, '5', 5, 'nike', 'airforce', '371f58a9be9b8faba677741ac6b12050.jpg', '{\"material\":[\"lateks\",\"nejlon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'),
(61, 1453973760, 1463061442, 1, 1, 'VAN HEUSEN', 'van-heusen', 30, 1, 0, 0, 1, '', '', '', '', 500, 0, '1', 4, 'benetton', NULL, '744f8a702159b78c379ed35dc8200eb9.jpg', '{\"material\":[\"lateks\",\"polimer\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(62, 1453973836, 1461250728, 1, 2, 'TOOTAL', 'tootal', 30, 1, 0, 0, 1, '', '', '', '', 670, 0, '2', 2, 'hm', NULL, '8aafb240eb2626fb201353890a8d9035.jpg', '{\"material\":[\"nubuk\",\"silikon\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"sinij\"}'),
(63, 1453973886, 1453987345, 1, 3, 'OXFORD', 'oxford', 30, 0, 0, 1, 1, '', '', '', '', 490, 0, '3', 0, 'next', NULL, '69fb49c55e1dbae657920116c3121aca.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(64, 1453973952, 1453987430, 1, 4, 'MORLEY', 'morley', 30, 0, 1, 0, 1, '', '', '', '', 600, 680, '4', 0, 'nike', NULL, 'e88ca78f3d83a09ec4d9a087f7488465.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'),
(65, 1453974019, 1461250735, 1, 5, 'SHIRTING', 'shirting', 30, 0, 1, 0, 1, '', '', '', '', 500, 530, '5', 5, 'benetton', NULL, '343f2ed7a53c89842f76680def0ff912.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"nejlon\",\"polimer\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"green\"}'),
(66, 1453974080, 1453987621, 1, 7, 'AERTEX', 'aertex', 30, 0, 0, 0, 2, '', '', '', '', 549, 0, '7', 0, 'nike', 'airhuarache', '4000f9750e94224c62a0c5a4e155a753.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"kozha\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'),
(67, 1453974669, 1454059251, 1, 1, 'Marnelly', 'marnelly', 29, 0, 1, 0, 1, '', '', '', '', 2500, 3050, '1', 1, 'hm', NULL, 'cf92f6be7ef042db8506fd217337ec04.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"polimer\",\"silikon\",\"tekstil\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(68, 1453974739, 1454059255, 1, 2, 'MIRAGE', 'mirage', 29, 0, 1, 0, 1, '', '', '', '', 2300, 2570, '2', 1, 'hm', NULL, '9b2bd4df4c45164ff451721eec1393a4.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(69, 1453974795, 1454059259, 1, 3, 'Sinta', 'sinta', 29, 0, 0, 0, 2, '', '', '', '', 2499, 0, '3', 1, 'benetton', NULL, 'af5e3e1d7e0cefb9d417502078673523.jpg', '{\"material\":[\"zamsha\",\"kozha\",\"nejlon\",\"nubuk\",\"sintetika\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(70, 1453975300, 1454009693, 1, 1, 'GANT', 'gant', 34, 1, 0, 0, 1, '', '', '', '', 1500, 0, '1', 5, 'benetton', NULL, '0d8ad8265bc795e5f0b1e46f1872f01d.jpg', '{\"material\":[\"iskusstvennaja-zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"turquoise\"}'),
(71, 1453975356, 1454004432, 1, 2, 'Alberto Guardiani', 'alberto-guardiani', 34, 0, 0, 1, 1, '', '', '', '', 1200, 0, '2', 1, 'hm', NULL, '99d4ab2baa6efb497393b8caa546b7d2.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"0\"}'),
(72, 1453975422, 1454003835, 1, 3, 'Animas Code', 'animas-code', 34, 0, 0, 1, 1, '', '', '', '', 1400, 0, '3', 5, 'newbalance', '1400', '0e6af6118a01535c6d6b8cb9b9b1cc95.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(73, 1453975543, 1454006743, 1, 1, 'Baldinini', 'baldinini', 33, 0, 1, 0, 1, '', '', '', '', 800, 900, '1', 0, 'benetton', NULL, 'f61df09878d457ee66fc7e1fd14addbd.jpg', '{\"material\":[\"nejlon\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'),
(74, 1453975599, 1454006751, 1, 2, 'Bally', 'bally', 33, 0, 0, 0, 2, '', '', '', '', 1300, 0, '2', 0, 'newbalance', '1300', '2b58dd8bf1316ed14ede88d7fe1a9a92.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(75, 1453975667, 1454006756, 1, 3, 'Barracuda', 'barracuda', 33, 0, 0, 0, 0, '', '', '', '', 2100, 0, '3', 0, NULL, NULL, '6acb9c32679cf8b76b7d4a1138f4ea24.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"beige\"}'),
(76, 1453975845, 1458248564, 1, 1, 'Sneakers Versace medusa', 'sneakers-versace-medusa', 32, 0, 1, 0, 1, '', '', '', '', 1100, 1300, '1', 4, 'nike', 'airforce', 'aa1d555bcf57d51b72b13f70e12d5036.png', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'),
(77, 1453975896, 1490015486, 1, 2, 'Rick Owens', 'rick-owens', 32, 0, 0, 0, 0, '', '', '', '', 1200, 970, '2', 3, 'newbalance', NULL, 'ac95a9367b31d93cf5c8f6e5bf1a34de.jpg', '{\"material\":[\"nejlon\",\"silikon\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(78, 1453975946, 1454061169, 1, 3, 'Kanye West Yeezy 750 Boost Low', 'kanye-west-yeezy-750-boost-low', 32, 0, 0, 0, 2, '', '', '', '', 1000, 880, '3', 2, 'newbalance', '1400', 'f30fe5c63fd4f0f9e382e275606aa7e5.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(79, 1453976111, 1471585831, 1, 1, 'Ray Ban Wayfarer', 'ray-ban-wayfarer', 36, 0, 1, 0, 1, '', '', '', '', 395, 790, '1', 3, 'benetton', NULL, '9286138925af6f01592d369df039cffc.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(80, 1453976168, 1454062780, 1, 2, 'Ray Ban Aviator', 'olivia', 36, 0, 1, 1, 1, '', '', '', '', 395, 790, '2', 0, 'hm', NULL, '27a7915a23137d61f99bc2a20df0dc61.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(81, 1453976216, 1454062891, 1, 3, 'GANT', 'gant1701', 36, 0, 0, 0, 1, '', '', '', '', 504, 0, '3', 0, 'benetton', NULL, '3e7b9e0e1683ca0fe9a851c4c3051520.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(82, 1453976329, 1462888568, 1, 1, 'Женская сумка D&G', 'zhenskaja-sumka-dg', 35, 0, 0, 1, 1, '', '', '', '', 890, 0, '1', 4, 'benetton', NULL, '81734926419a4401f8322ece3a5fe691.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"kozha\",\"poliestr\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"yellow\"}'),
(83, 1453976392, 1454061671, 1, 2, 'Женская сумка D&G', 'zhenskaja-sumka-dg6134', 35, 0, 1, 0, 1, '', '', '', '', 1300, 1500, '2', 0, 'hm', NULL, '06020e5f548b14bbe0d3cddd1ddb9609.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(84, 1453982705, 1453986781, 1, 1, 'Two Women In The World', 'two-women-in-the-world', 31, 0, 1, 0, 1, '', '', '', '', 680, 709, '1', 1, 'twowomenintheworld', NULL, '878a7e4e4ce127d11d0f01e49b165543.jpg', '{\"material\":[\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"sinij\"}'),
(85, 1453985288, 1464611176, 1, 1, 'Paige', 'paige', 31, 0, 0, 0, 0, '', '', '', '', 1645, 0, '1', 10, 'passarelladeathsquad', NULL, '1e45fd1d34c616984a94829c43c9d251.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"]}'),
(86, 1453985577, 1453986790, 1, 1, 'Victoria Beckham', 'victoria-beckham', 31, 0, 0, 0, 0, '', '', '', '', 1500, 0, '1', 0, 'victoriabeckham', NULL, '8de18a7355287270b3df43c05a1048c0.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'),
(87, 1453985693, 1465034664, 1, 1, 'Victoria Beckham', 'victoria-beckham2339', 31, 0, 0, 0, 2, '', '', '', '', 1405, 0, '1', 1, 'victoriabeckham', NULL, '2130622858813fec5259b99f8b452e75.jpg', '{\"material\":[\"poliestr\",\"hlopok\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(88, 1453988109, 1454059290, 1, 1, 'Burberry Brit', 'burberry-brit', 29, 0, 0, 0, 2, '', '', '', '', 1680, 0, '1', 3, 'hm', NULL, '650e1c2c39fcd2db0fe5552ce0a96813.jpg', '{\"material\":[\"zamsha\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'),
(89, 1453999405, 1454004747, 1, 1, 'Burberry', 'burberry', 34, 0, 1, 0, 1, '', '', '', '', 800, 970, '1', 2, 'newbalance', NULL, 'e16e898eaab10b2a8288cc73e4da458c.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'),
(90, 1453999511, 1454004745, 1, 1, 'Dolce & Gabbana', 'dolce--gabbana', 34, 0, 1, 0, 1, '', '', '', '', 1000, 1350, '1', 6, 'benetton', NULL, '996c9737bb19cec0cdf8545920a35024.jpg', '{\"material\":[\"kozha\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"light_blue\"}'),
(91, 1454004856, 1454004928, 1, 1, 'Gucci', 'gucci', 34, 0, 0, 0, 2, '', '', '', '', 1590, 0, '1', 1, 'newbalance', NULL, '1a89bd77c02b30bb960a2758628ff129.jpg', '{\"material\":[\"kozha\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'),
(92, 1454005034, 1464611193, 1, 1, 'Bottega Veneta', 'bottega-veneta', 34, 0, 0, 0, 0, '', '', '', '', 1354, 0, '1', 4, 'hm', NULL, '27386468296175ab291d15df6925661d.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"coral\"}'),
(93, 1454005265, 1454005359, 1, 1, 'Hermes', 'hermes', 34, 0, 0, 0, 0, '', '', '', '', 1654, 0, '1', 0, 'nike', NULL, '222d27610fe00ae41888d76765c172b5.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"korichnevyj\"}'),
(94, 1454005514, 1454005532, 1, 1, 'Lacoste', 'lacoste', 34, 0, 0, 0, 2, '', '', '', '', 800, 0, '1', 1, NULL, NULL, '0123777411cf5317bb713d9afdc3c9c1.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"leto\"],\"tsvet\":\"beige\"}'),
(95, 1454006167, 1459953214, 1, 1, 'Christian Louboutin', 'christian-louboutin', 33, 0, 1, 0, 1, '', '', '', '', 1987, 2133, '1', 1, 'newbalance', NULL, 'aedca7492b0710de73e4d3553a0b317c.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"leto\"],\"tsvet\":\"sinij\"}'),
(96, 1454006239, 1455883752, 1, 1, 'Christian Louboutin', 'christian-louboutin5172', 33, 0, 0, 1, 1, '', '', '', '', 1679, 0, '1', 2, 'benetton', NULL, '296c2f7b532ab1905d9813cd4843d955.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'),
(97, 1454006518, 1454006721, 1, 1, 'Hermes', 'hermes8759', 33, 0, 0, 1, 1, '', '', '', '', 1733, 0, '1', 0, 'nike', NULL, 'f733af1bc7c73d6def2e19d94fcebd15.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(98, 1454006577, 1460018408, 1, 1, 'Phillip Plein', 'phillip-plein', 33, 1, 0, 0, 1, '', '', '', '', 987, 0, '1', 2, 'benetton', NULL, 'ce1d58dc0da917077043c1edf6de746a.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"kozha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'),
(99, 1454006650, 1454006713, 1, 0, 'Christian Louboutin', 'christian-louboutin6404', 33, 1, 0, 0, 1, '', '', '', '', 1267, 0, '', 0, 'nike', NULL, 'eb148d52f4cc6c272ee88bacf2776732.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"polimer\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"0\"}'),
(100, 1454058542, 1454058563, 1, 0, 'Dior 2015', 'dior-2015', 30, 0, 0, 0, 2, '', '', '', '', 845, 0, '', 1, 'hm', NULL, '400f456673efc583825c497288dbdca7.jpg', '{\"material\":[\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"white\"}'),
(101, 1454058665, 1463997612, 1, 0, 'Alexander McQueen', 'alexander-mcqueen', 30, 0, 0, 0, 0, '', '', '', '', 750, 0, '', 4, 'benetton', NULL, 'bd1b148c4d131aaa82c04b236c01de3c.jpg', '{\"material\":[\"polimer\",\"sintetika\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"osen\"],\"tsvet\":\"0\"}'),
(102, 1454058981, 1454059244, 1, 0, 'Dsquared', 'dsquared', 29, 0, 0, 0, 0, '', '', '', '', 2400, 0, '', 2, 'next', NULL, 'c75370b832affc3be9d433178f864ead.jpg', '{\"material\":[\"kozha\",\"poliestr\",\"tekstil\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(103, 1454059079, 1454059238, 1, 0, 'Burberry', 'burberry5235', 29, 0, 0, 0, 1, '', '', '', '', 1220, 0, '', 1, 'benetton', NULL, '73487bc7d72b9ca7f581b1e2c87ed78d.jpg', '{\"material\":[\"zamsha\",\"kozha\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\"],\"tsvet\":\"chernyj\"}'),
(104, 1454059182, 1457985913, 1, 0, 'Burberry кожаная', 'burberry-kozhanaja', 29, 0, 0, 0, 0, '', '', '', '', 850, 0, '', 2, 'hm', NULL, '348b0a5a662583ff1543ba0703034249.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(105, 1454059444, 1454059512, 1, 0, 'Hermes', 'hermes5183', 33, 0, 0, 0, 1, '', '', '', '', 1580, 0, '', 1, 'benetton', NULL, '3ee3635b29378b2c1b92d0ee8a8262ae.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\"],\"tsvet\":\"korichnevyj\"}'),
(106, 1454060603, 1454061145, 1, 0, 'Dsquared2', 'dsquared2', 32, 0, 0, 1, 1, '', '', '', '', 1200, 0, '', 0, 'newbalance', NULL, 'bcedcb826fce171cb73c27dcf27e2b85.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\"],\"tsvet\":\"white\"}'),
(107, 1454060703, 1464168802, 1, 0, 'Nike чёрные', 'nike-chernye', 32, 0, 0, 1, 1, '', '', '', '', 984, 0, '', 2, 'nike', NULL, '1e2e2f992f6e9d1aad557278cd9d56b7.jpg', '{\"material\":[\"zamsha\",\"nejlon\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'),
(108, 1454060834, 1479218247, 1, 0, 'D&G серые', 'dg-serye', 32, 1, 0, 0, 1, '', '', '', '', 1640, 0, '', 2, 'newbalance', NULL, 'a4211640da4bb4ad88bf464938bdda63.jpg', '{\"material\":[\"kozha\",\"polimer\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"italija\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(109, 1454060948, 1485500892, 1, 0, 'Rick Owens', 'rick-owens8331', 32, 1, 0, 0, 1, '', '', '', '', 1400, 0, '', 3, 'newbalance', NULL, '40d2333010aa10e1f683eaee65ffeb1f.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(110, 1454061760, 1454061809, 1, 0, 'Женская сумка Dior', 'zhenskaja-sumka-dior', 35, 0, 1, 0, 1, '', '', '', '', 1800, 2000, '', 0, 'hm', NULL, '8243a32288f42683b19c2e6246d32434.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(111, 1454061862, 1454061926, 1, 0, 'Женская сумка Furla', 'zhenskaja-sumka-furla', 35, 0, 0, 0, 2, '', '', '', '', 1360, 0, '', 0, 'benetton', NULL, '2bc9719c017326e92508f46b404f342f.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"nubuk\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"rozovyj\"}'),
(112, 1454062022, 1454062063, 1, 0, 'Мужская сумка Fred Perry', 'muzhskaja-sumka-fred-perry', 35, 0, 0, 0, 2, '', '', '', '', 1120, 0, '', 0, 'hm', NULL, '82a09f8eae1e2e17f6f4d38a116fe1f9.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'),
(113, 1454062111, 1454062156, 1, 0, 'Мужская сумка Fred Perry', 'muzhskaja-sumka-fred-perry5223', 35, 0, 0, 0, 0, '', '', '', '', 1227, 0, '', 0, 'hm', NULL, '027e8a2609c1af3e188c8638f016fe97.jpg', '{\"material\":[\"kozha\",\"poliestr\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"seryj\"}'),
(114, 1454062198, 1454062273, 1, 0, 'Мужская сумка на пояс D&G', 'muzhskaja-sumka-na-pojas-dg', 35, 0, 0, 0, 0, '', '', '', '', 1413, 0, '', 0, 'hm', NULL, '992e36604939e00adee689ee475add0a.jpg', '{\"material\":[\"nubuk\",\"polimer\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(115, 1454063053, 1456335246, 1, 0, 'Porsche Design', 'porsche-design', 36, 0, 0, 1, 1, '', '', '', '', 618, 0, '', 1, 'hm', NULL, '5bb962a05bf5a36940a77ba77e496ac6.jpg', '{\"material\":[\"iskusstvennaja-zamsha\",\"kozha\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(116, 1454063205, 1460022189, 1, 0, 'Cartier', 'cartier', 36, 0, 0, 1, 1, '', '', '', '', 890, 0, '', 1, 'benetton', NULL, 'bf2ae8500081b45c16b214f51e50d39e.jpg', '{\"material\":[\"kozha\",\"lateks\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(117, 1454063317, 1461219408, 1, 0, 'Ferrari', 'ferrari', 36, 0, 0, 0, 0, '', '', '', '', 796, 0, '', 2, 'hm', NULL, '3d087c18ee28a88fc99dc78933d842a1.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"poliestr\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(118, 1454063440, 1454063488, 1, 0, 'Porsche Design', 'porsche-design9374', 36, 0, 0, 0, 0, '', '', '', '', 619, 0, '', 0, 'benetton', NULL, '69efd778c57c7f5e8561eec011853107.jpg', '{\"material\":[\"kozha\",\"lateks\",\"polimer\",\"silikon\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(119, 1454063524, 1454323431, 1, 0, 'Мужские очки Модель 2368c2', 'muzhskie-ochki-model-2368c2', 36, 0, 0, 0, 2, '', '', '', '', 589, 0, '', 2, 'benetton', NULL, '92f3b935559751b2265a53c2fecf62ef.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"korichnevyj\"}'),
(120, 1454319163, 1461219368, 1, 0, 'Джинсы Philipp Plein', 'dzhinsy-philipp-plein', 31, 0, 0, 0, 1, '', '', '', '', 1368, 0, '', 2, 'passarelladeathsquad', NULL, 'c0fec459280ee93b12b4e4a711705309.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"poliestr\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"sinij\"}'),
(121, 1454319233, 1454319250, 1, 0, 'Джинсы мужские Balmain', 'dzhinsy-muzhskie-balmain', 31, 0, 0, 0, 2, '', '', '', '', 2500, 0, '', 0, 'nike', NULL, '971c2e8906aa445de06e2a0bf33f0b87.jpg', '{\"material\":[\"zamsha\",\"poliestr\",\"tekstil\",\"hlopok\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"0\"}'),
(122, 1454319307, 1492434833, 1, 0, 'Джинсы мужские Dsquared', 'dzhinsy-muzhskie-dsquared', 31, 1, 0, 0, 1, '', '', '', '', 1368, 0, '', 3, 'next', NULL, '8c7b7cecca6ff092982454f7334ab879.jpg', '{\"tsvet\":\"0\",\"sezon\":[\"zima\",\"osen\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"material\":[\"iskusstvennajazamsha\",\"iskusstvennajakozha\",\"nejlon\",\"sintetika\"]}'),
(123, 1454319356, 1454319373, 1, 0, 'Джинсы мужские Versace', 'dzhinsy-muzhskie-versace', 31, 0, 0, 1, 1, '', '', '', '', 1392, 0, '', 0, 'hm', NULL, 'a0977a205a70991491100a55f09d1368.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'),
(124, 1454319406, 1454319419, 1, 0, 'Женские джинсы Dsquared', 'zhenskie-dzhinsy-dsquared', 31, 0, 0, 0, 1, '', '', '', '', 1467, 0, '', 0, 'twowomenintheworld', NULL, '00538151c9f5b0c2548a2dd7aff6339e.jpg', '{\"material\":[\"lateks\",\"silikon\",\"tekstil\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\",\"osen\"],\"tsvet\":\"0\"}'),
(125, 1454319828, 1454319865, 1, 0, 'Мужская рубашка McQueen', 'muzhskaja-rubashka-mcqueen', 30, 0, 0, 0, 1, '', '', '', '', 1008, 0, '', 0, 'benetton', NULL, '59d38fdc0ca405a5b33d3c7841fd842b.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'),
(126, 1454319898, 1460016736, 1, 0, 'Рубашка Dior Homme', 'rubashka-dior-homme', 30, 1, 0, 0, 1, '', '', '', '', 1488, 0, '', 3, 'hm', NULL, '654581ae81effacc3a69f675c1fe3a94.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(127, 1454319945, 1454319955, 1, 0, 'Рубашка Givenchy', 'rubashka-givenchy', 30, 0, 0, 1, 1, '', '', '', '', 1739, 0, '', 0, 'next', NULL, '720d0d3efa105c644950af32522f4045.jpg', '{\"material\":[\"nejlon\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(128, 1454320002, 1461250714, 1, 0, 'Мужская рубашка Kenzo', 'muzhskaja-rubashka-kenzo', 30, 0, 0, 0, 0, '', '', '', '', 867, 0, '', 1, 'next', NULL, 'b22809828871f8e09526177f0baed2b1.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(129, 1454320064, 1460975257, 1, 0, 'Рубашка Givenchy', 'rubashka-givenchy2219', 30, 0, 0, 0, 2, '', '', '', '', 1699, 0, '', 1, 'nike', NULL, 'b872e6cbc18e01ce6047b418aa0b2f88.jpg', '{\"material\":[\"poliestr\",\"sintetika\",\"hlopok\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(130, 1454320424, 1458764141, 1, 0, 'Мужской пуховик Armani', 'muzhskoj-puhovik-armani', 29, 0, 0, 1, 1, '', '', '', '', 2107, 0, '', 3, 'benetton', NULL, '4c81d2327a33a5aa58e18501eccf17a5.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"sintetika\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"fioletovyj\"}'),
(131, 1454320481, 1454320498, 1, 0, 'Куртка Burberry кожаная', 'kurtka-burberry-kozhanaja', 29, 0, 0, 0, 2, '', '', '', '', 3510, 0, '', 0, 'hm', NULL, '2e49d83ce27e526f8e7cc578406fb5a8.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(132, 1454320548, 1454320563, 1, 0, 'Куртка Burberry кожаная', 'kurtka-burberry-kozhanaja5847', 29, 0, 0, 0, 0, '', '', '', '', 3400, 0, '', 0, 'next', NULL, 'cb0ab2f6740708dff3d7607f41163153.jpg', '{\"material\":[\"kozha\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(133, 1454320609, 1479125714, 1, 0, 'Женский плащ Karen Millen', 'zhenskij-plasch-karen-millen', 29, 0, 0, 1, 1, '', '', '', '', 2067, 0, '', 2, 'benetton', NULL, '7991833d4e097829b93dd9f278b40c14.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"seryj\"}'),
(134, 1454320662, 1454320674, 1, 0, 'Мужское пальто Burberry', 'muzhskoe-palto-burberry', 29, 0, 0, 0, 2, '', '', '', '', 2800, 0, '', 0, 'hm', NULL, '857b1bc39a911a1a24ae239ee59d16e2.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\",\"sintetika\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"zima\"],\"tsvet\":\"chernyj\"}'),
(135, 1454322286, 1454322300, 1, 0, 'Женские мокасины Tods Riancess белые', 'zhenskie-mokasiny-tods-riancess-belye', 34, 0, 0, 0, 2, '', '', '', '', 1920, 0, '', 0, 'benetton', NULL, '3bfdb6e9c6abd9d58741a9afd69a73d1.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"white\"}'),
(136, 1454322332, 1454322352, 1, 0, 'Женские мокасины Tods Riancess черные', 'zhenskie-mokasiny-tods-riancess-chernye', 34, 0, 0, 0, 2, '', '', '', '', 1920, 0, '', 0, 'hm', NULL, '98a88f9a611f19fda6d0507bfeb84ef4.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"zima\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(137, 1454322385, 1465597478, 1, 0, 'Мокасины Tods серые замшевые', 'mokasiny-tods-serye-zamshevye', 34, 1, 0, 0, 1, '', '', '', '', 1840, 0, '', 3, 'newbalance', NULL, '2ab31caa02f13ca7fe793fc6ffde3935.jpg', '{\"material\":[\"zamsha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"0\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"seryj\"}'),
(138, 1454322435, 1454605014, 1, 0, 'Мужские мокасины Prada', 'muzhskie-mokasiny-prada', 34, 0, 0, 1, 1, '', '', '', '', 1350, 0, '', 1, 'next', NULL, 'dd84e3af3ac388c165830cf5c2a99be1.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"poliestr\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"kitaj\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(139, 1454322489, 1479125703, 1, 0, 'Мужские мокасины Tods темно синие', 'muzhskie-mokasiny-tods-temno-sinie', 34, 1, 0, 0, 1, '', '', '', '', 1100, 0, '', 1, 'nike', NULL, '33d7d2b06133a49ca5fe0f732ebf5d5b.jpg', '{\"material\":[\"nubuk\",\"polimer\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"sinij\"}'),
(140, 1454323547, 1454323565, 1, 0, 'Туфли Giuseppe Zanotti бежевые кожаные', 'tufli-giuseppe-zanotti-bezhevye-kozhanye', 33, 0, 0, 0, 1, '', '', '', '', 2100, 0, '', 0, 'benetton', NULL, 'ce8d895d2c71cfb9a98da843140265a7.jpg', '{\"material\":[\"kozha\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"beige\"}'),
(141, 1454323591, 1454323600, 1, 0, 'Туфли Gianmarco Lorenzi чёрные', 'tufli-gianmarco-lorenzi-chernye', 33, 0, 0, 0, 1, '', '', '', '', 2400, 0, '', 0, 'newbalance', NULL, 'da41ddcc27ee136176fdce29738682b2.jpg', '{\"material\":[\"kozha\",\"silikon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'),
(142, 1454323628, 1454323643, 1, 0, 'Туфли классические Givenchy черные', 'tufli-klassicheskie-givenchy-chernye', 33, 0, 0, 0, 1, '', '', '', '', 2200, 0, '', 0, 'nike', NULL, '0677e1ad1f6541eee5747d28058c8180.jpg', '{\"material\":[\"zamsha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(143, 1454323679, 1459953240, 1, 0, 'Мужские туфли Louis Vuitton коричневые', 'muzhskie-tufli-louis-vuitton-korichnevye', 33, 0, 0, 0, 1, '', '', '', '', 1800, 0, '', 1, 'benetton', NULL, '68d868374b6db3d1cf9bea2f72027e31.jpg', '{\"material\":[\"lateks\",\"poliestr\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"korichnevyj\"}'),
(144, 1454323726, 1454323737, 1, 0, 'Мужские туфли Louis Vuitton черные', 'muzhskie-tufli-louis-vuitton-chernye', 33, 0, 0, 0, 1, '', '', '', '', 1800, 0, '', 0, 'newbalance', NULL, '43de9d0f84d292f30c5ac0451faff7de.jpg', '{\"material\":[\"kozha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"vesna\",\"leto\"],\"tsvet\":\"chernyj\"}'),
(145, 1454324137, 1454324226, 1, 0, 'Мужские sneakers Buscemi', 'muzhskie-sneakers-buscemi', 32, 0, 0, 0, 1, '', '', '', '', 1700, 0, '', 0, 'newbalance', NULL, '2c57611d39d0339269ecba567a749967.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"polimer\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"italija\",\"sezon\":[\"osen\"],\"tsvet\":\"chernyj\"}'),
(146, 1454324425, 1454324440, 1, 0, 'Кроссовки Giuseppe Zanotti', 'krossovki-giuseppe-zanotti', 32, 0, 0, 0, 1, '', '', '', '', 1440, 0, '', 0, 'nike', NULL, '120a2393910ce4cad4acd9300c124ca4.jpg', '{\"material\":[\"iskusstvennaja-kozha\",\"lateks\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(147, 1454324475, 1454324495, 1, 0, 'Кроссовки Y-3 ', 'krossovki-y-3-yohji-yamamoto', 32, 0, 0, 0, 1, '', '', '', '', 1160, 0, '', 0, 'newbalance', NULL, '2e70fbd4a6418b0d25f58157243ec680.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"tekstil\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"osen\"],\"tsvet\":\"yellow\"}'),
(148, 1454324542, 1454324558, 1, 0, 'Мужские кроссовки Louis Vuitton замшевые красные', 'muzhskie-krossovki-louis-vuitton-zamshevye-krasnye', 32, 0, 0, 0, 2, '', '', '', '', 2400, 0, '', 0, 'nike', NULL, '7e6ed9ff208f16a45ab64257ba9f3a72.jpg', '{\"material\":[\"zamsha\",\"nubuk\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'),
(149, 1454324590, 1454324601, 1, 0, 'Мужские кроссовки Prada чёрные', 'muzhskie-krossovki-prada-chernye', 32, 0, 0, 0, 1, '', '', '', '', 1300, 0, '', 0, 'newbalance', NULL, '8b80482102762d94fb58b6db7e57550c.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"poliestr\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"polsha\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(150, 1454324982, 1454324999, 1, 0, 'Женская сумка Roberto Cavalli', 'zhenskaja-sumka-roberto-cavalli', 35, 0, 0, 0, 1, '', '', '', '', 1280, 0, '', 0, 'benetton', NULL, 'f7202f3d2445ba1f4fa2839437e2b9b9.jpg', '{\"material\":[\"lateks\",\"nubuk\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"vesna\",\"zima\",\"leto\",\"osen\"],\"tsvet\":\"krasnyj\"}'),
(151, 1454325034, 1454325047, 1, 0, 'Клатч Alexander Mcqueen', 'klatch-alexander-mcqueen', 35, 0, 0, 0, 1, '', '', '', '', 1300, 0, '', 0, 'hm', NULL, '07ee6659e7a415e9d34f6a93b540908e.jpg', '{\"material\":[\"kozha\",\"silikon\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ssha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(152, 1454325084, 1454325108, 1, 0, 'Женская сумка Zara', 'zhenskaja-sumka-zara', 35, 0, 0, 0, 1, '', '', '', '', 900, 0, '', 0, 'benetton', NULL, '0a4eff388b67a57a083f654ac8149b0b.jpg', '{\"material\":[\"nejlon\",\"nubuk\",\"polimer\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(153, 1454325135, 1454325151, 1, 0, 'Мужская сумка Calvin Klein', 'muzhskaja-sumka-calvin-klein', 35, 0, 0, 0, 1, '', '', '', '', 827, 0, '', 0, 'hm', NULL, '698e3ecaee23d4e5f2f1530d0c240ad8.jpg', '{\"material\":[\"kozha\",\"nubuk\",\"silikon\"],\"sex\":\"muzhskoj\",\"proizvodstvo\":\"ukraina\",\"sezon\":[\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(154, 1454325181, 1455806204, 1, 0, 'Мужская сумка GUCCI', 'muzhskaja-sumka-gucci', 35, 0, 0, 0, 1, '', '', '', '', 760, 0, '', 0, 'benetton', NULL, 'a1263630a87a89b5226af70435ba8254.jpg', '{\"material\":[\"lateks\",\"nejlon\",\"sintetika\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\",\"sezon\":[\"zima\",\"leto\"],\"tsvet\":\"chernyj\"}'),
(155, 1462879116, 1462890105, 1, 0, 'Nke Air Huarache', 'nke-air-huarache', 32, 1, 0, 0, 1, 'Nike Air Huarache', 'Nike Air Huarache', 'Nike Nike Nike ', '', 1999, 0, '', 10, 'nike', 'airhuarache', '8d2ec33b46ebc352b67098759eebe5fc.jpg', '{\"material\":[\"kozha\",\"nejlon\",\"nubuk\",\"tekstil\"],\"sex\":\"uniseks\",\"proizvodstvo\":\"vetnam\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(156, 1462879768, 1462890101, 1, 0, 'Ruma Creepers ', 'ruma-creepers-', 32, 1, 1, 0, 1, '', '', '', '', 1499, 1795, 'Ruma Creepers ', 6, NULL, NULL, 'eab6d6b4851e790b52d71813e8537b8c.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"poliestr\"],\"sex\":\"zhenskij\",\"proizvodstvo\":\"indonezija\",\"sezon\":[\"vesna\",\"leto\",\"osen\"],\"tsvet\":\"chernyj\"}'),
(157, 1463996817, 1464614312, 1, 0, 'Подушка крассная', 'podushka-krassnaja', 40, 0, 0, 0, 1, '', '', '\r\n', '', 40, 0, '444', 14, NULL, NULL, '0cdf3a8e39c4cfb545c4e0da3a719c6f.jpg', '{\"sex\":\"0\"}'),
(158, 1464612611, 1464618713, 1, 0, 'Одеяло из шерсти', 'odejalo-iz-shersti', 41, 0, 0, 0, 1, '', '', '', '', 1000, 0, '1111', 3, 'fabricfrontline', NULL, '876558828e8bf44fd34c1638e82bfdb4.jpg', '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"tekstil\",\"hlopok\"],\"proizvodstvo\":\"polsha\",\"sezon\":[\"vesna\",\"osen\"],\"tsvet\":\"fioletovyj\"}'),
(159, 1464618360, 1464618360, 1, 0, 'терка для овощей', 'terka-dlja-ovoschej', 44, 0, 0, 0, 1, '', '', '', '', 35, 0, '123дж', 0, NULL, NULL, NULL, '{\"material\":[\"zamsha\",\"iskusstvennaja-zamsha\",\"iskusstvennaja-kozha\",\"kozha\",\"lateks\",\"nejlon\",\"nubuk\",\"poliestr\",\"polimer\",\"silikon\",\"sintetika\",\"tekstil\",\"hlopok\"],\"sex\":\"0\",\"proizvodstvo\":\"ukraina\",\"tsvet\":\"0\"}'),
(160, 1464618597, 1464685063, 1, 0, 'Терка универсальная', 'terka-universalnaja', 44, 0, 0, 0, 1, '', '', '', '', 55, 0, '1235дж', 0, NULL, NULL, '1b22ffdbad1330328b791e4a253e6d97.jpg', '{\"sex\":\"0\",\"proizvodstvo\":\"kitaj\"}'),
(161, 1464627001, 1464684951, 1, 0, 'Терки акция', 'terki-aktsija', 45, 0, 0, 1, 1, '', '', '', '', 50, 55, '2323', 3, NULL, NULL, 'c6121f9633e13ce34552ba96c741d096.jpg', '{\"tsvet\":\"seryj\"}'),
(162, 1464685147, 1464696138, 1, 0, 'Ложка', 'lozhka', 47, 0, 0, 0, 1, '', '', '', '', 25, 0, '222', 3, 'caf', NULL, 'ae7cd5e751f1f6c5305e881d8a38622d.jpg', '{\"tsvet\":\"0\"}'),
(163, 1465035027, 1491318723, 1, 0, ' Тёплая', 'teplaja', 51, 0, 0, 1, 1, '', '', '', '', 1400, 0, '111', 2, 'aldobrue', NULL, 'db6bf874a183ef6709366777e38b5f8b.jpg', '{\"tsvet\":\"beige\",\"sex\":\"zhenskij\",\"proizvodstvo\":\"italija\"}');

-- --------------------------------------------------------

--
-- Структура таблицы `фыв`
--

CREATE TABLE `фыв` (
  `id` int(10) NOT NULL,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  `name_cat` varchar(64) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `column` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `фыв`
--

INSERT INTO `фыв` (`id`, `created_at`, `updated_at`, `status`, `sort`, `name_cat`, `url`, `parent_id`, `image`, `icon`, `column`) VALUES
(28, 1504268019, 1504269109, 1, 0, 'Столы', 'tables', 20, 'icon-menu-left-3.png', NULL, 1),
(29, 1504268088, 1504268297, 1, 1, 'Комоды и тумбы', 'comods', 20, 'icon-menu-left-8.png', NULL, 2),
(33, 1504268214, 1504268297, 1, 2, 'Шкафы', 'shkafu', 20, 'icon-menu-left-7.png', NULL, 4),
(37, NULL, NULL, 1, 3, 'Кресла', 'kresla', NULL, 'icon-menu-left-1.png', NULL, NULL),
(38, NULL, NULL, 1, 4, 'Стулья', 'stylia', NULL, 'icon-menu-left-2.png', NULL, NULL),
(39, NULL, NULL, 1, 5, 'Кровати', 'krovati', NULL, 'icon-menu-left-4.png', NULL, NULL),
(40, NULL, NULL, 1, 6, 'Матрасы', 'matrasu', NULL, 'icon-menu-left-5.png', NULL, NULL),
(41, NULL, NULL, 1, 7, 'Мягкая мебель', 'myahkaya-mebel', NULL, 'icon-menu-left-6.png', NULL, NULL),
(42, NULL, NULL, 1, 8, 'Вешалки', 'veshalku', NULL, 'icon-menu-left-11.png', NULL, NULL),
(43, NULL, NULL, 1, 9, 'Прихожие', 'prihozhii', NULL, 'icon-menu-left-9.png', NULL, NULL),
(44, NULL, NULL, 1, 10, 'Гостинные', 'gostinue', NULL, 'icon-menu-left-10.png', NULL, NULL),
(45, NULL, NULL, 1, 11, 'Детские', 'detskie', NULL, 'icon-menu-left-12.png', NULL, NULL),
(46, NULL, NULL, 1, 12, 'Спальни', 'spalni', NULL, 'icon-menu-left-13.png', NULL, NULL),
(47, NULL, NULL, 1, 13, 'Модульная мебель', 'modulnaya-mebel', NULL, 'icon-menu-left-14.png', NULL, NULL),
(48, NULL, NULL, 1, 14, 'Комплекты офисной мебели', 'komplektu-office', NULL, 'icon-menu-left-15.png', NULL, NULL),
(52, 1508153358, NULL, 1, 0, 'Диваны', 'divany', NULL, 'icon-about-4.png', NULL, NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD KEY `rubric_id` (`rubric_id`) USING BTREE;

--
-- Индексы таблицы `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user_id`) USING BTREE,
  ADD KEY `blog` (`blog_id`) USING BTREE;

--
-- Индексы таблицы `blog_rubrics`
--
ALTER TABLE `blog_rubrics`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Индексы таблицы `callback`
--
ALTER TABLE `callback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hash` (`hash`) USING BTREE;

--
-- Индексы таблицы `carts_items`
--
ALTER TABLE `carts_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cart` (`cart_id`) USING BTREE,
  ADD KEY `id_goods` (`catalog_id`) USING BTREE,
  ADD KEY `cart_id` (`cart_id`) USING BTREE,
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE;

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`,`alias`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD KEY `cat_brand_alias` (`brand_alias`) USING BTREE,
  ADD KEY `image_link` (`image`) USING BTREE,
  ADD KEY `id` (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `catalog_comments`
--
ALTER TABLE `catalog_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE;

--
-- Индексы таблицы `catalog_images`
--
ALTER TABLE `catalog_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE,
  ADD KEY `image` (`image`) USING BTREE;

--
-- Индексы таблицы `catalog_questions`
--
ALTER TABLE `catalog_questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE;

--
-- Индексы таблицы `catalog_related`
--
ALTER TABLE `catalog_related`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`who_id`) USING BTREE,
  ADD KEY `related_id` (`with_id`) USING BTREE;

--
-- Индексы таблицы `catalog_specifications_values`
--
ALTER TABLE `catalog_specifications_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE,
  ADD KEY `spec_alias` (`specification_alias`) USING BTREE,
  ADD KEY `spec_val_alias` (`specification_value_alias`) USING BTREE;

--
-- Индексы таблицы `catalog_tree`
--
ALTER TABLE `catalog_tree`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Индексы таблицы `catalog_tree_brands`
--
ALTER TABLE `catalog_tree_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`) USING BTREE,
  ADD KEY `catalog_tree_id` (`catalog_tree_id`) USING BTREE;

--
-- Индексы таблицы `catalog_tree_specifications`
--
ALTER TABLE `catalog_tree_specifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_tree_id` (`catalog_tree_id`) USING BTREE,
  ADD KEY `specification_id` (`specification_id`) USING BTREE;

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD UNIQUE KEY `name` (`name`) USING BTREE,
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `block` (`group`) USING BTREE,
  ADD KEY `type` (`type`) USING BTREE;

--
-- Индексы таблицы `config_groups`
--
ALTER TABLE `config_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Индексы таблицы `config_types`
--
ALTER TABLE `config_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE;

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `action` (`alias`) USING BTREE;

--
-- Индексы таблицы `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cron`
--
ALTER TABLE `cron`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_id` (`gallery_id`) USING BTREE;

--
-- Индексы таблицы `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_2` (`alias`);

--
-- Индексы таблицы `instructions`
--
ALTER TABLE `instructions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `mail_templates`
--
ALTER TABLE `mail_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `alias` (`alias`) USING BTREE,
  ADD KEY `model_brand_alias` (`brand_alias`) USING BTREE;

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`) USING BTREE;

--
-- Индексы таблицы `orders_items`
--
ALTER TABLE `orders_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`) USING BTREE,
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE,
  ADD KEY `size_id` (`size_id`) USING BTREE;

--
-- Индексы таблицы `orders_simple`
--
ALTER TABLE `orders_simple`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_id` (`catalog_id`) USING BTREE;

--
-- Индексы таблицы `popup_messages`
--
ALTER TABLE `popup_messages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `reviewsMain`
--
ALTER TABLE `reviewsMain`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seo_links`
--
ALTER TABLE `seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link` (`link`) USING BTREE;

--
-- Индексы таблицы `seo_redirects`
--
ALTER TABLE `seo_redirects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seo_scripts`
--
ALTER TABLE `seo_scripts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `place` (`place`) USING BTREE;

--
-- Индексы таблицы `seo_templates`
--
ALTER TABLE `seo_templates`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sitemap`
--
ALTER TABLE `sitemap`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sitemenu`
--
ALTER TABLE `sitemenu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sitemenu_catalog`
--
ALTER TABLE `sitemenu_catalog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD KEY `type_id` (`type_id`) USING BTREE;

--
-- Индексы таблицы `specifications_types`
--
ALTER TABLE `specifications_types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `specifications_values`
--
ALTER TABLE `specifications_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`) USING BTREE,
  ADD KEY `specification_id` (`specification_id`) USING BTREE;

--
-- Индексы таблицы `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`) USING BTREE;

--
-- Индексы таблицы `subscribe_mails`
--
ALTER TABLE `subscribe_mails`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `topMenu`
--
ALTER TABLE `topMenu`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`) USING BTREE,
  ADD UNIQUE KEY `hash` (`hash`) USING BTREE,
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- Индексы таблицы `users_networks`
--
ALTER TABLE `users_networks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Индексы таблицы `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ip` (`ip`) USING BTREE,
  ADD KEY `country` (`country`) USING BTREE;

--
-- Индексы таблицы `visitors_hits`
--
ALTER TABLE `visitors_hits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`) USING BTREE;

--
-- Индексы таблицы `visitors_referers`
--
ALTER TABLE `visitors_referers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`) USING BTREE;

--
-- Индексы таблицы `вуд`
--
ALTER TABLE `вуд`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `фыв`
--
ALTER TABLE `фыв`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `access`
--
ALTER TABLE `access`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT для таблицы `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `blog_rubrics`
--
ALTER TABLE `blog_rubrics`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT для таблицы `callback`
--
ALTER TABLE `callback`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT для таблицы `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `carts_items`
--
ALTER TABLE `carts_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT для таблицы `catalog_comments`
--
ALTER TABLE `catalog_comments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `catalog_images`
--
ALTER TABLE `catalog_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1099;
--
-- AUTO_INCREMENT для таблицы `catalog_questions`
--
ALTER TABLE `catalog_questions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `catalog_related`
--
ALTER TABLE `catalog_related`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT для таблицы `catalog_specifications_values`
--
ALTER TABLE `catalog_specifications_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3464;
--
-- AUTO_INCREMENT для таблицы `catalog_tree`
--
ALTER TABLE `catalog_tree`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT для таблицы `catalog_tree_brands`
--
ALTER TABLE `catalog_tree_brands`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `catalog_tree_specifications`
--
ALTER TABLE `catalog_tree_specifications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `config`
--
ALTER TABLE `config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT для таблицы `config_groups`
--
ALTER TABLE `config_groups`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `config_types`
--
ALTER TABLE `config_types`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `content`
--
ALTER TABLE `content`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `control`
--
ALTER TABLE `control`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT для таблицы `cron`
--
ALTER TABLE `cron`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `instructions`
--
ALTER TABLE `instructions`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `log`
--
ALTER TABLE `log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT для таблицы `mail_templates`
--
ALTER TABLE `mail_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT для таблицы `models`
--
ALTER TABLE `models`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT для таблицы `orders_items`
--
ALTER TABLE `orders_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT для таблицы `orders_simple`
--
ALTER TABLE `orders_simple`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `popup_messages`
--
ALTER TABLE `popup_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `reviewsMain`
--
ALTER TABLE `reviewsMain`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `seo_links`
--
ALTER TABLE `seo_links`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `seo_redirects`
--
ALTER TABLE `seo_redirects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `seo_scripts`
--
ALTER TABLE `seo_scripts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `seo_templates`
--
ALTER TABLE `seo_templates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `sitemap`
--
ALTER TABLE `sitemap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT для таблицы `sitemenu`
--
ALTER TABLE `sitemenu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `sitemenu_catalog`
--
ALTER TABLE `sitemenu_catalog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT для таблицы `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `specifications`
--
ALTER TABLE `specifications`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `specifications_types`
--
ALTER TABLE `specifications_types`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `specifications_values`
--
ALTER TABLE `specifications_values`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT для таблицы `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `subscribe_mails`
--
ALTER TABLE `subscribe_mails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `topMenu`
--
ALTER TABLE `topMenu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `users_networks`
--
ALTER TABLE `users_networks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `visitors_hits`
--
ALTER TABLE `visitors_hits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `visitors_referers`
--
ALTER TABLE `visitors_referers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `вуд`
--
ALTER TABLE `вуд`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT для таблицы `фыв`
--
ALTER TABLE `фыв`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `access`
--
ALTER TABLE `access`
  ADD CONSTRAINT `access_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`rubric_id`) REFERENCES `blog_rubrics` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD CONSTRAINT `blog_comments_ibfk_1` FOREIGN KEY (`blog_id`) REFERENCES `blog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `blog_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `carts_items`
--
ALTER TABLE `carts_items`
  ADD CONSTRAINT `carts_items_ibfk_1` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carts_items_ibfk_2` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD CONSTRAINT `catalog_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `catalog_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_comments`
--
ALTER TABLE `catalog_comments`
  ADD CONSTRAINT `catalog_comments_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `вуд` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_images`
--
ALTER TABLE `catalog_images`
  ADD CONSTRAINT `catalog_images_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `вуд` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_questions`
--
ALTER TABLE `catalog_questions`
  ADD CONSTRAINT `catalog_questions_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `вуд` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_related`
--
ALTER TABLE `catalog_related`
  ADD CONSTRAINT `catalog_related_ibfk_1` FOREIGN KEY (`who_id`) REFERENCES `вуд` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_related_ibfk_2` FOREIGN KEY (`with_id`) REFERENCES `вуд` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_specifications_values`
--
ALTER TABLE `catalog_specifications_values`
  ADD CONSTRAINT `catalog_specifications_values_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `вуд` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_specifications_values_ibfk_2` FOREIGN KEY (`specification_alias`) REFERENCES `specifications` (`alias`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_specifications_values_ibfk_3` FOREIGN KEY (`specification_value_alias`) REFERENCES `specifications_values` (`alias`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_tree_brands`
--
ALTER TABLE `catalog_tree_brands`
  ADD CONSTRAINT `catalog_tree_brands_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_tree_brands_ibfk_2` FOREIGN KEY (`catalog_tree_id`) REFERENCES `catalog_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `catalog_tree_specifications`
--
ALTER TABLE `catalog_tree_specifications`
  ADD CONSTRAINT `catalog_tree_specifications_ibfk_1` FOREIGN KEY (`catalog_tree_id`) REFERENCES `catalog_tree` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `catalog_tree_specifications_ibfk_2` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `фыв` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `config`
--
ALTER TABLE `config`
  ADD CONSTRAINT `config_ibfk_1` FOREIGN KEY (`group`) REFERENCES `config_groups` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `config_ibfk_2` FOREIGN KEY (`type`) REFERENCES `config_types` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD CONSTRAINT `gallery_images_ibfk_1` FOREIGN KEY (`gallery_id`) REFERENCES `gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `models`
--
ALTER TABLE `models`
  ADD CONSTRAINT `models_ibfk_1` FOREIGN KEY (`brand_alias`) REFERENCES `brands` (`alias`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders_items`
--
ALTER TABLE `orders_items`
  ADD CONSTRAINT `orders_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_items_ibfk_2` FOREIGN KEY (`catalog_id`) REFERENCES `вуд` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `orders_simple`
--
ALTER TABLE `orders_simple`
  ADD CONSTRAINT `orders_simple_ibfk_1` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `specifications`
--
ALTER TABLE `specifications`
  ADD CONSTRAINT `specifications_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `specifications_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `specifications_values`
--
ALTER TABLE `specifications_values`
  ADD CONSTRAINT `specifications_values_ibfk_1` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `users_roles` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users_networks`
--
ALTER TABLE `users_networks`
  ADD CONSTRAINT `users_networks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `visitors_hits`
--
ALTER TABLE `visitors_hits`
  ADD CONSTRAINT `visitors_hits_ibfk_1` FOREIGN KEY (`ip`) REFERENCES `visitors` (`ip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `visitors_referers`
--
ALTER TABLE `visitors_referers`
  ADD CONSTRAINT `visitors_referers_ibfk_1` FOREIGN KEY (`ip`) REFERENCES `visitors` (`ip`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
