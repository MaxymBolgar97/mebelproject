<?php

return [
    'frontend' => [
        'Ajax', 'Auth', 'Index', 'Tops', 'Excel', 'Order', 'Sale', 'User', 'Cart', 'Delivery', 'Garant', 'Reviews', 'Product', 'News', 'Gallery', 'FAQ', 'Contacts', 'TopCategory', 'Articles', 'News', 'Cart', 'User', 'Catalog', 'Subscribe',
        'Search', 'Sitemap', 'Contact', 'Reviews', 'Category', 'Content',
    ],
    'backend' => [
        'Ajax', 'Auth', 'Index', 'Tops', 'Excel', 'Order', 'Sale', 'Cart', 'User', 'Delivery', 'Garant', 'Reviews', 'Product', 'News', 'Category', 'Gallery', 'FAQ', 'Contacts', 'TopCategory', 'Catalog', 'Config', 'Contacts', 'Content', 'Index', 'Log', 'Visitors', 'Blog', 'Blacklist',
        'MailTemplates', 'Menu', 'Orders', 'Seo', 'Subscribe', 'User', 'Multimedia', 'Statistic', 'Reviews', 'Crop',
        'Translates'
    ],
];