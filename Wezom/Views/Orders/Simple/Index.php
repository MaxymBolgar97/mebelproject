<div class="rowSection">
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa fa-reorder"></i>
                    <?php echo $pageName; ?>
                    <span class="label label-primary"><?php echo $count; ?></span>
                    <a style="margin-left: 15px" href="/wezom/excel/write"><i style="font-size: 22px" class="fa fa-file-excel-o" aria-hidden="true"></i> Отчет в Excel</a>
                </div>
                <div class="toolbar no-padding" id="ordersToolbar" data-uri="<?php echo Core\Arr::get($_SERVER, 'REQUEST_URI'); ?>">
                    <div class="btn-group">
                        <li class="btn btn-xs">
                            <a href="/wezom/<?php echo Core\Route::controller(); ?>/index">
                                <i class="fa fa-refresh"></i>
                                <span class="hidden-xx"><?php echo __('Сбросить'); ?></span>
                            </a>
                        </li>
                        <span class="btn btn-xs dropdownToggle dropdownSelect">
                            <i class="fa fa-filter"></i>
                            <span class="current-item"><?php echo isset($_GET['status']) ? ( Core\Arr::get($_GET, 'status') ? __('Прочитанные') : __('Непрочитанные') ) : __('Все'); ?></span>
                            <span class="caret"></span>
                        </span>
                        <ul class="dropdownMenu pull-right">
                            <li>
                                <a href="<?php echo Core\Support::generateLink('status', NULL); ?>">
                                    <i class="fa fa-filter"></i><?php echo __('Все'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo Core\Support::generateLink('status', 1); ?>">
                                    <i class="fa fa-filter"></i><?php echo __('Прочитанные'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo Core\Support::generateLink('status', 0); ?>">
                                    <i class="fa fa-filter"></i><?php echo __('Непрочитанные'); ?>
                                </a>
                            </li>
                        </ul>

                        <li title="<?php echo __('Выберите дату или период'); ?>" class="range rangeOrderList btn btn-xs bs-tooltip">
                            <a href="#">
                                <i class="fa fa-calendar"></i>
                                <span><?php echo Core\Support::getWidgetDatesRange(); ?></span>
                                <i class="caret"></i>
                            </a>
                        </li>

                    </div>
                </div>
            </div>
            <div class="widget">
                <div class="widgetContent">
                    <table class="table table-striped table-hover checkbox-wrap ">
                        <thead>
                            <tr>
                                <th class="checkbox-head">
                                    <label><input type="checkbox"></label>
                                </th>
                                <th>№</th>
                                <th><?php echo __('Товар'); ?></th>
                                <th><?php echo __('Сумма'); ?></th>
                                <th><?php echo __('Телефон'); ?></th>
                                <th><?php echo __('Доставка'); ?></th>
                                <th><?php echo __('Дата'); ?></th>
                                <th><?php echo __('Опубликовано'); ?>?</th>
                                <th class="nav-column textcenter">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < count($result); $i++): ?>
                                <tr data-id="<?php echo $obj->id; ?>">
                                    <td class="checkbox-column">
                                        <label><input type="checkbox"></label>
                                    </td>
                                    <td><?php echo $result[$i]->id ?></td>
                                    <td>
                                        <ul>
                                            <?php for ($j = 0; $j < count($cartItems[$i]); $j++): ?>
                                                <?php if ($cartItems[$i][$j]->name): ?>
                                                    <li><a href="/wezom/items/edit/<?php echo $cartItems[$i][$j]->catalog_id ?>" target="_blank"><?php echo $cartItems[$i][$j]->name; ?> -> <?php echo $cartItems[$i][$j]->cost; ?> грн.</a></li>
                                                <?php else: ?>
                                                    <i style="color: #aaa;">( <?php echo __('Удален'); ?> )</i>
                                                <?php endif ?>
                                            <?php endfor; ?>
                                        </ul>
                                    </td>
                                    <td><?php echo $result[$i]->totalSum ?> грн.</td>
                                    <td><a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $result[$i]->id ?>"><?php echo $result[$i]->phone; ?></a></td>
                                    <?php if($result[$i]->delivery == 1):?>
                                    <td>Самовывоз</td>
                                    <?php else:?>
                                    <td>Доставка</td>
                                    <?php endif;?>
                                    <td><?php echo $result[$i]->created_at ? date('d.m.Y', $result[$i]->created_at) : '---'; ?></td>
                                    <td width="45" valign="top" class="icon-column status-column">
                                        <?php echo Core\View::widget(['status' => $result[$i]->status, 'id' => $result[$i]->id], 'StatusList'); ?>
                                    </td>
                                    <td class="nav-column">
                                        <ul class="table-controls">
                                            <li>
                                                <a class="bs-tooltip dropdownToggle" href="javascript:void(0);" title="<?php echo __('Управление'); ?>"><i class="fa fa-cog size14"></i></a>
                                                <ul class="dropdownMenu pull-right">
                                                    <li>
                                                        <a href="/wezom/<?php echo Core\Route::controller(); ?>/edit/<?php echo $result[$i]->id; ?>" title="<?php echo __('Редактировать'); ?>"><i class="fa fa-pencil"></i> <?php echo __('Редактировать'); ?></a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a onclick="return confirm('<?php echo __('Это действие необратимо. Продолжить?'); ?>');" href="/wezom/<?php echo Core\Route::controller(); ?>/delete/<?php echo $obj->id; ?>" title="<?php echo __('Удалить'); ?>"><i class="fa fa-trash-o text-danger"></i> <?php echo __('Удалить'); ?></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                    <?php echo $pager; ?>
                </div>
            </div>
        </div>
    </div>
    <span id="parameters" data-table="<?php echo $tablename; ?>"></span>