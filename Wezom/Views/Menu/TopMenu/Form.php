<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(['name' => 'name', 'value' => __('Отправить'), 'class' => 'submit btn btn-primary pull-right']); ?>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj ? $obj->status : 1); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'name' => 'FORM[name]',
                            'value' => $obj->name,
                            'class' => 'valid',
                        ], __('Название')); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input([
                            'id' => 'f_link',
                            'name' => 'FORM[alias]',
                            'value' => $obj->alias,
                            'class' => 'valid',
                        ], __('Ссылка')); ?>
                        <div class="thisLink"><span class="mainLink"><?php echo 'http://'.Core\Arr::get($_SERVER, 'HTTP_HOST'); ?></span><span class="samaLink"></span></div>
                    </div>
					<div class="form-group">
                        <?php echo \Forms\Builder::select('<option value="0">' . __('Верхний уровень') . '</option>'.$tree,
                            $obj->parent_id, [
                                'name' => 'FORM[parent_id]',
                                'class' => 'valid',
                            ], __('Родительский пункт')); ?>
                    </div>
					<div class="form-group">
                        <?php echo \Forms\Builder::select($columns,
                            $obj->column, [
                                'name' => 'FORM[column]',
                                'class' => '',
                            ], __('Колонка (для второго уровня вложенности)')); ?>
                    </div>
					<div class="form-group">
                        <label class="control-label"><?php echo __('Иконка (для второго уровня вложенности). Рекомендованный размер 20х22px'); ?></label>
                        <div class="contentImage">
                            <?php if (is_file( HOST . Core\HTML::media('images/sitemenu_catalog/original/'.$obj->image, false) )): ?>
                                <div class="contentImageView">
                                    <a href="<?php echo Core\HTML::media('images/sitemenu_catalog/original/'.$obj->image); ?>" class="mfpImage">
                                        <img src="<?php echo Core\HTML::media('images/sitemenu_catalog/small/'.$obj->image); ?>" />
                                    </a>
                                </div>
                                <div class="contentImageControl">
                                    <a class="btn btn-danger" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">
                                        <i class="fa fa-remove"></i>
                                        <?php echo __('Удалить изображение'); ?>
                                    </a> 
                                </div>
                            <?php else: ?>
                                <input type="file" name="file" />
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>

<script type="text/javascript">
    function generate_link() {
        var link = $('#f_link').val();
        if(link != '') {
            if(link[0] != '/') {
                link = '/' + link;
            }
        }
        $('.samaLink').text(link);
    }
    $(document).ready(function(){
        generate_link();
        $('body').on('keyup', '#f_link', function(){ generate_link(); });
        $('body').on('change', '#f_link', function(){ generate_link(); });
    });
</script>