<?php
    namespace Wezom\Modules\Catalog\Models;

    use Core\QB\DB;
    use Core\Message;
    use Core\Arr;

    use Wezom\Modules\Catalog\Models\CatalogTreeBrands AS CTB;
    use Wezom\Modules\Catalog\Models\CatalogTreeSpecifications AS CTS;

    class Groups extends \Core\Common {

        public static $table = 'catalog_tree';
        public static $image = 'catalog_tree';
        public static $rules = [];


        public static function countKids($id) {
            $result = DB::select([DB::expr('COUNT(id)'), 'count'])
                ->from('catalog_tree')
                ->where('parent_id', '=', $id);
            return $result->count_all();
        }

        //Количество товаров
        public static function countItems($id) {
            $result = DB::select([DB::expr('COUNT(id)'), 'count'])
                ->from('catalog')
                ->where('parent_id', '=', $id);
            return $result->count_all();
        }

        public static function valid($data = [])
        {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Название группы товаров не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'alias' => [
                    [
                        'error' => __('Алиас не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Алиас должен содержать только латинские буквы в нижнем регистре, цифры, "-" или "_"!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9\-_]*$/',
                    ],
                ],
            ];
            return parent::valid($data);
        }

    }