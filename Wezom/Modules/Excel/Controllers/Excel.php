<?php

namespace Wezom\Modules\Excel\Controllers;

use Core\Route;
use Core\View;
use Core\Config;
use Core\Pager\Pager;
use Core\HTTP;
use Core\Widgets;
use Modules\Base;
use Modules\Content\Models\Control;
use PHPExcel_Writer_Excel5;
use PHPExcel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Color;
use PHPExcel_Style_Border;
use Wezom\Modules\Excel\Models\Excel as Model;

class Excel extends Base {

    public $current;

    public function before() {
        parent::before();
        $this->current = Control::getRow(Route::controller(), 'alias', 1);
        if (!$this->current) {
            return Config::error();
        }

        $this->_template = 'Text';
    }

    public function writeAction() {
        if (Config::get('error')) {
            return false;
        }
        $orders = Model::selectOrders();
        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Таблица умножения');

        $titles = ['ID ЗАКАЗА', 'ДАТА', 'СТАТУС', 'ТЕЛЕФОН', 'ИМЯ', 'АДРЕС', 'ТЕКСТ', 'ДОСТАВКА', 'EMAIL', 'ЭТАЖ', 'СУММА ЗАКАЗА'];
        for($i = 0; $i < count($titles); $i++){
            $sheet->getCellByColumnAndRow($i)->setValue($titles[$i]);
            $sheet->getColumnDimensionByColumn($i)->setAutoSize(true);
            $sheet->getStyleByColumnAndRow($i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyleByColumnAndRow($i)->getFill()->getStartColor()->setRGB('000000');
            $sheet->getStyleByColumnAndRow($i)->getFont()->getColor()->setRGB('FFFFFF');
            $sheet->getStyleByColumnAndRow($i)->getFont()->setBold(true);
            $sheet->getStyleByColumnAndRow($i)->getFont()->setSize(15);
            $sheet->getStyleByColumnAndRow($i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyleByColumnAndRow($i)->getBorders()->getAllBorders()->applyFromArray(array('style' =>PHPExcel_Style_Border::BORDER_MEDIUM,'color' => array('rgb' => 'FFFFFF')));
        }
        
        for ($i = 0; $i < count($orders); $i++){
                $sheet->setCellValueByColumnAndRow(0, $i + 2, $orders[$i]->id);
                $sheet->setCellValueByColumnAndRow(1, $i + 2, gmdate("d-m-Y", $orders[$i]->created_at));
                $sheet->setCellValueByColumnAndRow(2, $i + 2, $orders[$i]->status == 1 ? 'Обработан' : 'Не обработан');
                $sheet->setCellValueByColumnAndRow(3, $i + 2, $orders[$i]->phone);
                $sheet->setCellValueByColumnAndRow(4, $i + 2, $orders[$i]->name);
                $sheet->setCellValueByColumnAndRow(5, $i + 2, $orders[$i]->adress);
                $sheet->setCellValueByColumnAndRow(6, $i + 2, $orders[$i]->text);
                $sheet->setCellValueByColumnAndRow(7, $i + 2, $orders[$i]->delivery == 1 ? 'Доставка' : 'Самовывоз');
                $sheet->setCellValueByColumnAndRow(8, $i + 2, $orders[$i]->email);
                $sheet->setCellValueByColumnAndRow(9, $i + 2, $orders[$i]->floor);
                $sheet->setCellValueByColumnAndRow(10, $i + 2, $orders[$i]->totalSum . ' грн.');

                
        }
        $dateTime = date("Y-m-d H:i:s");
        // Выводим HTTP-заголовки
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=report_$dateTime.xls");

        // Выводим содержимое файла
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');

    }
}
