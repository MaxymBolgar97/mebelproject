<?php

namespace Wezom\Modules\Excel\Models;

use Core\QB\DB;
use Core\Common;
use Core\User AS U;

class Excel extends Common {

    public static function selectOrders() {
        $id = DB::select()
                ->from('orders_simple')
                ->find_all();
        return $id;
    }

}
