<?php   
    
    return [
        'wezom/menu/index' => 'menu/menu/index',
        'wezom/menu/index/page/<page:[0-9]*>' => 'menu/menu/index',
        'wezom/menu/edit/<id:[0-9]*>' => 'menu/menu/edit',
        'wezom/menu/delete/<id:[0-9]*>' => 'menu/menu/delete',
        'wezom/menu/add' => 'menu/menu/add',
        
        'wezom/menu/topMenu' => 'menu/topMenu/index',
        'wezom/topMenu/page/<page:[0-9]*>' => 'menu/topMenu/index',
        'wezom/topMenu/edit/<id:[0-9]*>' => 'menu/topMenu/edit',
        'wezom/topMenu/delete/<id:[0-9]*>' => 'menu/topMenu/delete',
        'wezom/topMenu/add' => 'menu/topMenu/add',
    ];
