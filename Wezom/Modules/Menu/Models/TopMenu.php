<?php
    namespace Wezom\Modules\Menu\Models;

    use Core\Arr;
    use Core\Message;

    class TopMenu extends \Core\Common {

        public static $table = 'topMenu';
		public static $image = 'topMenu';
        public static $rules = [];

        public static function valid($data = [])
        {
            static::$rules = [
                'name' => [
                    [
                        'error' => __('Название пункта меню не может быть пустым!'),
                        'key' => 'not_empty',
                    ],
                ],
                'alias' => [
                    [
                        'error' => __('Ссылка не может быть пустой!'),
                        'key' => 'not_empty',
                    ],
                    [
                        'error' => __('Ссылка должна содержать только латинские буквы в нижнем регистре, цифры, "/", "?", "&", "=", "-" или "_"!'),
                        'key' => 'regex',
                        'value' => '/^[a-z0-9\-_\/\?\=\&]*$/',
                    ],
                ],
            ];
            return parent::valid($data);
        }
		
		public static function getRowsAsTree($status = null, $sort = null, $type = null, $limit = null, $offset = null, $filter = true)
		{
			$result = self::getRows($status, $sort, $type, $limit, $offset, $filter);
			
			$menu = [];
			foreach ($result as $obj) {
				$menu[$obj->parent_id][] = $obj;
			}
			
			return $menu;
		}

    }