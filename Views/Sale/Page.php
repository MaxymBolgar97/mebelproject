<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                            </svg>
                                        </span>
                                        <a href="/" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Акции</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Акции</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid ig-10 iv-20">
                        <?php foreach ($sale as $item):?>
                        <div class="cell cell--8 cell--ms12 cell--sm24">
                            <div class="index-know">
                                <a href="<?php $item->url?>" class="index-know__image image image--cover">
                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/slider/$item->image")?>" alt=""> </a>
                                <div class="grid i-10">
                                    <div class="cell cell--7 gright">
                                        <div class="index-know__date">
                                            <span><?php echo gmdate("d-m-Y", $item->created_at); ?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--17">
                                        <a href="<?php echo $item->url?>" class="index-know__name">
                                            <span><?php echo $item->name?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                        <div class="cell cell--8 cell--ms12 cell--sm24 js-reload-container">
                            <div class="grid grid--jcenter">
                                <div class="cell">
                                    <div class="reload js-reload" data-url="hidden/news-list.php" data-params='{"id": "222", "page": 1}'>
                                        <div class="grid grid--col grid--acenter iv-10">
                                            <div class="cell">
                                                <div class="reload__icon">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-reload')?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="reload__text">
                                                    <span>Загрузить еще</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pb-ms30"></div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
