<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Фотогаллерея</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Фотогаллерея</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid i-10">
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Кухни на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Детская мебель на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-3.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Офисная мебель на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-4.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Шкафы-купе на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-5.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Спальни на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Кухни на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Детская мебель на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-3.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Офисная мебель на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-4.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Шкафы-купе на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-5.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Спальни на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Кухни на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="cell cell--6 cell--md8 cell--ms12 cell--xs24">
                            <a href="category.html" class="cat-block">
                                <div class="cat-block__inner">
                                    <div class="cat-block__image image image--hcover">
                                        <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg')?>" alt=""> </div>
                                    <div class="cat-block__info">
                                        <span>Детская мебель на заказ
                                            <i>
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right')?>" />
                                                </svg>
                                            </i>
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-100 pt-md50 pt-ms30"></div>
            <?php echo Widgets::get('Index_Block4'); ?>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>