<?php

use Core\HTML;
use Core\Widgets;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">
    <?php echo Widgets::get('Head'); ?>
    <body>
        <main>
            <?php echo Widgets::get('Header'); ?>
            <section>
                <div class="block-top pv-10">
                    <div class="grid grid--lg pg-30">
                        <div class="cell cell--24">
                            <div class="grid ig-10">
                                <div class="cell cell--24">

                                    <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                    <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                        <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                            <span class="breadcrumbs__left">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                                </svg>
                                            </span>
                                            <a href="/" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                            <span class="breadcrumbs__right">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                                </svg>
                                            </span>
                                        </span>
                                        <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                            <span class="breadcrumbs__current" property="v:title">Доставка и оплата</span>
                                            <span class="breadcrumbs__right">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div class="title">
                                        <span>Доставка и оплата</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid--lg pg-30 pt-50 pt-ms30">
                    <div class="cell cell--24">
                        <div class="grid i-50 i-lg20">
                            <div class="cell cell--24">
                                <div class="grid i-50 i-lg20">
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="block6__text view-text mb-50 mb-lg30">
                                            <p>Заказывая доставку товара сторонними курьерскими службами (Нова пошта, Мист Экспресс ) - при получении обязательно проверяйте наличие всего товара в заказе, а так же его внешний вид.</p>
                                        </div>
                                        <div class="block6__title block6__title--left mb-50 mb-lg30">
                                            <span>Сборка мебели:</span>
                                        </div>
                                        <div class="grid ig-10 iv-20 iv-lg10">
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-1.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Сборка корпусной мебели на дому у заказчика –</p>
                                                    <p>
                                                        <b>БЕСПЛАТНО</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-2.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Сборка/разборка мягкой мебели:</p>
                                                    <p>Диван –
                                                        <b>20 грн.</b>
                                                    </p>
                                                    <p>Мягкий уголок –
                                                        <b>30 грн</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-3.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>При покупке выставочного образца</p>
                                                    <p>стоимость разборки/сборки:</p>
                                                    <p>Кровать -
                                                        <b>50 грн.</b>
                                                    </p>
                                                    <p>Шкаф -
                                                        <b>100 грн.</b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="block6__image-block">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/delivery-3.jpg')?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="hr"></div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid i-50 i-lg20">
                                    <div class="cell cell--12 cell--ms24 cell--msoend">
                                        <div class="block6__image-block">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/delivery-1.jpg')?>" alt="">
                                        </div>
                                    </div>
                                    <div class="cell cell--12 cell--ms24 cell--msostart">
                                        <div class="block6__title block6__title--left mb-50 mb-lg30">
                                            <span>Стоимость доставки:</span>
                                        </div>
                                        <div class="grid ig-10 iv-20 iv-lg10">
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-4.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Доставка мебели до подъезда</p>
                                                    <p>
                                                        <b>150грн</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-5.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Подъём на этаж платный</p>
                                                    <p>
                                                        <b>5-40 грн/этаж</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-6.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Занос в частный дом и 1-й этаж –</p>
                                                    <p>
                                                        <b>10-25 грн.</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-7.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>За пределами Николаева доставка мебели -</p>
                                                    <p>
                                                        <b>10,0 грн/км.</b>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-8.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text">
                                                    <p>Доставка и сборка мебели по Украине</p>
                                                    <p>
                                                        <b>оговаривается индивидуально</b>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block6__text view-text mt-30">
                                            <p>Также мы доставляем в пригород:</p>
                                            <p>Ш. Балка, Терновка - 160 грн Мешково-Погорелово, Октябрьское, Балабановка, Б.Корениха – 190 грн Матвеевка, Кульбакино – 180 грн М.Корениха – 260 грн</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="hr"></div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid i-50 i-lg20">
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="block6__title block6__title--left mb-50 mb-lg30">
                                            <span>Способы оплаты:</span>
                                        </div>
                                        <div class="grid ig-10 iv-20 iv-lg10">
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-9.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text mb-20">
                                                    <span>Наличными в магазинах «Мебель-АРТ»</span>
                                                </div>
                                                <div class="block6__desc view-text">
                                                    <p>Оплата производится исключительно в национальной валюте. В подтверждение оплаты мы выдаем Вам чек</p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-10.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text mb-20">
                                                    <span>Картой через терминал ПриватБанка в магазинах «Мебель-АРТ»</span>
                                                </div>
                                                <div class="block6__desc view-text">
                                                    <p>Оплата производится через терминал ПриватБанка (портативный) в магазине «Мебель-АРТ» с помощью карты Visa / MasterCard любого банка.</p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-11.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text mb-20">
                                                    <span>Безналичный расчет согласно выписанного счета-фактуры. Мы работает как с НДС так и без НДС</span>
                                                </div>
                                                <div class="block6__desc view-text">
                                                    <p>Оплата по безналичному расчету осуществляется следующим образом: после оформления заказа, менеджер магазина факсом или электронной почтой вышлет Вам счет-фактуру, который Вы сможете оплатить в кассе отделения любого банка или с расчетного счета Вашей фирмы. Для юридических лиц пакет всех необходимых документов предоставляется вместе с товаром</p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-12.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text mb-20">
                                                    <span>Через систему платежей Приват24</span>
                                                </div>
                                                <div class="block6__desc view-text">
                                                    <p>Оплата производится через систему интернет платежей Приват24 на карточный счет ПриватБанка</p>
                                                </div>
                                            </div>
                                            <div class="cell cell--4">
                                                <div class="block6__icon">
                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-delivery-13.png')?>" alt="">
                                                </div>
                                            </div>
                                            <div class="cell cell--20">
                                                <div class="block6__text mb-20">
                                                    <span>Через терминал Ibox</span>
                                                </div>
                                                <div class="block6__desc view-text">
                                                    <p>Оплата на карту ПриватБанка через Ibox (терминалы систем моментальных платежей)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--12 cell--ms24">
                                        <div class="block6__image-block">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/delivery-2.jpg')?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt-100 pt-ms30"></div>
                <?php echo Widgets::get('Index_Block4'); ?>
            </section>
            <?php echo Widgets::get('Footer'); ?>
        </main>
        <?php echo Widgets::get('Scripts'); ?>
    </body>

</html>
