<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Контакты</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Контакты</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pt-50 pt-ms30">
                <div class="cell cell--24">
                    <div class="grid ig-20 iv-40 iv-lg20 iv-ms10">
                        <div class="cell cell--8 cell--sm24 cell--smostart">
                            <div class="contacts">
                                <div class="grid i-10">
                                    <div class="cell cell--24 cell--sm12 cell--xs24">
                                        <div class="grid i-10">
                                            <div class="cell cell--24">
                                                <div class="grid grid--nowrap i-10">
                                                    <div class="cell cell--nogrow cell--noshrink">
                                                        <div class="contacts__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-address')?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--grow">
                                                        <div class="contacts__title mt-10 mb-15">
                                                            <span>Адрес магазина</span>
                                                        </div>
                                                        <div class="contacts__text">
                                                            <p>г. Николаев, ул. Бузника, 5</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cell cell--0 cell--sm24">
                                                <div class="grid grid--nowrap i-10">
                                                    <div class="cell cell--nogrow cell--noshrink">
                                                        <div class="contacts__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-email')?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--grow">
                                                        <div class="contacts__title mt-10 mb-15">
                                                            <span>Почта</span>
                                                        </div>
                                                        <a href="mailto:admin@mebel-art.com.ua" class="contacts__link">
                                                            <span>admin@mebel-art.com.ua</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--sm12 cell--xs24">
                                        <div class="grid grid--nowrap i-10">
                                            <div class="cell cell--nogrow cell--noshrink">
                                                <div class="contacts__icon">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone')?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell cell--grow">
                                                <div class="contacts__title mt-10 mb-15">
                                                    <span>Телефоны</span>
                                                </div>
                                                <div class="grid grid--col">
                                                    <a href="tel:(0512) 72-01-22" class="contacts__link">
                                                        <span>(0512) 72-01-22</span>
                                                    </a>
                                                    <a href="tel:(0512) 58-08-05" class="contacts__link">
                                                        <span>(0512) 58-08-05</span>
                                                    </a>
                                                    <a href="tel:(0512) 58-50-30" class="contacts__link">
                                                        <span>(0512) 58-50-30</span>
                                                    </a>
                                                    <a href="tel:+38(096)929-10-45" class="contacts__link">
                                                        <span>+38(096)929-10-45</span>
                                                    </a>
                                                    <a href="tel:+38(099)909-54-38" class="contacts__link">
                                                        <span>+38(099)909-54-38</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--sm0">
                                        <div class="grid grid--nowrap i-10">
                                            <div class="cell cell--nogrow cell--noshrink">
                                                <div class="contacts__icon">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-email')?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell cell--grow">
                                                <div class="contacts__title mt-10 mb-15">
                                                    <span>Почта</span>
                                                </div>
                                                <a href="mailto:admin@mebel-art.com.ua" class="contacts__link">
                                                    <span>admin@mebel-art.com.ua</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--16 cell--sm24 cell--smostart">
                            <div class="map-block">
                                <div class="map map--height js-map" data-lat="46.96647" data-lng="31.961574"></div>
                            </div>
                        </div>
                        <div class="cell cell--16 cell--sm24 cell--smoend">
                            <div class="map-block map-block--right">
                                <div class="map map--height js-map" data-lat="46.9545075" data-lng="32.0175816"></div>
                            </div>
                        </div>
                        <div class="cell cell--8 cell--sm24 cell--smostart">
                            <div class="contacts">
                                <div class="grid i-10">
                                    <div class="cell cell--24 cell--sm12 cell--xs24">
                                        <div class="grid i-10">
                                            <div class="cell cell--24">
                                                <div class="grid grid--nowrap i-10">
                                                    <div class="cell cell--nogrow cell--noshrink">
                                                        <div class="contacts__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-address')?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--grow">
                                                        <div class="contacts__title mt-10 mb-15">
                                                            <span>Адрес магазина</span>
                                                        </div>
                                                        <div class="contacts__text">
                                                            <p>г. Николаев, 5-я Слободская, 125</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cell cell--0 cell--sm24">
                                                <div class="grid grid--nowrap i-10">
                                                    <div class="cell cell--nogrow cell--noshrink">
                                                        <div class="contacts__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-email')?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--grow">
                                                        <div class="contacts__title mt-10 mb-15">
                                                            <span>Почта</span>
                                                        </div>
                                                        <a href="mailto:admin@mebel-art.com.ua" class="contacts__link">
                                                            <span>admin@mebel-art.com.ua</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--sm12 cell--xs24">
                                        <div class="grid grid--nowrap i-10">
                                            <div class="cell cell--nogrow cell--noshrink">
                                                <div class="contacts__icon">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone')?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell cell--grow">
                                                <div class="contacts__title mt-10 mb-15">
                                                    <span>Телефоны</span>
                                                </div>
                                                <div class="grid grid--col">
                                                    <a href="tel:(0512) 55-27-31" class="contacts__link">
                                                        <span>(0512) 55-27-31</span>
                                                    </a>
                                                    <a href="tel:(0512) 57-20-53" class="contacts__link">
                                                        <span>(0512) 57-20-53</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--sm0">
                                        <div class="grid grid--nowrap i-10">
                                            <div class="cell cell--nogrow cell--noshrink">
                                                <div class="contacts__icon">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-email')?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell cell--grow">
                                                <div class="contacts__title mt-10 mb-15">
                                                    <span>Почта</span>
                                                </div>
                                                <a href="mailto:admin@mebel-art.com.ua" class="contacts__link">
                                                    <span>admin@mebel-art.com.ua</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hide js-map-script" data-key="AIzaSyDL6xIhFeOJeE9nXsObhPKfD1wRV4xFknE" data-language="ru" data-marker="<?php echo HTML::media('pic/marker.png')?>"></div>
            <div class="pt-100 pt-md50 pt-ms30"></div>
            <?php echo Widgets::get('Index_Block4'); ?>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>