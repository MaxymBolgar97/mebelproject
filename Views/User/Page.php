<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-left" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Личный кабинет</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Личный кабинет</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid ig-30 i-ms10">
                        <div class="cell cell--5 cell--md6 cell--ms0">
                            <div class="account-list">
                                <div class="grid grid--acenter i-15">
                                    <div class="cell cell--24 cell--ms">
                                        <div class="account-list__item is-active">
                                            <span>Личная информация</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--ms">
                                        <a href="history.html" class="account-list__item">
                                            <span>Мои заказы</span>
                                        </a>
                                    </div>
                                    <div class="cell cell--24 cell--ms mt-50 mt-ms0">
                                        <a href="/logout" class="account-list__item">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-logout" />
                                            </svg>
                                            <span>Выйти</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--19 cell--md18 cell--ms24 prelative">
                            <div class="hr hr--vleft ms-hide"></div>
                            <div class="account">
                                <div class="grid i-10">
                                    <div class="cell cell--3 cell--md5 cell--xs7">
                                        <div class="account__name">
                                            <span>ФИО</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md19 cell--xs17">
                                        <div class="account__value">
                                            <span><?php echo $user->last_name . ' ' . $user->name . ' ' . $user->middle_name ?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--md5 cell--xs7">
                                        <div class="account__name">
                                            <span>Email</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md19 cell--xs17">
                                        <div class="account__value">
                                            <span><?php echo $user->email?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--md5 cell--xs7">
                                        <div class="account__name">
                                            <span>Телефон</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md19 cell--xs17">
                                        <div class="account__value">
                                            <span><?php echo $user->phone?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--md5 cell--xs7">
                                        <div class="account__name">
                                            <span>Адрес</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md19 cell--xs17">
                                        <div class="account__value">
                                            <span><?php echo $user->adress?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--md5 cell--xs7">
                                        <div class="account__name">
                                            <span>Этаж</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md19 cell--xs17">
                                        <div class="account__value">
                                            <span><?php echo $user->floor?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 mt-50 mt-ms20">
                                        <div class="grid i-10">
                                            <div class="cell">
                                                <a href="user/editPassword" class="btn btn--green">
                                                    <span>Изменить пароль</span>
                                                </a>
                                            </div>
                                            <div class="cell">
                                                <a href="user/editProfile" class="btn">
                                                    <span>Редактировать профиль</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
