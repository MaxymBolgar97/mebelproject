<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <a href="/" class="breadcrumbs__link breadcrumbs__link--first" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-left" />
                                            </svg>
                                        </span>
                                        <a href="/user" class="breadcrumbs__link breadcrumbs__link--last" rel="v:url" property="v:title">Личный кабинет</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Редактирование личной информации</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Редактирование личной информации</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid ig-30 i-ms10">
                        <div class="cell cell--5 cell--md6 cell--ms0">
                            <div class="account-list">
                                <div class="grid grid--acenter i-15">
                                    <div class="cell cell--24 cell--ms">
                                        <div class="account-list__item is-active">
                                            <span>Личная информация</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--24 cell--ms">
                                        <a href="history.html" class="account-list__item">
                                            <span>Мои заказы</span>
                                        </a>
                                    </div>
                                    <div class="cell cell--24 cell--ms mt-50 mt-ms0">
                                        <a href="/logout" class="account-list__item">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-logout" />
                                            </svg>
                                            <span>Выйти</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--19 cell--md18 cell--ms24 prelative">
                            <div class="hr hr--vleft ms-hide"></div>
                            <div class="account form js-form" data-form="true" data-ajax="hidden/response.php">
                                <form action="/user/editProfile" method="POST">
                                    <div class="grid grid--col ig-10 iv-15">
                                        <div class="cell cell--16 cell--md24">
                                            <div class="account__label mb-5">
                                                <span>ФИО*</span>
                                            </div>
                                            <div class="control-holder control-holder--text">
                                                <input type="text" name="name" data-name="name" required data-rule-word="true" value="<?php echo $user->last_name . ' ' . $user->name . ' ' . $user->middle_name ?>">
                                            </div>
                                        </div>
                                        <div class="cell cell--16 cell--md24">
                                            <div class="account__label mb-5">
                                                <span>Адрес*</span>
                                            </div>
                                            <div class="control-holder control-holder--text">
                                                <input type="text" name="address" data-name="address" required data-rule-minlength="2" value="<?php echo $user->adress ?>">
                                            </div>
                                        </div>
                                        <div class="cell cell--16 cell--md24">
                                            <div class="grid ig-10 iv-15">
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="account__label mb-5">
                                                        <span>Этаж*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="text" name="number" data-name="number" required value="<?php echo $user->floor ?>">
                                                    </div>
                                                </div>
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="account__label mb-5">
                                                        <span>Email*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="email" name="email" data-name="email" required data-rule-email="true" value="<?php echo $user->email ?>">
                                                    </div>
                                                </div>
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="account__label mb-5">
                                                        <span>Телефон*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="tel" name="tel" data-name="tel" required data-rule-phone="true" value="<?php echo $user->phone ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cell cell--24 mt-50 mt-ms20">
                                            <div class="grid i-10">
                                                <div class="cell">
                                                    <div class="btn btn--green js-form-submit">
                                                        <button><span>Сохранить</span></button>
                                                    </div>
                                                </div>
                                                <div class="cell">
                                                    <a href="/user" class="btn">
                                                        <span>Отмена</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
