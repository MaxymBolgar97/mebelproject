<?php 	use Core\Widgets; 
	use Core\Arr;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">
<head>
	<?php echo Arr::get($_widgets, 'Head'); ?>
    <?php foreach ($_seo['scripts']['head'] as $script): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
</head>
<body>
	<?php foreach ($_seo['scripts']['body'] as $script): ?>
		<?php echo $script; ?>
	<?php endforeach ?>
	<main>
		<?php echo Arr::get($_widgets, 'Header'); ?>
		<section>
			<div class="block-top pv-10">
				<div class="grid grid--lg pg-30">
					<div class="cell cell--24">
						<div class="grid ig-10">
							<div class="cell cell--24">
								<?php echo $_breadcrumbs; ?>
							</div>
							<div class="cell">
								<h1 class="title"><?php echo Arr::get($_seo, 'h1'); ?></h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php echo $_content; ?>
			<?php echo Arr::get($_widgets, 'News_Other'); ?>	
			
			<?php if ( Arr::get($_widgets, 'Advantages')): ?>
				<div class="hr"></div>
				<?php echo Arr::get($_widgets, 'Advantages'); ?>
				<div class="hr"></div>
			<?php endif; ?>
				
			<?php echo Arr::get($_widgets, 'CallTechnologist'); ?>	
			<?php echo Arr::get($_widgets, 'AskQuestion'); ?>	
			
		</section>
		<?php echo Arr::get($_widgets, 'Footer'); ?>
	</main>
	<?php echo Arr::get($_widgets, 'Scripts'); ?>
</body>
</html>
