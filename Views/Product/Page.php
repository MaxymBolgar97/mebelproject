<?php

use Core\HTML;
use Core\Widgets;
use Core\Route;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--mg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <a href="/top/<?php echo $categoryTop->alias ?>" class="breadcrumbs__link" rel="v:url" property="v:title"><?php echo $categoryTop->name ?></a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-left" />
                                            </svg>
                                        </span>
                                        <a href="/category/<?php echo $category->alias ?>" class="breadcrumbs__link breadcrumbs__link--last" rel="v:url" property="v:title"><?php echo $category->name ?></a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title"><?php echo $product->name ?></span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell cell--24">
                                <div class="grid grid--acenter grid--jbetween">
                                    <div class="cell cell--20">
                                        <div class="title">
                                            <span><?php echo $product->name ?></span>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <div class="title__label">
                                            <span>Код товара:
                                                <b><?php echo $product->artikul ?></b>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-info pv-30 js-wcart-item__add-container" data-id="id2221">
                <div class="grid grid--mg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10 iv-20">
                            <div class="cell cell--8 cell--md12 cell--sm24">
                                <div class="grid mb-15">
                                    <div class="product-info__images js-product-images">
                                        <div>
                                            <div class="product-info__images-item image image--contain">
                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$product->image") ?>" alt=""> </div>
                                        </div>
                                        <div>
                                            <div class="product-info__images-item image image--contain">
                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$product->image") ?>" alt=""> </div>
                                        </div>
                                        <div>
                                            <div class="product-info__images-item image image--contain">
                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$product->image") ?>" alt=""> </div>
                                        </div>
                                        <div>
                                            <div class="product-info__images-item image image--contain">
                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$product->image") ?>" alt=""> </div>
                                        </div>
                                    </div>
                                    <div class="product-info__image js-product-image js-mfp-container js-zoomsl-container">
                                        <div>
                                            <div class="product-info__image-item image image--contain js-mfp-ajax" data-url="hidden/product-image.php" data-param='{"current": 0}' data-nozoom="true">
                                                <img class="js-zoomsl" src="<?php echo HTML::media("images/$product->image") ?>" data-src="<?php echo HTML::media("images/$product->image") ?>" data-large="<?php echo HTML::media("images/$product->image") ?>" alt="">
                                                <div class="product-labels pl-10">
                                                    <div class="grid grid--col i-5">
                                                        <?php if ($product->top != 0): ?>
                                                            <div class="cell">
                                                                <div class="product-labels__item product-labels__item--hit">
                                                                    <span>ХИТ</span>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if ($product->percent != 0): ?>
                                                            <div class="cell">
                                                                <div class="product-labels__item product-labels__item--special">
                                                                    <span><?php echo $product->percent ?>%</span>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--6 cell--lg7 cell--md12 cell--sm24">
                                <div class="grid i-10 mb-0">
                                    <div class="cell">
                                        <div class="btn-link btn-link--small js-compare" data-id="id2221">
                                            <div class="btn-link__icon">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-compare'); ?>" />
                                                </svg>
                                            </div>
                                            <span>К сравнению</span>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                            <div class="btn-link__icon">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-favorite') ?>" />
                                                </svg>
                                            </div>
                                            <span>В избранное</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-info__title mb-10 sm-hide">
                                    <span>Характеристики</span>
                                </div>
                                <div class="product-info__short-desc view-text js-short-desc-content sm-hide">
                                    <div class="grid ig-10">
                                        <span><?php echo $product->charecter ?></span>
                                    </div>
                                </div>
                                <!-- @TODO отображается только если характеристик больше 10 -->
                                <div class="product-info__link mt-5 js-short-desc sm-hide">
                                    <span class="js-short-desc-text" data-show="Все характеристики" data-hide="Скрыть характеристики">Все характеристики</span>
                                </div>
                                <div class="grid grid--acenter ig-10 mt-20 mt-sm0">
                                    <div class="cell">
                                        <div class="product-info__stars">
                                            <?php $emptStars = 5 - $rating; ?>
                                            <?php for ($i = 0; $i < $rating; $i++): ?>
                                                <div class="product-info__star is-full">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                    </svg>
                                                </div>
                                            <?php endfor; ?>
                                            <?php if ($emptStars != 0): ?>
                                                <?php for ($i = 0; $i < $emptStars; $i++): ?>
                                                    <div class="product-info__star">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                        </svg>
                                                    </div>
                                                <?php endfor; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <div class="product-info__raiting-text">
                                            <span><?php echo $reviewsCount ?> отзыв(-а),(-ов)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="social mt-20">
                                    <div class="grid grid--acenter i-10">
                                        <div class="cell">
                                            <div class="social__title">
                                                <span>Поделиться</span>
                                            </div>
                                        </div>
                                        <div class="cell">
                                            <script type="text/javascript">(function () {
                                                    if (window.pluso)
                                                        if (typeof window.pluso.start == "function")
                                                            return;
                                                    if (window.ifpluso == undefined) {
                                                        window.ifpluso = 1;
                                                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                                        s.type = 'text/javascript';
                                                        s.charset = 'UTF-8';
                                                        s.async = true;
                                                        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                                        var h = d[g]('body')[0];
                                                        h.appendChild(s);
                                                    }
                                                })();</script>
                                            <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="facebook,twitter,google,email" data-url="product.html"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--5 cell--lg4 cell--md12 cell--xs24 cell--xsoend">
                                <script>
                                    window.productParams = {
                                        'productId': 'id2221',
                                        'price': 5013, //Текущая цена с учетом параметров (меняется в зависимости от того, что выбрали)
                                        'list': {
                                            'paramId1': {
                                                'itemName': 'Кожа SPLIT',
                                                'valueName': 'SP-A13',
                                                'valueImage': 'images/product-param-1.jpg',
                                                'list': [
                                                    {
                                                        'itemId': '1',
                                                        'valueId': '13'
                                                    }
                                                ]
                                            },
                                            'paramId2': {
                                                'itemName': 'Дерев. элементы',
                                                'valueName': 'Ясень',
                                                'valueImage': 'images/product-param-2.jpg',
                                                'list': [
                                                    {
                                                        'itemId': '1',
                                                        'valueId': '13'
                                                    }
                                                ]
                                            },
                                            'paramId3': {
                                                'itemName': 'Механизм',
                                                'valueName': 'Anyfix',
                                                'valueImage': 'images/product-param-3.jpg',
                                                'list': [
                                                    {
                                                        'itemId': '1',
                                                        'valueId': '13'
                                                    }
                                                ]
                                            },
                                            'paramId4': {
                                                'itemName': 'Тип роликов',
                                                'valueName': 'Прорезиненные',
                                                'valueImage': 'images/product-param-4.jpg',
                                                'list': [
                                                    {
                                                        'itemId': '1',
                                                        'valueId': '13'
                                                    },
                                                    {
                                                        'itemId': '2',
                                                        'valueId': '15'
                                                    }
                                                ]
                                            }
                                        }
                                    };
                                </script>
                            </div>
                            <div class="cell cell--5 cell--md12 cell--xs24">
                                <div class="product-info__info">
                                    <div class="product-info__info-head">
                                        <?php if ($product->cost_old != 0): ?>
                                            <div class="product-info__info-price-old">
                                                <span><?php echo $product->cost_old ?> грн</span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="product-info__info-price">
                                            <span class="js-prod-price"><?php echo $product->cost ?></span>
                                            <span>грн</span>
                                        </div>
                                    </div>
                                    <div class="product-info__info-body">
                                        <div class="grid i-7">
                                            <div class="cell cell--24">
                                                <!-- @TODO Классы для статусов: в наличии - is-avaliable, заканчивается - is-ends, нет в наличии - is-not-available -->
                                                <?php if ($product->cost_old != 0): ?>
                                                    <div class="product-block__old-price">
                                                        <span>от <?php echo $product->cost_old ?> грн.</span>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="product-block__price">
                                                    <span>от <?php echo $product->cost ?> грн</span>
                                                </div>
                                                <?php if ($product->available == 1): ?>
                                                    <div class="product-block__status is-available">
                                                        <span>В наличии</span>
                                                    </div>
                                                <?php elseif ($product->available == 2): ?>
                                                    <div class="product-block__status is-ends">
                                                        <span>Заканчивается</span>
                                                    </div>
                                                <?php elseif ($product->available == 0): ?>
                                                    <div class="product-block__status is-not-available">
                                                        <span>Нет в наличии</span>
                                                    </div>
                                                <?php endif ?>
                                            </div>
                                            <div class="cell cell--24">
                                                <?php if (is_array($_SESSION['prod'])): ?>
                                                    <?php if (!in_array($product->id, $_SESSION['prod'])): ?>
                                                        <div class="btn btn--green btn--big btn--full">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                            </svg>
                                                            <a href="/cart?id=<?php echo $product->id ?>"><span class="js-change-text">Купить</span></a>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="btn btn--green btn--big btn--full">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                            </svg>
                                                            <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"><span class="js-change-text">В корзине</span></a>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <div class="btn btn--green btn--big btn--full">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                        </svg>
                                                        <a href="/cart?id=<?php echo $product->id ?>"><span class="js-change-text">Купить</span></a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="cell cell--24">
                                                <div class="product-info__info-btn grid grid--acenter">
                                                    <div class="cell cell--12">
                                                        <div class="btn btn--full btn--green-trans js-mfp-ajax js-prod-btn" data-url="<?php echo HTML::media('hidden/buy-one.php') ?>" data-param=''>
                                                            <span>Купить в 1 клик</span>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--12">
                                                        <div class="btn btn--full btn--green-trans js-mfp-ajax js-prod-btn" data-url="<?php echo HTML::media('hidden/buy-kredit.php') ?>" data-param=''>
                                                            <span>Купить в кредит</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cell cell--24">
                                                <a href="#" class="product-info__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/product-shipping.php') ?>">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-shipping') ?>" />
                                                    </svg>
                                                    <span>Доставка и сборка</span>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <a href="#" class="product-info__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/product-payment.php') ?>">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-payment') ?>" />
                                                    </svg>
                                                    <span>Оплата</span>
                                                </a>
                                            </div>
                                            <div class="cell cell--24">
                                                <a href="#" class="product-info__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/product-guarant.php') ?>">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-guaranty') ?>" />
                                                    </svg>
                                                    <span>Гарантия</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pb-30">
                <div class="cell cell--24">
                    <div class="grid i-15">
                        <div class="cell cell--16 cell--ms24">
                            <div class="product-tabs">
                                <div class="product-tabs__tabs js-tabs">
                                    <div class="product-tabs__tab-inner">
                                        <div class="grid">
                                            <div class="cell">
                                                <div class="product-tabs__tab js-tabs-link is-active">
                                                    <span>Описание</span>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="product-tabs__tab js-tabs-link">
                                                    <span>Характеристики</span>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="product-tabs__tab js-tabs-link">
                                                    <span>Отзывы</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-tabs__tab-content js-tabs-content is-active">
                                        <div class="grid">
                                            <div class="cell cell--22 cell--xl24">
                                                <div class="product-tabs__text view-text">
                                                    <p><?php echo $product->desc ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-tabs__tab-content js-tabs-content">
                                        <div class="grid">
                                            <div class="cell cell--22 cell--xl24">
                                                <div class="product-attr">
                                                    <p><?php echo $product->charecter ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-tabs__tab-content js-tabs-content">
                                        <div class="grid">
                                            <div class="cell cell--22 cell--xl24">
                                                <div class="review-form">
                                                    <div class="review-form__title mb-10">
                                                        <span>Напишите свой отзыв</span>
                                                    </div>
                                                    <div class="review-form__content p-20">
                                                        <form name='review' action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="POST">
                                                            <div class="form js-form" data-form="true" data-ajax="hidden/response.php">
                                                                <div class="grid i-10">
                                                                    <div class="cell cell--12 cell--sm24">
                                                                        <div class="grid i-10">

                                                                            <div class="cell cell--24">
                                                                                <div class="form__label mb-5">
                                                                                    <span>Представьтесь, пожалуйста *</span>
                                                                                </div>
                                                                                <div class="control-holder control-holder--text">
                                                                                    <input type="text" name="name" data-name="name" required data-rule-word="true">
                                                                                </div>
                                                                            </div>
                                                                            <div class="cell cell--24">
                                                                                <div class="form__label mb-5">
                                                                                    <span>Ваш email *</span>
                                                                                </div>
                                                                                <div class="control-holder control-holder--text">
                                                                                    <input type="email" name="email" data-name="email" required data-rule-email="true">
                                                                                </div>
                                                                            </div>
                                                                            <div class="cell cell--24">
                                                                                <div class="control-holder control-holder--flag">
                                                                                    <div class="grid grid--acenter ig-10 mt-10">
                                                                                        <div class="cell">
                                                                                            <div class="form__label">
                                                                                                <span>Оценка *</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="cell">
                                                                                            <div class="grid grid--acenter ig-5">
                                                                                                <div class="cell">
                                                                                                    <label class="review-form__star js-star" data-star="1" data-text="ужасная модель" data-color="#cc0000">
                                                                                                        <input type="radio" name="review" data-name="review" value="1" required data-msg="Выберите вашу оценку">
                                                                                                        <svg>
                                                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                        </svg>
                                                                                                    </label>
                                                                                                    <label class="review-form__star js-star" data-star="2" data-text="плохая модель" data-color="#990000">
                                                                                                        <input type="radio" name="review" data-name="review" value="2" required data-msg="Выберите вашу оценку">
                                                                                                        <svg>
                                                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                        </svg>
                                                                                                    </label>
                                                                                                    <label class="review-form__star js-star" data-star="3" data-text="обычная модель" data-color="#999999">
                                                                                                        <input type="radio" name="review" data-name="review" value="3" required data-msg="Выберите вашу оценку">
                                                                                                        <svg>
                                                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                        </svg>
                                                                                                    </label>
                                                                                                    <label class="review-form__star js-star" data-star="4" data-text="хорошая модель" data-color="#148514">
                                                                                                        <input type="radio" name="review" data-name="review" value="4" required data-msg="Выберите вашу оценку">
                                                                                                        <svg>
                                                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                        </svg>
                                                                                                    </label>
                                                                                                    <label class="review-form__star js-star" data-star="5" data-text="отличная модель" data-color="#56b318">
                                                                                                        <input type="radio" name="review" data-name="review" value="5" required data-msg="Выберите вашу оценку">
                                                                                                        <svg>
                                                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                        </svg>
                                                                                                    </label>
                                                                                                </div>
                                                                                                <div class="cell">
                                                                                                    <div class="review-form__star-text js-star-text" data-text="" data-color="transparent"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="cell cell--24">
                                                                                            <div>
                                                                                                <label for="review" class="has-error" style="display: none;"></label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <div class="cell cell--12 cell--sm24">
                                                                        <div class="form__label mb-5">
                                                                            <span>Текст Отзыва *</span>
                                                                        </div>
                                                                        <div class="control-holder control-holder--text">
                                                                            <textarea name="message" data-name="message" required data-rule-minlength="10"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="cell cell--12 cell--sm24">
                                                                        <div class="grid grid--jend grid--smjstart">
                                                                            <div class="btn btn--green js-form-submit">
                                                                                <button><span>Оставить отзыв</span></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </form>
                                                </div>
                                                <div class="product-reviews mt-30">
                                                    <div class="product-reviews__title">
                                                        <span>Мнение покупателей</span>
                                                    </div>
                                                    <?php foreach ($reviews as $review): ?>
                                                        <div class="product-reviews__item mv-15 pb-15">
                                                            <div class="grid grid--nowrap i-10">
                                                                <div class="cell cell--nogrow cell--noshrink">
                                                                    <div class="product-reviews__image image image--contain">
                                                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php HTML::media('images/no-avatar.png') ?>" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="cell cell--grow">
                                                                    <div class="grid grid--acenter grid--jbetween pt-10 ig-10 iv-5">
                                                                        <div class="cell">
                                                                            <div class="grid grid--acenter ig-10 iv-5">
                                                                                <div class="cell">
                                                                                    <div class="product-reviews__name">
                                                                                        <span><?php echo $review->name ?></span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cell">
                                                                                    <div class="product-reviews__date">
                                                                                        <span><?php echo gmdate("d-m-Y", $review->created_at); ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cell">
                                                                            <div class="grid grid--acenter i-5">
                                                                                <div class="cell">
                                                                                    <div class="product-reviews__stars">
                                                                                        <?php $countStars = null; ?>
                                                                                        <?php for ($i = 0; $i < $review->stars; $i++): ?>
                                                                                            <div class="product-reviews__star is-full">
                                                                                                <svg>
                                                                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                </svg>
                                                                                            </div>
                                                                                            <?php $countStars++; ?>
                                                                                        <?php endfor; ?>
                                                                                        <?php $emptyStars = 5 - $countStars; ?>
                                                                                        <?php if ($emptyStars > 0): ?>
                                                                                            <?php for ($i = 0; $i < $emptyStars; $i++): ?>
                                                                                                <div class="product-reviews__star">
                                                                                                    <svg>
                                                                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-star') ?>" />
                                                                                                    </svg>
                                                                                                </div>
                                                                                            <?php endfor; ?>
                                                                                        <?php endif; ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cell">
                                                                                    <div class="product-reviews__desc"><?php echo $countStars; ?></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cell cell--24">
                                                                            <div class="product-reviews__desc mt-5">
                                                                                <p><?php echo $review->text ?></p>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--8 cell--ms24">
                            <div class="product-similar">
                                <div class="product-similar__title mb-20">
                                    <span>Похожие товары</span>
                                </div>
                                <div class="grid i-10">
                                    <?php foreach ($likeProducts as $likeProduct): ?>
                                        <div class="cell cell--24 cell--ms12 cell--sm24">
                                            <div class="product-similar__item js-wcart-item__add-container" data-id="id1112">
                                                <div class="grid i-5">
                                                    <div class="cell cell--10 cell--md24 cell--ms10">
                                                        <a href="/product/<?php echo $likeProduct->alias ?>" class="product-similar__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$likeProduct->image") ?>" alt="">
                                                            <div class="product-labels">
                                                                <div class="grid grid--col i-5">
                                                                    <?php if ($likeProduct->top != 0): ?>
                                                                        <div class="cell">
                                                                            <div class="product-labels__item product-labels__item--hit">
                                                                                <span>ХИТ</span>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <?php if ($likeProduct->percent != 0): ?>
                                                                        <div class="cell">
                                                                            <div class="product-labels__item product-labels__item--special">
                                                                                <span>-<?php echo $likeProduct->percent ?>%</span>
                                                                            </div>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="cell cell--14 cell--md24 cell--ms14">
                                                        <div class="grid grid--acenter i-5">
                                                            <div class="cell cell--24">
                                                                <a href="/product/<?php echo $likeProduct->alias ?>" class="product-similar__name">
                                                                    <span><?php echo $likeProduct->name ?></span>
                                                                </a>
                                                            </div>
                                                            <div class="cell cell--15">
                                                                <div class="product-similar__price-block">
                                                                    <?php if ($likeProduct->cost_old != 0): ?>
                                                                        <div class="product-similar__old-price">
                                                                            <span>от <?php echo $likeProduct->cost_old ?> грн</span>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                    <div class="product-similar__price">
                                                                        <span>от <?php echo $likeProduct->cost ?> грн</span>
                                                                    </div>
                                                                    <?php if ($product->available == 1): ?>
                                                                        <div class="product-block__status is-available">
                                                                            <span>В наличии</span>
                                                                        </div>
                                                                    <?php elseif ($product->available == 2): ?>
                                                                        <div class="product-block__status is-ends">
                                                                            <span>Заканчивается</span>
                                                                        </div>
                                                                    <?php elseif ($product->available == 0): ?>
                                                                        <div class="product-block__status is-not-available">
                                                                            <span>Нет в наличии</span>
                                                                        </div>
                                                                    <?php endif ?>
                                                                </div>
                                                            </div>
                                                            <div class="cell cell--9">
                                                                <div class="btn btn--full btn--green js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id1112","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="<?php echo HTML::media('hidden/cart-mfp.php') ?>">
                                                                    <svg>
                                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart') ?>" />
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <div class="block1 pv-15">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--acenter grid--jcenter i-20">
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-1') ?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Сборка корпусной мебели на дому у заказчика – БЕСПЛАТНО</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-2') ?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Более 3000 товаров лучших мебельных брендов</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-3') ?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Лёгкий возврат в течение 14 дней без объяснения причин</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <!--            <div class="block2 pv-30">
                            <div class="grid grid--lg pg-30">
                                <div class="cell cell--24">
                                    <div class="block2__name">
                                        <span>Просмотренные товары</span>
                                    </div>
                                </div>
                            </div>
                            <div class="block2__content">
                                <div class="block2__nav">
                                    <div class="grid grid--acenter grid--jbetween i-20">
                                        <div class="cell">
                                            <div class="block2__btn">
                                                <a href="index.html" class="btn btn--upper">
                                                    <span>Посмотреть все</span>
                                                    <svg>
                                                    <use xlink:href="svg/sprite.svg#icon-arrow-right" />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="cell">
                                            <div class="grid grid--acenter i-5">
                                                <div class="cell">
                                                    <div class="block2__prev js-slider-prev">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#arrow-left"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                                <div class="cell">
                                                    <div class="block2__label">
                                                        <b class="js-index-product-number"></b> /
                                                        <span class="js-index-product-count"></span>
                                                    </div>
                                                </div>
                                                <div class="cell">
                                                    <div class="block2__next js-slider-next">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#arrow-right"></use>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block2__slider owl-carousel js-index-product">
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2225">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-5.jpg" alt=""> </a>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare " data-id="id2225">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2225">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-available">
                                                            <span>В наличии</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2225","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2224">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-4.jpg" alt=""> </a>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare " data-id="id2224">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2224">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-available">
                                                            <span>В наличии</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2224","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2223">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-3.jpg" alt=""> </a>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare " data-id="id2223">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2223">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__old-price">
                                                            <span>от 1814 грн</span>
                                                        </div>
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-not-available">
                                                            <span>Нет в наличии</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2223","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2222">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-2.jpg" alt=""> </a>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare is-active" data-id="id2222">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2222">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-ends">
                                                            <span>Заканчивается</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2222","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2221">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-1.jpg" alt=""> </a>
                                                        <div class="product-labels">
                                                            <div class="grid grid--col i-5">
                                                                <div class="cell">
                                                                    <div class="product-labels__item product-labels__item--hit">
                                                                        <span>ХИТ</span>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="product-labels__item product-labels__item--special">
                                                                        <span>-20%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare " data-id="id2221">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-available">
                                                            <span>В наличии</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2221","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2222">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-2.jpg" alt=""> </a>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare is-active" data-id="id2222">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2222">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-ends">
                                                            <span>Заканчивается</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2222","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="block2__item">
                                        <div class="product-block  js-wcart-item__add-container" data-id="id2221">
                                            <div class="grid grid--acenter i-10">
                                                <div class="cell cell--24">
                                                    <div class="product-block__image-block">
                                                        <a href="product.html" class="product-block__image image image--contain">
                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/product-1.jpg" alt=""> </a>
                                                        <div class="product-labels">
                                                            <div class="grid grid--col i-5">
                                                                <div class="cell">
                                                                    <div class="product-labels__item product-labels__item--hit">
                                                                        <span>ХИТ</span>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="product-labels__item product-labels__item--special">
                                                                        <span>-20%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-block__info">
                                                            <div class="grid grid--col i-5">
            
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-compare " data-id="id2221">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="cell">
                                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                                                        <div class="btn-link__icon">
                                                                            <svg>
                                                                            <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <a href="product.html" class="product-block__name">
                                                        <span>Кресло руководителя Orion Steel Chrome (SP-A, SP-I)</span>
                                                    </a>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="product-block__price-block">
                                                        <div class="product-block__price">
                                                            <span>от 1270 грн</span>
                                                        </div>
                                                        <div class="product-block__status is-available">
                                                            <span>В наличии</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell cell--12">
                                                    <div class="btn btn--green btn--full js-mfp-ajax js-wcart-item__add-button" data-param='{"WezomCart":{"id":"id2221","controller":"itemAdd","params":{"quantity":{"selected":1}}}}' data-url="hidden/cart-mfp.php">
                                                        <svg>
                                                        <use xlink:href="svg/sprite.svg#icon-cart" />
                                                        </svg>
                                                        <span class="js-change-text" data-text='{"default": "Купить", "added": "В корзине"}'>Купить</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
