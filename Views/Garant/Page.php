<?php

use Core\HTML;
use Core\Widgets;
?>
    <?php echo Widgets::get('Head'); ?>
    <body>
        <main>
            <?php echo Widgets::get('Header'); ?>
            <section>
                <div class="block-top pv-10">
                    <div class="grid grid--lg pg-30">
                        <div class="cell cell--24">
                            <div class="grid ig-10">
                                <div class="cell cell--24">

                                    <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                    <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                        <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                            <span class="breadcrumbs__left">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                                </svg>
                                            </span>
                                            <a href="/" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                            <span class="breadcrumbs__right">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                                </svg>
                                            </span>
                                        </span>
                                        <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                            <span class="breadcrumbs__current" property="v:title">Гарантия</span>
                                            <span class="breadcrumbs__right">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                                </svg>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div class="title">
                                        <span>Доставка и оплата</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid grid--lg pg-30 pt-50 pt-ms30">
                    <div class="cell cell--24">
                        <div class="grid i-50 i-lg20">
                            <div class="cell cell--12 cell--sm24 cell--smoend">
                                <div class="block6__image-block">
                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/guarant-1.jpg')?>" alt="">
                                </div>
                            </div>
                            <div class="cell cell--12 cell--sm24 cell--smostart">
                                <div class="block6__title block6__title--left mb-50 mb-lg30">
                                    <span>Гарантийный срок
                                        <br>эксплуатации мебели</span>
                                </div>
                                <div class="block6__text view-text mb-20 ml-50 ml-md0">
                                    <p>Детской мебели и для общественных помещений –
                                        <b>18 месяцев</b>
                                    </p>
                                    <p>Бытовой мебели –
                                        <b>24 месяца</b> с момента поставки</p>
                                </div>
                                <div class="block6__desc view-text mb-50 ml-50 ml-md0 mb-lg30">
                                    <p>За исключением случаев неосторожной эксплуатации или преднамеренного повреждения по вине Покупателя.</p>
                                    <p>В случае выявления Покупателем каких-либо дефектов, компания Мебель-АРТ в Николаеве обязуется устранить недостатки в течение
                                        <b>30 дней</b>.</p>
                                </div>
                                <div class="view-text">
                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('pic/guarant-2.png')?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt-100 pt-ms30"></div>
                <?php echo Widgets::get('Index_Block4'); ?>
            </section>
            <?php echo Widgets::get('Footer'); ?>
        </main>
        <?php echo Widgets::get('Scripts'); ?>
    </body>
