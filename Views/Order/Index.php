<?php

use Core\HTML;
use Core\Widgets;
use Core\Route;
?>
<?php echo Widgets::get('Head'); ?>
<style>
</style>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-left" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Оформление заказа</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Оформление заказа</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid ig-50 i-lg15">
                        <div class="cell cell--13 cell--ms24">
                            <div class="order">
                                <form action="/order/confirm" method="POST">
                                    <input type="hidden" name="userID" value="<?php echo $userID?>">
                                    <input type="hidden" name="hash" value="<?php echo $hash?>">
                                    <input type="hidden" name="totalSum" value="<?php echo $totalSum?>">
                                    <div class="grid i-15">
                                        <div class="cell cell--24">
                                            <div class="form__label">
                                                <span>ФИО*</span>
                                            </div>
                                            <div class="control-holder control-holder--text">
                                                <input type="text" name="name" data-name="name" required data-rule-word="true">
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="form__label">
                                                <span>Адрес*</span>
                                            </div>
                                            <div class="control-holder control-holder--text">
                                                <input type="text" name="adress" data-name="address" required data-rule-minlength="2">
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="grid i-10">
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="form__label">
                                                        <span>Этаж*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="text" name="floor" data-name="number" required>
                                                    </div>
                                                </div>
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="form__label">
                                                        <span>Email*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="email" name="email" data-name="email" required data-rule-email="true">
                                                    </div>
                                                </div>
                                                <div class="cell cell--8 cell--xs24">
                                                    <div class="form__label">
                                                        <span>Телефон*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="phone" name="tel" data-name="tel" required data-rule-phone="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="form__label mb-10">
                                                <span>Доставка*</span>
                                            </div>
                                            <div class="control-holder control-holder--flag">
                                                <div class="grid i-5">
                                                    <div class="cell cell--24">
                                                        <label>
                                                            <input type="radio" name="shipping" data-name="delivery" value="1" required checked>
                                                            <ins></ins>
                                                            <span>Самовызов</span>
                                                        </label>
                                                    </div>
                                                    <div class="cell cell--24">
                                                        <label>
                                                            <input type="radio" name="shipping" data-name="delivery" value="2" required>
                                                            <ins></ins>
                                                            <span>Доставка</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div>
                                                    <label for="agree" class="has-error" style="display: none;"></label>
                                                </div>
                                            </div>
                                            <div class="hr mv-10"></div>
                                            <div class="form__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/product-shipping.php')?>">
                                                <span>Подробнее о доставке</span>
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="form__label">
                                                <span>Примечание к заказу</span>
                                            </div>
                                            <div class="control-holder control-holder--text">
                                                <textarea name="message" data-name="message" data-rule-minlength="10"></textarea>
                                            </div>
                                            <div class="form__text mt-10">
                                                <span>Нажимая кнопку "Заказ подтверждаю" вы соглашаетесь с
                                                    <a href="/faq" target="_blank" class="form__link">условиями предоставления персональных данных</a> интернет-магазину Мебель 048 </span>
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="btn btn--green js-form-submit">
                                                <button name="ordering"><span>Заказ подтверждаю</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="cell cell--11 cell--ms24">
                            <div class="cart-order">
                                <div class="cart-order__head">
                                    <div class="cart-order__title">
                                        <span>В корзине</span>
                                    </div>
                                    <div class="cart-order__text">
                                        <span><?php echo $totalCount ?> товара(ов) на сумму
                                            <span class="cart-order__select"><?php echo $totalSum ?> грн.</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="cart-order__body">
                                    <?php foreach ($products as $product):?>
                                    <div class="cart-order__item">
                                        <div class="grid i-10">
                                            <div class="cell cell--5 cell--sm6">
                                                <a href="" class="cart-order__image">
                                                    <img src="<?php echo HTML::media('images/'.$product['image'])?>" alt="">
                                                </a>
                                            </div>
                                            <div class="cell cell--19 cell--sm18">
                                                <div class="grid i-5">
                                                    <div class="cell cell--12">
                                                        <a href="index.html" class="cart-order__name">
                                                            <span><?php echo $product['name']?></span>
                                                        </a>
                                                    </div>
                                                    <div class="cell cell--6">
                                                        <div class="cart-order__count">
                                                            <span><?php echo $product['count']?> шт.</span>
                                                        </div>
                                                    </div>
                                                    <div class="cell cell--6">
                                                        <div class="cart-order__price">
                                                            <span><?php echo $product['summ']?> грн.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr mv-20"></div>
                                    <?php endforeach;?>
                                    <div class="cart-order__foot pb-20">
                                        <div class="grid grid--jbetween i-10">
                                            <div class="cell">
                                                <div class="cart-order__info">
                                                    <span>ИТОГО:</span>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="cart-order__total">
                                                    <span><?php echo $totalSum ?> грн.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gcenter mt-20">
                                        <a href="/cart">
                                            <div class="cart-order__name">
                                                <span>Редактировать заказ</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
