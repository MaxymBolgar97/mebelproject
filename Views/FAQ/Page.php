<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left')?>" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">О компании</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right')?>" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>О компании</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50">
                <div class="cell cell--24">
                    <div class="grid i-20">
                        <div class="cell cell--12 cell--ms24">
                            <div class="block6__text view-text">
                                <p>Компания "МЕБЕЛЬ-АРТ" – это профессионалы мебельного искусства, это искусство делать мебель! Всё началось в 2000 году с обычного гаража - тщательность сборки, требовательность к качеству исходных материалов и к готовой продукции с первого дня стали основными принципами компании, остаются ими и сейчас. В процессе роста и развития компании появился опыт, пришли надёжные поставщики, совершенствовались технологии производства и оборудование, появилось умение экономить, сохраняя высокое качество.</p>
                            </div>
                            <div class="grid ig-10 iv-20 mt-50">
                                <div class="cell cell--4">
                                    <div class="block6__icon">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-1.png')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--20">
                                    <div class="block6__text mb-20">
                                        <span>Развитие</span>
                                    </div>
                                    <div class="block6__desc view-text">
                                        <p>На сегодня - компания "МЕБЕЛЬ-АРТ" имеет самое современное производство по изготовлению корпусной мебели для дома и офиса, сеть магазинов по оптовой и розничной продаже, расположенных непосредственно рядом с производством, дизайн-студию, интернет-магазин мебели в Николаеве и других городах Украины.Во всех этих подразделениях трудятся профессионалы высокого уровня, способные реализовать самые смелые пожелания заказчика, которые могут решать сложные технологические задачи. Близость фабрики и магазинов даёт экономию транспортных расходов, это делает нашу мебель более доступной по цене.</p>
                                    </div>
                                </div>
                                <div class="cell cell--4">
                                    <div class="block6__icon">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-2.png')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--20">
                                    <div class="block6__text mb-20">
                                        <span>Цель</span>
                                    </div>
                                    <div class="block6__desc view-text">
                                        <p>Цель компании - повышение качества обслуживания покупателей, расширение ассортимента, завоевание и удержание доверия к компании "МЕБЕЛЬ-АРТ", увеличение числа постоянных клиентов, создание наилучшего соотношения цены и качества продукции.</p>
                                    </div>
                                </div>
                                <div class="cell cell--4">
                                    <div class="block6__icon">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-3.png')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--20">
                                    <div class="block6__text mb-20">
                                        <span>Миссия</span>
                                    </div>
                                    <div class="block6__desc view-text">
                                        <p>Мы видим своё предназначение в том, чтобы каждый наш покупатель вместе с мебелью приобретал комфорт, удобство и надёжность по доступной цене.</p>
                                    </div>
                                </div>
                                <div class="cell cell--4">
                                    <div class="block6__icon">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-4.png')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--20">
                                    <div class="block6__text mb-20">
                                        <span>Продукция</span>
                                    </div>
                                    <div class="block6__desc view-text">
                                        <p>Мы предлагаем купить мебель для дома: в гостиную, в спальню, в детскую комнату, в кухню, в прихожую. Мебель для офиса: письменные и компьютерные столы, стулья и кресла, шкафы и стеллажи, кабинеты руководителя и многое другое. На всю мебель распространяется гарантия качества, мы обеспечиваем доставку и сборку купленной мебели домой и на предприятия.</p>
                                    </div>
                                </div>
                                <div class="cell cell--4">
                                    <div class="block6__icon">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-5.png')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--20">
                                    <div class="block6__text mb-20">
                                        <span>Планы</span>
                                    </div>
                                    <div class="block6__desc view-text">
                                        <p>Мы планируем продолжать работу по повышению контроля над качеством выпускаемой продукции, улучшать уровень обслуживания покупателей, создавать новые, современные элементы мебели. Для расширения ассортимента – привлекать к сотрудничеству другихпроизводителей.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--12 cell--ms24">
                            <div class="grid iv-20">
                                <div class="cell cell--24 cell--msoend">
                                    <div class="block6__image-block">
                                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/block6-1.jpg')?>" alt="">
                                    </div>
                                </div>
                                <div class="cell cell--24 cell--msostart">
                                    <div class="grid ig-10 iv-20">
                                        <div class="cell cell--4">
                                            <div class="block6__icon">
                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/icon-about-6.png')?>" alt="">
                                            </div>
                                        </div>
                                        <div class="cell cell--20">
                                            <div class="block6__text mb-20">
                                                <span>В заключение</span>
                                            </div>
                                            <div class="block6__desc view-text">
                                                <p>Наши дизайнеры и технологи систематически посещают специализированные выставки, изучают новые тенденции искусства создания мебели, это отражается на многообразии вариантов предлагаемой продукции. Кроме готовой мебели, вы можете сделать индивидуальный заказ – в этом случае наши менеджеры всё рассчитают, технологи сделают необходимые замеры.</p>
                                                <p>Дизайнеры создадут несколько вариантов расстановки мебели и её цветового решения, представят 3D план Вашего заказа, останется только оплатить и убедиться, что с компанией «МЕБЕЛЬ-АРТ» поменять обстановку легко и приятно. Мы занимаемся искусством создания мебели, чтобы Вас окружали комфортные условия на работе и дома, чтобы качество Вашей жизни возрастало. Мы и в дальнейшем, будем стремиться создавать качественную, удобную мебель, делая её доступной для каждого покупателя, и мы всегда будем рады видеть Вас в числе наших клиентов.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="view-text">
                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/about-1.jpg')?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <div class="block1 pv-15">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--acenter grid--jcenter i-20">
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-1')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Сборка корпусной мебели на дому у заказчика – БЕСПЛАТНО</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-2')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Более 3000 товаров лучших мебельных брендов</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-3')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Лёгкий возврат в течение 14 дней без объяснения причин</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <div class="pt-100 pt-md50 pt-ms30"></div>
            <?php echo Widgets::get('Index_Block4'); ?>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>