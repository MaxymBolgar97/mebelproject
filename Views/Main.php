<?php

use Core\HTML;
use Core\Widgets;
use Core\Arr;
?>
<!DOCTYPE html>
<html lang="ru" dir="ltr" class="no-js">

    <?php echo Widgets::get('Head'); ?>

    <body>
        <main>
            <?php echo Widgets::get('Header'); ?>
            <section>
                <?php echo Widgets::get('Index_MainSlider'); ?>
                <?php echo Widgets::get('Index_Block1'); ?>
                <div class="hr"></div>
                <?php echo Widgets::get('Index_Block2'); ?>
                <?php echo Widgets::get('Index_Block3'); ?>
                <?php echo Widgets::get('Index_Block4'); ?>
                <div class="pb-30"></div>
                <?php echo Widgets::get('Index_Block5'); ?>
                <?php echo Widgets::get('Index_Block6'); ?>
            </section>
            <?php echo Widgets::get('Footer'); ?>
        </main>
        <?php echo Widgets::get('Scripts'); ?>

    </body>

</html>
