<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-left') ?>" />
                                            </svg>
                                        </span>
                                        <a href="index.html" class="breadcrumbs__link breadcrumbs__link--first breadcrumbs__link--last" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right') ?>" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Отзывы</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-right') ?>" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span>Отзывы</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pv-50 pv-ms30">
                <div class="cell cell--24">
                    <div class="grid grid--acenter grid--jbetween ig-10 iv-20">
                        <div class="cell cell--sm0">
                            <?php echo $pager; ?>
                        </div>
                        <div class="cell">
                            <div class="btn js-link" data-link=".js-review-form">
                                <span>Добавить отзыв</span>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="hr"></div>
                        </div>
                        <div class="cell cell--24">
                            <div class="reviews">
                                <div class="grid iv-10">
                                    <?php foreach ($reviews as $review): ?>
                                        <div class="cell cell--24">
                                            <div class="reviews__item reviews__item--short js-mfp-container">
                                                <div class="reviews__item-inner">
                                                    <div class="grid grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink gright">
                                                            <div class="reviews__date">
                                                                <span><?php echo gmdate("d-m-Y", $review->created_at); ?></span>
                                                            </div>
                                                            <div class="reviews__quote">
                                                                <svg>
                                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-quote') ?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="reviews__name">
                                                                <span><?php echo $review->name ?></span>
                                                            </div>
                                                            <div class="reviews__text">
                                                                <div class="reviews__text-inner js-text-inner">
                                                                    <p><?php echo $review->text ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cell cell--24">
                                            <div class="reviews__item reviews__item--answer js-mfp-container">
                                                <div class="reviews__item-inner">
                                                    <div class="grid grid--nowrap i-10">
                                                        <div class="cell cell--nogrow cell--noshrink gright">
                                                            <div class="reviews__date">
                                                                <span><?php echo gmdate("d-m-Y", $review->created_at); ?></span>
                                                            </div>
                                                            <div class="reviews__logo">
                                                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('pic/logo-short.png') ?>" alt="">
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--grow">
                                                            <div class="reviews__name">
                                                                <span>Ответ Администрации</span>
                                                            </div>
                                                            <div class="reviews__text">
                                                                <div class="reviews__text-inner js-text-inner">
                                                                    <p><?php echo $review->answer ?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--24">
                            <div class="grid grid--jcenter pt-20">
                                <div class="cell">

                                    <div class="pagination">
                                        <div class="grid grid--jcenter">
                                            <div class="cell">
                                                <div class="grid grid--nowrap">
                                                  <?php echo $pager; ?>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-30 js-review-form"></div>
            <div class="block4">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="block4__title">
                            <span>Оставить отзыв</span>
                        </div>
                        <div class="block4__container">
                            <div class="grid grid--jbetween">
                                <div class="cell cell--12 cell--ms24 pr-20 pr-ms10 pr-sm0">
                                    <div class="block4__name mb-20">
                                        <span>Оставьте свой отзыв</span>
                                    </div>
                                    <div class="block4__text mb-20">
                                        <p>Ваше мнение очень важно для нас</p>
                                    </div>
                                    <div class="form js-form" data-form="true" data-ajax="<?php echo HTML::media('hidden/response.php') ?>">
                                        <form action="/reviews" method="POST">
                                            <div class="grid i-10">
                                                <div class="cell cell--12 cell--xs24">
                                                    <div class="form__label mb-5">
                                                        <span>Ваше имя*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="text" name="name" data-name="name" required data-rule-word="true">
                                                    </div>
                                                </div>
                                                <div class="cell cell--12 cell--xs24">
                                                    <div class="form__label mb-5">
                                                        <span>Email*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <input type="email" name="email" data-name="email" required data-rule-email="true">
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <div class="form__label mb-5">
                                                        <span>Сообщение*</span>
                                                    </div>
                                                    <div class="control-holder control-holder--text">
                                                        <textarea name="message" data-name="message" required data-rule-minlength="10"></textarea>
                                                    </div>
                                                </div>
                                                <div class="cell cell--24">
                                                    <div class="btn btn--green btn--full js-form-submit">
                                                        <button>Отправить</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="block4__image ms-hide">
                                <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('pic/block4-1.png') ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pt-100 pt-ms30"></div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
