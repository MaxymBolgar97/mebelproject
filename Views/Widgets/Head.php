<?php
use Core\HTML;
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Главная страница - Верстка | (c) Wezom | www.wezom.com.ua</title>

	<meta property="og:title" content="Заголовок страницы">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="site.com">
	<meta property="og:url" content="http://site.com/current-page">
	<meta property="og:description" content="краткий текст (новости, товара и т.д)">

	<meta name="format-detection" content="telephone=no">
	<meta name="format-detection" content="address=no">

	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo HTML::media('favicons/apple-touch-icon-57x57.png')?>">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo HTML::media('favicons/apple-touch-icon-60x60.png')?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo HTML::media('favicons/apple-touch-icon-72x72.png')?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo HTML::media('favicons/apple-touch-icon-76x76.png')?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo HTML::media('favicons/apple-touch-icon-114x114.png')?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo HTML::media('favicons/apple-touch-icon-120x120.png')?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo HTML::media('favicons/apple-touch-icon-144x144.png')?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo HTML::media('favicons/apple-touch-icon-152x152.png')?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo HTML::media('favicons/apple-touch-icon-180x180.png')?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo HTML::media('favicons/favicon-32x32.png')?>">
	<link rel="icon" type="image/png" sizes="194x194" href="<?php echo HTML::media('favicons/favicon-194x194.png')?>">
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo HTML::media('favicons/android-chrome-192x192.png')?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo HTML::media('favicons/favicon-16x16.png')?>">
	<link rel="manifest" href="<?php echo HTML::media('favicons/manifest.json')?>">
	<link rel="mask-icon" href="<?php echo HTML::media('favicons/safari-pinned-tab.svg')?>" color="#0164af">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="<?php echo HTML::media('favicons/mstile-144x144.png')?>">
	<meta name="theme-color" content="#ffffff">
	<link rel="icon" type="image/x-icon" href="<?php echo HTML::media('favicons/favicon.ico')?>">
	<link rel="shortcut icon" href="<?php echo HTML::media('favicons/favicon.ico')?>">
	<meta name="msapplication-config" content="<?php echo HTML::media('favicons/browserconfig.xml')?>">

	<script>!function(a,b){"function"==typeof define&&define.amd?define([],function(){return a.svg4everybody=b()}):"object"==typeof exports?module.exports=b():a.svg4everybody=b()}(this,function(){function a(a,b){if(b){var c=document.createDocumentFragment(),d=!a.getAttribute("viewBox")&&b.getAttribute("viewBox");d&&a.setAttribute("viewBox",d);for(var e=b.cloneNode(!0);e.childNodes.length;)c.appendChild(e.firstChild);a.appendChild(c)}}function b(b){b.onreadystatechange=function(){if(4===b.readyState){var c=b._cachedDocument;c||(c=b._cachedDocument=document.implementation.createHTMLDocument(""),c.body.innerHTML=b.responseText,b._cachedTarget={}),b._embeds.splice(0).map(function(d){var e=b._cachedTarget[d.id];e||(e=b._cachedTarget[d.id]=c.getElementById(d.id)),a(d.svg,e)})}},b.onreadystatechange()}function c(c){function d(){for(var c=0;c<l.length;){var g=l[c],h=g.parentNode;if(h&&/svg/i.test(h.nodeName)){var i=g.getAttribute("xlink:href");if(e&&(!f.validate||f.validate(i,h,g))){h.removeChild(g);var m=i.split("#"),n=m.shift(),o=m.join("#");if(n.length){var p=j[n];p||(p=j[n]=new XMLHttpRequest,p.open("GET",n),p.send(),p._embeds=[]),p._embeds.push({svg:h,id:o}),b(p)}else a(h,document.getElementById(o))}}else++c}k(d,67)}var e,f=Object(c),g=/\bTrident\/[567]\b|\bMSIE (?:9|10)\.0\b/,h=/\bAppleWebKit\/(\d+)\b/,i=/\bEdge\/12\.(\d+)\b/;e="polyfill"in f?f.polyfill:g.test(navigator.userAgent)||(navigator.userAgent.match(i)||[])[1]<10547||(navigator.userAgent.match(h)||[])[1]<537;var j={},k=window.requestAnimationFrame||setTimeout,l=document.getElementsByTagName("use");e&&d()}return c});</script>
	<script>document.addEventListener("DOMContentLoaded", function() { svg4everybody({}); });</script>

	<!--[if IE]><meta http-equiv="imagetoolbar" content="no"><![endif]-->
	<!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->

	<link rel="stylesheet" href="<?php echo HTML::media('css/jquery-plugins.css')?>">
	<link rel="stylesheet" href="<?php echo HTML::media('css/init.css')?>">
</head>