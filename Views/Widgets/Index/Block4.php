<?php

use Core\HTML;
?>
<div class="block4">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="block4__title">
                <span>Вызвать технолога</span>
            </div>
            <div class="block4__container">
                <div class="grid grid--jbetween">
                    <div class="cell cell--9 cell--ms14 cell--sm24 pr-20 pr-ms10 pr-sm0">
                        <div class="block4__name mb-20">
                            <span>Оставьте заявку</span>
                        </div>
                        <div class="block4__text mb-20">
                            <p>и наш специалист перезвонит Вам в течении часа</p>
                        </div>
                        <form action="/" method="POST">
                            <div class="grid i-10">
                                <div class="cell cell--12 cell--xs24">
                                    <div class="form__label mb-5">
                                        <span>Ваше имя*</span>
                                    </div>
                                    <div class="control-holder control-holder--text">
                                        <input type="text" name="name" data-name="name" required data-rule-word="true">
                                    </div>
                                </div>
                                <div class="cell cell--12 cell--xs24">
                                    <div class="form__label mb-5">
                                        <span>Телефон*</span>
                                    </div>
                                    <div class="control-holder control-holder--text">
                                        <input type="tel" name="tel" data-name="tel" required data-rule-phone="true">
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <div class="btn btn--green btn--full js-form-submit">
                                        <button name="send"><span>Оставить заявку</span></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="cell cell--15 cell--ms10 cell--sm24 pl-20 pl-ms10 pl-sm0">
                        <div class="block4__icon mt-50 mt-lg30 sm-hide">
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone2') ?>" />
                            </svg>
                        </div>
                        <div class="block4__text mt-20">
                            <p>Или позвоните:</p>
                        </div>
                        <a href="tel:096 929-10-45" class="block4__tel">
                            <span>096 929-10-45</span>
                        </a>
                        <a href="tel:099 909-54-38" class="block4__tel">
                            <span>099 909-54-38</span>
                        </a>
                    </div>
                </div>
                <div class="block4__image ms-hide">
                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('pic/block4-1.png') ?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>