<?php

use Core\HTML;
?>
<div class="block5 pv-30 pt-ms0">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="block5__title">
                <span>Отзывы клиентов</span>
            </div>
        </div>
    </div>
    <div class="pv-30 pg-lg30 prelative">
        <div class="block5__nav">
            <div class="grid grid--acenter grid--jbetween i-20">
                <div class="cell">
                    <div class="block5__btn">
                        <a href="index.html" class="btn btn--upper">
                            <span>Посмотреть все</span>
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="cell">
                    <div class="grid grid--acenter i-5">
                        <div class="cell">
                            <div class="block5__prev js-slider-prev">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-left') ?>"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block5__label">
                                <b class="js-index-slider-number"></b> /
                                <span class="js-index-slider-count"></span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block5__next js-slider-next">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block5__slider owl-carousel js-index-slider">
            <?php foreach($reviews as $review): ?>
            <div class="block5__item">
                <div class="index-review">
                    <div class="grid i-10 i-md5 i-ms10">
                        <div class="cell cell--7 gright">
                            <div class="index-review__date">
                                <span><?php echo gmdate("d-m-Y", $review->created_at); ?></span>
                            </div>
                        </div>
                        <div class="cell cell--17">
                            <div class="index-review__name">
                                <span><?php echo $review->name ?></span>
                            </div>
                        </div>
                        <div class="cell cell--7 gright">
                            <div class="index-review__quote">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-quote') ?>" />
                                </svg>
                            </div>
                        </div>
                        <div class="cell cell--17">
                            <div class="index-review__text js-text-height">
                                <div class="index-review__text-inner js-text-inner">
                                    <p><?php echo $review->text ?></p>
                                </div>
                            </div>
                            <div class="index-review__link js-text-more">
                                <span>Читать полностью</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
<div class="block5 pv-30 pt-ms0">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="block5__title">
                <span>Полезно знать</span>
            </div>
        </div>
    </div>
    <div class="pv-30 pg-lg30 prelative">
        <div class="block5__nav">
            <div class="grid grid--acenter grid--jbetween i-20">
                <div class="cell">
                    <div class="block5__btn">
                        <a href="index.html" class="btn btn--upper">
                            <span>Посмотреть все</span>
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="cell">
                    <div class="grid grid--acenter i-5">
                        <div class="cell">
                            <div class="block5__prev js-slider-prev">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-left') ?>"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block5__label">
                                <b class="js-index-slider-number"></b> /
                                <span class="js-index-slider-count"></span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block5__next js-slider-next">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block5__slider owl-carousel js-index-slider">
            <?php foreach($news as $article): ?>
            <div class="block5__item">
                <div class="index-know">
                    <a href="index.html" class="index-know__image image image--cover">
                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$article->image") ?>" alt=""> </a>
                    <div class="grid i-10">
                        <div class="cell cell--7 gright">
                            <div class="index-know__date">
                                <span><?php echo gmdate("d-m-Y", $article->created_at); ?></span>
                            </div>
                        </div>
                        <div class="cell cell--17">
                            <a href="index.html" class="index-know__name">
                                <span><?php echo $article->name?></span>
                            </a>
                            <div class="index-know__desc">
                                <p><?php echo $article->description?>…</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>