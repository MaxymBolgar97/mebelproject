<?php

use Core\HTML;
?>
<div class="block3 pv-50 pv-lg30">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid">
                <div class="cell cell--10 cell--ms12 cell--sm24">
                    <div class="block3__left">
                        <div class="grid grid--smacenter grid--smcol iv-10">
                            <div class="cell cell--16 cell--lg20 cell--sm">
                                <div class="block3__left-image">
                                    <div class="block3__left-image-inner">
                                        <div class="block3__left-image-block">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/block3-2.png') ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--16 cell--lg20 cell--sm">
                                <div class="block3__left-title">
                                    <span>Мебель на заказ</span>
                                </div>
                            </div>
                            <div class="cell cell--16 cell--lg20 cell--sm">
                                <div class="block3__left-text sm-gcenter">
                                    <p>Мебель на заказ порой становится единственным верным и возможным решением, ведь фабричные изделия изготавливаются по стандарту, не учитывая всех особенностей планировки помещения. В таких ситуациях, индивидуальная мебель приходит на помощь.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cell cell--14 cell--ms12 cell--sm0 prelative">
                    <div class="block3__right">
                        <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/block3-1.jpg') ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pv-50 pv-lg30 pg-lg30 prelative">
        <div class="block3__slider owl-carousel js-index-cat">
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Кухни на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Детская мебель на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-3.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Офисная мебель на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-4.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Шкафы-купе на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-5.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Спальни на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Кухни на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Детская мебель на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-3.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Офисная мебель на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-4.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Шкафы-купе на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-5.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Спальни на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-1.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Кухни на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <div class="block3__item">
                <a href="/contacts" class="cat-block">
                    <div class="cat-block__inner">
                        <div class="cat-block__image image image--hcover">
                            <!-- @TODO Картинки должны иметь размеры 476х675 рх -->
                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/cat-block-2.jpg') ?>" alt=""> </div>
                        <div class="cat-block__info">
                            <span>Детская мебель на заказ
                                <i>
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                    </svg>
                                </i>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="block3__nav">
            <div class="grid grid--acenter grid--jbetween i-20">
                <div class="cell">
                    <div class="block3__btn">
                        <a href="/contacts" class="btn btn--upper">
                            <span>Посмотреть все</span>
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="cell">
                    <div class="grid grid--acenter i-5">
                        <div class="cell">
                            <div class="block3__prev js-slider-prev">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-left') ?>"></use>
                                </svg>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block3__label">
                                <b class="js-index-cat-number"></b> /
                                <span class="js-index-cat-count"></span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block3__next js-slider-next">
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="block3__content view-text">
                <p>Продукция нашей компании всегда отличалась высоким уровнем качества, стильным внешним видом и широкой функциональностью. Мебель индивидуальным размерам изготавливается в кратчайшие сроки, а собранный за многие годы автопарк позволяет безопасно и быстро доставлять товар в любою точку города и области.</p>
                <p>Что бы обсудить особенности процесса изготовления корпусной мебели, а также получить ответы на возникшие вопросы, Вам следует оставить заявку в “контактной форме”, или же позвонить по телефону, указанному в шапке сайта. Наши технологи и дизайнеры приедут в удобное для Вас время, для того, чтобы снять замеры помещения, а также составить первичный эскиз.</p>
            </div>
        </div>
    </div>
</div>