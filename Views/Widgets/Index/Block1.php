<?php

use Core\HTML;
?>
<div class="block1 pv-15">
    <div class="grid grid--lg pg-30">
        <div class="cell cell--24">
            <div class="grid grid--acenter grid--jcenter i-20">
                <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                    <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                        <div class="block1__icon">
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-1') ?>" />
                            </svg>
                        </div>
                        <div class="block1__label">?</div>
                        <div class="block1__text">
                            <span>Сборка корпусной мебели на дому у заказчика – БЕСПЛАТНО</span>
                        </div>
                    </div>
                </div>
                <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                    <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                        <div class="block1__icon">
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-2') ?>" />
                            </svg>
                        </div>
                        <div class="block1__label">?</div>
                        <div class="block1__text">
                            <span>Более 3000 товаров лучших мебельных брендов</span>
                        </div>
                    </div>
                </div>
                <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                    <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php') ?>">
                        <div class="block1__icon">
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-3') ?>" />
                            </svg>
                        </div>
                        <div class="block1__label">?</div>
                        <div class="block1__text">
                            <span>Лёгкий возврат в течение 14 дней без объяснения причин</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>