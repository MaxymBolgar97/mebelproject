<?php

use Core\HTML;
?>
<div class="block2 pv-30">
    <div class="block2__tabs js-tabs">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="block2__tab-inner">
                    <div class="grid">
                        <div class="cell">
                            <div class="block2__tab js-tabs-link is-active">
                                <span>Хиты продаж</span>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="block2__tab js-tabs-link">
                                <span>Распродажа</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block2__tab-content js-tabs-content is-active">
            <div class="block2__nav">
                <div class="grid grid--acenter grid--jbetween i-20">
                    <div class="cell">
                        <div class="block2__btn">
                            <a href="/tops" class="btn btn--upper">
                                <span>Посмотреть все</span>
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="grid grid--acenter i-5">
                            <div class="cell">
                                <div class="block2__prev js-slider-prev">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-left') ?>"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="block2__label">
                                    <b class="js-index-product-number"></b> /
                                    <span class="js-index-product-count"></span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="block2__next js-slider-next">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block2__slider owl-carousel js-index-product">
                <?php foreach ($productsSale as $product): ?>
                    <div class="block2__item">
                        <div class="product-block  js-wcart-item__add-container" data-id="id2221">
                            <div class="grid grid--acenter i-10">
                                <div class="cell cell--24">
                                    <div class="product-block__image-block">
                                        <a href="/product" class="product-block__image image image--contain">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/' . $product->image) ?>" alt=""> </a>
                                        <div class="product-labels">
                                            <div class="grid grid--col i-5">
                                                <div class="cell">
                                                    <div class="product-labels__item product-labels__item--hit">
                                                        <span>ХИТ</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-block__info">
                                            <div class="grid grid--col i-5">

                                                <div class="cell">
                                                    <div class="btn-link btn-link--small js-compare " data-id="id2221">
                                                        <div class="btn-link__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-compare') ?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell">
                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                                        <div class="btn-link__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-favorite') ?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <a href="/product/<?php echo $product->alias ?>" class="product-block__name">
                                        <span><?php echo $product->name ?></span>
                                    </a>
                                </div>
                                <div class="cell cell--12">
                                    <div class="product-block__price-block">
                                        <?php if ($product->cost_old != 0): ?>
                                            <div class="product-block__old-price">
                                                <span>от <?php echo $product->cost_old ?> грн.</span>
                                            </div>
                                        <?php endif; ?>
                                        <div class="product-block__price">
                                            <span>от <?php echo $product->cost ?> грн.</span>
                                        </div>
                                        <?php if ($product->available == 1): ?>
                                            <div class="product-block__status is-available">
                                                <span>В наличии</span>
                                            </div>
                                        <?php elseif ($product->available == 2): ?>
                                            <div class="product-block__status is-ends">
                                                <span>Заканчивается</span>
                                            </div>
                                        <?php elseif ($product->available == 0): ?>
                                            <div class="product-block__status is-not-available">
                                                <span>Нет в наличии</span>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="cell cell--12">
                                    <?php if (is_array($_SESSION['prod'])): ?>
                                        <?php if (in_array($product->id, $_SESSION['prod'])): ?>
                                            <a class="btn btn--green btn--full" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                </svg>
                                                <span>В корзине</span>
                                            </a>
                                        <?php else: ?>
                                            <a class="btn btn--green btn--full" href="/product/<?php echo $product->alias ?>">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                </svg>
                                                <span>Купить</span>
                                            </a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a class="btn btn--green btn--full" href="/product/<?php echo $product->alias ?>">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                            </svg>
                                            <span>Купить</span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="block2__tab-content js-tabs-content">
            <div class="block2__nav">
                <div class="grid grid--acenter grid--jbetween i-20">
                    <div class="cell">
                        <div class="block2__btn">
                            <a href="/sale" class="btn btn--upper">
                                <span>Посмотреть все</span>
                                <svg>
                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="grid grid--acenter i-5">
                            <div class="cell">
                                <div class="block2__prev js-slider-prev">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-left') ?>"></use>
                                    </svg>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="block2__label">
                                    <b class="js-index-product-number"></b> /
                                    <span class="js-index-product-count"></span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="block2__next js-slider-next">
                                    <svg>
                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block2__slider owl-carousel js-index-product">
                <?php foreach ($productsHits as $product): ?>
                    <div class="block2__item">
                        <div class="product-block  js-wcart-item__add-container" data-id="id2221">
                            <div class="grid grid--acenter i-10">
                                <div class="cell cell--24">
                                    <div class="product-block__image-block">
                                        <a href="/product/<?php echo $product->alias ?>" class="product-block__image image image--contain">
                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media('images/' . $product->image) ?>" alt=""> </a>
                                        <div class="product-labels">
                                            <div class="grid grid--col i-5">
                                                <div class="cell">
                                                    <div class="product-labels__item product-labels__item--special">
                                                        <span><?php echo $product->percent ?> %</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-block__info">
                                            <div class="grid grid--col i-5">

                                                <div class="cell">
                                                    <div class="btn-link btn-link--small js-compare " data-id="id2221">
                                                        <div class="btn-link__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-compare') ?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cell">
                                                    <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                                        <div class="btn-link__icon">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-favorite') ?>" />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24">
                                    <a href="/product/<?php echo $product->alias ?>" class="product-block__name">
                                        <span><?php echo $product->name ?></span>
                                    </a>
                                </div>
                                <div class="cell cell--12">
                                    <div class="product-block__price-block">
                                        <div class="product-block__old-price">
                                            <span>от <?php echo $product->cost_old ?> грн.</span>
                                        </div>
                                        <div class="product-block__price">
                                            <span>от <?php echo $product->cost ?> грн.</span>
                                        </div>
                                        <?php if ($product->available == 1): ?>
                                            <div class="product-block__status is-available">
                                                <span>В наличии</span>
                                            </div>
                                        <?php elseif ($product->available == 2): ?>
                                            <div class="product-block__status is-ends">
                                                <span>Заканчивается</span>
                                            </div>
                                        <?php elseif ($product->available == 0): ?>
                                            <div class="product-block__status is-not-available">
                                                <span>Нет в наличии</span>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <div class="cell cell--12">
                                    <?php if (is_array($_SESSION['prod'])): ?>
                                        <?php if (in_array($product->id, $_SESSION['prod'])): ?>
                                            <a class="btn btn--green btn--full" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                </svg>
                                                <span>В корзине</span>
                                            </a>
                                        <?php else: ?>
                                            <a class="btn btn--green btn--full" href="/product/<?php echo $product->alias ?>">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                </svg>
                                                <span>Купить</span>
                                            </a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a class="btn btn--green btn--full" href="/product/<?php echo $product->alias ?>">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                            </svg>
                                            <span>Купить</span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>