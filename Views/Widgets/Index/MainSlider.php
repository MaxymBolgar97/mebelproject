<?php

use Core\HTML;
?>
<div class="index-main-slider ms-hide">
    <div class="index-main-slider__list js-index-main-slider">
        <?php foreach ($slider as $item): ?>
            <div>
                <a href="<?php echo $item->url ?>" class="index-main-slider__item">
                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/slider/$item->image") ?>" alt="">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="index-main-slider__link">
        <div class="index-main-slider__dots js-index-slider-dots"></div>
        <a href="/top/sale" class="link">
            <span>ВСЕ АКЦИИ</span>
            <svg>
            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-left') ?>" />
            </svg>
        </a>
    </div>
</div>
<div class="index-main-slider ms-show">
    <div class="index-main-slider__list js-index-main-slider">
        <?php foreach ($slider as $item): ?>
            <div>
                <a href="<?php echo $item->url ?>" class="index-main-slider__item">
                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$item->image") ?>" alt="">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="index-main-slider__link">
        <div class="index-main-slider__dots js-index-slider-dots"></div>
        <a href="/top/sale" class="link">
            <span>ВСЕ АКЦИИ</span>
            <svg>
            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-arrow-right') ?>" />
            </svg>
        </a>
    </div>
</div>