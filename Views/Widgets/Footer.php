<?php

use Core\HTML;
?>
<footer>
    <div class="footer-top pv-20 ms-hide">
        <div class="grid grid--lg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--acenter grid--nowrap grid--jbetween i-10">
                    <div class="cell">
                        <div class="grid grid--acenter grid--nowrap i-10">
                            <?php foreach($topMenu as $item): ?>
                            <div class="cell">
                                <a href="<?php echo $item->url?>" class="footer-top__link">
                                    <span><?php echo $item->name?></span>
                                </a>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="btn btn--green js-mfp-ajax" data-url="<?php echo HTML::media('hidden/calltechno.php') ?>">
                            <svg>
                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-menu-top-9') ?>" />
                            </svg>
                            <span class="md-hide">Вызвать технолога</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <div class="footer-middle__bg grid grid--nowrap ms-hide">
            <div class="footer-middle__left cell cell--lg10"></div>
            <div class="footer-middle__right cell cell--lg14"></div>
        </div>
        <div class="footer-middle__content pv-30">
            <div class="grid grid--lg pg-30">
                <div class="cell cell--24">
                    <div class="grid i-20 i-ms10">
                        <div class="cell cell--10 cell--ms24">
                            <div class="grid i-ms10">
                                <div class="cell cell--24 cell--ms12 cell--sm24">
                                    <div class="grid i-10 mb-0">
                                        <div class="cell cell--10 cell--lg11 cell--ms9 cell--sm12">
                                            <div class="footer-middle__tel">
                                                <i>
                                                    <img src="<?php echo HTML::media('pic/icon-tel-ukrtelecom.png') ?>" alt="">
                                                </i>
                                                <div>
                                                    <a href="tel:(0512) 72-01-22">(0512) 72-01-22</a>
                                                    <a href="tel:(0512) 58-08-05">(0512) 58-08-05</a>
                                                </div>
                                            </div>
                                            <div class="footer-middle__tel">
                                                <i>
                                                    <img src="<?php echo HTML::media('pic/icon-tel-vodafone.png') ?>" alt="">
                                                </i>
                                                <div>
                                                    <a href="tel:(099) 909-54-38">(099) 909-54-38</a>
                                                </div>
                                            </div>
                                            <div class="footer-middle__tel">
                                                <i>
                                                    <img src="<?php echo HTML::media('pic/icon-tel-kyivstar.png') ?>" alt="">
                                                </i>
                                                <div>
                                                    <a href="tel:(096) 929-10-45">(096) 929-10-45</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cell cell--11 cell--lg13 cell--ms12 cell--sm12">
                                            <div class="footer-middle__text mb-20">
                                                <span class="lg-block ms-inline xs-block">Пн-Сб 9:00-18:00,</span>
                                                <span class="lg-block ms-inline xs-block">Вс 9:00-16:00</span>
                                            </div>
                                            <div class="btn btn--white btn--full js-mfp-ajax" data-url="<?php echo HTML::media('hidden/callme.php') ?>">
                                                <span>Обратный звонок</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell cell--24 cell--ms12 cell--sm24">
                                    <div class="footer-middle__title mb-10 xs-gcenter">
                                        <span>Подписаться на новости и акции</span>
                                    </div>
                                    <form action="/" method="POST">
                                        <div class="grid i-10 mb-0">
                                            <div class="cell cell--10 cell--lg11 cell--ms12 cell--xs24">
                                                <div class="control-holder control-holder--text">
                                                    <input type="email" name="email" data-name="email" placeholder="Введите E-mail" required data-rule-email="true">
                                                </div>
                                            </div>
                                            <div class="cell cell--11 cell--lg13 cell--ms12 cell--xs24">
                                                <div class="btn btn--white btn--full js-form-submit">
                                                    <button name="send"><span>Подписаться</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="grid grid--acenter i-10 mb-0 xs-gcenter">
                                        <div class="cell cell--10 cell--lg11 cell--ms12 cell--xs24">
                                            <div class="footer-middle__title">
                                                <span>Присоединяйтесь к нам:</span>
                                            </div>
                                        </div>
                                        <div class="cell cell--11 cell--lg13 cell--ms12 cell--xs24">
                                            <div class="grid grid--jbetween grid--xsjcenter i-xs5">
                                                <div class="cell">
                                                    <a href="<?php echo $socials[1]->zna?>" class="footer-middle__social footer-middle__social--facebook" target="_blank">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-facebook') ?>" />
                                                        </svg>
                                                    </a>
                                                </div>
                                                <div class="cell">
                                                    <a href="<?php echo $socials[0]->zna?>" class="footer-middle__social footer-middle__social--google-plus" target="_blank">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-google-plus') ?>" />
                                                        </svg>
                                                    </a>
                                                </div>
                                                <div class="cell">
                                                    <a href="<?php echo $socials[2]->zna?>" class="footer-middle__social footer-middle__social--instagram" target="_blank">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-instagram') ?>" />
                                                        </svg>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--14 cell--ms0">
                            <div class="grid i-8">
                                <div class="cell cell--16">
                                    <div class="grid i-10">
                                        <?php foreach($categories as $category): ?>
                                        <div class="cell cell--24">
                                            <div class="footer-middle__title">
                                                <a href="/top/<?php echo $category->alias?>"><span><?php echo $category->name?></span></a>
                                            </div>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <div class="cell cell--8">
                                    <div class="grid i-10">
                                        <?php foreach ($topCat as $cat): ?>
                                        <div class="cell cell--24">
                                            <a href="/top/<?php echo $cat->alias?>" class="footer-middle__title">
                                                <span><?php echo $cat->name?></span>
                                            </a>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--10 cell--ms14 cell--sm24">
                            <div class="grid grid--acenter grid--smjcenter i-10">
                                <div class="cell">
                                    <a href="index.html" class="logo logo--small">
                                        <img src="<?php echo HTML::media('pic/logo-small.png') ?>" alt="Мебель 048">
                                    </a>
                                </div>
                                <div class="cell">
                                    <div class="footer-middle__copyright">
                                        <p><span><?php echo $cop->zna?></span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--14 cell--ms10 cell--sm24">
                            <div class="grid grid--jend grid--smjcenter">
                                <div class="cell">
                                    <a href="http://wezom.com.ua" class="footer-middle__wezom" target="_blank">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#wezom-logo') ?>" />
                                        </svg>
                                        <div>
                                            <span><?php echo $cop->zna?></span>
                                            <div>Агентство Wezom</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-back js-footer-back js-link is-hide" data-link="header">
        <svg>
        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-up') ?>" />
        </svg>
    </div>
</footer>
