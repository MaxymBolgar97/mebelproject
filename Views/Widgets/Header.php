<?php

use Core\HTML;
?>
<header>
    <div class="header-top ms-hide">
        <div class="header-top__inner grid grid--nowrap grid--mg pl-30">
            <div class="cell cell--24">
                <div class="grid grid--nowrap grid--jbetween">
                    <div class="cell">
                        <div class="grid ig-10 ig-md5">
                            <?php foreach ($topMenu as $itemMenu): ?>
                                <div class="cell">
                                    <a href="<?php echo $itemMenu->url ?>" class="header-top__item">
                                        <span><?php echo $itemMenu->name ?></span>
                                    </a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="grid grid--nowrap ig-20 ig-md10">
                            <div class="cell header-top__bg-light">
                                <div class="grid grid--nowrap ig-10">
                                    <div class="cell">
                                        <a href="index.html" class="header-top__item" target="_blank">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-facebook') ?>" />
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="cell">
                                        <a href="index.html" class="header-top__item" target="_blank">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-youtube') ?>" />
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="cell">
                                        <a href="index.html" class="header-top__item" target="_blank">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-google-plus') ?>" />
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="cell">
                                        <a href="index.html" class="header-top__item" target="_blank">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-instagram') ?>" />
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="cell header-top__bg-dark">
                                <?php if ($user): ?>
                                    <a class="header-top__item header-top__item--dark" href="/user">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-user') ?>" />
                                        </svg>
                                        <span class="md-hide"><?php echo $user->email ?></span>
                                    </a>
                                <?php else: ?>
                                    <div class="header-top__item header-top__item--dark js-mfp-ajax" data-url="<?php echo HTML::media('hidden/auth.php') ?>">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-user') ?>" />
                                        </svg>
                                        <span class="md-hide">Вход в кабинет</span>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle ms-hide">
        <div class="grid grid--nowrap grid--mg pg-30">
            <div class="cell cell--24">
                <div class="grid grid--acenter i-10">
                    <div class="cell cell--11 cell--xl10 cell--md12">
                        <div class="grid grid--acenter i-10 pv-20">
                            <div class="cell cell--10 cell--xl12">
                                <?php if ($_SERVER['REQUEST_URI'] == '/'): ?>
                                    <div class="logo">
                                        <img src="<?php echo HTML::media('pic/logo.png') ?>" alt="Мебель 048">
                                    </div>
                                <?php else: ?>
                                    <a href='/'>
                                        <div class="logo">
                                            <img src="<?php echo HTML::media('pic/logo.png') ?>" alt="Мебель 048">
                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                            <div class="cell cell--14 cell--xl12">
                                <div class="header-middle__text">
                                    <span>Украина, г. Одесса, Киевское шоссе, 28
                                        <a href="https://www.google.com/maps/dir//Одеса+Київське+шосе+28/@16z" class="header-middle__link" target="_blank">Схема проезда</a>
                                    </span>
                                </div>
                                <form class="form mt-5 js-form" action="index.html">
                                    <div class="control-holder control-holder--text">
                                        <div class="header-middle__search">
                                            <input type="text" name="search" placeholder="Поиск" autocomplete="off" required="" data-rule-minlength="2" data-rule-noempty="true">
                                            <div class="js-form-submit">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-search') ?>" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="cell cell--13 cell--xl14 cell--md12">
                        <div class="grid grid--acenter grid--mdjend i-10 pv-20">
                            <div class="cell cell--5 cell--lg6 cell--md">
                                <div class="grid grid--jcenter">
                                    <div class="cell">
                                        <div class="header-middle__text">
                                            <span>График работы</span>
                                        </div>
                                        <div class="grid grid--acenter ig-5 mt-5">
                                            <div class="cell">
                                                <div class="header-middle__btn-text">
                                                    <span>Пн-Сб</span>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="header-middle__text-dark">
                                                    <span>9:00-18:00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid grid--acenter ig-5 mt-5">
                                            <div class="cell">
                                                <div class="header-middle__btn-text">
                                                    <span>Вс</span>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="header-middle__text-dark">
                                                    <span>9:00-16:00</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--9 cell--lg7 cell--md">
                                <div class="grid grid--acenter i-3">
                                    <div class="cell cell--3 cell--lg0"></div>
                                    <div class="cell cell--21 cell--lg24 cell--md0">
                                        <div class="header-middle__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/callme.php') ?>">
                                            <span>Перезвонить вам?</span>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--md">
                                        <div class="header-middle__icon-big">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone') ?>" />
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="cell cell--21 cell--md">
                                        <div class="header-middle__tel-block js-hover-container">
                                            <div class="grid grid--nowrap grid--acenter i-3">
                                                <div class="cell cell--md0">
                                                    <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-big">
                                                        <span><?php echo $phone->zna ?></span>
                                                    </a>
                                                </div>
                                                <div class="cell">
                                                    <div class="header-middle__icon-small js-hover">
                                                        <svg>
                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-triangle') ?>" />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="header-middle__tel-popup">
                                                <!-- @TODO Этот номер равен тому, что вверху. Он отображается на мобильной версии, когда верхний скрыт -->
                                                <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10 md-show">
                                                    <span><?php echo $phone->zna ?></span>
                                                </a>
                                                <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                    <span><?php echo $phone->zna ?></span>
                                                </a>
                                                <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                    <span><?php echo $phone->zna ?></span>
                                                </a>
                                                <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                    <span><?php echo $phone->zna ?></span>
                                                </a>
                                                <div class="header-middle__link js-mfp-ajax md-show" data-url="<?php echo HTML::media('hidden/callme.php') ?>">
                                                    <span>Перезвонить вам?</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cell cell--3 cell--lg0"></div>
                                    <div class="cell cell--21 cell--lg24 cell--md0">
                                        <div class="header-middle__text">
                                            <span>Бесплатно с мобильных и городских</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--5 cell--lg6 cell--md">
                                <div class="grid iv-3 ig-md10 cell--md">
                                    <div class="cell cell--24 cell--md">
                                        <a href="index.html" class="btn-link btn-link--small">
                                            <div class="btn-link__icon">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-compare') ?>" />
                                                </svg>
                                                <div class="btn-link__label md-show">
                                                    <span class="js-compare-count">6</span>
                                                </div>
                                            </div>
                                            <span class="md-hide">
                                                <span>Сравнение (</span>
                                                <span class="js-compare-count">6</span>
                                                <span>)</span>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="cell cell--24 cell--md">
                                        <a href="index.html" class="btn-link btn-link--small">
                                            <div class="btn-link__icon">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-favorite') ?>" />
                                                </svg>
                                                <div class="btn-link__label md-show">
                                                    <span class="js-favorite-count">0</span>
                                                </div>
                                            </div>
                                            <span class="md-hide">
                                                <span>Избранное (</span>
                                                <span class="js-favorite-count">0</span>
                                                <span>)</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--5 cell--md">
                                <a href="/cart">
                                    <div class="btn-link btn-link--big">
                                        <div class="btn-link__icon">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart') ?>" />
                                            </svg>
                                            <div class="btn-link__label">
                                                <span class="js-cart-count"><?php echo $countCart ?></span>
                                            </div>
                                        </div>
                                        <div class="btn-link__content md-hide">
                                            <span>Корзина</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom js-fixed-bg">
        <div class="header-bottom__inner js-fixed-content js-mfp-fixed-element">
            <div class="grid grid--nowrap grid--lg grid--jcenter pg-30 pg-xs10">
                <div class="cell cell--24">
                    <div class="grid grid--nowrap grid--jcenter ig-10">
                        <div class="cell cell--24">
                            <div class="grid grid--nowrap grid--jcenter grid--lgjstart ig-5 ig-ms10">
                                <div class="cell header-bottom__fix">
                                    <div class="logo js-fixed-item ms-show">
                                        <img src="<?php echo HTML::media('pic/logo.png') ?>" alt="Мебель 048">
                                    </div>
                                </div>
                                <div class="cell cell--0 cell--ms">
                                    <a href="#mobile-menu" class="mobile-menu-link">
                                        <div class="icon-link__icon" title="Меню">
                                            <svg>
                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-list') ?>" />
                                            </svg>
                                        </div>
                                    </a>
                                    <div class="hide">
                                        <nav id="mobile-menu" class="mobile-menu js-mobile-menu is-first">
                                            <ul>
                                                <li class="mobile-menu__head">
                                                    <span class="mobile-menu__left">
                                                        <span class="mobile-menu__text">Меню</span>
                                                    </span>
                                                    <span class="mobile-menu__right">
                                                        <span class="mobile-menu__close js-menu-close">
                                                            <svg>
                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-close') ?>" />
                                                            </svg>
                                                        </span>
                                                    </span>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span>
                                                        <form class="form js-form" action="index.html">
                                                            <span class="control-holder control-holder--text">
                                                                <span class="header-middle__search">
                                                                    <input type="text" name="search" data-name="search" placeholder="Поиск" required data-rule-noempty="true" data-rule-minlength="2">
                                                                    <div class="js-form-submit">
                                                                        <svg>
                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-search') ?>" />
                                                                        </svg>
                                                                    </div>
                                                                </span>
                                                            </span>
                                                        </form>
                                                    </span>
                                                </li>
                                                <li class="mobile-menu__item mobile-menu__item--select">
                                                    <span class="js-mobile-open">
                                                        <span>Каталог</span>
                                                    </span>
                                                    <ul>
                                                        <?php foreach ($categories as $category): ?>
                                                            <li class="mobile-menu__item">
                                                                <a href="/top/<?php echo $category->alias ?>">
                                                                    <span><?php echo $category->name ?></span>
                                                                </a>
                                                                <?php if (is_array($arr[$category->id]) || is_object($arr[$category->id])): ?>
                                                                    <ul>
                                                                        <?php foreach ($arr[$category->id] as $item): ?>
                                                                            <li>
                                                                                <a href="/category/<?php echo $item->alias ?>">
                                                                                    <span><?php echo $item->name ?></span>
                                                                                </a>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span class="js-mfp-ajax" data-url="<?php echo HTML::media('hidden/calltechno.php') ?>">
                                                        <span>Вызвать технолога</span>
                                                    </span>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span class="js-mobile-open">
                                                        <span>Навигация</span>
                                                    </span>
                                                    <ul>
                                                        <?php foreach ($topMenu as $itemMenu): ?>
                                                            <li class="mobile-menu__item is-active">
                                                                <a href="<?php echo $itemMenu->url ?>">
                                                                    <span><?php echo $itemMenu->name ?></span>
                                                                </a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                        <li class="mobile-menu__item">
                                                            <a href="index.html">
                                                                <span>Акции</span>
                                                            </a>
                                                        </li>
                                                        <li class="mobile-menu__item">
                                                            <a href="index.html">
                                                                <span>Сравнение</span>
                                                            </a>
                                                        </li>
                                                        <li class="mobile-menu__item">
                                                            <a href="index.html">
                                                                <span>Избранное</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span class="js-mobile-open">
                                                        <span>Наши телефоны</span>
                                                    </span>
                                                    <ul>
                                                        <li class="mobile-menu__item">
                                                            <a href="tel:<?php echo $phone->zna ?>">
                                                                <span><?php echo $phone->zna ?></span>
                                                            </a>
                                                        </li>
                                                        <li class="mobile-menu__item">
                                                            <a href="tel:0-800-210-690">
                                                                <span><?php echo $phone->zna ?></span>
                                                            </a>
                                                        </li>
                                                        <li class="mobile-menu__item">
                                                            <a href="tel:0-800-210-690">
                                                                <span><?php echo $phone->zna ?></span>
                                                            </a>
                                                        </li>
                                                        <li class="mobile-menu__item">
                                                            <a href="tel:0-800-210-690">
                                                                <span><?php echo $phone->zna ?></span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span class="js-mobile-open">
                                                        <span>Аккаунт</span>
                                                    </span>
                                                    <ul>
                                                        <?php if ($user): ?>
                                                            <li class="mobile-menu__item">
                                                                <span class="js-mfp-ajax" data-url="<?php echo HTML::media('hidden/auth.php') ?>">
                                                                    <a href="/user"><span><?php echo $user->email ?></span></a>
                                                                </span>
                                                            </li>
                                                        <?php else: ?>
                                                            <li class="mobile-menu__item">
                                                                <span class="js-mfp-ajax" data-url="<?php echo HTML::media('hidden/auth.php') ?>">
                                                                    <span>Вход/Регистрация</span>
                                                                </span>
                                                            </li>
                                                        <?php endif; ?>
                                                    </ul>
                                                </li>
                                                <li class="mobile-menu__item">
                                                    <span>
                                                        <span class="mobile-menu__panel">
                                                            <span class="grid ig-20">
                                                                <div class="cell">
                                                                    <a href="index.html" class="header-top__item">
                                                                        <svg>
                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-facebook') ?>" />
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <div class="cell">
                                                                    <a href="index.html" class="header-top__item">
                                                                        <svg>
                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-youtube') ?>" />
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <div class="cell">
                                                                    <a href="index.html" class="header-top__item">
                                                                        <svg>
                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-google-plus') ?>" />
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <div class="cell">
                                                                    <a href="index.html" class="header-top__item">
                                                                        <svg>
                                                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-instagram') ?>" />
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                            </span>
                                                        </span>
                                                    </span>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="cell cell--24 cell--ms0">
                                    <div class="main-menu js-main-menu">
                                        <div class="main-menu__item js-menu-all prelative ">
                                            <div class="main-menu__link main-menu__link--blue main-menu__link--big js-menu-all-link">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-list') ?>" />
                                                </svg>
                                                <span class="is-fixed-close">Вся мебель</span>
                                            </div>
                                            <!-- @TODO Наполнить меню можно из json-ов, которые находятся в файле hidden/menu.php. Это утвержденные категории!-->
                                            <div class="main-menu__popup js-menu-popup">
                                                <div class="main-menu__popup-content js-menu-left">
                                                    <!-- Пункты 2-го уровня-->
                                                    <div class="main-menu__popup-inner">
                                                        <?php foreach ($categories as $category): ?>
                                                            <div class="main-menu__popup-item">
                                                                <a href="/top/<?php echo $category->alias ?>" class="main-menu__popup-item-link">
                                                                    <div class="main-menu__popup-icon image image--contain">
                                                                        <img src="<?php echo HTML::media("images/catalog_tree/$category->image") ?>" alt=""> </div>
                                                                    <span><?php echo $category->name ?></span>
                                                                    <div class="main-menu__popup-link">
                                                                        <!-- Проверка на наличия обьектов третьего уровня для того что бы
                                                                         определить выводить свг иконку или нет-->
                                                                        <?php if (is_array($arr[$category->id]) || is_object($arr[$category->id])): ?>
                                                                            <svg>
                                                                            <use xlink:href="<?php echo HTML::media('svg/sprite.svg#arrow-right') ?>" />
                                                                            </svg>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </a>
                                                                <!-- Пункты 3-го уровня-->
                                                                <!-- Проверка на наличия обьектов третьего уровня для того что бы
                                                                         определить выводить ли вообще 3-ий уровень-->
                                                                <?php if (is_array($arr[$category->id]) || is_object($arr[$category->id])): ?>
                                                                    <div class="main-menu__popup-container js-menu-content">
                                                                        <div class="grid ig-10">
                                                                            <!-- Вывожу итемы 3-го уровня-->
                                                                            <?php foreach ($arr[$category->id] as $item): ?>
                                                                                <div class="cell cell--6 cell--lg8">

                                                                                    <a href="/category/<?php echo $item->alias ?>" class="main-menu__popup-item2">
                                                                                        <div class="main-menu__popup-image image image--contain">
                                                                                            <img class="lazy-img" data-src="<?php echo HTML::media("images/$item->image") ?>" alt="">
                                                                                        </div>
                                                                                        <div class="main-menu__popup-name">
                                                                                            <span><?php echo $item->name ?></span>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>
                                                                            <?php endforeach; ?>

                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <div class="main-menu__popup-item">
                                                        <div class="main-menu__popup-item-link main-menu__popup-item-link--blue js-menu-open">
                                                            <span class="js-change-text" data-default="Все категории" data-change="Свернуть">Все категории</span>
                                                            <div class="main-menu__popup-bottom">
                                                                <svg>
                                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-menu-bottom') ?>" />
                                                                </svg>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php foreach ($lvl1[0] as $top): ?>
                                            <div class="main-menu__item js-main-menu-item">
                                                <a href="/top/<?php echo $top->alias ?>" class="main-menu__link js-main-menu-link">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media("svg/sprite.svg#$top->image") ?>" />
                                                    </svg>
                                                    <span><?php echo $top->name ?></span>
                                                </a>
                                                <?php if (is_array($lvl2[$top->id]) || is_object($lvl2[$top->id])): ?>
                                                    <div class="main-menu__window">
                                                        <?php foreach ($lvl2[$top->id] as $level2): ?>
                                                            <div class="main-menu__window-block">
                                                                <div class="main-menu__window-item">
                                                                    <a href="/top/<?php echo $level2->alias ?>" class="main-menu__window-title">

                                                                        <div class="main-menu__window-image image image--contain">
                                                                            <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$level2->image") ?>" alt=""> </div>
                                                                        <span><?php echo $level2->name ?></span>
                                                                    </a>
                                                                    <?php if (is_array($lvl3[$level2->id]) || is_object($lvl3[$level2->id])): ?>

                                                                        <?php foreach ($lvl3[$level2->id] as $ur3): ?>
                                                                            <div class="main-menu__window-item2">
                                                                                <a href="/category/<?php echo $ur3->alias ?>" class="main-menu__window-link2">
                                                                                    <span><?php echo $ur3->name ?></span>
                                                                                </a>
                                                                            </div>

                                                                        <?php endforeach; ?>

                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                        <div class="main-menu__item is-fixed-close">
                                            <div class="main-menu__link main-menu__link--red js-mfp-ajax" data-url="<?php echo HTML::media('hidden/calltechno.php') ?>">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-menu-top-9') ?>" />
                                                </svg>
                                                <span>Вызвать
                                                    <br>технолога</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell header-bottom__fix">
                            <div class="grid grid--nowrap grid--jcenter ig-5">
                                <div class="cell">
                                    <div class="header-middle__tel-block js-fixed-item js-hover-container ms-show">
                                        <div class="grid grid--nowrap grid--acenter i-3 js-hover">
                                            <div class="cell">
                                                <div class="header-middle__icon-big">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-phone') ?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="header-middle__icon-small">
                                                    <svg>
                                                    <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-triangle') ?>" />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="header-middle__tel-popup">
                                            <!-- @TODO Этот номер равен тому, что вверху. Он отображается на мобильной версии, когда верхний скрыт -->
                                            <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                <span><?php echo $phone->zna ?></span>
                                            </a>
                                            <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                <span><?php echo $phone->zna ?></span>
                                            </a>
                                            <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                <span><?php echo $phone->zna ?></span>
                                            </a>
                                            <a href="tel:<?php echo $phone->zna ?>" class="header-middle__tel-small mb-10">
                                                <span><?php echo $phone->zna ?></span>
                                            </a>
                                            <div class="header-middle__link js-mfp-ajax" data-url="<?php echo HTML::media('hidden/callme.php') ?>">
                                                <span>Перезвонить вам?</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div class="btn-link btn-link--big" >
                                        <a href='/cart'><div class="btn-link__icon">
                                                <svg>
                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart') ?>" />
                                                </svg>
                                                <div class="btn-link__label">
                                                    <span class="js-cart-count"><?php echo $countCart ?></span>
                                                </div>
                                            </div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>