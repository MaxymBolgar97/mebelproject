<?php

use Core\HTML;
use Core\Widgets;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <!-- @TODO Есть 2 вида блока: узкий с модификатором grid--lg и широкий с модификатором grid--mg -->
            <div class="block-top pv-10">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <a href="/" class="breadcrumbs__link breadcrumbs__link--first" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__left">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-left" />
                                            </svg>
                                        </span>
                                        <a href="/news" class="breadcrumbs__link breadcrumbs__link--last" rel="v:url" property="v:title">Статьи</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title"><?php echo $article->name?></span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title">
                                    <span><?php echo $article->name?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid grid--lg pg-30 pt-50 pt-ms30 pb-30">
                <div class="cell cell--24">
                    <div class="grid i-50 i-lg20">
                        <div class="cell cell--12 cell--sm24">
                            <div class="block6__text mb-30">
                                <p><?php echo $article->description?></p>
                            </div>
                            <div class="social mt-20">
                                <div class="grid grid--acenter i-10">
                                    <div class="cell">
                                        <div class="social__title">
                                            <span>Поделиться</span>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <script type="text/javascript">(function () {
                                                if (window.pluso)
                                                    if (typeof window.pluso.start == "function")
                                                        return;
                                                if (window.ifpluso == undefined) {
                                                    window.ifpluso = 1;
                                                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                                    s.type = 'text/javascript';
                                                    s.charset = 'UTF-8';
                                                    s.async = true;
                                                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                                    var h = d[g]('body')[0];
                                                    h.appendChild(s);
                                                }
                                                                                    })();</script>
                                        <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="facebook,twitter,google,email" data-url="product.html"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cell cell--12 cell--sm24">
                            <div class="prelative">
                                <div class="block6__bg-right"></div>
                                <div class="block6__image-right">
                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$article->image")?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid--ms pv-50 pv-lg30">
                        <div class="cell cell--24">
                            <div class="grid i-30 i-lg15">
                                <div class="cell cell--24">
                                    <div class="block6__desc view-text">
                                        <?php echo $article->text?>
                                    </div>
                                </div>
                                <div class="cell cell--24 gcenter">
                                    <a href="/news" class="link link--blue">
                                        <svg>
                                        <use xlink:href="svg/sprite.svg#icon-return" />
                                        </svg>
                                        <span>Вернуться к списку</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr"></div>
                </div>
            </div>
            <div class="block5 pv-30 pt-ms0">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="block5__title">
                            <span>Другие статьи</span>
                        </div>
                    </div>
                </div>
                <div class="pv-30 pg-lg30 prelative">
                    <div class="block5__nav">
                        <div class="grid grid--acenter grid--jbetween i-20">
                            <div class="cell">
                                <div class="block5__btn">
                                    <a href="index.html" class="btn btn--upper">
                                        <span>Посмотреть все</span>
                                        <svg>
                                        <use xlink:href="svg/sprite.svg#icon-arrow-right" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="grid grid--acenter i-5">
                                    <div class="cell">
                                        <div class="block5__prev js-slider-prev">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#arrow-left"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <div class="block5__label">
                                            <b class="js-index-slider-number"></b> /
                                            <span class="js-index-slider-count"></span>
                                        </div>
                                    </div>
                                    <div class="cell">
                                        <div class="block5__next js-slider-next">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#arrow-right"></use>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="block5__slider owl-carousel js-index-slider">
                        <?php foreach($result as $news): ?>
                        <div class="block5__item">
                            <div class="index-know">
                                <a href="index.html" class="index-know__image image image--cover">
                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="images/index-know-1.jpg" alt=""> </a>
                                <div class="grid i-10">
                                    <div class="cell cell--7 gright">
                                        <div class="index-know__date">
                                            <span><?php echo gmdate("d-m-Y", $new->created_at); ?></span>
                                        </div>
                                    </div>
                                    <div class="cell cell--17">
                                        <a href="index.html" class="index-know__name">
                                            <span><?php echo $new->name?></span>
                                        </a>
                                        <div class="index-know__desc">
                                            <p><?php echo $new->description?>…</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>