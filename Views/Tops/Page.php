<?php

use Core\HTML;
use Core\Widgets;
use Core\Route;
?>
<?php echo Widgets::get('Head'); ?>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <div class="block-top pv-10">
                <div class="grid grid--mg pg-30">
                    <div class="cell cell--24">
                        <div class="grid ig-10">
                            <div class="cell cell--24">

                                <!-- @TODO Первый пункт-ссылка имеет модификатор breadcrumbs__link--first, а последний модификатор breadcrumbs__link--last и стрелку влево -->
                                <div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <a href="/" class="breadcrumbs__link breadcrumbs__link--first" rel="v:url" property="v:title">Главная</a>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="breadcrumbs__item" typeof="v:Breadcrumb">
                                        <span class="breadcrumbs__current" property="v:title">Хиты продаж</span>
                                        <span class="breadcrumbs__right">
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#icon-right" />
                                            </svg>
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="cell">
                                <div class="title title--list js-title-container">
                                    <div class="title__bg js-title-link"></div>
                                    <div class="title__inner js-title-link">
                                        <div class="title__link">
                                            <span>Хиты продаж</span>
                                            <svg>
                                            <use xlink:href="svg/sprite.svg#arrow-down" />
                                            </svg>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            
            <div class="hr"></div>
            <div class="grid grid--mg pg-30">
                <div class="cell cell--24">
                    <div class="grid iv-15 mb-0">

                        <div class="cell cell--19 cell--lg18 cell--md17 cell--ms24">
                            <div class="product-blocks mb-30">
                                <div class="grid">
                                    <?php foreach ($result as $product): ?>
                                        <div class="cell cell--6 cell--lg8 cell--md12 cell--xs24">
                                            <div class="product-blocks__item">
                                                <div class="product-block product-block--noscale js-wcart-item__add-container" data-id="id2221">
                                                    <div class="grid grid--acenter i-10">
                                                        <div class="cell cell--24">
                                                            <div class="product-block__image-block">
                                                                <a href="product.html" class="product-block__image image image--contain">
                                                                    <img class="lazy-img" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="<?php echo HTML::media("images/$product->image") ?>" alt=""> </a>
                                                                <div class="product-labels">
                                                                    <div class="grid grid--col i-5">
                                                                        <?php if ($product->top != 0): ?>
                                                                            <div class="cell">
                                                                                <div class="product-labels__item product-labels__item--hit">
                                                                                    <span>ХИТ</span>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                        <?php if ($product->percent != 0): ?>
                                                                            <div class="cell">
                                                                                <div class="product-labels__item product-labels__item--special">
                                                                                    <span><?php echo $product->percent ?>%</span>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                </div>
                                                                <div class="product-block__info">
                                                                    <div class="grid grid--col i-5">

                                                                        <div class="cell">
                                                                            <div class="btn-link btn-link--small js-compare " data-id="id2221">
                                                                                <div class="btn-link__icon">
                                                                                    <svg>
                                                                                    <use xlink:href="svg/sprite.svg#icon-compare" />
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cell">
                                                                            <div class="btn-link btn-link--small js-favorite" data-id="id2221">
                                                                                <div class="btn-link__icon">
                                                                                    <svg>
                                                                                    <use xlink:href="svg/sprite.svg#icon-favorite" />
                                                                                    </svg>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--24">
                                                            <a href="/product/<?php echo $product->alias ?>" class="product-block__name">
                                                                <span><?php echo $product->name ?></span>
                                                            </a>
                                                        </div>
                                                        <div class="cell cell--12">
                                                            <div class="product-block__price-block">
                                                                <?php if ($product->cost_old != 0): ?>
                                                                    <div class="product-block__old-price">
                                                                        <span>от <?php echo $product->cost_old ?> грн.</span>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="product-block__price">
                                                                    <span>от <?php echo $product->cost ?> грн</span>
                                                                </div>
                                                                <?php if ($product->available == 1): ?>
                                                                    <div class="product-block__status is-available">
                                                                        <span>В наличии</span>
                                                                    </div>
                                                                <?php elseif ($product->available == 2): ?>
                                                                    <div class="product-block__status is-ends">
                                                                        <span>Заканчивается</span>
                                                                    </div>
                                                                <?php elseif ($product->available == 0): ?>
                                                                    <div class="product-block__status is-not-available">
                                                                        <span>Нет в наличии</span>
                                                                    </div>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                        <div class="cell cell--12">
                                                            <a class="btn btn--green btn--full" href="/product/<?php echo $product->alias ?>">
                                                                <svg>
                                                                <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-cart'); ?>" />
                                                                </svg>
                                                                <span>Купить</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="product-block__attrs">
                                                        <div class="product-block__attr">
                                                            <span><?php echo $product->charecter ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <div class="block1 pv-15">
                <div class="grid grid--lg pg-30">
                    <div class="cell cell--24">
                        <div class="grid grid--acenter grid--jcenter i-20">
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-1')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Сборка корпусной мебели на дому у заказчика – БЕСПЛАТНО</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-2')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Более 3000 товаров лучших мебельных брендов</span>
                                    </div>
                                </div>
                            </div>
                            <div class="cell cell--7 cell--lg8 cell--ms12 cell--xs24">
                                <div class="block1__item js-mfp-ajax" data-url="<?php echo HTML::media('hidden/block1.php')?>">
                                    <div class="block1__icon">
                                        <svg>
                                        <use xlink:href="<?php echo HTML::media('svg/sprite.svg#icon-block1-3')?>" />
                                        </svg>
                                    </div>
                                    <div class="block1__label">?</div>
                                    <div class="block1__text">
                                        <span>Лёгкий возврат в течение 14 дней без объяснения причин</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hr"></div>
            <div class="block7">
                <div class="grid grid--lg pg-30 pv-50">
                    <div class="cell cell--24">
                        <div class="view-text">
                            <h2 style="text-align: center;">Красивые кожаные диваны в городе
                                <br>Одесса: каталог, фото и цены</h2>
                            <br>
                            <h5>
                                <img style="float: left; margin: 0 50px 50px 0;" src="<?php echo HTML::media('images/category-desc-1.jpg')?>" width="500" height="317" />Купить диван в Николаеве на сегодняшний день не составляет никакого труда. Наверное, никто не возразит, что гостиную без него представить довольно сложно. Правильно выбранное изделие способно полностью изменить внешний вид помещения своим присутствием. Форма конкретной модели, яркий или приглушенный цвет, дополнительные элементы в виде столиков, или встроенных баров &ndash; решение принимать только Вам. Первая задача, которая стоит перед нами &ndash; это определить назначение товара.</h5>
                            <p>В том случаи, если вам нужна модель для гостиной комнаты &ndash; лучшим вариантом будет покупка без механизма трансформации. Основным достоинством такого изделия может стать его невысокая цена. Для приема гостей, или родственников, остающихся на ночевку покупка должна иметь комфортную область для сна. Лучше всего на это роль годится раскладной диван. Однако выбор дивана трансформера необходимо с максимальной ответственностью и внимательностью.</p>
                            <p>Если вы просто желаете обновить вашу мягкую мебель, наши сотрудники с радостью предоставят вам квалифицированную и своевременную помощи в выборе нужного товара, мы непременно подберем современную модель которые высоко оценят близкие и друзья.</p>
                            <h3>Качественные диваны для отдыха и сна</h3>
                            <p>Чтобы вы не покупали, в первую очередь нужно правильно расставить приоритеты и постараться выяснить: каким потребностям и требованиям должен соответствовать конечный продукт. Если вы хотите купить продукт с ортопедическими свойствами, Вам нужно решить насколько часто вы будете использовать его в качестве основного средства для сна. В случаях редкого применения в качестве постели, приобретение такого продукта вовсе необязательно. Малогабаритные диваны с пружинным блоком станут отличным решением. Когда продукт выступает в роли кровати все время, то самым оптимальным решением станет - покупка изделия, обладающего матрасом на основе независимых пружин. Они позаботятся об образцовом положении всего тела и сохранят мебель от неравномерного проседания пружин.</p>
                            <p>Если, ваш будущая покупка должна совмещать в себе функции комфортного сидения и здорового сна, то наиболее оптимальным решением будет приобрести продукт с механизмом раскладывания аккордеон или софа. Такая механика очень надежная и проста в ежедневном использовании. Безусловно, элитная мебель не может стоит дешево, однако нельзя экономить на своем здоровье, ведь купить его Вы уже не сможете, неважно какое количество денег Вы готовы заплатить.</p>
                            <p>При выборе обязательно обращайте внимание на материалы, из которых изготовлен корпус (как правило это массивы из разных пород дерев). Не забывайте о дополнительных элементах, которые могут пригодиться в быту: ниша для постельного белья, встроенные бары и прочее.</p>
                            <p>Возможно вам будет интересно узнать о
                                <a href="/contacts">мягких уголках в Одессе</a>.</p>
                            <h3>Красивые диваны как элементы интерьера</h3>
                            <p>Процесс выбора ложе, которое будет органично и естественно смотреться в гостиной или же спальне может оказаться не простым. Большой или маленький, раскладная или модульная конструкция, кожаная или тканевая обивка?</p>
                            <p>Практически для каждого вида доступен широкий ассортимент обивочных тканей. Кроме того, Вы не ограничены конкретным типом и цветом. В большинстве интернет магазинов присутствует возможность комбинировать несколько видов материла, тем самым создавая уникальные и неповторимые цветовые сочетания для интерьера любой сложности.</p>
                            <p>Кроме того, вы можете выбрать необходимый размер, подходящую форму деревянных элементов (ножки, подлокотники) и прочее. Конечное детище непременно станет значимой частью интерьера как офисного, так и домашнего помещения.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
