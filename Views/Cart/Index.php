<?php

use Core\HTML;
use Core\Widgets;
use Core\Route;
?>
<?php echo Widgets::get('Head'); ?>
<style>
    h2{
        text-align: center;
        margin-bottom: 10px;
    }
    form {
        display: block;
        text-align: center;
    }
    .item {
        display: block;
        margin: 5px 300px;
        border: 1px #000 solid;
        padding: 10px;
    }
    .order {
        margin-top: 30px;
    }
    button {
        display: block;
        border: 1px #000 solid;
        padding: 5px;
        margin: 5px;
        cursor: pointer;
    }
    input {
        border: 1px #000 solid;
    }
    label {
        display: block;
    }
</style>
<body>
    <main>
        <?php echo Widgets::get('Header'); ?>
        <section>
            <h2>Ваша корзина</h2>
            <div class="cell cell--19 cell--md18 cell--ms24 prelative">
                <div class="hr hr--vleft ms-hide"></div>
                <div class="grid grid--col ig-10 iv-15">
                    <form action="/order" method="POST">
                        <?php foreach ($products as $product): ?>
                            <div class="cell cell--24 cell--md24 item">
                                <div class="account__label mb-5">
                                    <input name="productID[]" type="hidden" value="<?php echo $product->id ?>">
                                    <input name="productIMAGE[<?php echo $product->id ?>]" type="hidden" value="<?php echo $product->image ?>">
                                    <input name="productNAME[<?php echo $product->id ?>]" type="hidden" value="<?php echo $product->name ?>">
                                    <span><?php echo $product->name ?></span>
                                    <input name="productCOST[<?php echo $product->id ?>]" type="hidden" value="<?php echo $product->cost ?>">
                                    <img width="150px" src="<?php echo HTML::media('images/' . $product->image)?>">
                                    <label>Количество:</label>
                                    <input name="counts[<?php echo $product->id ?>]" placeholder="Количество" value="1">
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <hr>
                        <?php if (isset($_SESSION['prod'])): ?>
                            <div class="cell cell--24 cell--md24 item">
                                <input type="hidden" name="hash" value="<?php echo md5(time()) ?>">
                                <div class="control-holder control-holder--text">
                                    <button name='order'>Оформить заказ</button>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="cell cell--24 cell--md24 item">
                                <input type="hidden" name="hash" value="<?php echo md5(time()) ?>">
                                <div class="control-holder control-holder--text">
                                    <span>Ваша корзина пуста</span>
                                </div>
                            </div>
                        <?php endif; ?>
                    </form>
                </div> 
                <div class="cell cell--24 cell--md24 item">
                    <div class="control-holder control-holder--text">
                        <a href="cart/clear"><span>Очистить корзину</span></a>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <?php echo Widgets::get('Footer'); ?>
    </main>
    <?php echo Widgets::get('Scripts'); ?>
</body>
